<?php

    $title       = "L-Citrulina Malato 500Mg 120 Doses";
    $description = "A L-Citrulina Malato é indicada para o aumento do desempenho anaeróbio e aeróbico, melhorando o estado físico e muscular. É útil também para o..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">L-Citrulina Malato 500Mg 120 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/l-citrulina-malato-500mg-120-doses.png" alt="l-citrulina-malato-500mg-120-doses" title="l-citrulina-malato-500mg-120-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>MAIS ENERGIA E RESISTÊNCIA</h2>
                        <p class="text-justify">A L-Citrulina Malato é indicada para o aumento do desempenho anaeróbio e aeróbico, melhorando o estado físico e muscular. É útil também para o desenvolvimento de massa muscular, redução da fadiga e aumento da produção de energia. A combinação de malato e citrulina tem mecanismos que ajudam a aumentar a resistência física, além de ser um composto útil para o de massa muscular e força. Ele também acelera a recuperação de esforços intensos e reduz a dor muscular após o exercício.</p>
                        <br>
                        <h2>BENEFÍCIOS</h2>
                        <p class="text-justify">A L-Citrulina Malato  melhora da resistência física e no aumento de massa muscular. Atua também na produção de energia e na redução da fadiga. É indicado principalmente para turbinar o anaeróbio e aeróbico, melhorando o estado físico e muscular. Proporciona aumento no ganho de massa muscular e turbina o estado físico e muscular. Reduz a fadiga muscular e auxilia na resistência física. Eleva a produção de energia, melhorando o desempenho físico. Melhora o desempenho anaeróbico e aeróbico.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>