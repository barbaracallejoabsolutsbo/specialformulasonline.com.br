<?php

    $title       = "Vitamina B9 (Ácido Fólico) 800Mcg 60 Cápsulas";
    $description = "Aliado do cérebro, das gestantes e do coraçãonutriente também é bom para a imunidade e a saúde da pele, unhas e cabelo. O Ácido Fólico (vitamina B9)..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Vitamina B9 (Ácido Fólico) 800Mcg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/vitamina-b9-(acido-folico)-800mcg-60-capsulas.png" alt="vitamina-b9-(acido-folico)-800mcg-60-capsulas" title="vitamina-b9-(acido-folico)-800mcg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">Aliado do cérebro, das gestantes e do coraçãonutriente também é bom para a imunidade e a saúde da pele, unhas e cabelo. O Ácido Fólico (vitamina B9) é uma vitamina essencial que mantém a produção de células vermelhas do sangue, é necessário para a reparação do ADN e sintetização, e o funcionamento global do corpo.</p>
                        <br>
                        <h2>BENEFÍCIOS</h2>
                        <p class="text-justify">Gravidez Saudável: O ácido fólico é mais frequentemente associada com a gravidez, pois é um fato conhecido que o ácido fólico é essencial para o desenvolvimento do bebê. O ácido fólico é fundamental para o desenvolvimento inicial do bebê. Tubo neural se desenvolve nas primeiras semanas após a concepção e deficiência de ácido fólico pode levar a uma má formação. Baixos níveis de ácido fólico pode também levar ao aborto. Mulher que está planejando engravidar ou estão grávidas devem conversar com seus médicos sobre a suplementação com ácido fólico.</p>
                    </div>
                </div>
                <p class="text-justify">Prevenção do Cancro: Verificou-se que o cancro pode ser impedida com o ácido fólico, especialmente o cancro do cólon. Este nutriente trabalha na reparação e manutenção de células dentro do corpo, e isso pode ajudar a limpar os radicais livres que podem causar câncer. Saúde do Coração: Consumir ácido fólico em uma base diária é uma das melhores maneiras de proteger seu coração. Saúde do Cérebro: Estudos descobriu que o ácido fólico pode melhorar os sintomas da depressão, esquizofrenia, e até mesmo autismo. Degeneração Macular: Ácido fólico pode ser benéfica no tratamento de degeneração macular relacionada com a idade (AMD) uma vez que esta doença ocular tem sido associada a níveis elevados de homocisteína e ácido fólico é conhecido para os níveis mais baixos deste aminoácido.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>