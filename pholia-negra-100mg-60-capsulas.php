<?php

    $title       = "Pholia Negra 100Mg 60 Cápsulas";
    $description = "PHOLIA NEGRA é um extrato padronizado de Ilex p. Ela contribui para perda de peso, melhorando a resposta sinalizadora da saciedade, através do retardo..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Pholia Negra 100Mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/pholia-negra-100mg-60-capsulas.png" alt="pholia-negra-100mg-60-capsulas" title="pholia-negra-100mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>Descrição</h2>
                        <p class="text-justify">PHOLIA NEGRA é um extrato padronizado de Ilex p. Ela contribui para perda de peso, melhorando a resposta sinalizadora da saciedade, através do retardo do tempo de esvaziamento gástrico e da redução da leptina circulante. Altera também o metabolismo de ácidos graxos e de glicose, diminuindo a formação de gordura visceral. A PHOLIA NEGRA auxilia na redução da obesidade e no controle de peso corporal em dietas de redução de peso através do atraso de tempo de resposta em relação ao esvaziamento gástrico, ou seja, com a PHOLIA NEGRA você demora mais tempo para sentir fome novamente.</p>
                        <p class="text-justify">A PHOLIA NEGRA é um fitoterápico já usado a séculos por povos indígenas como bebida medicinal, a espécie ganhou atenção em toda América Latina chegando até aos Estados Unidos e Europa como superior ao chá verde e ao goldenberry. Fitoterapia (do grego therapeia=tratamento e phyton=vegetal) é o estudo das plantas medicinais e suas aplicações na cura das doenças.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>