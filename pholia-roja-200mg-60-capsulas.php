<?php

    $title       = "Pholia Roja 200Mg 60 Cápsulas";
    $description = "Beta-caroteno melhor conhecido como um precursor da vitamina A, é um importante antioxidante que ajuda a prevenir danos de radicais livres prejudiciais no corpo"; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Pholia Roja 200Mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/pholia-roja-200mg-60-capsulas.png" alt="pholia-roja-200mg-60-capsulas" title="pholia-roja-200mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>COMO FUNCIONA A PHOLIA ROJA?</h2>
                        <p class="text-justify">A PHOLIA ROJA® trabalha em nosso organismo quebrando as gorduras de nosso organismo e contribui inibindo a vontade de comer, pois os alimentos ficam por mais tempo em nosso estômago. Ele atua também reduzindo os níveis de colesterol, leptina, triglicerídeos e possui ação antioxidante que auxilia no combate contra os radicais livres.</p>
                        <br>
                        <h2>EFEITOS BENEFÍCOS DA PHOLIA ROJA</h2>
                        <ul>
                            <li>Auxilia no processo de perda de peso;</li>
                            <li>Diminui as medidas abdominais;</li>
                            <li>Estimulam as células a consumir mais glicose e gordura acima da média;</li>
                            <li>Excelente inibidor de apetite, mantendo a sensação de saciedade por mais tempo.</li>
                        </ul>
                    </div>
                </div>
                <h2>OBSERVAÇÕES</h2>
                <p class="text-justify">Embora a Pholia Roja seja classificada como um remédio natural ele não é indicado para gestantes, pessoas com problemas cardíacos ou diabéticos, pois podem potencializar os efeitos de medicamentos hipoglicemiantes - medicamentos usados para diminuir a quantidade de glicose (açúcar) no sangue (glicemia). Antes de consumir a Pholia Roja recomenda-se buscar orientação de um profissional habilitado.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>