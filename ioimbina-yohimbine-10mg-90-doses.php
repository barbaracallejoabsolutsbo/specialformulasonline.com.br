<?php

    $title       = "Ioimbina (Yohimbine) 10Mg 90 Doses";
    $description = "Ao ser consumida, a Ioimbina promove o aumento da excitabilidade da célula, ou seja, faz com que a célula aumente o consumo de energia..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Ioimbina (Yohimbine) 10Mg 90 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/ioimbina-yohimbine-10mg-90-doses.png" alt="ioimbina-(yohimbine)-10mg-90-doses" title="ioimbina-(yohimbine)-10mg-90-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>IOIMBINA (YOHIMBINE)</h2>
                        <p class="text-justify">Ao ser consumida, a Ioimbina promove o aumento da excitabilidade da célula, ou seja, faz com que a célula aumente o consumo de energia, promovendo um maior gasto energético, favorecendo a redução de peso e a queima de gorduras localizadas.</p>
                        <br>
                        <h2>GORDURA LOCALIZADA</h2>
                        <p class="text-justify">A Ioimbina pode ser usada para ajudar a perder a gordura localizada. Por mais que se pratique exercícios, essa gordura é extremamente difícil de ser eliminada e costuma ser mais comum na barriga dos homens e nos glúteos e coxas femininas.</p>
                    </div>
                </div>
                <h2>IMPOTÊNCIA SEXUAL</h2>
                <p class="text-justify">Por ser um antagonista de adrenoceptores alfa-2 farmacologicamente bem caracterizado, com atividade no sistema nervoso central e periférico, tem sido usada há mais de um século no tratamento da impotência sexual.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Este medicamento é contraindicado para pacientes que sofram de úlcera, doenças renais ou cardíacas, não é recomendado seu uso concomitante com anti-hipertensivo clonidina, tranquilizantes e antidepressivos. Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. O uso do produto durante o período de amamentação sem orientação médica, também não é recomendado.</p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. Nunca compre o produto sem orientação de um profissional habilitado. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>