<?php

    $title       = "5 Htp 100Mg 30 Doses (Grifonia Simplicifolia)";
    $description = "O 5-HTP é um precursor direto da serotonina e derivado da planta Griffonia simplicifolia. Esta planta tem sido usada tradicionalmente na medicina..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">5 Htp 100Mg 30 Doses (Grifonia Simplicifolia)</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/5-htp-100mg-30-doses-grifonia-simplicifolia.png" alt="5-htp-100mg-30-doses-(grifonia-simplicifolia)" title="5-htp-100mg-30-doses-(grifonia-simplicifolia)">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>AÇÃO ANSIOLÍTICA, ANTIDEPRESSIVA E REGULADORA DO HUMOR</h2>
                        <p class="text-justify">O 5-HTP é um precursor direto da serotonina e derivado da planta Griffonia simplicifolia. Esta planta tem sido usada tradicionalmente na medicina africana para inibir diarreia, vômito e constipação, e também como um afrodisíaco. A suplementação diária com L-5-Hidroxitriptofano possui ação no equilíbrio das desordens do SNC, tais como o sono, a memória, o aprendizado e a regulação da temperatura e do humor.</p>
                        <br>
                        <h2>AUXILIA NO COMBATE AO ESTRESSE</h2>
                        <p class="text-justify">Também auxilia no comportamento sexual, nas funções cardiovasculares, contrações musculares, na regulação endócrina e na depressão. No estresse crônico, há um aumento na concentração de cortisol plasmático. Isto pode inibir a conversão do triptofano – ingerido através dos alimentos – em L-5-Hidroxitriptofano, mas não inibe a conversão do 5-HTP em 5-HT (serotonina). Por essa razão, o 5 HTP se tornou um poderoso aliado no tratamento ao estresse.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Não se recomenda associar o L-5-Hidroxitriptofano com Cloridrato de Fluoxetina e Cloridrato de Sertralina Nunca compre medicamento sem orientação de um profissional habilitado. Imagens meramente ilustrativas. Pessoas com hipersensibilidade à substância não devem ingerir o produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o medicamento com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o medicamento se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Todo medicamento deve ser mantido fora do alcance das crianças.</p>
                <p class="advertencias text-justify">Este medicamento não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do medicamento durante o período de amamentação também não é recomendado. Este medicamento não deve ser utilizado por menores de 18 anos sem orientação médica. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.” </p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>