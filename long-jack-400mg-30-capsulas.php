<?php

    $title       = "Long Jack 400Mg 30 Cápsulas";
    $description = "O Longjack é popularmente conhecido devido a sua propriedade afrodisíaca pela sua capacidade de estimular a produção ou ação de hormônios andrógenos..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Long Jack 400Mg 30 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/long-jack-400mg-30-capsulas.png" alt="long-jack-400mg-30-capsulas" title="long-jack-400mg-30-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É LONG JACK?</h2>
                        <p class="text-justify">O Longjack é popularmente conhecido devido a sua propriedade afrodisíaca pela sua capacidade de estimular a produção ou ação de hormônios andrógenos, principalmente testosterona. Assim, ele pode ser usado como uma alternativa para a terapia de reposição de testosterona e para o tratamento de osteoporose em homens com deficiência de androgênio, que são os hormônios que determinam nossas características sexuais.</p>
                        <br>
                        <h2>OS NÍVEIS DE TESTOSTERONA NO ORGANISMO</h2>
                        <p class="text-justify">Tanto em homens como em mulheres o pico de testosterona ocorre entre os 25 e 30 anos de idade e depois dessa faixa etária tende a cair de 1-2% ao ano. Aos 60 anos de idade os níveis ficam entre 40-50% quando comparados aos níveis de indivíduos jovens, podendo sofrer alterações de acordo com o estilo de vida, nível de estresse, dieta e padrão de sono. A manutenção dos níveos de testosterona trazem inúmeros benefícios incluindo o aumento de massa muscular, redução de gordura corporal, melhora do vigor físico e mental e melhora do bem-estar geral do indivíduo.</p>
                    </div>
                </div>
                <h2>EFEITOS E CARACTERÍSTICAS DO LONG JACK</h2>
                <p class="text-justify">Também conhecida como Eurycoma longifolia Jack, o Long Jack contém um grupo de pequenos peptídios referidos como "Euripeptídeos" que são conhecidos por terem efeitos em melhorar o estado de energia e desejo sexual em estudos com ratos. Os efeitos do Longjack em restaurar os níveis normais de testosterona não parecem estar relacionados com a estimulação da síntese de testosterona, mas sim devido ao aumento da taxa de lançamento de testosterona livre. Desta forma, ele pode não ser considerado tanto um reforço de testosterona, mas sim uma manutenção e restauração de níveis de testosterona normais. A fração bioativa da raiz Eurycoma Longifolia oferece uma capacidade demonstrada para melhorar os níveis de testosterona, aumentar o tamanho e força muscular, melhorar o bem-estar geral, acelerar a recuperação do exercício, aprimorar a perda de peso, reduzir o estresse, e reduzir os sintomas de fadiga.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>