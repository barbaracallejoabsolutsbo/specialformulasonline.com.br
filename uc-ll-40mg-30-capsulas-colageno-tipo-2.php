<?php

    $title       = "Uc II® 40Mg 30 Cápsulas (Colágeno Tipo 2)";
    $description = "O UC-II® é um nutracêutico inovador que tem a função de diminuir o desgaste nas articulações. O ativo age juntamente com o sistema imunológico..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Uc II® 40Mg 30 Cápsulas (Colágeno Tipo 2)</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/uc-ll-40mg-30-capsulas-colageno-tipo-2.png" alt="uc-ll-40mg-30-capsulas-(colageno-tipo-2)" title="uc-ll-40mg-30-capsulas-(colageno-tipo-2)">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>UC-2 COM SELO DE AUTENTICIDADE</h2>
                        <p class="text-justify">O UC-II® é um nutracêutico inovador que tem a função de diminuir o desgaste nas articulações. O ativo age juntamente com o sistema imunológico para reduzir a dor, o desconforto, a inflamação e assim melhorar a flexibilidade e a mobilidade articular.</p>
                        <br>
                        <h2>INTERHEALTH NUTRACÊUTICO PREMIADO 2013</h2>
                        <p class="text-justify">O último estudo com UC-II® conduzido pela InterHealth, ganhou o primeiro lugar na 10ª Conferência Anual de Suplementos Nutricionais de 2013, da Scripps Center for Integrative Medicine, na Califórnia (EUA). Foi comprovado que UC-II® apresenta excelentes resultados em indivíduos saudáveis, praticantes de esportes, aliviando as dores e tempo de duração do desconforto após o exercício, com melhora da mobilidade e flexibilidade.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado. </p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>