<?php

    $title       = "Mix Right 10g 30 Sachês";
    $description = "O Mix Right é uma mistura de 8 aminoácidos capazes de gerar 99% de aproveitamento para a estrutura corporal. Aminoácidos essenciais são usados..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Mix Right 10g 30 Sachês</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/mix-right-10g-30-saches.png" alt="mix-right-10g-30-saches" title="mix-right-10g-30-saches">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>ESTIMULA A SÍNTESE PROTEICA</h2>
                        <p class="text-justify">O Mix Right é uma mistura de 8 aminoácidos capazes de gerar 99% de aproveitamento para a estrutura corporal. Aminoácidos essenciais são usados para abastecer os músculos de trabalho e estimular a síntese proteica.</p>
                        <h2>MELHORA DA FADIGA DE TREINO</h2>
                        <p class="text-justify">Na área esportiva seu uso está associado com a promoção do anabolismo proteico muscular, atuando contra a fadiga central e melhorando da imunocompetência.</p>
                        <h2>COMBATE A LESÃO MUSCULAR</h2>
                        <p class="text-justify">Além disso, ele diminui a lesão muscular induzida pelo exercício físico e aumenta a performance de indivíduos que se exercitam em ambientes quentes.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>