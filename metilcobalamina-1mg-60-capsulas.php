<?php

    $title       = "Metilcobalamina 1mg 60 Cápsulas";
    $description = "Metilcobalamina é uma forma ativa de vitamina B12. É utilizada no tratamento de deficiências de vitamina B12 no organismo, incluindo anemia megaloblástica..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Metilcobalamina 1mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/metilcobalamina-1mg-60-capsulas.png" alt="metilcobalamina-1mg-60-capsulas" title="metilcobalamina-1mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">Metilcobalamina é uma forma ativa de vitamina B12. É utilizada no tratamento de deficiências de vitamina B12 no organismo, incluindo anemia megaloblástica. Pode ter atividades antiterogênicas, neuroprotetoras, anticancerígenas e desintoxicantes. Tem também atividade antialérgica e humormoduladora. Sua deficiência é muito frequente entre idosos, vegetarianos e indivíduos que adotam baixa dieta proteica ou apresentam problemas de absorção intestinal. Sua deficiência leva a transtornos hematológicos, neurológicos e cardiovasculares, principalmente, por interferir no metabolismo da hemocisteína e nas reações de metilação do organismo.</p>
                        <br>
                        <h2 class="advertencias text-center">Advertências</h2>
                        <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                    </div>
                </div>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>