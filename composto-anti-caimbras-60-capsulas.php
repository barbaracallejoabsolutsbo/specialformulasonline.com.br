<?php

    $title       = "Composto Anti-Câimbras 60 Cápsulas";
    $description = "Caracterizada pela contração dolorosa e involuntária dos músculos, a cãibra é um problema comum que apesar de não sinalizar nenhuma doença grave..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Composto Anti-Câimbras 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/composto-anti-caimbras-60-capsulas.png" alt="composto-anti-caimbras-60-capsulas" title="composto-anti-caimbras-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">Caracterizada pela contração dolorosa e involuntária dos músculos, a cãibra é um problema comum que apesar de não sinalizar nenhuma doença grave, funciona como um termômetro que mede o equilíbrio de água e nutrientes no nosso organismo. Pensando nisso desenvolvemos um composto contendo as vitaminas e minerais essências para combate a câimbras e fadiga muscular. Possui fontes de minerais e vitaminas, nutrientes importantes para o funcionamento do organismo, inclusive para os músculos.</p>
                        <p class="text-justify">A câimbra é uma contração rápida e dolorosa de um músculo que pode afetar qualquer parte do corpo, mas que, normalmente surge nos pés, mãos ou pernas, especialmente a panturrilha e a parte de trás da coxa. Geralmente, as câimbras não são graves e surgem após fazer exercício físico intenso, devido à falta de água no músculo. No entanto, também podem acontecer devido a problemas de saúde como falta de vitaminas e minerais.a irreversível em países desenvolvidos, de acordo com as informações históricas no artigo.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>