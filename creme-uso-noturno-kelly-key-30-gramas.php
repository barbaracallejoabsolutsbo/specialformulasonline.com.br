<?php

    $title       = "Creme Uso Noturno (Kelly Key) 30 gramas";
    $description = "É um ótimo aliado para eliminar aquelas manchinhas que dificultam a maquiagem, deixam a com um aspecto nada saudável e ainda atrapalham a autoestima."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Creme Uso Noturno (Kelly Key) 30 gramas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/creme-uso-noturno-kelly-key-30-gramas.png" alt="creme-uso-noturno-(kelly-key)-30-gramas" title="creme-uso-noturno-(kelly-key)-30-gramas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>ÁCIDO KÓJICO</h2>
                        <p class="text-justify">É um ótimo aliado para eliminar aquelas manchinhas que dificultam a maquiagem, deixam a com um aspecto nada saudável e ainda atrapalham a autoestima. Não é uma substância agressiva e, por isso, seus resultados não são tão rápidos e potentes. Mas, após 15 dias, já é possível perceber os efeitos do tratamento.</p>
                        <p class="text-justify">Produzido a partir de um cogumelo japonês chamado koji, o ácido kójico tem como principal benefício o clareamento de manchas. Ao ser aplicado na pele manchada devido a atrito, acne, sol, métodos depilatórios, alergias ou envelhecimento, a substância inibe a produção de melanina, componente presente no corpo responsável por dar cor à epiderme</p>
                    </div>
                </div>
                <p class="text-justify">Alguns dias após a aplicação, a pele começa a descamar, mas o paciente não deve forçar a descamação para não ocasionar o efeito contrário. O ativo desse ácido estimula a pele a produzir novas células em uma camada mais profunda da epiderme, removendo a camada mais superficial e resultando no clareamento da região.</p>
                <br>
                <h2>ARBUTIN</h2>
                <p class="text-justify">É um despigmentante funcional, composto de peptídeos fracionados, leucocyte extract (INCI), seletivamente, através de métodos precisos de processamento. O melawhite atua como um inibidor específico e competitivo da tirosinase, diminuindo a formação do pigmento da pele, a melanina. Desta forma, o melawhite pode auxiliar a minimizar o bronzeamento da pele e pigmentações pré-existentes e pós adquiridas, como sardas manchas senis e etc.</p>
                <p class="text-justify">Arbutin é um poderoso despigmentante de origem natural com excelente performance comprovada. Ele foi criado para acabar com os inconvenientes técnicos de um dos mais eficientes despigmentantes do mercado, a Hidroquinona. Além da sua elevada instabilidade na presença de luz, a Hidroquinona também apresenta caráter lesivo quando em contato com a pele. Age bloqueando a ação da tirosinase, sem causar irritação. Ele impede a produção de melanina no ponto da aplicação. O resultado é o desaparecimento visível das manchas sem provocar efeitos tóxicos no usuário.</p>
                <p class="text-justify">HydrOlive+ é um extrato natural da oliva de origem mediterrânica, composto por uma alta concentração de hidroxitirosol (20%), tirosol e outros polifenóis. Esta combinação produz efeitos sinérgicos, potencializando as propriedades antioxidantes. HydrOlive+ possui potente ação clareadora, antioxidante e calmante comparado com a Vitamina E e Coenzima Q10, consequentemente promove maior efeito contra o eritema provocado pela exposição UV</p>
                <h2>SUPEROX-C</h2>
                <p class="text-justify">Superox-C é extraído do Kakadu Plum, uma superfruta oriunda da Austrália. Todos os anos as árvores de Kakadu são expostas a uma grande incidência de raios solares devido à fina camada de ozônio presente no país, com isso, desenvolveu um sistema de defesa composto por um coquetel extremamente potente de antioxidantes como vitamina C, polifenois (ácido Gálico e ácido Elágico) e vitaminas A e E, que combatem os danos dos raios UV e protegem dos radicais livres. Assim, Superox-C™ atua inibindo o ataque dos radicais livres devido à sua alta atividade antioxidante. Este mecanismo preserva a integridade das proteínas da derme, promovendo maior firmeza e sustentação da pele.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="https://web.whatsapp.com/send?phone=5511944930219&text=Ol%C3%A1%21%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20da%20Exaustyair%20-%20Sistemas%20de%20Exaust%C3%A3o%20e%20Ventila%C3%A7%C3%A3o%20Industrial"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>