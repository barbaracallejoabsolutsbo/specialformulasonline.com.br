<?php

    $title       = "Creme Para Pele Madura 40+ 30 Gramas";
    $description = "Beta-caroteno melhor conhecido como um precursor da vitamina A, é um importante antioxidante que ajuda a prevenir danos de radicais livres prejudiciais no corpo"; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Creme Para Pele Madura 40+ 30 Gramas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/creme-para-pele-madura-40.png" alt="creme-para-pele-madura-40" title="creme-para-pele-madura-40">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>ESTRIOL & ÁCIDO HIALURÔNICO</h2>
                        <p class="text-justify">Ativa indiretamente a produção de colágeno pelas células da pele, diminuindo rugas e a flacidez. Quando os níveis de estrógeno caem bastante, o uso do creme com hormônio seria então uma forma de tentar compensar essa perda. O ácido hialurônico é indicado principalmente para melhorar o viço da pele, suavizando rugas e outras marcas da idade. Quando usado de forma injetável, ele pode ser usado no contorno de face, lábios (contorno e volume), sulcos naso-labiais, sulco naso-jugal (olheiras), rugas faciais, e para repor volume em face, mãos e algumas regiões corporais.</p>
                        <br>
                        <h2>STRUCTURINE & PROGESTERONA</h2>
                        <p class="text-justify">Combate os radicais livres produzidos pelo organismo devido ao fumo, estresse, má alimentação e resíduos alimentares. Previne resfriado por estimular o sistema imunológico, distúrbios da coagulação sanguínea e lesões hepáticas. A Progesterona por sua vez, ajuda a reduzir ou eliminar ondas de calor associadas com a menopausa. Reduz alterações nos padrões de sangramento. Ajuda a reduzis os sintomas da TPM. Normaliza os ciclos menstruais. Ajuda a reduzir ou eliminar completamente as alterações de humor. Aumenta a sensação de bem-estar.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>