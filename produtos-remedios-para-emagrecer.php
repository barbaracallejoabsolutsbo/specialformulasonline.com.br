<?php

    $title       = "Remédios para Emagrecer";
    $description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elementum ultrices commodo. Donec quis dictum tortor."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "home",
        "produtos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="container">
            <section class="nossos-produtos catego-prod container">
                <h1 class="text-center">Remédios para Emagrecer</h1>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="imagens/ioimbina-yohimbine-5mg-60-doses.png" alt="ioimbina-(yohimbine)-5mg-60-doses" title="ioimbina-(yohimbine)-5mg-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Ioimbina (Yohimbine) 5Mg 60 Doses</h3>
                            <p>Ao ser consumida, a Ioimbina promove o aumento...</p>
                            </div>
                            <a class="btn-entrectt" href="ioimbina-yohimbine-5mg-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="imagens/morosil-500mg-30-doses.png" alt="morosil-500mg-30-doses" title="morosil-500mg-30-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Morosil 500Mg 30 Doses</h3>
                            <p>Obtido a partir do suco de laranjas vermelhas Moro...</p>
                            </div>
                            <a class="btn-entrectt" href="morosil-500mg-30-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="imagens/orlistat-120mg-60-capsulas.png" alt="orlistat-120mg-60-capsulas" title="orlistat-120mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Orlistat 120Mg 60 Cápsulas</h3>
                            <p>Orlistat é um fármaco direcionado para o tratamento...</p>
                            </div>
                            <a class="btn-entrectt" href="orlistat-120mg-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="imagens/melatonina-3mg-100-doses.png" alt="melatonina-3mg-100-doses" title="melatonina-3mg-100-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Melatonina 3Mg 100 Doses</h3>
                            <p>A Melatonina Spefical Fórmulas traz inúmeros benefícios...</p>
                            </div>
                            <a class="btn-entrectt" href="melatonina-3mg-100-doses.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="imagens/cactin-drenagem-linfatica-500mg-60-capsulas.png" alt="cactin-drenagem-linfatica-500mg-60-capsulas" title="cactin-drenagem-linfatica-500mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Cactin Drenagem Linfática 500mg 60 Cápsulas</h3>
                            <p>Cactin é o extrato seco obtido do fruto do cacto Opuntia...</p>
                            </div>
                            <a class="btn-entrectt" href="cactin-drenagem-linfatica-500mg-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="imagens/termogenico-abelhinha-120-capsulas.png" alt="termogenico-abelhinha-120-capsulas" title="termogenico-abelhinha-120-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Termogênico Abelhinha 120 Cápsulas</h3>
                            <p>Aumenta o gasto calórico durante a atividade física e...</p>
                            </div>
                            <a class="btn-entrectt" href="termogenico-abelhinha-120-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="imagens/picolinato-de-cromo-350mcg-90-capsulas.png" alt="picolinato-de-cromo-350mcg-90-capsulas" title="picolinato-de-cromo-350mcg-90-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Picolinato De Cromo 350Mcg 90 Cápsulas</h3>
                            <p>O picolinato de cromo é especialmente indicado no caso...</p>
                            </div>
                            <a class="btn-entrectt" href="picolinato-de-cromo-350mcg-90-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="imagens/ioimbina-yohimbine-10mg-90-doses.png" alt="ioimbina-(yohimbine)-10mg-90-doses" title="ioimbina-(yohimbine)-10mg-90-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Ioimbina (Yohimbine) 10Mg 90 Doses</h3>
                            <p>Ao ser consumida, a Ioimbina promove o aumento da excitabilidade...</p>
                            </div>
                            <a class="btn-entrectt" href="ioimbina-yohimbine-10mg-90-doses.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="imagens/morosil-500mg-60-doses.png" alt="morosil-500mg-60-doses" title="morosil-500mg-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Morosil 500Mg 60 Doses</h3>
                            <p>Obtido a partir do suco de laranjas vermelhas Moro...</p>
                            </div>
                            <a class="btn-entrectt" href="morosil-500mg-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="imagens/garcinia-cambogia-500mg-60-doses.png" alt="garcinia-cambogia-500mg-60-doses" title="garcinia-cambogia-500mg-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Garcinia Cambogia 500Mg 60 Doses</h3>
                            <p>A Garcinia Cambogia é uma fruta nativa do sul da Ásia...</p>
                            </div>
                            <a class="btn-entrectt" href="garcinia-cambogia-500mg-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="imagens/l-carnitina-500mg-60-doses.png" alt="l-carnitina-500mg-60-doses" title="l-carnitina-500mg-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>L-Carnitina 500Mg 60 Doses</h3>
                            <p>Pouco conhecida ainda, a l-carnitina é um ótimo suplemento...</p>
                            </div>
                            <a class="btn-entrectt" href="l-carnitina-500mg-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="imagens/picolinato-de-cromo-100-mcg-60-doses.png" alt="picolinato-de-cromo-100-mcg-60-doses" title="picolinato-de-cromo-100-mcg-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Picolinato De Cromo 100 Mcg 60 Doses</h3>
                            <p>O Picolinato de cromo é considerado como a melhor fonte de cromo...</p>
                            </div>
                            <a class="btn-entrectt" href="picolinato-de-cromo-100-mcg-60-doses.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="imagens/faseolamina-cassiolamina-glucomannan-60-doses.png" alt="faseolamina-cassiolamina-glucomannan-60-doses" title="faseolamina-cassiolamina-glucomannan-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Faseolamina + Cassiolamina + Glucomannan 60 Doses</h3>
                            <p>De maneira resumida, pode-se dizer que a faseolamina bloqueia...</p>
                            </div>
                            <a class="btn-entrectt" href="faseolamina-cassiolamina-glucomannan-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="imagens/phytgen-200mg-30-capsulas.png" alt="phytgen-200mg-30-capsulas" title="phytgen-200mg-30-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>PhyTgen 200mg 30 Cápsulas</h3>
                            <p>PhyTgen é a combinação tecnológica perfeita do mais potente...</p>
                            </div>
                            <a class="btn-entrectt" href="phytgen-200mg-30-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="imagens/insea-2-250mg-60-capsulas.png" alt="insea-2-250mg-60-capsulas" title="insea-2-250mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Insea 2 250Mg 60 Cápsulas</h3>
                            <p>InSea2 é um combinado dos polifenóis de Ascophyllum nodosum...</p>
                            </div>
                            <a class="btn-entrectt" href="insea-2-250mg-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="imagens/pholia-magra-300mg-60-capsulas.png" alt="pholia-magra-300mg-60-capsulas" title="pholia-magra-300mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Pholia Magra 300Mg 60 Cápsulas</h3>
                            <p>É um tipo de suplemento produzido a partir da erva chamada...</p>
                            </div>
                            <a class="btn-entrectt" href="pholia-magra-300mg-60-capsulas.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="imagens/l-carnitina-1000mg-300-ml.png" alt="l-carnitina-1000mg-300-ml" title="l-carnitina-1000mg-300-ml" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>L-Carnitina 1000Mg 300 Ml</h3>
                            <p>A L-Carnitina é um nutriente de extrema eficiência utilizado por pessoas que procuram não perder tempo na hora de melhorar o condicionamento físico. Por proporcionar uma variedade de benefícios para o corpo...</p>
                            </div>
                            <a class="btn-entrectt" href="l-carnitina-1000mg-300-ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="imagens/diuretico-com-cactin-drenagem-linfatica-30-doses.png" alt="diuretico-com-cactin-drenagem-linfatica-30-doses" title="diuretico-com-cactin-drenagem-linfatica-30-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Diurético Com Cactin Drenagem Linfática 30 Doses</h3>
                            <p>O Diurético com Cactin Drenagem Linfática , desenvolvido para...</p>
                            </div>
                            <a class="btn-entrectt" href="diuretico-com-cactin-drenagem-linfatica-30-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="imagens/morosil-500mg-120-doses.png" alt="morosil-500mg-120-doses" title="morosil-500mg-120-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Morosil 500Mg 120 Doses</h3>
                            <p>Obtido a partir do suco de laranjas vermelhas Moro...</p>
                            </div>
                            <a class="btn-entrectt" href="morosil-500mg-120-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="imagens/cetilistat-60mg-60-capsulas.png" alt="cetilistat-60mg-60-capsulas" title="cetilistat-60mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Cetilistat 60mg 60 Cápsulas</h3>
                            <p>O cetilistat é um medicamento desenvolvido para tratamento da...</p>
                            </div>
                            <a class="btn-entrectt" href="cetilistat-60mg-60-capsulas.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="imagens/modulip-gc-200mg-60-capsulas.png" alt="modulip-gc-200mg-60-capsulas" title="modulip-gc-200mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Modulip GC 200Mg 60 Cápsulas</h3>
                            <p>É desejo de muita gente, emagrecer sem perder massa magra...</p>
                            </div>
                            <a class="btn-entrectt" href="modulip-gc-200mg-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="imagens/thiomucase-em-creme-135-utr-60g.png" alt="thiomucase-em-creme-135-utr-60g" title="thiomucase-em-creme-135-utr-60g" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Thiomucase Em Creme 135 UTR 60g</h3>
                            <p>A Thiomucase se tornou uma grande aliada das pessoas que procuram...</p>
                            </div>
                            <a class="btn-entrectt" href="thiomucase-em-creme-135-utr-60g.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="imagens/neuravena-500mg-30-capsulas.png" alt="neuravena-500mg-30-capsulas" title="neuravena-500mg-30-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Neuravena 500mg 30 Cápsulas</h3>
                            <p>Neuravena é o extrato da parte aérea de uma variedade especifica...</p>
                            </div>
                            <a class="btn-entrectt" href="neuravena-500mg-30-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="imagens/modulip-gc-200mg-30-capsulas.png" alt="modulip-gc-200mg-30-capsulas" title="modulip-gc-200mg-30-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Modulip GC 200Mg 30 Cápsulas</h3>
                            <p>É desejo de muita gente, emagrecer sem perder massa magra...</p>
                            </div>
                            <a class="btn-entrectt" href="modulip-gc-200mg-30-capsulas.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="imagens/cordia-ecalyculata-vell-300mg-60-capsulas.png" alt="cordia-ecalyculata-vell-300mg-60-capsulas" title="cordia-ecalyculata-vell-300mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Cordia Ecalyculata Vell 300mg 60 Cápsulas</h3>
                            <p>Rica em cafeína, alantoína, potássio, taninos e óleos essenciais...</p>
                            </div>
                            <a class="btn-entrectt" href="cordia-ecalyculata-vell-300mg-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="imagens/piperina-pimenta-preta-15mg-90-capsulas.png" alt="piperina-(pimenta-preta)-15mg-90-capsulas" title="piperina-(pimenta-preta)-15mg-90-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Piperina (Pimenta Preta) 15mg 90 Cápsulas</h3>
                            <p>O Piperina apresenta função digestiva e ativa o metabolismo...</p>
                            </div>
                            <a class="btn-entrectt" href="piperina-pimenta-preta-15mg-90-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="imagens/adipogen-300mg-90-capsulas.png" alt="adipogen-300mg-90-capsulas" title="adipogen-300mg-90-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Adipogen 300mg 90 Cápsulas</h3>
                            <p>Adipogen 300mg possui propriedades eficazes no gerenciamento...</p>
                            </div>
                            <a class="btn-entrectt" href="adipogen-300mg-90-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="imagens/lowat-450mg-60-capsulas.png" alt="lowat-450mg-60-capsulas" title="lowat-450mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Lowat 450mg 60 Cápsulas</h3>
                            <p>Lowat é um produto seguro e eficaz na perda de peso...</p>
                            </div>
                            <a class="btn-entrectt" href="lowat-450mg-60-capsulas.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="imagens/banaba-leaf-lagerstroemia-speciosa-250mg-60-capsulas.png" alt="banaba-leaf-(lagerstroemia speciosa)-250mg-60-capsulas" title="banaba-leaf-(lagerstroemia speciosa)-250mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Banaba Leaf (Lagerstroemia speciosa) 250mg 60 Cápsulas</h3>
                            <p>Banaba Leaf melhora o metabolismo de açúcares e lipídeos...</p>
                            </div>
                            <a class="btn-entrectt" href="banaba-leaf-lagerstroemia-speciosa-250mg-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="imagens/cactiberry-energetico-com-cactin-drenagem-linfatica.png" alt="cactiberry-energetico-com-cactin-drenagem-linfatica" title="cactiberry-energetico-com-cactin-drenagem-linfatica" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>CactiBerry Energético Com Cactin Drenagem Linfática - 60 Doses</h3>
                            <p>CactiBerry Energético Com Cactin Drenagem Linfática...</p>
                            </div>
                            <a class="btn-entrectt" href="cactiberry-energetico-com-cactin-drenagem-linfatica.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="imagens/pholia-roja-200mg-60-capsulas.png" alt="pholia-roja-200mg-60-capsulas" title="pholia-roja-200mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Pholia Roja 200Mg 60 Cápsulas</h3>
                            <p>A PHOLIA ROJA® trabalha em nosso organismo quebrando...</p>
                            </div>
                            <a class="btn-entrectt" href="pholia-roja-200mg-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/alluvia-100mg-30-capsulas.png" alt="alluvia-100mg-30-capsulas" title="alluvia-100mg-30-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Alluvia 100mg 30 Cápsulas</h3>
                            <p>Alluvia é um extrato derivado de uma nova variedade de Camellia...</p>
                            </div>
                            <a class="btn-entrectt" href="alluvia-100mg-30-capsulas.php">Saiba +</a>
                        </div>
                    </div>
            </section>
        </div>    
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>