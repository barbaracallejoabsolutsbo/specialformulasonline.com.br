<?php

    $title       = "Fo Ti (He Shou Wu) 100Mg 60 Cápsulas";
    $description = "He shou wu significa homem de cabelo preto, seu nome surgiu após um chinês idoso ter alegado que após utilização da raiz restaurou sua vitalidade e juventude."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Fo Ti (He Shou Wu) 100Mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/fo-ti-he-shou-wu-100mg-60-capsulas.png" alt="fo-ti-(he-shou-wu)-100mg-60-capsulas" title="fo-ti-(he-shou-wu)-100mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>FO TI (HE SHOU WU) 100MG</h2>
                        <p class="text-justify">He shou wu significa homem de cabelo preto, seu nome surgiu após um chinês idoso ter alegado que após utilização da raiz restaurou sua vitalidade e juventude e voltou a ficar com cabelos pretos. Esta capacidade de restaurar a cor natural do cabelo é um dos seus atributos mais conhecidos. O Fo-ti é composto de potássio, magnésio, fósforo, glicosídeos fenólicos, ácidos gordurosos não saturados, lecitina, dentre outras substâncias químicas. Ajuda a evitar o envelhecimento precoce. É um dos principais tônicos herbários usados na medicina popular asiática.</p>
                        <p class="text-justify">A raiz do Fo-ti ajuda a manter a cor do cabelo e fortalece os olhos, glândulas endócrinas, sangue, ossos e músculos. É indicada nos casos de constipação causadas por ressecamento no intestino. Na China, milhões de pessoas tomam regularmente fo-ti para o seu rejuvenescimento e por suas propriedades tonificantes. Ele é usado para aumentar a função hepática e renal e para limpar o sangue.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="https://web.whatsapp.com/send?phone=5511944930219&text=Ol%C3%A1%21%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20da%20Exaustyair%20-%20Sistemas%20de%20Exaust%C3%A3o%20e%20Ventila%C3%A7%C3%A3o%20Industrial"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>