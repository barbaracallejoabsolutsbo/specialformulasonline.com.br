<?php

    $title       = "Produtos";
    $description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elementum ultrices commodo. Donec quis dictum tortor."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "home",
        "produtos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="container">
            <section class="nossos-produtos catego-prod container">
                <h1 class="text-center">Remédio para Emagrecimento</h1>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/morosil-500mg-30-doses.png" alt="morosil-500mg-30-doses" title="morosil-500mg-30-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>MOROSIL 500MG 30 DOSES COM SELO DE AUTENTICIDADE</h3>
                            <p>Obtido a partir do suco de laranjas vermelhas Moro...</p>
                            </div>
                            <a class="btn-entrectt" href="morosil-500mg-30-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                             <img src="<?php echo $url; ?>imagens/.png" alt="" title="" class="img-responsive">
                            <div class="descricao-prod">
                            <h3></h3>
                            <p>...</p>
                            </div>
                            <a class="btn-entrectt" href=".php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/.png" alt="" title="" class="img-responsive">
                            <div class="descricao-prod">
                            <h3></h3>
                            <p>...</p>
                            </div>
                            <a class="btn-entrectt" href=".php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                             <img src="<?php echo $url; ?>imagens/.png" alt="" title="" class="img-responsive">
                            <div class="descricao-prod">
                            <h3></h3>
                            <p>...</p>
                            </div>
                            <a class="btn-entrectt" href=".php">Saiba +</a>
                        </div>
                    </div>
            </section>
        </div>    
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>