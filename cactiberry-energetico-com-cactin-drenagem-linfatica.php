<?php

    $title       = "CactiBerry Energético Com Cactin Drenagem Linfática - 60 Doses";
    $description = "Beta-caroteno melhor conhecido como um precursor da vitamina A, é um importante antioxidante que ajuda a prevenir danos de radicais livres prejudiciais no corpo"; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">CactiBerry Energético Com Cactin Drenagem Linfática - 60 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/cactiberry-energetico-com-cactin-drenagem-linfatica.png" alt="cactiberry-energetico-com-cactin-drenagem-linfatica" title="cactiberry-energetico-com-cactin-drenagem-linfatica">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>Descrição</h2>
                        <p class="text-justify">CactiBerry Energético Com Cactin Drenagem Linfática é uma associação energética de três ativos: Cactin, Goji Berry e Raspberry Ketone, que contribuem para redução de gordura abdominal e equilibra os fluidos do corpo.</p>
                        <p class="text-justify">Cactin é extraído do fruto do opuntia ficus-indica, popularmente chamado de figo da Índia, um tipo de cacto muito comum no México, que tem efeito diurético. Auxilia na eliminação do excesso de fluidos sem a perda de minerais e tem uma potente ação antioxidante, por conter polifenóis.</p>
                        <p class="text-justify">Goji Berry tem potente ação antioxidante e alta concentração de vitamina C, que chega a ser 50 vezes maior que a da laranja, o que melhora o sistema imunológico e o humor, evita problemas oftalmológicos e derrames, além de auxiliar no emagrecimento. Também tem vitaminas do Complexo B que auxiliam a diminuir a fadiga e estresse e a melhorar o funcionamento do cérebro.</p>
                    </div>
                </div>
                <p class="text-justify">Raspberry Ketone (Framboesa ketona), responsável pelo aroma das framboesas vermelhas, tem benefícios na perda de peso e na queima de gordura corporal. Aumenta a temperatura central do corpo, logo a capacidade de queima de gordura também aumenta. Apresenta, ainda, ação na regulação da adiponectina, responsável pela modulação da glicemia, ácidos graxos e lipólise.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>