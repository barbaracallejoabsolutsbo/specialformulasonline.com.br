<?php

    $title       = "Termogênico Abelhinha 120 Cápsulas";
    $description = "Aumenta o gasto calórico durante a atividade física e acelera o metabolismo basal (metabolismo ativo mesmo em repouso), promove sensação..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Termogênico Abelhinha 120 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/termogenico-abelhinha-120-capsulas.png" alt="termogenico-abelhinha-120-capsulas" title="termogenico-abelhinha-120-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>BENEFÍCIOS</h2>
                        <p class="text-justify">Aumenta o gasto calórico durante a atividade física e acelera o metabolismo basal (metabolismo ativo mesmo em repouso), promove sensação de saciedade para facilitar dietas, inibe a vontade de comer doces, regulando a insulina e "bloqueando" a glicose, possui ação diurética e antioxidante, além de aumentar a disposição, fornecendo mais energia para o seu dia-a-dia.</p>
                        <h2 class="advertencias text-center">Advertências</h2>
                        <p class="advertencias text-justify">CONTÉM CAFEÍNA. Não tomar junto com outros suplementos que contenham esse ativo. Este produto é contra indicado para pacientes que sofram de úlcera, doenças renais ou cardíacas, não é recomendado seu uso concomitante com anti-hipertensivo clonidina, tranquilizantes e antidepressivos. É contra indicado para pessoas que sofram de insônia, gestantes, lactantes, crianças e hepatopatas. É contra indicado para pessoas com sensibilidade a cafeína. Pode causar cefaleia, tonturas, arritmias, ansiedade e insônia. Nunca compre medicamento sem orientação de um profissional habilitado. Imagens meramente ilustrativas. Pessoas com hipersensibilidade à substância não devem ingerir o produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. 1Não use o produto com o prazo de validade vencido. 1Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade.</p>
                    </div>
                </div>
                <p class="advertencias text-justify">Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Todo medicamento deve ser mantido fora do alcance das crianças. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. Este medicamento não deve ser utilizado por menores de 18 anos sem orientação médica. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO. "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>