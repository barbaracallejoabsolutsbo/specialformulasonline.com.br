<?php

    $title       = "Pill Food 60 Doses";
    $description = "Pill Food é um complexo alimentar formado por vitaminas, proteínas e aminoácidos que participam do desenvolvimento do cabelo."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Pill Food 60 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/pill-food-60-doses.png" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>Silício Orgânico </h2>
                        <p class="text-justify"><strong>Composição:</strong> Cada 1 Cápsula contém: </p>
                        <ul>
                            <li>DL Metionina 200mg</li>
                            <li>L Cisteína 80mg</li>
                            <li>L Cistina 25mg</li>
                            <li>Queratina 25mg</li>
                            <li>Extrato de milho 20mg</li>
                            <li>Cálcio Pantotenato 25mg</li>
                            <li>Vitamina B2 1mg</li>
                            <li>Vitamina B6 10mg</li>
                            <li>Biotina 200mcg</li>
                            <li>Vitamina E (alfa-tocoferol) 3mg</li>
                            <li>Excipiente* q.s.p. 1 cápsula</li>
                            <li>* amido, talco, estearato de magnésio, dióxido de silício coloidal</li>
                        </ul>
                        <h3>O QUE É?</h3>
                        <p class="text-justify">Pill Food é um complexo alimentar formado por vitaminas, proteínas e aminoácidos que participam do desenvolvimento do cabelo, da estrutura da pele e das unhas. Trata-se de um complexo à base de vitaminas, minerais e hidrolisados de proteína. Juntas, essas substâncias promovem o fortalecimento significativo dos cabelos e unhas. Ademais, o produto também proporciona uma melhora considerável da qualidade e beleza da pele.</p>
                        <h3>PARA QUE SERVE?</h3>
                        <p class="text-justify">O produto possui alta eficiência, entregando todos os resultados prometidos, desde que seja utilizado regularmente. A vitamina B2, é responsável por fortalecer cabelos e unhas, inclusive por estimular seu crescimento. O complexo também traz vitamina E, que estimula a circulação sanguínea no couro cabeludo, contribuindo com a saúde de seus folículos, além de ser um excelente antioxidante. A Biotina é essencial para o bom funcionamento das células do corpo, agindo diretamente na formação da pele. O Pill Food ainda conta com colágeno, que melhora a sustentação da pele, e pantotenato de cálcio, que aumenta a resistência de unhas e cabelos. Conta com aminoácidos que aumentam a resistência do tecido capilar, previnem a alopecia e inibem a queda dos fios.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Nunca compre medicamento sem orientação de um profissional habilitado. Imagens meramente ilustrativas. Pessoas com hipersensibilidade à substância não devem ingerir o produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o medicamento com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o medicamento se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Todo medicamento deve ser mantido fora do alcance das crianças. Este medicamento não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do medicamento durante o período de amamentação também não é recomendado. Este medicamento não deve ser utilizado por menores de 18 anos sem orientação médica. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>