<?php

    $title       = "Suplemento para Cabelo Femme 60 Doses SENKEDA";
    $description = "Complexo formado por vitaminas, minerais e outros ativos, que juntos inibem a queda capilar, o Senkeda Suplemento para Cabelo Femme Special Fórmulas"; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Suplemento para Cabelo Femme 60 Doses SENKEDA</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/suplemento-para-cabelo-femme-60-doses.png" alt="suplemento-para-cabelo-femme-60-doses" title="suplemento-para-cabelo-femme-60-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>SENKEDA SUPLEMENTO PARA CABELO FEMME</h2>
                        <p class="text-justify">Complexo formado por vitaminas, minerais e outros ativos, que juntos inibem a queda capilar, o Senkeda Suplemento para Cabelo Femme Special Fórmulas é um poderoso estimulante para o crescimento dos fios.</p>
                        <br>
                        <h2>Trata a Calvície e Queda Capilar</h2>
                        <p class="text-justify">O Senkeda Suplemento para Cabelo Femme Special Fórmulas foi desenvolvido com ativos comprovadamente eficazes no tratamento da calvície e da queda capilar feminina.</p>
                    </div>
                </div>
                <h2>COMPOSIÇÃO SENKEDA SUPLEMENTO PARA CABELO FEMME</h2>
                <h3>Complexo B Hair</h3>
                <p class="text-justify">Essencial para manter a saúde das células no couro cabeludo, evitando a queda capilar.</p>
                <h3>Selênio (como quelato)</h3>
                <p class="text-justify">Contém enzimas antioxidantes que previnem os danos aos folículos capilares.</p>
                <h3>Zinco (como quelato)</h3>
                <p class="text-justify">Fortalece os fios! O Zinco é um nutriente essencial para manter a saúde dos cabelos.</p>
                <h3>Saw palmeto</h3>
                <p class="text-justify">Trata-se de uma planta medicinal muito utilizada para tratar a calvície e a queda de cabelos.</p>
                <h3>Dutasterida</h3>
                <p class="text-justify">Segundo diversos estudos, é considerada uma das substâncias mais eficazes contra a alopecia.</p>
                <h3>Silício (como quelato)</h3>
                <p class="text-justify">Mineral essencial para a regeneração da pele e fortalecimento dos cabelos e unhas.</p>
                <div class="row img-vantagens">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 vantagens-1">
                        <img src="imagens/vantagens/cabelo-feminino.png" alt="cabelo-feminino" title="1 a 2 meses">
                        <h3 class="cabelos">1 a 2 meses</h3>
                        <p>Nos primeiros dois meses, os resultados podem não ser tão expressivos, inclusive os cabelos podem parecer mais finos. Normal, é só esperar, mantenha a tranquilidade.</p>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 vantagens-1">
                        <img src="imagens/vantagens/cabelo-feminino-02.png" alt="cabelo-feminino-02" title="2 a 4 meses">
                        <h3 class="cabelos">2 a 4 meses</h3>
                        <p>Entre dois e quatro meses a queda de cabelo reduzirá muito, ou até mesmo revertida para crescimento de novos fios. Dica: monte um diário, tire fotos e acompanhe seu progresso.</p>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 vantagens-1">
                        <img src="imagens/vantagens/cabelo-feminino-03.png" alt="cabelo-feminino-03" title="4 a 6 meses">
                        <h3 class="cabelos">4 a 6 meses</h3>
                        <p>A partir desta fase, a queda deverá ter reduzido muito, e até parado. É normal para muitas pessoas notar o crescimento de novos cabelos, especialmente na coroa da cabeça.</p>
                    </div>
                </div>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>