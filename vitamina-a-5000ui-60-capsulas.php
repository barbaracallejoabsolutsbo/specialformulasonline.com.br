<?php

    $title       = "Vitamina A 5.000Ui 60 Cápsulas";
    $description = "A Vitamina A não apenas fortalece os pontos de entrada do corpo humano, como as membranas mucosas, o revestimento ocular, do sistema respiratório..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Vitamina A 5.000Ui 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/vitamina-a-5000ui-60-capsulas.png" alt="vitamina-a-5000ui-60-capsulas" title="vitamina-a-5000ui-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>VITAMINA A</h2>
                        <h3>POR QUE TOMAR A VITAMINA A?</h3>
                        <p class="text-justify">Imunidade: A Vitamina A não apenas fortalece os "pontos de entrada" do corpo humano, como as membranas mucosas, o revestimento ocular, do sistema respiratório, urinário e intestinal, também é essencial para os linfócitos,ou glóbulos brancos, que lutam contra as infecções no corpo.</p>
                        <p class="text-justify">A Vitamina A, também conhecida como retinol, uma vitamina solúvel em gordura que ocorre somente em alimentos animais. Carotenóides, entretanto, servem como uma ótima fonte de alfa, beta e gama carotenos que nossos corpos convertem em vitamina A e podem ser encontrados em muitos vegetais e frutas. São indispensáveis para a nossa saúde e especialmente abundantes em alimentos como damasco, brócolis, melão de polpa amarela, cenouras, couve, fígado, manga, pimentão verde, espinafre e batata doce.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se trata de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>