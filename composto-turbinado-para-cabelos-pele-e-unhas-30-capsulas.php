<?php

    $title       = "Composto Turbinado Para Cabelos, Pele E Unhas 30 Cápsulas";
    $description = "Nutricolin é o silício inteligente fundamental para manter a elasticidade e firmeza da pele e a força e a saúde de cabelos e unhas, pois estimula as proteínas."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Composto Turbinado Para Cabelos, Pele E Unhas 30 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/composto-turbinado-para-cabelos-pele-e-unhas-30-capsulas.png" alt="composto-turbinado-para-cabelos-pele-e-unhas-30-capsulas" title="composto-turbinado-para-cabelos-pele-e-unhas-30-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>COMPOSTO TURBINADO PARA CABELOS, PELE E UNHAS</h2>
                        <ul>
                            <h3>NUTRICOLIN</h3>
                            <p>Nutricolin é o silício inteligente fundamental para manter a elasticidade e firmeza da pele e a força e a saúde de cabelos e unhas, pois estimula as proteínas da beleza: colágeno, elastina e queratina.</p>
                            <h3>ZINCO</h3>
                            <p>Quando se trata de beleza, o Zinco exerce uma poderosa influência sobre a boa aparência da pele e dos cabelos, evitando males como a acne e retardando o aparecimento dos sinais do envelhecimento.</p>
                            <h3>EXSYNUTRIMENT</h3>
                            <p>O Exsynutriment é um composto a base de silício orgânico que atua no tecido conjuntivo e reestrutura as fibras de colágeno e elastina, promovendo a firmeza da pele de dentro para fora. Além disso, previne o envelhecimento e fortalece unhas e cabelos.</p>
                        </ul>
                    </div>
                </div>
                <ul>
                    <h3>VITAMINA C</h3>
                    <p>Possui propriedades antioxidantes, ou seja, é capaz de eliminar os radicais livres, reduzindo os sinais de envelhecimento. Atua como antissinais e promove renovação celular.</p>
                    <h3>SELÊNIO</h3>
                    <p>O selênio é um mineral essencial para nosso organismo. Possui ação antioxidante e auxilia na prevenção dos danos causados pelos radicais livres. Restaura firmeza, elasticidade e hidratação da pele e cabelos.</p>
                    <h3>BIO-ARCT</h3>
                    <p>Um dos seus principais constituintes é o dipeptídeo citrulyl-arginina, que é capaz de aumentar a energia e estimular o crescimento celular para a biossíntese de proteínas, em particular do colágeno.</p>
                    <h3>COLÁGENO</h3>
                    <p>A principal função do colágeno é dar firmeza à pele. Poderoso, também ajuda a adiar as terríveis ruguinhas, que denunciam a idade. Além de ser responsável por manter as células da pele firmes.</p>
                </ul>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>