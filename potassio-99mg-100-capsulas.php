<?php

    $title       = "Potássio 99Mg 100 Cápsulas";
    $description = "Potássio é um mineral que ajuda os rins a funcionar normalmente. Também exerce uma função principal na contração cardíaca, do esqueleto..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Potássio 99Mg 100 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/potassio-99mg-100-capsulas.png" alt="potassio-99mg-100-capsulas" title="potassio-99mg-100-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>POTÁSSIO</h2>
                        <p class="text-justify">Potássio é um mineral que ajuda os rins a funcionar normalmente. Também exerce uma função principal na contração cardíaca, do esqueleto e muscular suave, fazendo com que seja um importante nutriente para alcançar uma função muscular, digestiva e cardíaca normal. Uma dieta com alto teor de potássio de frutas, vegetais e legumes é geralmente recomendada para obter a máxima saúde cardíaca.</p>
                        <p class="text-justify">O potássio exerce um papel integrante no corpo humano no controle do equilíbrio da água no corpo. Funciona junto com o sódio de forma a manter níveis consistentes de água no corpo. O potássio também é necessário para um funcionamento adequado dos impulsos nervosos e a contração de cada músculo no corpo humano e também ajuda a manter um ritmo cardíaco regular e normal. A outra função essencial do potássio no corpo humano é armazenar carboidratos que são depois utilizados para produção de energia.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado.</p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>