<?php

    $title       = "DMAE Bitartarato 250mg 60 Cápsulas";
    $description = "Com o processo natural do envelhecimento, todas as substâncias químicas e os precursores nutricionais que tonificam os músculos começam a diminuir."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">DMAE Bitartarato 250mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/dmae-bitartarato-250mg.png" alt="dmae-bitartarato-250mg" title="dmae-bitartarato-250mg">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>DMAE BITARTARATO 250MG</h2>
                        <p class="text-justify">Com o processo natural do envelhecimento, todas as substâncias químicas e os precursores nutricionais que tonificam os músculos começam a diminuir. A acetilcolina é um deles, sendo ela a responsável pelas contrações musculares. O DMAE Bitartarato age na acetilcolina, estimulando suas ações. Assim previne e trata rugas e flacidez do rosto, particularmente na região ao redor dos olhos e do pescoço. Reduz rugas e linhas de expressão. Pelo estímulo da produção de acetilcolina, age também como estimulante do SNC, em pessoas com problemas e dificuldades no aprendizado, atraso do desenvolvimento neuropsicomotor, coordenação motora deficiente e dificuldades de leitura e fala. Mais recentemente tem sido utilizado também no tratamento do mal de Alzheimer.</p>
                    </div>
                </div>
                <h2>BENEFÍCIOS</h2>
                <ul>
                    <li><strong>HUMOR:</strong> O DMAE eleva o humor, contrariando a depressão e o mau humor, e aumentando e melhorando as funções cognitivas como a memória e a concentração.</li>
                    <li><strong>SAÚDE MENTAL:</strong> O DMAE ajuda a proteger as membranas celulares no cérebro. Ao exercer um papel principal na fabricação de acetilcolina, uma substância responsável pelo funcionamento mental saudável, o DMAE interrompe a deterioração das células, o que pode ajudar a prevenir o envelhecimento prematuro.</li>
                    <li><strong>CUIDADOS DA PELE:</strong> Um dos benefícios mais promissores do DMAE é que, ao interromper a produção do ácido araquidônico, que pode causar rugas e envelhecimento da pele, o DMAE promove uma pele saudável.</li>
                </ul>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>