<?php

    $title       = "Óleo de Abacate 1000mg 60 Cápsulas";
    $description = "O Abacate (Persea americana) é uma fruta de origem americana cultivada em vários países de clima tropical e ameno. O óleo é extraído da polpa da fruta madura."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Óleo de Abacate 1000mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/oleo-de-abacate-1000mg-60-capsulas.png" alt="oleo-de-abacate-1000mg-60-capsulas" title="oleo-de-abacate-1000mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">O Abacate (Persea americana) é uma fruta de origem americana cultivada em vários países de clima tropical e ameno. O óleo é extraído da polpa da fruta madura, sendo rico em vitaminas e nutrientes que podem trazer diversos benefícios à saúde, é constituído de 60% a 84% de ácidos graxos insaturados e se destaca pelo seu alto teor de ácido oleico.</p>
                        <p class="text-justify">Encontramos também em sua composição o beta-sitosterol, que colabora para a saúde do coração, equilibrando os níveis de colesterol. O óleo de Abacate é rico também em ácidos palmítico, linoléico, palmitoléico, além de outros ácidos insaturados como o linolênico e araquídico. Possui ainda quantidades variáveis de materiais insaponificáveis (máx. 2%) consistindo de: Vitaminas A, B1, B2, C e D, aminoácidos, ácidos voláteis, esteróis (sitosterol, campesterol), hidrocarbonetos e lecitina.</p>
                    </div>
                </div>
                <p class="text-justify">O Óleo de Abacate auxilia na prevenção contra doenças cardiovasculares contribuindo para a redução do LDL e aumentando o HDL, na redução dos níveis de cortisol, hormônio relacionado ao aumento da compulsão alimentar e do acúmulo de gordura na região abdominal, auxiliando no controle de peso e emagrecimento. O beta-sitosterol presente nesse óleo tem muitas finalidades entre elas está a associação com a gordura saturada de outros alimentos, pois age bloqueando sua absorção pelo corpo, auxilia na prevenção de ginecomastia, retenção de líquidos, na redução de níveis de glicose no sangue e na prevenção e tratamento de câncer de próstata.</p>
                <p class="text-justify">É também considerado um poderoso antioxidante devido ao alto teor de vitamina E, que inibe a formação de radicais livres, contribuindo na prevenção dos sinais de envelhecimento. O Óleo de Abacate também é bastante indicado na prevenção de tratamento do raquitismo por conter inúmeros nutrientes em sua composição.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Gestantes, nutrizes e crianças até 3 (três) anos somente devem consumir esse produto sob orientação de nutricionista ou médico. Não contém glúten. Nunca compre medicamento sem orientação de um profissional habilitado. Imagens meramente ilustrativas. Pessoas com hipersensibilidade à substância não devem ingerir o produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Todo medicamento deve ser mantido fora do alcance das crianças. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. Este medicamento não deve ser utilizado por menores de 18 anos sem orientação médica. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>