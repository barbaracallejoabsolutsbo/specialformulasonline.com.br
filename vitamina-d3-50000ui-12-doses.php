<?php

    $title       = "Vitamina D3 50.000Ui 12 Doses";
    $description = "Conhecida como a vitamina do sol por ser formada no corpo pela ação dos raios solares na pele, a vitamina D3 é responsável por manter a saúde..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Vitamina D3 50.000Ui 12 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/vitamina-d3-50000ui-12-doses.png" alt="vitamina-d3-50000ui-12-doses" title="vitamina-d3-50000ui-12-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>A VITAMINA DO SOL</h2>
                        <p class="text-justify">Conhecida como a “vitamina do sol" por ser formada no corpo pela ação dos raios solares na pele, a vitamina D3 é responsável por manter a saúde dos dentes e ossos, ajudar contra a fraqueza muscular, além de proteger o corpo de outras deficiências e doenças.</p>
                        <br>
                        <h2>SEM PROBLEMAS COM OS OSSOS</h2>
                        <p class="text-justify">O termo vitamina D engloba um grupo de moléculas secosteróides derivadas do 7- deidrocolesterol (7-DHC) interligadas através de uma cascata de reações fotolíticas e enzimáticas que acontecem em células de diferentes tecidos. À vitamina D3 é primariamente atribuído o papel de importante regulador da fisiologia osteomineral, em especial do metabolismo do cálcio.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="text-justify">É contraindicado para pacientes que apresentam hipervitaminose D, elevadas taxas de cálcio ou fosfato na corrente sanguínea e também em casos de má-formação nos ossos. Pacientes com arteriosclerose, insuficiência cardíaca, hiperfosfatemia e insuficiência renal devem utilizar o medicamento sob orientação médica, avaliando o risco/benefício da administração da Vitamina D. Reações adversas mais comuns são: secura da boca, cefaleia, polidipsia, poliúria, perda de apetite, náuseas, vômitos, fadiga, sensação de fraqueza, dor muscular, prurido e perda de peso. Possui traços de lactose. Nunca compre medicamento sem orientação de um profissional habilitado. Imagens meramente ilustrativas. Pessoas com hipersensibilidade à substância não devem ingerir o produto.</p>
                <p class="advertencias text-justify">Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o medicamento com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o medicamento se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Todo medicamento deve ser mantido fora do alcance das crianças. Este medicamento não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do medicamento durante o período de amamentação também não é recomendado. Este medicamento não deve ser utilizado por menores de 18 anos sem orientação médica.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>