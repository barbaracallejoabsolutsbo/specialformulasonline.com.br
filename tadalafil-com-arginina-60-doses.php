<?php

    $title       = "Tadalafil Com Arginina 60 Doses";
    $description = "O Tadalafil com Arginina irá lhe proporcionar um bombeamento muscular permanente de até 36h após o uso, ou seja, proporcionando uma vascularização..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Tadalafil Com Arginina 60 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/tadalafil-com-arginina-60-doses.png" alt="tadalafil-com-arginina-60-doses" title="tadalafil-com-arginina-60-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>“VASODILATAÇÃO E PUMP SEM IGUAL!”</h2>
                        <p class="text-justify">O Tadalafil com Arginina irá lhe proporcionar um bombeamento muscular permanente de até 36h após o uso, ou seja, proporcionando uma vascularização incrível mesmo quando parar de ingerir o produto. Com isso o produto também proporciona um ganho de massa muscular, veias saltadas, resistência e fôlego durante os treinos, ganho de força, aumentando consideravelmente o nível de cargas nos exercícios, além de ajudar na potência sexual.</p>
                        <p class="text-justify">O Tadalafil aumenta o fluxo sanguíneo em todo o corpo, proporcionando um PUMP como nunca visto antes. A Arginina, famosa precursora de óxido nítrico, é a responsável por gerar vasodilatação em todo pré-treino nacional e importado. A combinação destes dois ativos proporcionou o melhor vasodilatador do mercado! Tadalafil é um vasodilatador com inúmeros benefícios tanto para os naturais quanto para os hormonizados, em blast, cruise, ou tpc, além dos seus diversos benefícios para a saúde se usado a longo prazo, inclusive ao coração.</p>
                    </div>
                </div>
                <p class="text-justify">A L-arginina participa de diversas reações que ocorrem no organismo, como na produção de energia; na recuperação das células; na eliminação da amônia através da urina; na liberação do hormônio do crescimento e na formação do óxido nítrico. O óxido nítrico, produzido pelo organismo, a partir da arginina, auxilia no controle da pressão arterial e da circulação do sangue e auxilia nas defesas do organismo contra infecções. Estas ações promovem melhor aproveitamento da glicose (açúcar) do sangue para a produção de energia; redução da amônia do organismo e aumento da circulação do sangue, resultando em melhor e maior capacidade muscular e resistência ao esforço.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Em estudos clínicos, o Tadalafil mostrou aumentar os efeitos hipotensivos dos nitratos. Supõe-se que isto seja resultado dos efeitos combinados dos nitratos e Tadalafil na via óxido nítrico/GMPc. Portanto, a administração de Tadalafil a pacientes que estão usando qualquer forma de nitrato orgânico é contraindicada. É contra indicado para homens com problemas cardiovasculares e com restrição de atividade sexual. O Tadalafil não deve ser usada em pacientes com conhecida hipersensibilidade ao Tadalafil ou a qualquer componente da formulação. O uso de Tadalafil não é indicado para mulheres. Pacientes com insuficiência renal crônica, insuficiência hepática severa e diabéticos, devem fazer uso com acompanhamento médico. Evite a automedicação.</p>
                <p class="advertencias text-justify"> Medicamentos mesmo livres de obrigação de prescrição médica, merecem cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Nunca compre medicamento sem orientação de um profissional habilitado. Imagens meramente ilustrativas. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o medicamento com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o medicamento se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Todo medicamento deve ser mantido fora do alcance das crianças. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. Este medicamento não deve ser utilizado por menores de 18 anos sem orientação médica. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO". "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>