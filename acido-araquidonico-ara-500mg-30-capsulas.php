<?php

    $title       = "Ácido Araquidônico (ARA) 500Mg 30 Cápsulas";
    $description = "O ácido araquidônico é um ácido gordo polinsaturado ômega 6 de cadeia longa que o corpo produz a partir do ácido linoléico e que também está presente..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Ácido Araquidônico (ARA) 500Mg 30 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/acido-araquidonico-ara-500mg-30-capsulas.png" alt="acido-araquidonico-(ara)-500mg-30-capsulas" title="acido-araquidonico-(ara)-500mg-30-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">O ácido araquidônico é um ácido gordo polinsaturado ômega 6 de cadeia longa que o corpo produz a partir do ácido linoléico e que também está presente em pequenas quantidades em alguns alimentos, especialmente em carnes e peixes.</p>
                        <br>
                        <h2>O QUE INDICAM OS ESTUDOS?</h2>
                        <p class="text-justify">Já foram realizados vários estudos que parecem indicar que o ácido araquidônico possui propriedades anabólicas que são especialmente interessantes para os praticantes de musculação e todos aqueles que procuram obter aumentos de força e hipertrofia muscular. O primeiro estudo que investigou este suplemento aplicado em seres humanos foi realizado em 2006 e teve a duração de 50 dias, durante os quais os investigadores administraram 1 grama de ácido araquidônico a um grupo e um placebo a um outro grupo de controlo, ao mesmo tempo que seguiam um programa de treino com pesos. Os investigadores deste estudo verificaram que a suplementação com ácido araquidônico provocou um aumento da citoquina inflamatória IL-6 e dos níveis de PGE2. Também concluíram que o ácido arquidônico pode aumentar a capacidade anaeróbica e diminuir a resposta inflamatória ao treino.</p>
                    </div>
                </div>
                <h2>OS INVESTIGADORES CONCLUÍRAM:</h2>
                <p class="text-justify">O Ácido araquidônico desempenha um papel importante na construção muscular e ajuda a gerenciar termogenese da gordura corporal. Serve como um regulador da síntese de proteína do núcleo (crescimento muscular). Ele suporta o processo anabólico, atuando como precursor de compostos semelhantes a hormônios chamados prostaglandinas, especialmente PGE2 e PGF2.</p>
                <br>
                <h2>COMO FUNCIONA?</h2>
                <p class="text-justify">O Ácido araquidônico é armazenado no interior da membrana da célula, e é responsável por alterações adaptativas de sinalização em resposta ao dano muscular e estímulos relacionados. Rotina de exercícios pode reduzir os níveis de ácido araquidônico e da produção de anabolizantes prostaglandinas, criando a necessidade de suplementação de ARA durante o treinamento pesado, intenso e desempenho atlético. O ARA é mais comumente usado por fisiculturistas e atletas de peso em esportes que exigem força anaeróbio (Musculação, natação, corrida...). Níveis Ácido araquidônico reduzidos estão associados com o treinamento de estagnação, declínio, capacidade de estimular o crescimento muscular e dor muscular de início tardio (DMT) após o exercício.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>