<?php

    $title       = "Libido Feminino";
    $description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elementum ultrices commodo. Donec quis dictum tortor."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "home",
        "produtos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="container">
            <section class="nossos-produtos catego-prod container">
                <h1 class="text-center">Libido Feminino</h1>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/ioimbina-yohimbine-5mg-60-doses.png" alt="ioimbina-(yohimbine)-5mg-60-doses" title="ioimbina-(yohimbine)-5mg-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Ioimbina (Yohimbine) 5Mg 60 Doses</h3>
                            <p>Ao ser consumida, a Ioimbina promove o aumento da excitabilidade...</p>
                            </div>
                            <a class="btn-entrectt" href="ioimbina-yohimbine-5mg-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/ioimbina-yohimbine-10mg-90-doses.png" alt="ioimbina-(yohimbine)-10mg-90-doses" title="ioimbina-(yohimbine)-10mg-90-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Ioimbina (Yohimbine) 10Mg 90 Doses</h3>
                            <p>Ao ser consumida, a Ioimbina promove o aumento da excitabilidade...</p>
                            </div>
                            <a class="btn-entrectt" href="ioimbina-yohimbine-10mg-90-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/saw-palmetto-160mg-60-doses.png" alt="saw-palmetto-160mg-60-doses" title="saw-palmetto-160mg-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Saw Palmetto 160Mg 60 Doses</h3>
                            <p>Saw Palmetto, de nome científico Serenoa repens...</p>
                            </div>
                            <a class="btn-entrectt" href="saw-palmetto-160mg-60-doses.php">Saiba +</a>
                        </div>
                    </div>
            </section>
        </div>    
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>