<?php

    $title       = "Cyanotis Vaga 200Mg 60 Cápsulas";
    $description = "Cyanotis VAGA 200MG é uma planta muito utilizada na medicina popular, que possui em sua composição o beta-ecdisterona ou beta-ecdisona esteroide..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Cyanotis Vaga 200Mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/cyanotis-vaga-200mg-60-capsulas.png" alt="cyanotis-vaga-200mg-60-capsulas" title="cyanotis-vaga-200mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>CYANOTIS VAGA</h2>
                        <p class="text-justify">Cyanotis VAGA 200MG é uma planta muito utilizada na medicina popular, que possui em sua composição o beta-ecdisterona ou beta-ecdisona esteroide. Sua obtenção é realizada para diversas aplicações: na suplementação alimentar e também em formulações cosméticas, entre outros. CYANOTIS VAGA pode ser utilizado como suplementação esportiva, proporcionando o aumento muscular e permitindo um melhor desempenho físico. O uso do fitoterápico CYANOTIS VAGA é indicado para pessoas que desejam aumentar a massa muscular e melhorar a resistência física, pois o produto auxilia o aumento da produção de testosterona, que é um hormônio essencial para o desenvolvimento muscular.</p>
                        <p class="text-justify">Cyanotis VAGA é um fitoterápico que oferece muitos benefícios para a saúde. Entre eles, pode-se destacar:</p>
                        <ul>
                            <li>Capacidade de proporcionar aumento da massa muscular;</li>
                            <li>Melhora do desempenho físico;</li>
                            <li>Auxílio na preservação de órgãos e tecidos;</li>
                            <li>Pode estabilizar possíveis lesões celulares, diminuindo os processos degenerativos de órgãos e tecidos;</li>
                            <li>Melhora da função hepática.</li>
                        </ul>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado.</p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>