<?php

    $title       = "Gelatina 500Mg 60 Cápsulas";
    $description = "A gelatina nada mais é do que uma substância naturalmente incolor normalmente obtida a partir do cozimento de determinados tecidos animais."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Gelatina 500Mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/gelatina-500mg-60-capsulas.png" alt="gelatina-500mg-60-capsulas" title="gelatina-500mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>GELATINA 500MG</h2>
                        <p class="text-justify">A gelatina nada mais é do que uma substância naturalmente incolor normalmente obtida a partir do cozimento de determinados tecidos animais, como a pele e a cartilagem bovina. Obtém-se a gelatina a partir da hidrólise parcial do colágeno, a proteína que dá sustentação à pele e participa da estrutura dos ossos e tendões.</p>
                        <br>
                        <h2>POR QUE SUPLEMENTAR COM A GELATINA?</h2>
                        <p class="text-justify">A partir dos 25 anos de idade o corpo começa a reduzir gradualmente sua produção natural de colágeno. Uma pessoa com 50 anos produz apenas 35% do colágeno de que necessita para sua saúde. Uma suplementação da proteína é então mais do que necessária para repor as perdas naturais da idade. Como é rica na proteína de origem animal, a gelatina serve para manter a saúde da pele, ossos, cabelos e unha. Consumir gelatina regularmente também pode colaborar para a perda de peso, manutenção do tônus muscular e ainda evitar o surgimento das indesejadas celulites e estrias. O colágeno é ainda indispensável para o processo de cicatrização. Entre os nutrientes da gelatina, podemos destacar: vitaminas A, C e D, zinco, selênio e sódio. A gelatina fornece ainda 9 dos 10 aminoácidos essenciais (a substância não é fonte de triptofano) e não contém gordura ou colesterol.</p>
                    </div>
                </div>
                <p class="text-justify">Além de atuar para a manutenção do tônus muscular, a gelatina também serve como fonte de aminoácidos importantes para a construção de massa magra. Ainda que não contenha triptofano e seus níveis de metionina deixem a desejar, a gelatina chega a fornecer mais de 20% da ingestão diária recomendada de proteína (levando-se em consideração um consumo de 12 gramas de gelatina em pó para um adulto de 60 kg). O fato de não ser uma proteína completa significa que você deverá ter outras fontes de proteína na dieta para ganhar massa muscular, mas a gelatina pode ser uma alternativa para quem procura variar as fontes proteicas na dieta. Outro benefício da gelatina no ganho de massa muscular se deve à atuação do aminoácido L-glutamina. Presente em grande quantidade na gelatina, a L-glutamina ajuda a elevar os níveis de hormônio do crescimento (HGH). Este por sua vez estimula a síntese de proteínas necessárias para a hipertrofia muscular.</p>
                <br>
                <h2>Contribui para a Saúde da Pele</h2>
                <p class="text-justify">Não há dúvida de que um dos maiores benefícios da gelatina é exatamente sua atuação sobre a saúde da pele. O alimento é uma excelente fonte de prolina, hidroxiprolina e glicina, três aminoácidos utilizados pelo corpo para sintetizar o colágeno. E como com o passar do tempo a pele fica mais fraca e flácida, uma reposição constante do colágeno é mais do que necessária. O corpo precisa de aproximadamente 1 g de colágeno por quilo de peso corporal para repor as perdas diárias da proteína. Como já vimos no entanto, a idade (e agentes agressores como os radicais livres) reduz a produção natural de colágeno, motivo pelo qual a gelatina pode ser uma grande aliada para combater a flacidez e as estrias, e também manter o brilho e a hidratação da pele.</p>
                <h2>Previne a perda óssea</h2>
                <p class="text-justify">Você provavelmente já sabe que o corpo precisa de cálcio para manter a saúde dos ossos, mas além do mineral, o colágeno também é indispensável para evitar a osteoporose. Os ossos são formados por aproximadamente 90-95% de colágeno. Portanto, nada mais natural do que consumir gelatina para repor as perdas ósseas decorrentes não apenas da idade, mas também da falta de atividade física.</p>
                <h2>Acalma a mente</h2>
                <p class="text-justify">Não, você não leu errado: um dos benefícios da gelatina é uma mente menos acelerada e mais calma. E essa propriedade da gelatina se deve à presença da glicina, um aminoácido que atua como um neurotransmissor “calmante’. A glicina também é convertida em outro neurotransmissor, a serina, que por sua vez reduz o stress, aumenta o foco e melhora a memória.</p>
                <h2>Auxiliar do tratamento para celulite</h2>
                <p class="text-justify">Embora mais de 90% das mulheres apresentem algum grau de celulite, boa parte delas quer deixar de fazer parte das estatísticas. Uma pele lisinha, sem “furos” e firme é o objetivo de muitas mulheres. Apesar do colágeno não ser uma pílula anticelulite, ele pode contribuir para diminuir o aspecto de “casca de laranja”. Tudo isso porque a celulite surge exatamente quando o colágeno da pele se torna enfraquecido e o tecido adiposo fica mais visível. A gelatina pode contribuir para repor o colágeno de que a pele necessita para prevenir e tratar a celulite já existente.</p>
                <h2>Perda de Peso</h2>
                <p class="text-justify">Gelatina não emagrece, mas pode ser considerada um excelente alimento para as dietas para perder peso. Além de trazer saciedade com poucas calorias, a gelatina é ainda uma fonte de proteínas. Essas propriedades da gelatina garantem um maior controle do apetite, já que a gelatina é volumosa e suas proteínas retardam a digestão. O resultado é uma diminuição da vontade de comer, uma vez que o estômago permanece preenchido por mais tempo. O maior controle do apetite promovido pela gelatina pode também estar relacionado ao GLP-1, ou peptídeo semelhante ao glucagon. Secretado por células intestinais, o hormônio está ligado aos mecanismos de saciedade. Um estudo publicado em 2008 no periódico “Eating and Weight Disorders” sugere que os níveis de GLP-1 aumentaram significativamente quando pacientes obesos consumiam gelatina. É claro que a gelatina não faz milagres, sendo portanto necessário que seu consumo venha acompanhado de uma dieta hipocalórica e rica em verduras, legumes e cereais integrais.</p>
                <h2>Controle da Glicemia</h2>
                <p class="text-justify">A ausência de açúcar na gelatina (portanto nada de gelatina rica em açúcar para quem está preocupado com as taxas de glicose) faz do alimento uma boa opção para diminuir a fome sem impactar a glicemia sanguínea.Essa propriedade da gelatina traz dois benefícios: -Diminui a liberação de insulina, o hormônio que favorece o acúmulo de gordura;-Facilita o controle do apetite, já que alterações bruscas nas taxas de glicose podem aumentar a vontade de comer carboidratos.</p>
                <h2>Fortalece as articulações</h2>
                <p class="text-justify">A alta concentração de proteínas na gelatina ajuda a reforçar os ligamentos e tendões que unem os músculos aos ossos. A gelatina fornece os aminoácidos necessários (prolina e glicina) para a formação de tecido cartilaginoso, propriedade que por sua vez mantém as articulações hidratadas e menos propensas a rupturas. Por esse motivo, a gelatina pode não apenas combater as dores articulares como também ajudar prevenir a artrose e a osteoporose. Para obter esse efeito, no entanto, é necessário que você consuma gelatina diariamente, e tenha uma dieta repleta de alimentos ricos em vitamina C.</p>
                <h2>Essencial para a saúde oral</h2>
                <p class="text-justify">Não é à toa que os dentistas dizem que nossos dentes são um reflexo direto da alimentação. Com a exceção do esmalte, todas as demais estruturas dentárias contêm colágeno. A proteína é fundamental para a estrutura do dente e também para garantir sua fixação nas gengivas.</p>
                <h2>Promove a saúde capilar</h2>
                <p class="text-justify">Com o passar dos anos os fios de cabelo tendem a ficar mais finos, quebradiços e sem brilho. Como o colágeno compõe os folículos capilares (ou pilosos) e é o responsável pela saúde de cauda uma das estruturas capilares, consumir gelatina pode ser uma boa maneira de manter os fios mais fortes e saudáveis.</p>
                <h2>Melhora o sono</h2>
                <p class="text-justify">Este é mais um dos benefícios da gelatina que podem ser relacionados à presença da glicina. Pesquisas indicam que o aminoácido pode melhorar os ciclos de sono e estimular outros neurotransmissores responsáveis pela duração e qualidade do sono. E uma noite bem dormida contribui não apenas para a saúde de modo geral mas também facilita a perda de peso (já está comprovado que dormir pouco aumenta o apetite) e melhora o aspecto da pele.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>