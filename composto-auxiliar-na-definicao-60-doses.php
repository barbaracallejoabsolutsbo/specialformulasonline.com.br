<?php

    $title       = "Composto Auxiliar Na Definição - 60 Doses";
    $description = "O Composto Auxiliar na Definição é um mix de ativos que potencializa os resultados dos processos de emagrecimento e definição muscular..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Composto Auxiliar Na Definição - 60 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/composto-auxiliar-na-definicao-60-doses.png" alt="composto-auxiliar-na-definicao-60-doses" title="composto-auxiliar-na-definicao-60-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>DESCRIÇÃO</h2>
                        <p class="text-justify">O Composto Auxiliar na Definição é um mix de ativos que potencializa os resultados dos processos de emagrecimento e definição muscular. Seus ativos aceleram a queima de gordura por diversos mecanismos diferentes e transformam a gordura acumulada em energia. Além disso, o Composto Auxiliar na Definição possui ativos com propriedades anabólicas, que proporcionam o aumento da massa muscular.</p>
                        <br>
                        <h2>ATIVOS</h2>
                        <p class="text-justify"><strong>Ilex paraguarienses</strong> é composta por várias substâncias bioativas. A combinação dos alcaloides cafeína, teofilina, metilxantinas e teobromina são consideradas os responsáveis pela ação termogênica e aumento da saciedade (retarda o esvaziamento gástrico), resultando no aumento do gasto calórico e, simultaneamente, promover a lipólise, o que faz dele um sucesso nos programas de emagrecimento. O Capsiate é um análogo não pungente de capsaicina do Capsicum annuum L - CH-19 Sweet, que raramente contém capsaicinoides pungente.</p>
                    </div>
                </div>
                <p class="text-justify">A <strong>Laranja amarga Citrus aurantium</strong> é um alcaloide de ação adrenérgica que estimula a transformação de gorduras em energia. O Cromo está intimamente ligado ao metabolismo da glicose, por sua presença no Fator de Tolerância à Glicose (FTG), e desempenha um papel importante na liberação da insulina. Sua deficiência provoca a falha na utilização da glicose e alterações no metabolismo das proteínas e lipídeos.</p>
                <p class="text-justify"><strong>Curcuma longa L</strong> é um insumo ativo que contém curcumina com poderosa ação anti-inflamatória e antioxidante. O Gengibre apresenta inúmeras propriedades farmacológicas, exercendo ação nos sistemas digestivos, nervoso central e cardiovascular. Age como estimulante para o trato gastrintestinal, aumentando o peristaltismo e o tônus do músculo intestinal. Possui ação termogênica e energética. O Cissus quadrangularis é indicado no tratamento da obesidade, distúrbios do colesterol, glicose, diabetes e envelhecimento.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">O produto pode interferir com inibidores da MAO e com drogas anti-hipertensivas (pelo aumento da secreção catecolaminérgica) Pode causar dificuldades para dormir, cólicas, excitação nervosa, gastrite, náusea. Imagens meramente ilustrativas. Pessoas com hipersensibilidade à substância não devem ingerir o produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o medicamento se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Todo medicamento deve ser mantido fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado.</p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>