<?php

    $title       = "Centella Asiática 500Mg 60 Cápsulas";
    $description = "Centella Asiatica 500mg da linha conhecida como gotu kola na cultura indiana, a Centella Asiática é rica em saponinas (também chamadas de triterpenóides)..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Centella Asiática 500Mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/centella-asiatica-500mg-60-capsulas.png" alt="centella-asiatica-500mg-60-capsulas" title="centella-asiatica-500mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>COMBATE À FLACIDEZ E CELULITE</h2>
                        <p class="text-justify">Centella Asiatica 500mg da linha conhecida como gotu kola na cultura indiana, a Centella Asiática é rica em saponinas (também chamadas de triterpenóides), glicosídeos que conferem à planta grande parte de seus benefícios. Tradicionalmente, a Centella Asiática tem sido utilizada para fortalecer o sistema circulatório, auxiliar no tratamento da celulite e da gordura localizada, entre outras.</p>
                        <br>
                        <h2>PELE FIRME</h2>
                        <p class="text-justify">Os constituintes da fração triterpênica da Centella Asiática atuam normalizando a produção de colágeno ao nível dos fibroblastos, promovendo o restabelecimento de uma trama colágena normal e flexível permitindo a liberação da gordura localizada graças à possibilidade de penetração das enzimas lipolíticas. Promove a normalização das trocas metabólicas entre a corrente sanguínea e os adipócitos.</p>
                    </div>
                </div>
                <h2>FACILITA A CICATRIZAÇÃO DE FERIDAS</h2>
                <p class="text-justify">Esta função é ainda auxiliada pela melhora da circulação venosa de retorno e pela diminuição da fragilidade capilar, que combate os processos degenerativos do tecido venoso. Também controla a fixação da prolina e alanina, elementos fundamentais na formação do colágeno. Sua ação sobre os edemas de origem venosa orientam o tratamento das celulites localizadas.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>