<?php

    $title       = "Acne e Oliosidade";
    $description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elementum ultrices commodo. Donec quis dictum tortor."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "home",
        "produtos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="container">
            <section class="nossos-produtos catego-prod container">
                <h1 class="text-center">Acne e Oliosidade</h1>
                
                <div id="list">
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/betacaroteno-50mg.png" alt="betacaroteno-50mg" title="betacaroteno-50mg" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Betacaroteno 50Mg</h3>
                                <p>Beta-caroteno melhor conhecido como um precursor da vitamina A...</p>
                            </div>
                            <a class="btn-entrectt" href="betacaroteno50mg.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/betacaroteno-25000ui.png" alt="betacaroteno-25000ui" title="betacaroteno-25000ui" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Betacaroteno 25000Ui</h3>
                                <p>Betacaroteno, melhor conhecido como um precursor da vitamina A...</p>
                            </div>
                            <a class="btn-entrectt" href="betacaroteno-25000ui.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/cavalinha100mg.png" alt="cavalinha100mg" title="cavalinha100mg" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Cavalinha 100Mg 60 Cápsulas</h3>
                                <p>A cavalinha é uma planta medicinal que pode trazer vários benefícios...</p>
                            </div>
                            <a class="btn-entrectt" href="cavalinha-100mg.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/gel-creme-antiacne-azeloglicina.png" alt="gel-creme-antiacne-azeloglicina" title="gel-creme-antiacne-azeloglicina" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Gel Creme Antiacne Com Azeloglicina </h3>
                                <p>Azeloglicina® é um ativo derivado da condensação do ácido azeláico...</p>
                            </div>
                            <a class="btn-entrectt" href="gel-creme-antiacne-azeloglicina-60g.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/gel-noturno-para-acne-30g.png" alt="gel-noturno-para-acne-30g" title="gel-noturno-para-acne-30g" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Gel Noturno Para Tratamento Da Acne </h3>
                                <p>O peróxido de benzoíla é um dos medicamentos tópicos mais antigos...</p>
                            </div>
                            <a class="btn-entrectt" href="gel-noturno-para-acne-30g.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/oleo-de-linhaca-500mg.png" alt="oleo-de-linhaca-500mg" title="oleo-de-linhaca-500mg" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Óleo De Linhaça 500Mg</h3>
                                <p>O organismo humano tem necessidades próprias que precisam ser mantidas...</p>
                            </div>
                            <a class="btn-entrectt" href="oleo-de-linhaca-500mg.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/sabonete-auxiliar-no-tratamento-antiacne.png" alt="sabonete-auxiliar-no-tratamento-antiacne" title="sabonete-auxiliar-no-tratamento-antiacne" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Sabonete Auxiliar No Tratamento Antiacne</h3>
                                <p>Esse ácido possui propriedades esfoliantes e antimicrobianas...</p>
                            </div>
                            <a class="btn-entrectt" href="sabonete-auxiliar-no-tratamento-antiacne.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/sabonete-auxiliar-no-tratamento-de-oleosidade.png" alt="sabonete-auxiliar-no-tratamento-de-oleosidade" title="sabonete-auxiliar-no-tratamento-de-oleosidade" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Sabonete Auxiliar No Tratamento De Oleosidade</h3>
                                <p>O Extrato Glicólico de Hamamélis tem ação adstringente, vasoprotetora...</p>
                            </div>
                            <a class="btn-entrectt" href="sabonete-auxiliar-no-tratamento-de-oleosidade.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/tonico-facial-antioleosidade.png" alt="tonico-facial-antioleosidade" title="tonico-facial-antioleosidade" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Tônico Facial Antioleosidade</h3>
                                <p>agente umectante natural, conhecido por ser abundante na pele humana...</p>
                            </div>
                            <a class="btn-entrectt" href="tonico-facial-antioleosidade.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/vitamina-oral-para-acne.png" alt="vitamina-oral-para-acne" title="vitamina-oral-para-acne" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Vitamina Oral Para Acne</h3>
                                <p>Vitamina A: A forma mais pura da vitamina A é o retinol, que possui alto poder...</p>
                            </div>
                            <a class="btn-entrectt" href="vitamina-oral-para-acne.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/filtro-solar-facial-antiacne-30g.png" alt="filtro-solar-facial-antiacne-30g" title="filtro-solar-facial-antiacne-30g" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Filtro Solar Facial Antiacne 30G</h3>
                                <p>O Filtro Solar Facial Antiacne, da Special Fórmulas, foi desenvolvido...</p>
                            </div>
                            <a class="btn-entrectt" href="filtro-solar-facial-antiacne-30g.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/cisteamina-5-30g-creme.png" alt="cisteamina-5-30g-creme" title="cisteamina-5-30g-creme" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Cisteamina 5% 30g Creme</h3>
                                <p>Cisteamina é o novo ativo indicado para desordens de hiperpigmentação...</p>
                            </div>
                            <a class="btn-entrectt" href="vitamina-oral-para-acne.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/locao-auxiliar-no-tratamento-de-acne-100ml.png" alt="locao-auxiliar-not-ratamento-de-acne-100ml" title="locao-auxiliar-not-ratamento-de-acne-100ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Loção Auxiliar No Tratamento De Acne 100Ml</h3>
                                <p>Acnebiol é uma formulação especialmente desenvolvida para o tratamento...</p>
                            </div>
                            <a class="btn-entrectt" href="locao-auxiliar-not-ratamento-de-acne-100ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/serum-efeito-lifiting-retrator-de-poros-30ml.png" alt="serum-efeito-lifiting-retrator-de-poros" title="serum-efeito-lifiting-retrator-de-poros" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Sérum Efeito Lifiting Retrator De Poros 30ML</h3>
                                <p>O Sérum Efeito Lifting Retrator De Poros Oficialfarma foi desenvolvido...</p>
                            </div>
                            <a class="btn-entrectt" href="serum-efeito-lifiting-retrator-de-poros.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/gel-facial-luminecense-c-vit-c-biodisponivel-30g.png" alt="gel-facial-luminecense-c-vit-c-biodisponivel-30g" title="gel-facial-luminecense-c-vit-c-biodisponivel-30g" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Gel Facial LumineCense C/ Vit C Biodisponível - 30g</h3>
                                <p>LumineCense é uma molécula desenvolvida no Japão única...</p>
                            </div>
                            <a class="btn-entrectt" href="gel-facial-luminecense-c-vit-c-biodisponivel-30g.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/mousse-de-limpeza-facial-2-em-1-hidratacao.png" alt="mousse-de-limpeza-facial-2-em-1-hidratacao-e-regeneracao-da-pele-150ml" title="mousse-de-limpeza-facial-2-em-1-hidratacao-e-regeneracao-da-pele-150ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Mousse de Limpeza Facial 2 em 1 Hidratação</h3>
                                <p>A Mousse de Limpeza Facial 2 em 1 Hidratação e Regeneração da Pele 150m...</p>
                            </div>
                            <a class="btn-entrectt" href="mousse-de-limpeza-facial-2-em-1-hidratacao-e-regeneracao-da-pele-150ml.php">Saiba +</a>
                        </div>
                    </div>
                </div>
            </section>
        </div>    
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>