<?php

    $title       = "PhosfaTOR 3g (Ácido Fosfatídico 750mg) 30 Sachês";
    $description = "PhosfaTOR é uma mistura de fosfatidilserina à base de soja e ácido fosfatídico, que pode ser utilizada para a saúde cognitiva, redução do estresse e aplicada..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">PhosfaTOR 3g (Ácido Fosfatídico 750mg) 30 Sachês</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/phosfator-3g-acido-fosfatidico-750mg-30-saches.png" alt="phosfator-3g-(acido-fosfatidico-750mg)-30-saches" title="phosfator-3g-(acido-fosfatidico-750mg)-30-saches">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">PhosfaTOR é uma mistura de fosfatidilserina à base de soja e ácido fosfatídico, que pode ser utilizada para a saúde cognitiva, redução do estresse e aplicada para a recuperação de exercício. Além disso, o produto contém de 20-26% de fosfatidilserina e 20-25% de ácido fosfatídico.</p>
                        <p class="text-justify">Trata-se de um produto produzido de acordo com padrões de qualidade seguros e rigorosos. O processo de produção para o PhosfaTOR® é realizado por uma série de reações enzimáticas começando com os fosfolipídeos derivados de soja e fosfolipase D derivada do repolho. A reação enzimática é realizada em água e isenta de quaisquer solventes. Ao final do processo, PhosfaTOR® é extraído e estabilizado na forma de pó para o uso em aplicações finais de consumo.</p>
                    </div>
                </div>
                <h2>REDUÇÃO DE ESTRESSE</h2>
                <p class="text-justify">A suplementação com PhosfaTOR® tem mostrado seu impacto sobre a resposta ao estresse, incluindo a diminuição dos níveis de cortisol e angústia. Em um estudo duplo cego, placebo controle, os participantes ingeriram PhosfaTOR suplemento ou placebo, durante 3 (três) semanas. Depois de três semanas de suplementação, os participantes completaram o ‘Teste Social de Estresse’. O cortisol sérico e salivar foram mensurados, bem como o estresse notado. Participantes do estudo que ingeriram 400mg de PhosfaTOR tiveram resultados estatiscamente significativos, com diminuição dos níveis de cortisol e de angústia após o teste de estresse.</p>
                <br>
                <h2>SAÚDE COGNITIVA</h2>
                <p class="text-justify">Fosfatidilserina tem sido amplamente estudada como um ingrediente para a saúde cognitiva. Numerosos estudos têm demonstrado que a fosfatisilserina ajuda a melhorar as funções cerebrais que, por sua vez, declinam com a idade. Especificamente, a fosfatidilserina tem sido mostrada para melhora da retenção de memória e suporte da concentração mental. Além disso, a fosfatidilserina é o único ingrediente para a saúde cognitiva o qual o FDA permite uma alegação de saúde qualificada.</p>
                <br>
                <h2>RECUPERAÇÃO DO EXERCÍCIO</h2>
                <p class="text-justify">A suplementação com PhosfaTOR  também pode ser benéfica na recuperação de esportes. Quando alguns indivíduos e os atletas estão engajados em programas de treinamentos vigorosos, os níveis de cortisol podem aumentar. Níveis muito elevados de cortisol, no entanto, podem impedir o processo de recuperação após o exercício. O cortisol é um hormônio catabólico, o que ocasiona a quebra das proteínas musculares em seus componentes menores, os aminoácidos. Cortisol também previne que os aminoácidos entrem nas células musculares dos tecidos de reparação e reconstrução. A diminuição dos níveis de cortisol pode ajudar os atletas a treinar mais arduamente e melhorar o processo de recuperação após o exercício. Estudos têm mostrado que a fosfatidilserina não só reduz os níveis de cortisol após o exercício, mas também melhora a dor muscular e a percepção de bem estar.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>