<?php

    $title       = "Pantoficial Men 60 Cápsulas";
    $description = "Pantoficial Men é um suplemento de vitaminas e minerais, desenvolvido para auxiliar nos casos de queda capilar masculina. As vitaminas e minerais..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Pantoficial Men 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/pantoficial-men-60-capsulas.png" alt="pantoficial-men-60-capsulas" title="pantoficial-men-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">Pantoficial Men é um suplemento de vitaminas e minerais, desenvolvido para auxiliar nos casos de queda capilar masculina. As vitaminas e minerais dão suporte nutricional para auxiliar no crescimento dos cabelos. A presença da vitamina C auxilia na formação da queratina, principal proteína da haste capilar. O zinco auxilia na proteção dos folículos e a biotina ajuda na prevenção da queda de cabelos.</p>
                        <p class="text-justify">Com eficácia comprovada no tratamento contra o enfraquecimento capilar, o Pantoficial apresenta uma combinação poderosa de substâncias que são formuladas da maneira ideal para devolver a saúde aos cabelos. Sua ação fortalece a estrutura dos fios, interrompendo a queda, estimulando o crescimento e preenchendo as áreas com falhas. Além disso, promove a redução do frizz e das pontas duplas, hidrata profundamente e garante um brilho visível.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>