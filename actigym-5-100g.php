<?php

    $title       = "Actigym 5% 100G";
    $description = "Actigym é um ativo marinho obtido por biotecnologia a partir do microrganismo Bacillus sp., que habita as ilhas Bermudas no Oceano Atlântico..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Actigym 5% 100G</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/actigym-5-100g.png" alt="actigym-5-100g" title="actigym-5-100g">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">Actigym é um ativo marinho obtido por biotecnologia a partir do microrganismo Bacillus sp., que habita as ilhas Bermudas no Oceano Atlântico. Actigym possui como principais benefícios a melhora do tônus corporal, redefinição da silhueta, redução do peso e das medidas do abdômen, coxa e braço com resultados potencializados quando combinado com atividade física.</p>
                        <br>
                        <h2>PARA MODELAR A SILHUETA</h2>
                        <p class="text-justify">A pele tem uma excelente absorção, não deixando oleosa ou ressecada demais que poderia acabar sendo prejudicial. Como diz o título passa a ter uma linda silhueta de hoje em diante, podendo arrasar com aquelas roupas mais justinhas para as ocasiões especiais. De acordo com os testes feitos notou-se uma perda 2,8cm do abdômen em apenas 28 dias e 2,1cm nas coxas em mais de 30 dias. Sentir estes e outros efeitos positivos faz com que tenha o aumento da autoestima, que antes talvez tenha sido afetada por não se sentir bem consigo mesmo e para que tenha dias mais animados.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>