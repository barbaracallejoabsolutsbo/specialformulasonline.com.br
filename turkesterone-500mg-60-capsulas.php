<?php

    $title       = "Turkesterone 500Mg 60 Cápsulas";
    $description = "O Turkesterone é um suplemento que possui em sua composição ECDISTEROIDE. Este elemento é encontrado em insetos, onde atuam como fatores de crescimento..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Turkesterone 500Mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/turkesterone-500mg-60-capsulas.png" alt="turkesterone-500mg-60-capsulas" title="turkesterone-500mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>TURKESTERONE 500MG 60 CÁPSULAS</h2>
                        <p class="text-justify">O Turkesterone é um suplemento que possui em sua composição ECDISTEROIDE. Este elemento é encontrado em insetos, onde atuam como fatores de crescimento. O ecdisteroide pode ser encontrado em cerca de 6% das plantas conhecidas. Para o desenvolvimento da hipertrofia, o turkesterone também apresenta efeitos específicos bastante positivos. Ao acelerar o consumo de lipídios e carboidratos, ele se torna uma ferramenta eficaz para a queima de gordura.</p>
                        <p class="text-justify">Além disso, ele gera um efeito de retenção de nitrogênio nas células. Esta característica torna o metabolismo mais eficiente também em relação às proteínas. Isso gera benefícios catabólicos e anabólicos poderosos para a construção de músculos e massa magra. Um dos fatores positivos do Turkesterone é que este suplemento não apresenta efeitos colaterais tão agressivos quanto os anabolizantes existentes no mercado. Com o Turkesterone você aumenta a síntese de fibras musculares, gerando maior hipertrofia muscular e reduzindo a taxa de gordura do corpo em relação aos músculos. Outro fator positivo do Turkesterone é que ele potencializa o desempenho durante os treinos físicos.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado.</p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>