<?php

    $title       = "L-Carnitina 500Mg 60 Doses";
    $description = "Pouco conhecida ainda, a l-carnitina é um ótimo suplemento para atletas, fisiculturistas e também para aquelas pessoas que desejam apenas..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">L-Carnitina 500Mg 60 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/l-carnitina-500mg-60-doses.png" alt="l-carnitina-500mg-60-doses" title="l-carnitina-500mg-60-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">Pouco conhecida ainda, a l-carnitina é um ótimo suplemento para atletas, fisiculturistas e também para aquelas pessoas que desejam apenas complementar a alimentação com nutrientes saudáveis. Ela proporciona uma série de benefícios para o organismo. Conheça um pouco mais sobre este nutriente, fonte de energia. A l-carnitina é uma substância natural, produzida pelo fígado e rins, e armazenada nos músculos. Sua principal função é ajudar o corpo a transformar gordura em energia.</p>
                        <br>
                        <h2>GORDURA LOCALIZADA</h2>
                        <p class="text-justify">Quando utilizada por atletas, contribui para o aumenta o rendimento físico, tem efeito estimulante, proporciona a queima de gordura, e ajuda no fortalecimento do sistema imunológico. Atua também no desenvolvimento das faculdades mentais e auxilia a baixar os níveis de colesterol e triglicerídeos. Na prática, é ela quem realiza o transporte dos derivados da gordura para dentro das mitocôndrias, onde eles serão oxidados e transformados em energia. Ela é sintetizada através de dois aminoácidos essenciais: a lisina e a metionina, com a ação conjunta da niacina, vitamina B6, vitamina C e ferro.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>