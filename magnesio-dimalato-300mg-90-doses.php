<?php

    $title       = "Magnésio Dimalato 300Mg 90 Doses";
    $description = "O Magnésio é um mineral essencial para os todos os processos enzimáticos que ocorrem no nosso organismo. Atua principalmente na produção de energia..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Magnésio Dimalato 300Mg 90 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/magnesio-dimalato-300mg-90-doses.png" alt="magnesio-dimalato-300mg-90-doses" title="magnesio-dimalato-300mg-90-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>AUMENTA A RESISTÊNCIA FÍSICA E MENTAL</h2>
                        <p class="text-justify">O Magnésio é um mineral essencial para os todos os processos enzimáticos que ocorrem no nosso organismo. Atua principalmente na produção de energia. A suplementação de Magnésio contribui para muitos sistemas do organismo, em especial músculos e nervos. Atua em mais de 300 reações metabólicas, desempenhando papel fundamental no metabolismo da glicose, na homeostase insulínica e glicêmica, na síntese de adenosina trifosfato, proteínas e ácidos nucleicos.</p>
                        <br>
                        <h2>PREVINE A OSTEOPOROSE</h2>
                        <p class="text-justify">O Magnésio também é importante no processo de metabolização do Cálcio, da Vitamina C, Fósforo, Sódio e Potássio, sendo muito utilizado como suplemento por pessoas que não conseguem suprir suas necessidades diárias desse composto através da ingestão alimentar. Participa do processo de fixação do cálcio nos ossos, contribuindo para o fortalecimento ósseo. Além disso, promove efeito analgésico natural, auxiliando na redução da dor, recuperação muscular e melhora do desempenho durante a atividade física. O Magnésio é vital para saúde do tecido muscular e nervoso.</p>
                    </div>
                </div>
                <h2>AUXILIA NO TRATAMENTO DE DORES CRÔNICAS</h2>
                <p class="text-justify">Por conter ácido málico, ele atua diretamente em dores crônicas, e, portanto, é recomendado para doenças crônicas como a fibromialgia. Ele também é ótimo para quem sofre de insônia e taquicardias, pois o cérebro e o coração são os maiores depósitos de magnésio no corpo. O ácido málico é empregado em casos de fibromialgia e cansaço crônico.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">O magnésio interage com tetraciclina, ranitidina, cimetidina, dexametasona ou prednisona, diminuindo a absorção desses fármacos. Quando se fizer necessária a administração de qualquer um desses fármacos acima conjuntamente com o magnésio, recomenda-se administrar respeitando intervalo de 2 ou 3 horas entre um e outro. Não deve ser administrado em crianças menores de 6 anos, devido ao risco de hipermagnesemia. Nunca compre medicamento sem orientação de um profissional habilitado. Imagens meramente ilustrativas. Pessoas com hipersensibilidade à substância não devem ingerir o produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o medicamento com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o medicamento se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Todo medicamento deve ser mantido fora do alcance das crianças. Este medicamento não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica.</p>
                <p class="advertencias text-justify">O uso do medicamento durante o período de amamentação também não é recomendado. Este medicamento não deve ser utilizado por menores de 18 anos sem orientação médica. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO". "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>