<?php

    $title       = "Catalase 5000 UI 60 Cápsulas";
    $description = "A Catalase é uma enzima produzida naturalmente pelo organismo e que desempenha papel vital na decomposição do peróxido de hidrogênio e outros radicais livres..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Catalase 5000 UI 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/catalase-5000-ui-60-capsulas.png" alt="catalase-5000-ui-60-capsulas" title="Catalase 5000 UI 60 Cápsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">A Catalase é uma enzima produzida naturalmente pelo organismo e que desempenha papel vital na decomposição do peróxido de hidrogênio e outros radicais livres altamente reativos. Sua ação consiste em neutralizar o peróxido de hidrogênio (água oxigenada), responsável por interferir no funcionamento normal da melanina, pigmento que dá coloração aos cabelos e pele. Dessa forma, impede o surgimento de fios brancos e auxilia no tratamento do vitiligo, por parar a progressão da doença e estimular a repigmentação.</p>
                        <br>
                        <h2>ESTIMULA A REPIGMENTACÃO DA PELE ALTERADA PELO VITILIGO</h2>
                        <p class="text-justify">Nesse sentido, a catalase funciona como antioxidante de primeira linha de defesa contra o dano mediado por peróxido de hidrogênio em todos os órgãos e tecidos, e a sua redução está relacionada diretamente com os processos de envelhecimento. É importante ressaltar que no tratamento de vitiligo deve se levar em conta a forma clínica, tempo de evolução, atividade, área acometida, idade e a localização das manchas.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Nunca compre medicamento sem orientação de um profissional habilitado. Imagens meramente ilustrativas. Pessoas com hipersensibilidade à substância não devem ingerir o produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o medicamento com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o medicamento se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Todo medicamento deve ser mantido fora do alcance das crianças. Este medicamento não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do medicamento durante o período de amamentação também não é recomendado. Este medicamento não deve ser utilizado por menores de 18 anos sem orientação médica. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>