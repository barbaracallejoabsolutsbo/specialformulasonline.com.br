<?php

    $title       = "Barba";
    $description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elementum ultrices commodo. Donec quis dictum tortor."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "home",
        "produtos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="container">
            <section class="nossos-produtos catego-prod container">
                <h1 class="text-center">Barba</h1>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/shampoo-p-barba-e-cabelo-120ml.png" alt="shampoo-p-barba-e-cabelo-120ml" title="shampoo-p-barba-e-cabelo-120ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Shampoo P/ Barba E Cabelo 120ml</h3>
                                <p>O shampoo para barba e cabelo da Special Fórmulas...</p>
                            </div>
                            <a class="btn-entrectt" href="shampoo-p-barba-e-cabelo-120ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/minoxidil-5-em-trichosol-cabelos-e-barba-sem-alcool-60ml.png" alt="minoxidil-5-em-trichosol-cabelos-e-barba-(sem-alcool)-60ml" title="minoxidil-5-em-trichosol-cabelos-e-barba-(sem-alcool)-60ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Minoxidil 5% Em Trichosol Cabelos E Barba (Sem álcool) 60ML</h3>
                                <p>O Minoxidil 5% em TrichoSol Cabelo e Barba...</p>
                            </div>
                            <a class="btn-entrectt" href="minoxidil-5-em-trichosol-cabelos-e-barba-sem-alcool-60ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/minoxidil-5-em-creme-para-barba-120g.png" alt="minoxidil-5-em-creme-para-barba-120g" title="minoxidil-5-em-creme-para-barba-120g" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Minoxidil 5% Em Creme Para Barba 120G</h3>
                                <p>Beta-caroteno melhor conhecido como um precursor da vitamina A...</p>
                            </div>
                            <a class="btn-entrectt" href="minoxidil-5-em-creme-para-barba-120g.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/minoxidil-com-fator-de-crescimento-60g-p-barba.png" alt="minoxidil-com-fator-de-crescimento-60g-p-barba" title="minoxidil-com-fator-de-crescimento-60g-p-barba" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Minoxidil Com Fator De Crescimento 60G P/ Barba</h3>
                                <p>O Minoxidil 5% Com Fatores de Crescimento é uma fórmula...</p>
                            </div>
                            <a class="btn-entrectt" href="minoxidil-com-fator-de-crescimento-60g-p-barba.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/shampoo-regenerador-200ml.png" alt="shampoo-regenerador-200ml" title="shampoo-regenerador-200ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Shampoo Regenerador 200Ml</h3>
                                <p>O shampoo regenerador 200ML foi desenvolvido para uma aplicação...</p>
                            </div>
                            <a class="btn-entrectt" href="shampoo-regenerador-200ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/minoxidil-5-com-fator-de-crescimento-p-barba-120g.png" alt="minoxidil-5-com-fator-de-crescimento-p-barba-120g" title="minoxidil-5-com-fator-de-crescimento-p-barba-120g" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Minoxidil 5% Com Fator De Crescimento P/ Barba 120G</h3>
                                <p>A melatonina é um hormônio ligado ao ciclo circadiano...</p>
                            </div>
                            <a class="btn-entrectt" href="minoxidil-5-com-fator-de-crescimento-p-barba-120g.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/locao-de-fatores-de-crescimento-capilar-barba-60ml.png" alt="locao-de-fatores-de-crescimento-capilar-barba-60ml" title="locao-de-fatores-de-crescimento-capilar-barba-60ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Loção De Fatores De Crescimento Capilar / Barba 60Ml</h3>
                                <p>A Loção de fatores de crescimento capilar/barba...</p>
                            </div>
                            <a class="btn-entrectt" href="locao-de-fatores-de-crescimento-capilar-barba-60ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/queratina-100mg-90-capsulas.png" alt="queratina-100mg-90-capsulasa" title="queratina-100mg-90-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Queratina 100mg 90 Cápsulas</h3>
                                <p>A Queratina, da Special Fórmulas, foi desenvolvida...</p>
                            </div>
                            <a class="btn-entrectt" href="queratina-100mg-90-capsulas.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/condicionador-masculino-barba-e-cabelo-60ml.png" alt="condicionador-masculino-barba-e-cabelo-60ml" title="condicionador-masculino-barba-e-cabelo-60ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Condicionador Masculino Barba E Cabelo - 60ml</h3>
                                <p>O Condicionador Masculino Barba, e Cabelo da Special Fórmulas...</p>
                            </div>
                            <a class="btn-entrectt" href="condicionador-masculino-barba-e-cabelo-60ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/shampoo-p-barba-e-cabelo-120ml.png" alt="shampoo-p-barba-e-cabelo-120ml" title="shampoo-p-barba-e-cabelo-120ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Shampoo P/ Barba E Cabelo 120ml</h3>
                                <p>O shampoo para barba e cabelo da Special Fórmulas...</p>
                            </div>
                            <a class="btn-entrectt" href="shampoo-p-barba-e-cabelo-120ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/minoxidil-turbinado-120ml.png" alt="minoxidil-turbinado-120ml" title="minoxidil-turbinado-120ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Minoxidil Turbinado 120Ml</h3>
                                <p>O Minoxidil Turbinado é uma fórmula desenvolvida pela Special Fórmulas...</p>
                            </div>
                            <a class="btn-entrectt" href="minoxidil-turbinado-120ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/pill-food-turbinado-120-capsulas.png" alt="pill-food-turbinado-120-capsulas" title="pill-food-turbinado-120-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Pill Food Turbinado 120 Cápsulas</h3>
                                <p>Pill Food Turbinado é um complexo alimentar formado...</p>
                            </div>
                            <a class="btn-entrectt" href="pill-food-turbinado-120-capsulas.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/minoxidilcom-propilenoglicol-5-120ml-locao-capilar.png" alt="minoxidilcom-propilenoglicol-5-120ml-locao-capilar" title="minoxidilcom-propilenoglicol-5-120ml-locao-capilarl" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>MinoxidilCom Propilenoglicol 5% 120Ml - Loção Capilar</h3>
                                <p>O Minoxidil 5% Com Propilenoglicol...</p>
                            </div>
                            <a class="btn-entrectt" href="minoxidilcom-propilenoglicol-5-120ml-locao-capilar.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/minoxidil-5-com-biotina-120ml.png" alt="minoxidil-5-com-biotina-120ml" title="minoxidil-5-com-biotina-120ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Minoxidil 5% Com Biotina 120Ml</h3>
                                <p>O Minoxidil 5% Com Biotina é uma fórmula desenvolvida...</p>
                            </div>
                            <a class="btn-entrectt" href="minoxidil-5-com-biotina-120ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/pqq-pirroloquinolina-quinona-10mg-30-capsulas.png" alt="pqq-pirroloquinolina-quinona-10mg-30-capsulas" title="pqq-pirroloquinolina-quinona-10mg-30-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Pqq - Pirroloquinolina Quinona 10Mg 30 Cápsulas</h3>
                                <p>A Pirroloquinolina quinona (PQQ) é uma quinona...</p>
                            </div>
                            <a class="btn-entrectt" href="pqq-pirroloquinolina-quinona-10mg-30-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/biotina-5mg-60-capsulas.png" alt="biotina-5mg-60-capsulas" title="biotina-5mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Biotina 5Mg 60 Cápsulas</h3>
                                <p>A Biotina é uma vitamina hidrossolúvel do complexo B...</p>
                            </div>
                            <a class="btn-entrectt" href="biotina-5mg-60-capsulas.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/condicionador-masculino-barba-e-cabelo-60ml.png" alt="condicionador-masculino-barba-e-cabelo-60ml" title="condicionador-masculino-barba-e-cabelo-60ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Condicionador Masculino Barba E Cabelo - 60ml</h3>
                                <p>O Condicionador Masculino Barba, e Cabelo da Special Fórmulas...</p>
                            </div>
                            <a class="btn-entrectt" href="condicionador-masculino-barba-e-cabelo-60ml.php">Saiba +</a>
                        </div>
                    </div>
                        
            </section>
        </div>    
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>