<?php

    $title       = "Testofen 300mg 60 Cápsulas";
    $description = "Aumento de performance nos exercícios. Pertencente ao grupo dos fenusteroles derivado das sementes do extrato Trigonella foenum graecum..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Testofen 300mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/testofen-300mg-60-capsulas.png" alt="testofen-300mg-60-capsulas" title="testofen-300mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>TESTOFEN 300MG 60 CÁPSULAS</h2>
                        <p class="text-justify">Aumento de performance nos exercícios. Pertencente ao grupo dos fenusteroles derivado das sementes do extrato Trigonella foenum graecum padronizado em 50% de fenosídeo, glicosídeo rico em saponina bioativa (furostanol e esteroides) originário de países asiáticos.</p>
                        <p class="text-justify">O Testofen realiza uma ação específica junto a testosterona orgânica. Grande parte da testosterona do organismo está ligada as proteínas SHBG (Sex Hormone Binding Globulin) e albumina, ficando com apenas 2 a 3% deste hormônio (5-50pg/ml) circulante livre, como a forma biodisponível responsável pela a ação hormonal. O fenosídeo presente no Testofen promove um deslocamento de testosterona ligada ao SHBG para testosterona livre aumentando seus níveis em média 98-99% em relação à concentração inicial.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado.</p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>