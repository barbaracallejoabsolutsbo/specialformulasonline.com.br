<?php

    $title       = "Produtos";
    $description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elementum ultrices commodo. Donec quis dictum tortor."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "home",
        "produtos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="container">
            <section class="nossos-produtos catego-prod container">
                <h1 class="text-center">Péle</h1>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/betacaroteno-50mg.png" alt="betacaroteno-50mg" title="betacaroteno-50mg" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Betacaroteno 50Mg</h3>
                                <p>Beta-caroteno melhor conhecido como um precursor da vitamina A, é um importante antioxidante que ajuda...</p>
                            </div>
                            <a class="btn-entrectt" href="betacaroteno50mg.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/betacaroteno-25000ui.png" alt="betacaroteno-25000ui" title="betacaroteno-25000ui" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Betacaroteno 25000Ui</h3>
                                <p>Betacaroteno, melhor conhecido como um precursor da vitamina A, é um importante antioxidante que ajuda...</p>
                            </div>
                            <a class="btn-entrectt" href="betacaroteno-25000ui.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/cavalinha100mg.png" alt="cavalinha100mg" title="cavalinha100mg" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Cavalinha 100Mg 60 Cápsulas</h3>
                                <p>A cavalinha é uma planta medicinal que pode trazer vários benefícios para a saúde. Ela auxilia na redução de peso...</p>
                            </div>
                            <a class="btn-entrectt" href="cavalinha-100mg.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/gel-creme-antiacne-azeloglicina.png" alt="gel-creme-antiacne-azeloglicina" title="gel-creme-antiacne-azeloglicina" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Gel Creme Antiacne Com Azeloglicina </h3>
                                <p>Azeloglicina® é um ativo derivado da condensação do ácido azeláico ao aminoácido glicina, que garante total eficácia e segurança ao tratamento...</p>
                            </div>
                            <a class="btn-entrectt" href="gel-creme-antiacne-azeloglicina-60g.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/gel-noturno-para-acne-30g.png" alt="gel-noturno-para-acne-30g" title="gel-noturno-para-acne-30g" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Gel Noturno Para Tratamento Da Acne </h3>
                                <p>O peróxido de benzoíla é um dos medicamentos tópicos mais antigos usados no tratamento da acne. Trata-se de uma substância...</p>
                            </div>
                            <a class="btn-entrectt" href="gel-noturno-para-acne-30g.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/oleo-de-linhaca-500mg.png" alt="oleo-de-linhaca-500mg" title="oleo-de-linhaca-500mg" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Óleo De Linhaça 500Mg</h3>
                                <p>O organismo humano tem necessidades próprias que precisam ser mantidas em constante equilíbrio...</p>
                            </div>
                            <a class="btn-entrectt" href="oleo-de-linhaca-500mg.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/sabonete-auxiliar-no-tratamento-antiacne.png" alt="sabonete-auxiliar-no-tratamento-antiacne" title="sabonete-auxiliar-no-tratamento-antiacne" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Sabonete Auxiliar No Tratamento Antiacne</h3>
                                <p>Esse ácido possui propriedades esfoliantes e antimicrobianas, o que significa que afina a camada espessada da pele...</p>
                            </div>
                            <a class="btn-entrectt" href="sabonete-auxiliar-no-tratamento-antiacne.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/sabonete-auxiliar-no-tratamento-de-oleosidade.png" alt="sabonete-auxiliar-no-tratamento-de-oleosidade" title="sabonete-auxiliar-no-tratamento-de-oleosidade" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Sabonete Auxiliar No Tratamento De Oleosidade</h3>
                                <p>O Extrato Glicólico de Hamamélis tem ação adstringente, vasoprotetora, vasoconstritora, descongestionante...</p>
                            </div>
                            <a class="btn-entrectt" href="sabonete-auxiliar-no-tratamento-de-oleosidade.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/tonico-facial-antioleosidade.png" alt="tonico-facial-antioleosidade" title="tonico-facial-antioleosidade" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Tônico Facial Antioleosidade</h3>
                                <p>agente umectante natural, conhecido por ser abundante na pele humana, ele ajuda a pele e os cabelos a se manterem com uma aparência...</p>
                            </div>
                            <a class="btn-entrectt" href="tonico-facial-antioleosidade.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/vitamina-oral-para-acne.png" alt="vitamina-oral-para-acne" title="vitamina-oral-para-acne" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Vitamina Oral Para Acne</h3>
                                <p>Vitamina A: A forma mais pura da vitamina A é o retinol, que possui alto poder antioxidante...</p>
                            </div>
                            <a class="btn-entrectt" href="vitamina-oral-para-acne.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/filtro-solar-facial-antiacne-30g.png" alt="filtro-solar-facial-antiacne-30g" title="filtro-solar-facial-antiacne-30g" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Filtro Solar Facial Antiacne 30G</h3>
                                <p>O Filtro Solar Facial Antiacne, da Special Fórmulas, foi desenvolvido cuidadosamente para proteger a pele acneica. Sua formulação exclusiva contém...</p>
                            </div>
                            <a class="btn-entrectt" href="filtro-solar-facial-antiacne-30g.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/cisteamina-5-30g-creme.png" alt="cisteamina-5-30g-creme" title="cisteamina-5-30g-creme" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Cisteamina 5% 30g Creme</h3>
                                <p>Cisteamina é o novo ativo indicado para desordens de hiperpigmentação cutânea, atuando como corretor de pigmentos. Naturalmente presente no corpo...</p>
                            </div>
                            <a class="btn-entrectt" href="vitamina-oral-para-acne.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/locao-auxiliar-no-tratamento-de-acne-100ml.png" alt="locao-auxiliar-not-ratamento-de-acne-100ml" title="locao-auxiliar-not-ratamento-de-acne-100ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Loção Auxiliar No Tratamento De Acne 100Ml</h3>
                                <p>Acnebiol é uma formulação especialmente desenvolvida para o tratamento da pele com acne e oleosa, seja jovem ou adulta. Os princípios ativos do Acnebiol...</p>
                            </div>
                            <a class="btn-entrectt" href="locao-auxiliar-not-ratamento-de-acne-100ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/serum-efeito-lifiting-retrator-de-poros-30ml.png" alt="serum-efeito-lifiting-retrator-de-poros" title="serum-efeito-lifiting-retrator-de-poros" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Sérum Efeito Lifiting Retrator De Poros 30ML</h3>
                                <p>O Sérum Efeito Lifting Retrator De Poros Oficialfarma foi desenvolvido com um blend de ativos especiais que possuem propriedades hidratantes e atuam...</p>
                            </div>
                            <a class="btn-entrectt" href="serum-efeito-lifiting-retrator-de-poros.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/gel-facial-luminecense-c-vit-c-biodisponivel-30g.png" alt="gel-facial-luminecense-c-vit-c-biodisponivel-30g" title="gel-facial-luminecense-c-vit-c-biodisponivel-30g" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Gel Facial LumineCense C/ Vit C Biodisponível - 30g</h3>
                                <p>LumineCense é uma molécula desenvolvida no Japão única, que assegura uma vitamina C 100% estabilizada em solução aquosa extraída das geleiras...</p>
                            </div>
                            <a class="btn-entrectt" href="gel-facial-luminecense-c-vit-c-biodisponivel-30g.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/mousse-de-limpeza-facial-2-em-1-hidratacao.png" alt="mousse-de-limpeza-facial-2-em-1-hidratacao-e-regeneracao-da-pele-150ml" title="mousse-de-limpeza-facial-2-em-1-hidratacao-e-regeneracao-da-pele-150ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Mousse de Limpeza Facial 2 em 1 Hidratação</h3>
                                <p>A Mousse de Limpeza Facial 2 em 1 Hidratação e Regeneração da Pele 150ml da Oficialfarma, foi desenvolvido com ativos selecionados que auxiliam na limpeza...</p>
                            </div>
                            <a class="btn-entrectt" href="mousse-de-limpeza-facial-2-em-1-hidratacao-e-regeneracao-da-pele-150ml.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/melatonina-3mg-100-doses.png" alt="melatonina-3mg-100-doses" title="melatonina-3mg-100-doses" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Melatonina 3Mg 100 Doses</h3>
                                <p>A Melatonina Spefical Fórmulas traz inúmeros benefícios à saúde...</p>
                            </div>
                            <a class="btn-entrectt" href="melatonina-3mg-100-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/vitamina-c-500mg-60-doses.png" alt="vitamina-c-500mg-60-doses" title="vitamina-c-500mg-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Vitamina C 500Mg 60 Doses</h3>
                                <p>A vitamina C é uma vitamina que deve estar presente em nossas vidas ela nos...</p>
                            </div>
                            <a class="btn-entrectt" href="vitamina-c-500mg-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/vitamina-c-500mg-zinco-15mg-30-doses.png" alt="vitamina-c-500mg-zinco-15mg-30-doses" title="vitamina-c-500mg-zinco-15mg-30-doses" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Vitamina C 500mg + Zinco 15mg 30 Doses</h3>
                                <p>Vitamina C + Zinco, é uma excelente combinação de...</p>
                            </div>
                            <a class="btn-entrectt" href="vitamina-c-500mg-zinco-15mg-30-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/acido-hialuronico-1-gel-creme.png" alt="acido-hialuronico-1-gel-creme" title="acido-hialuronico-1-gel-creme" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Ácido Hialurônico 1% Gel Creme 30g</h3>
                                <p>O ÁCIDO HIALURÔNICO é um polissacarídeo da família das Glicosaminoglicanas...</p>
                            </div>
                            <a class="btn-entrectt" href="acido-hialuronico-1-gel-creme.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/acido-hialuronico-50mg-30-doses.png" alt="acido-hialuronico-50mg-30-doses" title="acido-hialuronico-50mg-30-doses" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Ácido Hialurônico 50Mg 30 Doses</h3>
                                <p>O ÁCIDO HIALURÔNICO é um polissacarídeo da família das Glicosaminoglicanas...</p>
                            </div>
                            <a class="btn-entrectt" href="acido-hialuronico-50mg-30-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/pqq-pirroloquinolina-quinona-10mg-30-capsulas.png" alt="pqq-pirroloquinolina-quinona-10mg-30-capsulas" title="pqq-pirroloquinolina-quinona-10mg-30-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Pqq - Pirroloquinolina Quinona 10Mg 30 Cápsulas</h3>
                                <p>A Pirroloquinolina quinona (PQQ) é uma quinona...</p>
                            </div>
                            <a class="btn-entrectt" href="pqq-pirroloquinolina-quinona-10mg-30-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/vitamina-a-10.000ui60-doses.png" alt="vitamina-a-10.000ui60-doses" title="vitamina-a-10.000ui60-doses" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Vitamina A 10.000Ui 60 Dos</h3>
                                <p>A Vitamina A não apenas fortalece os pontos de entrada do corpo humano...</p>
                            </div>
                            <a class="btn-entrectt" href="vitamina-a-10.000ui60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/oleo-de-semente-de-abobora-1000mg.png" alt="oleo-de-semente-de-abobora-1000mg" title="oleo-de-semente-de-abobora-1000mg" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Óleo de Semente de Abóbora 1000mg 80 Cápsulas</h3>
                                <p>O Óleo de Semente de Abóbora possui...</p>
                            </div>
                            <a class="btn-entrectt" href="oleo-de-semente-de-abobora-1000mg.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/acido-hialuronico-50mg.png" alt="acido-hialuronico-50mg" title="acido-hialuronico-50mg" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Ácido Hialurônico 50Mg 60 Doses</h3>
                                <p>O ÁCIDO HIALURÔNICO é um polissacarídeo da família...</p>
                            </div>
                            <a class="btn-entrectt" href="acido-hialuronico-50mg.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/pill-food-120-capsulas.png" alt="pill-food-120-capsulas" title="pill-food-120-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Pill Food 120 Cápsulas</h3>
                                <p>Pill Food é um complexo alimentar formado por vitaminas...</p>
                            </div>
                            <a class="btn-entrectt" href="pill-food-120-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/exsynutriment-150mg.png" alt="exsynutriment-150mg" title="exsynutriment-150mg" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Exsynutriment 150Mg 60 Cápsulas</h3>
                                <p>O Exsynutriment® é considerado um nutricosmético...</p>
                            </div>
                            <a class="btn-entrectt" href="exsynutriment-150mg.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/acido-kojico-dipalmitato-50g-gel-creme.png" alt="acido-kojico-dipalmitato-50g-gel-creme" title="acido-kojico-dipalmitato-50g-gel-creme" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Ácido Kójico Dipalmitato 50g Gel Creme</h3>
                                <p>O Ácido kójico Dipalmitato é um agente clareador...</p>
                            </div>
                            <a class="btn-entrectt" href="acido-kojico-dipalmitato-50g-gel-creme.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/dmae-bitartarato-250mg.png" alt="dmae-bitartarato-250mg" title="dmae-bitartarato-250mg" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>DMAE Bitartarato 250mg 60 Cápsulas</h3>
                                <p>Com o processo natural do envelhecimento...</p>
                            </div>
                            <a class="btn-entrectt" href="dmae-bitartarato-250mg.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/astaxantina-4mg.png" alt="astaxantina-4mg" title="astaxantina-4mg" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Astaxantina 4Mg 60 Cápsulas</h3>
                                <p>Ao contrário de muitos outros antioxidantes...</p>
                            </div>
                            <a class="btn-entrectt" href="astaxantina-4mg.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/vitamina-e-400ui.png" alt="vitamina-e-400ui" title="vitamina-e-400ui" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Vitamina E 400Ui 30 Doses</h3>
                                <p>A Vitamina E é uma vitamina lipossolúvel essencial na nutrição...</p>
                            </div>
                            <a class="btn-entrectt" href="vitamina-e-400ui.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/licopeno-10mg.png" alt="licopeno-10mg" title="licopeno-10mg" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Licopeno 10Mg 30 Cápsulas</h3>
                                <p>"Licopeno é um composto da família dos carotenoides...</p>
                            </div>
                            <a class="btn-entrectt" href="licopeno-10mg.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/silicio-organico-150mg.png" alt="silicio-organico-150mg" title="silicio-organico-150mg" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Silício Orgânico 150Mg 60 Cápsulas</h3>
                                <p>O Silício Orgânico Nutricolin é um complexo formado...</p>
                            </div>
                            <a class="btn-entrectt" href="silicio-organico-150mg.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/vitamina-a-5.000ui.png" alt="vitamina-a-5.000ui" title="vitamina-a-5.000ui" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Vitamina A 5.000Ui 60 Cápsulas</h3>
                                <p>Imunidade: A Vitamina A não apenas fortalece...</p>
                            </div>
                            <a class="btn-entrectt" href="vitamina-a-5.000ui.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/resveratrol-250mg.png" alt="resveratrol-250mg" title="resveratrol-250mg" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Resveratrol 250Mg 60Cápsulas</h3>
                                <p>O Resveratrol, é um composto fenólico...</p>
                            </div>
                            <a class="btn-entrectt" href="resveratrol-250mg.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/oli-ola-300mg-peeling.png" alt="oli-ola-300mg-peeling" title="oli-ola-300mg-peeling" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Oli Ola 300Mg Peeling 60 Cápsulas</h3>
                                <p>Ao promover uma ação de “peeling em cápsulas"...</p>
                            </div>
                            <a class="btn-entrectt" href="oli-ola-300mg-peeling.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/verisol-2,5g.png" alt="verisol-2,5g" title="verisol-2,5g" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Verisol 2,5g 30 Sachês</h3>
                                <p>O Verisol Special Fórmulas é o único colágeno...</p>
                            </div>
                            <a class="btn-entrectt" href="verisol-2,5g.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/polypodium-leucotomos-250mg.png" alt="polypodium-leucotomos-250mg" title="polypodium-leucotomos-250mg" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Polypodium Leucotomos 250Mg 30 Cápsulas</h3>
                                <p>Proteção da estrutura cutânea: PL apresenta efeitos anti-aging...</p>
                            </div>
                            <a class="btn-entrectt" href="polypodium-leucotomos-250mg.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/exsynutriment-150mg-30-doses.png" alt="exsynutriment-150mg-30-doses" title="exsynutriment-150mg-30-doses" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Exsynutriment 150Mg 30</h3>
                                <p>O Exsynutriment é considerado um nutricosmético...</p>
                            </div>
                            <a class="btn-entrectt" href="exsynutriment-150mg-30-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/fo-ti-(he-shou-wu)-100mg-60-capsulas.png" alt="fo-ti-(he-shou-wu)-100mg-60-capsulas" title="fo-ti-(he-shou-wu)-100mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Fo Ti (He Shou Wu) 100Mg 60 Cápsulas</h3>
                                <p>He shou wu significa homem de cabelo preto, seu nome surgiu...</p>
                            </div>
                            <a class="btn-entrectt" href="fo-ti-(he-shou-wu)-100mg-60-capsulas.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/vitamina-b5-(acido-antotenico)-500Mg-60-doses.png" alt="vitamina-b5-(acido-antotenico)-500Mg-60-doses" title="vitamina-b5-(acido-antotenico)-500Mg-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Vitamina B5 (Ácido Pantotênico) 500Mg 60 Doses</h3>
                                <p>Beta-caroteno melhor conhecido como um precursor da vitamina A...</p>
                            </div>
                            <a class="btn-entrectt" href="vitamina-b5-(acido-antotenico)-500Mg-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/trans-resveratrol-100mg-60-doses.png" alt="trans-resveratrol-100mg-60-doses" title="trans-resveratrol-100mg-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Trans Resveratrol 100Mg 60 Doses</h3>
                                <p>O Resveratrol, é um composto fenólico...</p>
                            </div>
                            <a class="btn-entrectt" href="trans-resveratrol-100mg-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/saw-palmetto-licopeno-60-capsulas.png" alt="saw-palmetto-licopeno-60-capsulas" title="saw-palmetto-licopeno-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Saw Palmetto + Licopeno 60 Cápsulas</h3>
                                <p>Associação fitoterápica para prevenir doenças...</p>
                            </div>
                            <a class="btn-entrectt" href="saw-palmetto-licopeno-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/selenio-boro-60-doses.png" alt="selenio-boro-60-doses" title="selenio-boro-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Selênio + Boro 60 Doses</h3>
                                <p>O Selênio é um mineral essencial que o organismo...</p>
                            </div>
                            <a class="btn-entrectt" href="selenio-boro-60-doses.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/exsynutriment-150mg-bio-arct-150mg-30-capsulas.png" alt="exsynutriment-150mg-bio-arct-150mg-30-capsulas" title="exsynutriment-150mg-bio-arct-150mg-30-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Exsynutriment 150mg + Bio Arct 150mg 30 Cápsulas</h3>
                                <p>A associação de Exsynutriment e Bio-Arct...</p>
                            </div>
                            <a class="btn-entrectt" href="exsynutriment-150mg-bio-arct-150mg-30-capsulas">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/nutricolin-300mg-30-capsulas.png" alt="nutricolin-300mg-30-capsulas" title="nutricolin-300mg-30-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Nutricolin 300Mg 30 Cápsulas</h3>
                                <p>O silício inteligente fundamental para manter...</p>
                            </div>
                            <a class="btn-entrectt" href="nutricolin-300mg-30-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/oli-ola-300mg-peeling-30-capsulas.png" alt="oli-ola-300mg-peeling-30-capsulas" title="oli-ola-300mg-peeling-30-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Oli Ola 300Mg Peeling 30 Cápsulas</h3>
                                <p>Ao promover uma ação de “peeling em cápsulas”...</p>
                            </div>
                            <a class="btn-entrectt" href="oli-ola-300mg-peeling-30-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/anti-aging-p-aumento-da-resistencia-da-pele -60g.png" alt="anti-aging-p-aumento-da-resistencia-da-pele -60g" title="anti-aging-p-aumento-da-resistencia-da-pele -60g" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Anti-Aging p/ Aumento Da Resistência Da Pele - 60g</h3>
                                <p>Lipossomas de Coenzima Q-10 é uma solução aquosa concentrada...</p>
                            </div>
                            <a class="btn-entrectt" href="anti-aging-p-aumento-da-resistencia-da-pele -60g.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/adipofill-5-30g-elimina-rugas-e-o-bigode-chines.png" alt="adipofill-5-30g-elimina-rugas-e-o-bigode-chines" title="adipofill-5-30g-elimina-rugas-e-o-bigode-chines" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Adipofill 5% 30g - Elimina Rugas E O Bigode Chinês</h3>
                                <p>O Adipofill™ Special Fórmulas foi desenvolvido...</p>
                            </div>
                            <a class="btn-entrectt" href="adipofill-5-30g-elimina-rugas-e-o-bigode-chines">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/lisina-prolina-60-capsulas.png" alt="lisina-prolina-60-capsulas" title="lisina-prolina-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Lisina + Prolina - 60 Cápsulas</h3>
                                <p>Lisina é um aminoácido essencial e indispensável...</p>
                            </div>
                            <a class="btn-entrectt" href="lisina-prolina-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/pueraria-mirifica-500mg-60-capsulas.png" alt="pueraria-mirifica-500mg-60-capsulas" title="pueraria-mirifica-500mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Pueraria Mirífica 500mg 60 Cápsulas</h3>
                                <p>A Pueraria Mirífica 500mg 60 cápsulas Special Fórmulas...</p>
                            </div>
                            <a class="btn-entrectt" href="pueraria-mirifica-500mg-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/sucupira-400mg-60-capsulas.png" alt="sucupira-400mg-60-capsulas" title="sucupira-400mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Sucupira 400Mg 60 Cápsulas</h3>
                                <p>Combate a artrite, artrose, reumatismo e gota...</p>
                            </div>
                            <a class="btn-entrectt" href="sucupira-400mg-60-capsulas.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/resveratrol-30mg-60-capsulas.png" alt="resveratrol-30mg-60-capsulas" title="resveratrol-30mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Resveratrol 30Mg 60 Cápsulas</h3>
                                <p>O Resveratrol, é um composto fenólico, do tipo estilbeno...</p>
                            </div>
                            <a class="btn-entrectt" href="resveratrol-30mg-60-capsulas">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/glycoxil-200mg-30-capsulas.png" alt="glycoxil-200mg-30-capsulas" title="glycoxil-200mg-30-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Glycoxil 200mg 30 Cápsulas</h3>
                                <p>Glycoxil é o resultado de uma modificação molecular...</p>
                            </div>
                            <a class="btn-entrectt" href="glycoxil-200mg-30-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/booster-de-colageno-tripla-acao-30-saches.png" alt="booster-de-colageno-tripla-acao-30-saches" title="booster-de-colageno-tripla-acao-30-saches" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Booster de Colágeno Tripla Ação 30 Sachês</h3>
                                <p>Três tipos de colágeno no mesmo produto! O Verisol...</p>
                            </div>
                            <a class="btn-entrectt" href="booster-de-colageno-tripla-acao-30-saches.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/catalase-5000-ui-60-capsulas.png" alt="catalase-5000-ui-60-capsulas" title="catalase-5000-ui-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Catalase 5000 UI 60 Cápsulas</h3>
                                <p>A Catalase é uma enzima produzida naturalmente...</p>
                            </div>
                            <a class="btn-entrectt" href="catalase-5000-ui-60-capsulas.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/pomada-de-mamacadela-para-vitilig-20-60g.png" alt="pomada-de-mamacadela-para-vitilig-20-60g" title="pomada-de-mamacadela-para-vitilig-20-60g" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Pomada De Mamacadela Para Vitiligo 20% 60g</h3>
                                <p>O Resveratrol, é um composto fenólico, do tipo estilbeno...</p>
                            </div>
                            <a class="btn-entrectt" href="pomada-de-mamacadela-para-vitilig-20-60g">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/vitis-vinifera-(semente-de-uva)-150mg-60-capsulas.png" alt="vitis-vinifera-(semente-de-uva)-150mg-60-capsulas" title="vitis-vinifera-(semente-de-uva)-150mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Vitis Vinifera (Semente De Uva) 150mg 60 Cápsulas</h3>
                                <p>Antioxidante, anti-inflamatória, antimicrobiana...</p>
                            </div>
                            <a class="btn-entrectt" href="glycoxil-200mg-30-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/glycoxil-200mg-30-capsulas.png" alt="glycoxil-200mg-30-capsulas" title="glycoxil-200mg-30-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Glycoxil 200mg 30 Cápsulas</h3>
                                <p>Glycoxil é o resultado de uma modificação molecular...</p>
                            </div>
                            <a class="btn-entrectt" href="glycoxil-200mg-30-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/raspberry-ketone-100mg-60-capsulas.png" alt="raspberry-ketone-100mg-60-capsulas" title="raspberry-ketone-100mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Raspberry Ketone 100Mg 60 Cápsulas</h3>
                                <p>Beta-caroteno melhor conhecido como um precursor...</p>
                            </div>
                            <a class="btn-entrectt" href="raspberry-ketone-100mg-60-capsulas.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/uc2-pqq-30-doses.png" alt="uc2-pqq-30-doses" title="uc2-pqq-30-doses" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Uc2 + Pqq 30 Doses</h3>
                                <p>Beta-caroteno melhor conhecido como um precursor da vitamina A...</p>
                            </div>
                            <a class="btn-entrectt" href="uc2-pqq-30-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/creme-para-pele-madura-40.png" alt="creme-para-pele-madura-40" title="creme-para-pele-madura-40" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Creme Para Pele Madura 40+ 30 Gramas</h3>
                                <p>Beta-caroteno melhor conhecido como um precursor da vitamina A...</p>
                            </div>
                            <a class="btn-entrectt" href="creme-para-pele-madura-40.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/exsynutriment-com-in-cell-30-capsulas.png" alt="exsynutriment-com-in-cell-30-capsulas" title="exsynutriment-com-in-cell-30-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Exsynutriment Com In Cell 30 Cápsulas</h3>
                                <p>In Cell é um ingrediente funcional preparado...</p>
                            </div>
                            <a class="btn-entrectt" href="exsynutriment-com-in-cell-30-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/nutricolin-300mg-60-capsulas.png" alt="nutricolin-300mg-60-capsulas" title="nutricolin-300mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Nutricolin 300Mg 60 Cápsulas</h3>
                                <p>NUTRICOLIN, o silício inteligente fundamental...</p>
                            </div>
                            <a class="btn-entrectt" href="nutricolin-300mg-60-capsulas.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/nutricolin-300mg-90-capsulas.png" alt="nutricolin-300mg-90-capsulas" title="nutricolin-300mg-90-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Nutricolin 300Mg 90 Cápsulas</h3>
                                <p>NUTRICOLIN, o silício inteligente fundamental para manter...</p>
                            </div>
                            <a class="btn-entrectt" href="nutricolin-300mg-90-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/bioblanc-2-serum-com-oliva-francesa-para-clareamento-cutaneo-30g.png" alt="bioblanc-2-serum-com-oliva-francesa-para-clareamento-cutaneo-30g" title="bioblanc-2-serum-com-oliva-francesa-para-clareamento-cutaneo-30g" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>BioBlanc 2% Serum Com Oliva Francesa Para Clareamento Cutâneo</h3>
                                <p>As oliveiras cultivadas para a fabricação...</p>
                            </div>
                            <a class="btn-entrectt" href="bioblanc-2-serum-com-oliva-francesa-para-clareamento-cutaneo-30g.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/veneno-de-cobra-syn-ake-4-30g.png" alt="veneno-de-cobra-syn-ake-4-30g" title="veneno-de-cobra-syn-ake-4-30g" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Veneno De Cobra Syn-Ake 4% 30g</h3>
                                <p>O Veneno de cobra “SYN® - AKE” Special Fórmulas...</p>
                            </div>
                            <a class="btn-entrectt" href="veneno-de-cobra-syn-ake-4-30g.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/composto-anti-manchas-faciais-belo-rosto-30-capsulas.png" alt="composto-anti-manchas-faciais-belo-rosto-30-capsulas" title="composto-anti-manchas-faciais-belo-rosto-30-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Composto Anti-Manchas Faciais BELO ROSTO</h3>
                                <p>O Oli Ola, cujo nome científico é Olea Europaea Fruit Extract...</p>
                            </div>
                            <a class="btn-entrectt" href="composto-anti-manchas-faciais-belo-rosto-30-capsulas.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/composto-turbinado-para-cabelos-pele-e-unhas-30-capsulas.png" alt="composto-turbinado-para-cabelos-pele-e-unhas-30-capsulas" title="composto-turbinado-para-cabelos-pele-e-unhas-30-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Composto Turbinado Para Cabelos, Pele E Unhas 30 Cápsulas</h3>
                                <p>Nutricolin é o silício inteligente fundamental...</p>
                            </div>
                            <a class="btn-entrectt" href="composto-turbinado-para-cabelos-pele-e-unhas-30-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/l-carnosina-500mg-60-doses.png" alt="l-carnosina-500mg-60-doses" title="l-carnosina-500mg-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>L-Carnosina 500Mg 60 Doses</h3>
                                <p>A l-carnosina é um pequeno peptídeo que contém dois aminoácidos...</p>
                            </div>
                            <a class="btn-entrectt" href="l-carnosina-500mg-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/goji-berry-500mg-concentrado-60-capsulas.png" alt="goji-berry-500mg-concentrado-60-capsulas" title="goji-berry-500mg-concentrado-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Goji Berry 500Mg Concentrado 60 Cápsulas</h3>
                                <p>Goji Berry 500mg da Special Fórmulas, foi desenvolvida...</p>
                            </div>
                            <a class="btn-entrectt" href="goji-berry-500mg-concentrado-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/lipowheat-70-15ml.png" alt="lipowheat-70-15ml" title="lipowheat-70-15ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Lipowheat 70% 15ML</h3>
                                <p>Beta-caroteno melhor conhecido como um precursor...</p>
                            </div>
                            <a class="btn-entrectt" href="lipowheat-70-15ml.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/gel-facial-luminecense-c-vit-c-biodisponivel-30g.png" alt="gel-facial-luminecense-c-vit-c-biodisponivel-30g" title="gel-facial-luminecense-c-vit-c-biodisponivel-30g" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Gel Facial LumineCense C/ Vit C Biodisponível - 30g</h3>
                                <p>LumineCense é uma molécula desenvolvida no Japão única...</p>
                            </div>
                            <a class="btn-entrectt" href="gel-facial-luminecense-c-vit-c-biodisponivel-30g.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/serum-efeito-lifiting-retrator-de-poros-30ml.png" alt="serum-efeito-lifiting-retrator-de-poros-30ml" title="serum-efeito-lifiting-retrator-de-poros-30ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Sérum Efeito Lifiting Retrator De Poros 30ML</h3>
                                <p>O Sérum Efeito Lifting Retrator De Poros Oficialfarma...</p>
                            </div>
                            <a class="btn-entrectt" href="serum-efeito-lifiting-retrator-de-poros-30ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/cartidyss-300mg-vit-c-120mg-30-capsulas.png" alt="cartidyss-300mg-vit-c-120mg-30-capsulas" title="cartidyss-300mg-vit-c-120mg-30-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Cartidyss 300mg + Vit C 120mg 30 Cápsulas</h3>
                                <p>Beta-caroteno melhor conhecido como um precursor da vitamina A...</p>
                            </div>
                            <a class="btn-entrectt" href="cartidyss-300mg-vit-c-120mg-30-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/exsynutriment-com-vit-c-30-capsulas.png" alt="exsynutriment-com-vit-c-30-capsulas" title="exsynutriment-com-vit-c-30-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Exsynutriment Com Vit C 30 Cápsulas</h3>
                                <p>É uma associação do silício orgânico de Exsynutriment...</p>
                            </div>
                            <a class="btn-entrectt" href="exsynutriment-com-vit-c-30-capsulas.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/bio-arct-100mg-30-capsulas.png" alt="bio-arct-100mg-30-capsulas" title="bio-arct-100mg-30-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Bio-Arct 100Mg 30 Cápsulas</h3>
                                <p>Bio Arct é uma biomassa marinha, proveniente de algas vermelhas...</p>
                            </div>
                            <a class="btn-entrectt" href="bio-arct-100mg-30-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/oli-ola-nutricolin-e-vitamina-c-30-capsulas.png" alt="oli-ola-nutricolin-e-vitamina-c-30-capsulas" title="oli-ola-nutricolin-e-vitamina-c-30-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Oli Ola, Nutricolin E Vitamina C 30 Cápsulas</h3>
                                <p>Beta-caroteno melhor conhecido como um precursor da vitamina A...</p>
                            </div>
                            <a class="btn-entrectt" href="oli-ola-nutricolin-e-vitamina-c-30-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/bronze-oficial-30-doses.png" alt="bronze-oficial-30-doses" title="bronze-oficial-30-doses" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Bronze Oficial 30 Doses</h3>
                                <p>Quando começa a esquentar as pessoas querem ostentar um corpo bronzeado...</p>
                            </div>
                            <a class="btn-entrectt" href="bronze-oficial-30-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/padinactive-nutri-100mg-60-capsulas.png" alt="padinactive-nutri-100mg-60-capsulas" title="padinactive-nutri-100mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Padinactive Nutri 100mg 60 Cápsulas</h3>
                                <p>PADINACTIVE® NUTRI traz um novo conceito de tratamento anti-rugas...</p>
                            </div>
                            <a class="btn-entrectt" href="padinactive-nutri-100mg-60-capsulas.php">Saiba +</a>
                        </div>
                    </div>
                     <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/creme-uso-diurno-(kelly-key)30ml.png" alt="creme-uso-diurno-(kelly-key)30ml" title="creme-uso-diurno-(kelly-key)30ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Creme Uso Diurno (Kelly Key) 30Ml</h3>
                                <p>Beta-caroteno melhor conhecido como um precursor da vitamina A...</p>
                            </div>
                            <a class="btn-entrectt" href="creme-uso-diurno-(kelly-key)30ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/locao-para-o-corpo-(kelly-key)-200ml.png" alt="locao-para-o-corpo-(kelly-key)-200ml" title="locao-para-o-corpo-(kelly-key)-200ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Loção Para O Corpo (Kelly Key) 200Ml</h3>
                                <p>Desenvolvida com ativos e nutrientes que garantem...</p>
                            </div>
                            <a class="btn-entrectt" href="locao-para-o-corpo-(kelly-key)-200ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/oxxynea-500mg-30-capsulas.png" alt="oxxynea-500mg-30-capsulas" title="oxxynea-500mg-30-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Oxxynea 500Mg 30 Cápsulas</h3>
                                <p>Obtido através de 22 frutas e verduras, o Oxxynea 500mg...</p>
                            </div>
                            <a class="btn-entrectt" href="oxxynea-500mg-30-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/superox-c-5-(kakadu-plum)-creme-facial-30g.png" alt="superox-c-5-(kakadu-plum)-creme-facial-30g" title="superox-c-5-(kakadu-plum)-creme-facial-30g" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Superox-C 5% (Kakadu Plum) Creme Facial 30G</h3>
                                <p>O Creme Superox – C Oficialfarma tem propriedades exclusivas...</p>
                            </div>
                            <a class="btn-entrectt" href="superox-c-5-(kakadu-plum)-creme-facial-30g.php">Saiba +</a>
                        </div>
                    </div>
                     <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/creme-uso-noturno-(kelly-key)-30-gramas.png" alt="creme-uso-noturno-(kelly-key)-30-gramas" title="creme-uso-noturno-(kelly-key)-30-gramas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Creme Uso Noturno (Kelly Key) 30 gramas</h3>
                                <p>É um ótimo aliado para eliminar aquelas manchinhas...</p>
                            </div>
                            <a class="btn-entrectt" href="creme-uso-noturno-(kelly-key)-30-gramas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/minoxidil-5-em-trichosol-cabelos-e-barba-(sem-alcool)-60ml.png" alt="minoxidil-5-em-trichosol-cabelos-e-barba-(sem-alcool)-60ml" title="minoxidil-5-em-trichosol-cabelos-e-barba-(sem-alcool)-60ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Minoxidil 5% Em Trichosol Cabelos E Barba (Sem álcool) 60ML</h3>
                                <p>O Minoxidil 5% em TrichoSol Cabelo e Barba...</p>
                            </div>
                            <a class="btn-entrectt" href="minoxidil-5-em-trichosol-cabelos-e-barba-(sem-alcool)-60ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/minoxidil-5-em-creme-para-barba-120g.png" alt="minoxidil-5-em-creme-para-barba-120g" title="minoxidil-5-em-creme-para-barba-120g" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Minoxidil 5% Em Creme Para Barba 120G</h3>
                                <p>Beta-caroteno melhor conhecido como um precursor da vitamina A...</p>
                            </div>
                            <a class="btn-entrectt" href="minoxidil-5-em-creme-para-barba-120g.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/minoxidil-com-fator-de-crescimento-60g-p-barba.png" alt="minoxidil-com-fator-de-crescimento-60g-p-barba" title="minoxidil-com-fator-de-crescimento-60g-p-barba" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Minoxidil Com Fator De Crescimento 60G P/ Barba</h3>
                                <p>O Minoxidil 5% Com Fatores de Crescimento é uma fórmula...</p>
                            </div>
                            <a class="btn-entrectt" href="minoxidil-com-fator-de-crescimento-60g-p-barba.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/shampoo-regenerador-200ml.png" alt="shampoo-regenerador-200ml" title="shampoo-regenerador-200ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Shampoo Regenerador 200Ml</h3>
                                <p>O shampoo regenerador 200ML foi desenvolvido para uma aplicação...</p>
                            </div>
                            <a class="btn-entrectt" href="shampoo-regenerador-200ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/minoxidil-5-com-fator-de-crescimento-p-barba-120g.png" alt="minoxidil-5-com-fator-de-crescimento-p-barba-120g" title="minoxidil-5-com-fator-de-crescimento-p-barba-120g" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Minoxidil 5% Com Fator De Crescimento P/ Barba 120G</h3>
                                <p>A melatonina é um hormônio ligado ao ciclo circadiano...</p>
                            </div>
                            <a class="btn-entrectt" href="minoxidil-5-com-fator-de-crescimento-p-barba-120g.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/locao-de-fatores-de-crescimento-capilar-barba-60ml.png" alt="locao-de-fatores-de-crescimento-capilar-barba-60ml" title="locao-de-fatores-de-crescimento-capilar-barba-60ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Loção De Fatores De Crescimento Capilar / Barba 60Ml</h3>
                                <p>A Loção de fatores de crescimento capilar/barba...</p>
                            </div>
                            <a class="btn-entrectt" href="locao-de-fatores-de-crescimento-capilar-barba-60ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/queratina-100mg-90-capsulas.png" alt="queratina-100mg-90-capsulasa" title="queratina-100mg-90-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Queratina 100mg 90 Cápsulas</h3>
                                <p>A Queratina, da Special Fórmulas, foi desenvolvida...</p>
                            </div>
                            <a class="btn-entrectt" href="queratina-100mg-90-capsulas.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/condicionador-masculino-barba-e-cabelo-60ml.png" alt="condicionador-masculino-barba-e-cabelo-60ml" title="condicionador-masculino-barba-e-cabelo-60ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Condicionador Masculino Barba E Cabelo - 60ml</h3>
                                <p>O Condicionador Masculino Barba, e Cabelo da Special Fórmulas...</p>
                            </div>
                            <a class="btn-entrectt" href="condicionador-masculino-barba-e-cabelo-60ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/shampoo-p-barba-e-cabelo-120ml.png" alt="shampoo-p-barba-e-cabelo-120ml" title="shampoo-p-barba-e-cabelo-120ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Shampoo P/ Barba E Cabelo 120ml</h3>
                                <p>O shampoo para barba e cabelo da Special Fórmulas...</p>
                            </div>
                            <a class="btn-entrectt" href="shampoo-p-barba-e-cabelo-120ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/minoxidil-turbinado-120ml.png" alt="minoxidil-turbinado-120ml" title="minoxidil-turbinado-120ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Minoxidil Turbinado 120Ml</h3>
                                <p>O Minoxidil Turbinado é uma fórmula desenvolvida pela Special Fórmulas...</p>
                            </div>
                            <a class="btn-entrectt" href="minoxidil-turbinado-120ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/pill-food-turbinado-120-capsulas.png" alt="pill-food-turbinado-120-capsulas" title="pill-food-turbinado-120-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Pill Food Turbinado 120 Cápsulas</h3>
                                <p>Pill Food Turbinado é um complexo alimentar formado...</p>
                            </div>
                            <a class="btn-entrectt" href="pill-food-turbinado-120-capsulas.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/minoxidilcom-propilenoglicol-5-120ml-locao-capilar.png" alt="minoxidilcom-propilenoglicol-5-120ml-locao-capilar" title="minoxidilcom-propilenoglicol-5-120ml-locao-capilarl" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>MinoxidilCom Propilenoglicol 5% 120Ml - Loção Capilar</h3>
                                <p>O Minoxidil 5% Com Propilenoglicol é uma fórmula...</p>
                            </div>
                            <a class="btn-entrectt" href="minoxidilcom-propilenoglicol-5-120ml-locao-capilar.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/minoxidil-5-com-biotina-120ml.png" alt="minoxidil-5-com-biotina-120ml" title="minoxidil-5-com-biotina-120ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Minoxidil 5% Com Biotina 120Ml</h3>
                                <p>O Minoxidil 5% Com Biotina é uma fórmula desenvolvida...</p>
                            </div>
                            <a class="btn-entrectt" href="minoxidil-5-com-biotina-120ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/pqq-pirroloquinolina-quinona-10mg-30-capsulas.png" alt="pqq-pirroloquinolina-quinona-10mg-30-capsulas" title="pqq-pirroloquinolina-quinona-10mg-30-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Pqq - Pirroloquinolina Quinona 10Mg 30 Cápsulas</h3>
                                <p>A Pirroloquinolina quinona (PQQ) é uma quinona...</p>
                            </div>
                            <a class="btn-entrectt" href="pqq-pirroloquinolina-quinona-10mg-30-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/biotina-5mg-60-capsulas.png" alt="biotina-5mg-60-capsulas" title="biotina-5mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Biotina 5Mg 60 Cápsulas</h3>
                                <p>A Biotina é uma vitamina hidrossolúvel do complexo B...</p>
                            </div>
                            <a class="btn-entrectt" href="biotina-5mg-60-capsulas.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/condicionador-masculino-barba-e-cabelo-60ml.png" alt="condicionador-masculino-barba-e-cabelo-60ml" title="condicionador-masculino-barba-e-cabelo-60ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Condicionador Masculino Barba E Cabelo - 60ml</h3>
                                <p>O Condicionador Masculino Barba, e Cabelo da Special Fórmulas...</p>
                            </div>
                            <a class="btn-entrectt" href="condicionador-masculino-barba-e-cabelo-60ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/shampoo-p-barba-e-cabelo-120ml.png" alt="shampoo-p-barba-e-cabelo-120ml" title="shampoo-p-barba-e-cabelo-120ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Shampoo P/ Barba E Cabelo 120ml</h3>
                                <p>O shampoo para barba e cabelo da Special Fórmulas...</p>
                            </div>
                            <a class="btn-entrectt" href="shampoo-p-barba-e-cabelo-120ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/minoxidil-turbinado-120ml.png" alt="minoxidil-turbinado-120ml" title="minoxidil-turbinado-120ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Minoxidil Turbinado 120Ml</h3>
                                <p>O Minoxidil Turbinado é uma fórmula desenvolvida pela Special Fórmulas...</p>
                            </div>
                            <a class="btn-entrectt" href="minoxidil-turbinado-120ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/pill-food-turbinado-120-capsulas.png" alt="pill-food-turbinado-120-capsulas" title="pill-food-turbinado-120-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Pill Food Turbinado 120 Cápsulas</h3>
                                <p>Pill Food Turbinado é um complexo alimentar formado...</p>
                            </div>
                            <a class="btn-entrectt" href="pill-food-turbinado-120-capsulas.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/cabelo-pele-e-unha-brave-60-doses-by-gabi-dezan.png" alt="cabelo-pele-e-unha-brave-60-doses-by-gabi-dezan" title="cabelo-pele-e-unha-brave-60-doses-by-gabi-dezan" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Cabelo, Pele e Unha Brave 60 Doses By Gabi Dezan+</h3>
                                <p>O composto constitui em um mix de vitaminas...</p>
                            </div>
                            <a class="btn-entrectt" href="cabelo-pele-e-unha-brave-60-doses-by-gabi-dezan.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/pantoficial-60-doses.png" alt="pantoficial-60-doses" title="pantoficial-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Betacaroteno 50Mg 30 Cápsulas</h3>
                                <p>O Pantoficial é um suplemento alimentar que serve...</p>
                            </div>
                            <a class="btn-entrectt" href="pantoficial-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/minoxidil-tonico-capilar-men-50ml.png" alt="minoxidil-tonico-capilar-men-50ml" title="minoxidil-tonico-capilar-men-50ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Minoxidil Tônico Capilar Men 50Ml</h3>
                                <p>Beta-caroteno melhor conhecido como um precursor...</p>
                            </div>
                            <a class="btn-entrectt" href="minoxidil-tonico-capilar-men-50ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/shampoo-para-dermatite-seborreica-110ml.png" alt="shampoo-para-dermatite-seborreica-110ml" title="shampoo-para-dermatite-seborreica-110ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Shampoo Para Dermatite Seborreica 110ML</h3>
                                <p>A caspa é uma doença crônica, que afeta muitas pessoas...</p>
                            </div>
                            <a class="btn-entrectt" href="biotina-5mg-60-capsulas.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/pantoficial-men-60-capsulas.png" alt="pantoficial-men-60-capsulas" title="pantoficial-men-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Pantoficial Men 60 Cápsulas</h3>
                                <p>Pantoficial Men é um suplemento de vitaminas e minerais...</p>
                            </div>
                            <a class="btn-entrectt" href="pantoficial-men-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/pueraria-mirifica-500mg-60-capsulas.png" alt="pueraria-mirifica-500mg-60-capsulas" title="pueraria-mirifica-500mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Pueraria Mirífica 500mg 60 Cápsulas</h3>
                                <p>A Pueraria Mirífica 500mg 60 cápsulas Special Fórmulas...</p>
                            </div>
                            <a class="btn-entrectt" href="pueraria-mirifica-500mg-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/oleo-de-coco-1000mg-120-capsulas.png" alt="oleo-de-coco-1000mg-120-capsulas" title="oleo-de-coco-1000mg-120-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Óleo De Coco 1000mg 120 Cápsulas</h3>
                                <p>O Óleo de Coco é rico em Ácidos Graxos de Cadeia Média...</p>
                            </div>
                            <a class="btn-entrectt" href="oleo-de-coco-1000mg-120-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/shampoo-com-carvao-ativado-200ml.png" alt="shampoo-com-carvao-ativado-200ml" title="shampoo-com-carvao-ativado-200ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Shampoo com Carvão Ativado 200ml</h3>
                                <p>O Shampoo com Carvão Ativado 200ml Special Fórmulas...</p>
                            </div>
                            <a class="btn-entrectt" href="shampoo-com-carvao-ativado-200ml.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/trichoxidill-tonico-capilar-femme-50ml.png" alt="trichoxidill-tonico-capilar-femme-50mls" title="trichoxidill-tonico-capilar-femme-50ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Trichoxidill Tônico Capilar Femme 50Ml</h3>
                                <p>Tônico Capilar Femme Special Fórmulas é uma fórmula 100% natural...</p>
                            </div>
                            <a class="btn-entrectt" href="trichoxidill-tonico-capilar-femme-50ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/suplemento-para-cabelo-femme-60-doses.png" alt="suplemento-para-cabelo-femme-60-doses" title="suplemento-para-cabelo-femme-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Suplemento para Cabelo Femme 60 Doses SENKEDA</h3>
                                <p>Complexo formado por vitaminas, minerais e outros ativos...</p>
                            </div>
                            <a class="btn-entrectt" href="suplemento-para-cabelo-femme-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/shampoo-anticaspa-com-cetoconazol-2.png" alt="shampoo-anticaspa-com-cetoconazol-2" title="shampoo-anticaspa-com-cetoconazol-2" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Shampoo Anticaspa Com Cetoconazol 2% 110ML</h3>
                                <p>O Shampoo Anticaspa Com Cetoconazol é indicado...</p>
                            </div>
                            <a class="btn-entrectt" href="shampoo-anticaspa-com-cetoconazol-2.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/shampoo-anticaspa-com-octopirox-e-oleo-de-melaleuca-200ml.png" alt="shampoo-anticaspa-com-octopirox-e-oleo-de-melaleuca-200ml" title="shampoo-anticaspa-com-octopirox-e-oleo-de-melaleuca-200ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Shampoo Anticaspa Com Octopirox E Óleo De Melaleuca 200ml</h3>
                                <p>A caspa acomete cerca de 40% da população...</p>
                            </div>
                            <a class="btn-entrectt" href="shampoo-anticaspa-com-octopirox-e-oleo-de-melaleuca-200ml.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/locao-de-fatores-de-crescimento-capilar-barba-60ml.png" alt="locao-de-fatores-de-crescimento-capilar-barba-60ml" title="locao-de-fatores-de-crescimento-capilar-barba-60ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Loção De Fatores De Crescimento Capilar / Barba 60Ml</h3>
                                <p>A Loção de fatores de crescimento capilar/barba...</p>
                            </div>
                            <a class="btn-entrectt" href="locao-de-fatores-de-crescimento-capilar-barba-60ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/anti-queda-capilar-novos-fios-120ml.png" alt="anti-queda-capilar-novos-fios-120ml" title="anti-queda-capilar-novos-fios-120ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Anti-Queda Capilar + Novos Fios 120Ml</h3>
                                <p>O Sérum Anti-queda + Novos fios, da Special Fórmulas...</p>
                            </div>
                            <a class="btn-entrectt" href="anti-queda-capilar-novos-fios-120ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/latanoprosta-com-minoxidil-100ml.png" alt="latanoprosta-com-minoxidil-100ml" title="latanoprosta-com-minoxidil-100ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>latanoprosta-com-minoxidil-100ml</h3>
                                <p>Latanoprosta Fagron é um análogo direto da prostaglandina F2-alfa...</p>
                            </div>
                            <a class="btn-entrectt" href="latanoprosta-com-minoxidil-100ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/condicionador-carvao-ativado.png" alt="condicionador-carvao-ativadol" title="condicionador-carvao-ativado" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Condicionador carvão Ativado</h3>
                                <p>Beta-caroteno melhor conhecido como um precursor da vitamina A...</p>
                            </div>
                            <a class="btn-entrectt" href="condicionador-carvao-ativado.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/tonico-capilar-alfaestradiol-(alfa17)-100ml.png" alt="tonico-capilar-alfaestradiol-(alfa17)-100ml" title="tonico-capilar-alfaestradiol-(alfa17)-100ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Tônico Capilar Alfaestradiol (Alfa17) 100ml</h3>
                                <p>Indicado para o tratamento e prevenção da alopecia....</p>
                            </div>
                            <a class="btn-entrectt" href="tonico-capilar-alfaestradiol-(alfa17)-100ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/shampoo-greasy-block-femina-200ml.png" alt="shampoo-greasy-block-femina-200ml" title="shampoo-greasy-block-femina-200ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>SHAMPOO GREASY BLOCK FEMINA 200ML</h3>
                                <p>Beta-caroteno melhor conhecido como um precursor da vitamina A...</p>
                            </div>
                            <a class="btn-entrectt" href="shampoo-greasy-block-femina-200ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/shampoo-p-barba-e-cabelo-120ml.png" alt="shampoo-p-barba-e-cabelo-120ml" title="shampoo-p-barba-e-cabelo-120ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Shampoo P/ Barba E Cabelo 120ml</h3>
                                <p>O shampoo para barba e cabelo da Special Fórmulas...</p>
                            </div>
                            <a class="btn-entrectt" href="shampoo-p-barba-e-cabelo-120ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/latanoprosta-espuma-capilar-100ml.png" alt="latanoprosta-espuma-capilar-100ml" title="latanoprosta-espuma-capilar-100ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Latanoprosta Espuma Capilar 100mL</h3>
                                <p>Visando garantir a qualidade dos produtos que necessitam de refrigeração, faremos envios exclusivamente de segunda-feira a quarta-feira....</p>
                            </div>
                            <a class="btn-entrectt" href="latanoprosta-espuma-capilar-100ml.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/oleo-de-abacate-1000mg-60-capsulas.png" alt="oleo-de-abacate-1000mg-60-capsulas" title="oleo-de-abacate-1000mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Óleo de Abacate 1000mg 60 Cápsulas</h3>
                                <p>O Abacate (Persea americana) é uma fruta de origem americana...</p>
                            </div>
                            <a class="btn-entrectt" href="oleo-de-abacate-1000mg-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/gelatina-500mg-60-capsulas.png" alt="gelatina-500mg-60-capsulas" title="gelatina-500mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Gelatina 500Mg 60 Cápsulas</h3>
                                <p>A gelatina nada mais é do que uma substância naturalmente...</p>
                            </div>
                            <a class="btn-entrectt" href="gelatina-500mg-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/composto-turbinado-para-cabelos-pele-e-unhas-30-capsulas.png" alt="composto-turbinado-para-cabelos-pele-e-unhas-30-capsulas" title="composto-turbinado-para-cabelos-pele-e-unhas-30-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Composto Turbinado Para Cabelos, Pele E Unhas 30 Cápsulas</h3>
                                <p>Nutricolin é o silício inteligente fundamental...</p>
                            </div>
                            <a class="btn-entrectt" href="composto-turbinado-para-cabelos-pele-e-unhas-30-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/rutina-150mg-60-capsulas-nupelli.png" alt="rutina-150mg-60-capsulas-nupelli" title="rutina-150mg-60-capsulas-nupelli" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Rutina 150mg 60 Cápsulas</h3>
                                <p>Rutina 150mg da Special Fórmulas, foi desenvolvida...</p>
                            </div>
                            <a class="btn-entrectt" href="rutina-150mg-60-capsulas-nupelli.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/leave-in-femme-60ml-fortalecedor-de-fios.png" alt="leave-in-femme-60ml-fortalecedor-de-fios" title="leave-in-femme-60ml-fortalecedor-de-fios" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Leave in Femme 60Ml - Fortalecedor de Fios SENKEDA</h3>
                                <p>O Senkeda Leave-in Femme Oficialfarma é um sérum...</p>
                            </div>
                            <a class="btn-entrectt" href="leave-in-femme-60ml-fortalecedor-de-fios.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/trichoxidil-25-locao-capilar-60ml.png" alt="trichoxidil-25-locao-capilar-60ml" title="trichoxidil-25-locao-capilar-60ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>TrichoXidil 2,5% Loção Capilar 60ml</h3>
                                <p>Indicado para combater a queda de cabelo, a Loção Capilar com TrichoXidil 2,5% da Special Formulas é uma fórmula 100% natural, livre de álcool...</p>
                            </div>
                            <a class="btn-entrectt" href="trichoxidil-25-locao-capilar-60ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/biosil-300mg-60 -capsulas.png" alt="biosil-300mg-60 -capsulas" title="biosil-300mg-60 -capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Biosil 300Mg 60 Cápsulas</h3>
                                <p>BioSil™ é a marca comercial e registrada da patente da molécula ch-OSA...</p>
                            </div>
                            <a class="btn-entrectt" href="biosil-300mg-60 -capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/serum-intensificador-de-brilho.png" alt="serum-intensificador-de-brilho" title="serum-intensificador-de-brilho" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Serum Intensificador De Brilho</h3>
                                <p>O D-pantenol é uma espécie de álcool hidrossolúvel e rico...</p>
                            </div>
                            <a class="btn-entrectt" href="serum-intensificador-de-brilho.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/keranat-150mg-por-2.5ml-150ml.png" alt="keranat-150mg-por-2.5ml-150ml" title="keranat-150mg-por-2.5ml-150ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Keranat 150mg Por 2,5ml 150ML</h3>
                                <p>O segredo contra a queda de cabelo e beleza dos fios...</p>
                            </div>
                            <a class="btn-entrectt" href="keranat-150mg-por-2.5ml-150ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/biosil-300mg-30-capsulas.png" alt="biosil-300mg-30-capsulas" title="biosil-300mg-30-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Biosil 300Mg 30 Cápsulas</h3>
                                <p>BioSil™ é a marca comercial e registrada da patente...</p>
                            </div>
                            <a class="btn-entrectt" href="biosil-300mg-30-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/exsynutriment-com-vit-c-30-capsulas.png" alt="exsynutriment-com-vit-c-30-capsulas" title="exsynutriment-com-vit-c-30-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Exsynutriment Com Vit C 30 Cápsulas</h3>
                                <p>É uma associação do silício orgânico de Exsynutriment...</p>
                            </div>
                            <a class="btn-entrectt" href="exsynutriment-com-vit-c-30-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/manteiga-de-karite-30g.png" alt="manteiga-de-karite-30g" title="manteiga-de-karite-30g" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Manteiga de Karité 30g</h3>
                                <p>Indicado pra hidratar pele e cabelos, a Manteiga...</p>
                            </div>
                            <a class="btn-entrectt" href="manteiga-de-karite-30g.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/padinactive-nutri-100mg-60-capsulas.png" alt="padinactive-nutri-100mg-60-capsulas" title="padinactive-nutri-100mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Padinactive Nutri 100mg 60 Cápsulas</h3>
                                <p>PADINACTIVE® NUTRI traz um novo conceito de tratamento anti-rugas...</p>
                            </div>
                            <a class="btn-entrectt" href="padinactive-nutri-100mg-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/shampoo-hidratante-com-babosa.png" alt="shampoo-hidratante-com-babosa" title="shampoo-hidratante-com-babosa" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Shampoo Hidratante com babosa</h3>
                                <p>O shampoo hidratante com Babosa 200ml Special Fórmulas...</p>
                            </div>
                            <a class="btn-entrectt" href="shampoo-hidratante-com-babosa.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/suplemento-para-cabelo-men-60-doses.png" alt="suplemento-para-cabelo-men-60-doses" title="suplemento-para-cabelo-men-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Suplemento para Cabelo Men 60 Doses</h3>
                                <p>Complexo formado por vitaminas, minerais e outros ativos...</p>
                            </div>
                            <a class="btn-entrectt" href="suplemento-para-cabelo-men-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/haired-x-50mg.png" alt="haired-x-50-ml-eclaire" title="haired-x-50-ml-eclaire" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Haired-X 50 ml – Éclaire</h3>
                                <p>Haired-X Special Fórmulas é o primeiro tônico de crescimento capilar...</p>
                            </div>
                            <a class="btn-entrectt" href="haired-x-50-ml-eclaire.php">Saiba +</a>
                        </div>
                    </div>
            </section>
        </div>    
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>