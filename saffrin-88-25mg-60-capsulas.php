<?php

    $title       = "Saffrin 88,25mg 60 Cápsulas";
    $description = "Saffrin estimula a sensação de saciedade, inibe a recaptação de serotonina e controla a compulsão por doces de forma natural. Saffrin é o extrato verdadeiro..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Saffrin 88,25mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/saffrin-88-25mg-60-capsulas.png" alt="saffrin-88-25mg-60-capsulas" title="saffrin-88-25mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>SAFFRIN 88,25MG</h2>
                        <p class="text-justify">Saffrin estimula a sensação de saciedade, inibe a recaptação de serotonina e controla a compulsão por doces de forma natural. Saffrin é o extrato verdadeiro do açafrão, também conhecido como o “ouro vermelho”, extraí- do e obtido por secagem dos três estigmas vermelhos da flor de Crocus sativus L.</p>
                        <br>
                        <h2>EFICÁCIA DO SAFFRIN</h2>
                        <p class="text-justify">Estudo IN VIVO: Controle da saciedade Estudo realizado em 60 mulheres sobrepesadas no período de oito semanas, utilizando 88,25 mg, 2 vezes ao dia de Saffrin.</p>
                        <p class="text-justify">Resultado: Saffrin promoveu redução do peso, aumento da saciedade e diminuição da frequência no hábito de beliscar.</p>
                        <p class="text-justify">Equilíbrio emocional: Estudos demonstraram que Saffrin diminui o estresse, a ansiedade, os sintomas da depressão, contribuindo assim para o equilíbrio emocional (Ghadrdoost B. et al., 2011; Wang Y et al., 2010; Akhondzadeh et al, 2005; Akhondzadeh et al., 2004).</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. </p>
                <p class="advertencias text-justify">Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>