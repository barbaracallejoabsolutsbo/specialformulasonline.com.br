<?php

    $title       = "Rhodiola Rosea 300Mg 30 Cápsulas";
    $description = "A Rhodiola é uma planta perene de folhas suculentas, comum em regiões frias do mundo, como o Ártico da Sibéria Oriental, as Montanhas Rochosas da América..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Rhodiola Rosea 300Mg 30 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/rhodiol-rosea-300mg-30-capsulas.png" alt="rhodiol-rosea-300mg-30-capsulas" title="rhodiol-rosea-300mg-30-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">A Rhodiola é uma planta perene de folhas suculentas, comum em regiões frias do mundo, como o Ártico da Sibéria Oriental, as Montanhas Rochosas da América do Norte e as montanhas da Ásia Central e Europa. Não é à toa que esta planta da família Crassulaceae recebeu o nome de Raiz de Ouro: Há séculos suas raízes da têm sido usadas pelas culturas da Europa Oriental e da Ásia para melhorar a resistência física, a disposição, a longevidade e a resistência a doenças provocadas pelas altitudes. A característica comum de todos esses benefícios está na origem do seu benefício: o seu potencial adaptogênico. A Rodiola rósea possui compostos que são capazes de prevenir os efeitos físicos e químicos do cansaço e do estresse.</p>
                        <br>
                        <h2>ALIADA DO CORAÇÃO</h2>
                        <p class="text-justify">Pesquisas já demonstraram que o uso de Rhodiola rosea é eficaz para diminuir ou eliminar danos nos tecidos cardiovasculares derivados do estresse e de trauma. Sua ação está na sua capacidade para diminuir a quantidade de catecolaminas e corticosteroides liberados pelas glândulas suprarrenais durante o estresse. Ela também auxilia na captação de mais cálcio intracelular no coração promovendo assim um maior potencial para a contração muscular cardíaca, ajudando a regular os batimentos cardíacos e a neutralizar arritmias. Além disso, o extrato de Rhodiola rosea também é útil na prevenção do declínio da força de contração do coração, causada por estresse ecológico, como, por exemplo, em condições de muito frio e altitude.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>