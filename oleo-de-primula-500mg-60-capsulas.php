<?php

    $title       = "Óleo De Primula 500Mg 60 Cápsulas";
    $description = "O Óleo de Prímula é extraído das sementes da Evening Primrose (Oenothera biennis) sendo uma das fontes mais ricas de Ácido Gama Linolênico (GLA)..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Óleo De Primula 500Mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/oleo-de-primula-500mg-60-capsulas.png" alt="oleo-de-primula-500mg-60-capsulas" title="oleo-de-primula-500mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>ÓLEO DE PRIMULA 500MG</h2>
                        <p class="text-justify">O Óleo de Prímula é extraído das sementes da Evening Primrose (Oenothera biennis) sendo uma das fontes mais ricas de Ácido Gama Linolênico (GLA), um ácido graxo essencial não produzido pelo nosso organismo. Esta planta era usada na América do Norte como alimento e da raiz, folhas, flores e caules os índios curandeiros faziam infusões e extratos emolientes, sedativa (tosse), estimulante da circulação sanguínea, nutriente capilar e para curar feridas. O Óleo de Prímula é amarelo pálido claro, com paladar e olfato típico. Refinado e desodorizado, extraído das sementes por prensagem a frio.</p>
                        <p class="text-justify">O Óleo de Prímula é usado como preventivo no endurecimento das artérias, síndrome pré-menstrual, esclerose múltipla e pressão alta. Pesquisas têm demonstrado que o Óleo de Prímula diminui os níveis de colesterol no sangue sendo importante também no tratamento da cirrose hepática. Tem atuação no alívio da dor e inflamação. Pesquisas anteriores demonstraram que o GLA é rapidamente convertido no organismo em Prostaglandina E, que tem ação antiinflamatória e imunossupressora. O Óleo de Prímula possui excepcionalmente alto índice de ácido linolênico (Ômega 6) e contém o tão importante ácido gama linolênico (GLA).</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado.</p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>