<?php

    $title       = "Biotina 5Mg 60 Cápsulas";
    $description = "A Biotina é uma vitamina hidrossolúvel do complexo B essencial para função normal das células, desempenhando um papel fundamental na manutenção..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Biotina 5Mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/biotina-5mg-60-capsulas.png" alt="biotina-5mg-60-capsulas" title="biotina-5mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>CABELO, PELE E UNHAS INCRÍVEIS</h2>
                        <p class="text-justify">A Biotina é uma vitamina hidrossolúvel do complexo B essencial para função normal das células, desempenhando um papel fundamental na manutenção da integridade da pele, unha e cabelos. É essencial para produção de glicogênio, ácido nucleico e aminoácidos, e tem participação na produção das células brancas do sangue e dos anticorpos. Aumenta o uso das vitaminas B12, B5 e do ácido fólico, e auxilia no fortalecimento da queda de cabelo e perda da pigmentação. Regula os níveis de gordura saturada (colesterol), diminui as dores musculares e câimbras, e é importante para tratamento de eczemas e dermatite.</p>
                        <br>
                        <h2>POR QUE TOMAR?</h2>
                        <p class="text-justify">Saúde ocular: Uma dieta com alto consumo de beta-caroteno, vitaminas C e E, e zinco é associada com um risco substancialmente reduzido de degeneração macular relacionada à idade em pessoas mais velhas. Degeneração macular relacionada à idade (AMD) é um transtorno degenerativo da macula, a parte central da retina, e é a causa mais comum de cegueira irreversível em países desenvolvidos, de acordo com as informações históricas no artigo.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Imagens meramente ilustrativas. Pessoas com hipersensibilidade à substância não devem ingerir o produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o medicamento se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Todo medicamento deve ser mantido fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado.</p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>