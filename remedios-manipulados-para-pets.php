<?php
    $title       = "Remedios manipulados para pets";
    $description = "Facilite as formas de administração de medicamentos para cachorros e gatos fabricando remédios manipulados para pets em forma de biscoitos e snacks.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Conheça nossos serviços para <strong>remédios manipulados para pets, </strong>exclusivos com a Special Fórmulas.Faça personalização de <strong>remédios manipulados para pets</strong> com doses precisas para seu bichinho. Feito na dose exata que seu pet precisa, são muito mais úteis e práticos na hora de medicar seu companheiro.</p>
<p>Com o preço muito mais em conta, podemos isolar componentes que podem ser alérgicos para seu bichinho e não colocar na fórmula, aqui você paga pelo que seu pet utiliza e não pelo conteúdo extra e o laboratório que o medicamento industrializado carrega. Assim também é possível associar e manipular melhor as substâncias de forma eficiente em um único medicamento para administração.</p>
<p>Facilite as formas de administração de medicamentos para cachorros e gatos fabricando <strong>remédios manipulados para pets</strong> em forma de biscoitos e snacks. Dessa forma é muito mais fácil evitar a rejeição dos medicamentos pelos seus bichinhos, com sabores e aromas que são muito mais agradáveis e apetitosos.</p>
<p>Encontre formas especiais para administrar os medicamentos para seus pets. Faça seus <strong>remédios manipulados para pets</strong> para consumo em pasta, líquidos, biscoitinhos e snacks, com toda qualidade e controle rigoroso para manipulação. Garanta o bem estar do seu animalzinho e adquira já os melhores manipulados para uso veterinário e também para humanos com a Special Fórmulas.</p>
<h2><strong>Eficiência e praticidade com remédios manipulados para pets</strong></h2>
<p>Sempre consulte o médico veterinário para suporte total para seus animaizinhos. Dessa forma é possível encontrar as melhores alternativas para receitas prescritas de <strong>remédios manipulados para pets,</strong> com todo suporte da Special Fórmulas para fabricação. Com prazos incríveis entregamos medicamentos e manipulados com pontualidade visando sempre a satisfação de todos os clientes. Compre conosco.</p>
<h2><strong>Mais informações sobre remédios manipulados para pets</strong></h2>
<p>Todas as espécies podem se beneficiar com a manipulação. Nossa farmácia possui uma equipe especializada de farmacêuticos treinados para manipulações específicas para cada prescrição de seu médico-veterinário. Respeitando critérios como idade, peso, hábitos alimentares, hábitos de rotina e espécie de cada animal, a eficácia dos <strong>remédios manipulados para pets</strong> é excelente, não só porque possui dosagens adequadas para cada pet, mas por conta da facilidade de administração, evitando a rejeição do medicamento por não serem palatáveis. Aqui fabricamos medicamentos para pets com formas de consumo que certamente seu companheiro irá adorar.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>