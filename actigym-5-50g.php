<?php

    $title       = "Actigym 5% 50G";
    $description = "O actigym foi obtido pela biotecnologia, através de microorganismo encontrados nas ilhas bermudas, que em alguns casos e deve ser apenas indicado..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Actigym 5% 50G</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/actigym-5-50g.png" alt="actigym-5-50g" title="actigym-5-50g">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>PARA MODELAR A SILHUETA</h2>
                        <p class="text-justify">O actigym foi obtido pela biotecnologia, através de microorganismo encontrados nas ilhas bermudas, que em alguns casos e deve ser apenas indicado por profissionais da área para ter efeitos potencializadores se associado ao morosil que combate a gordura abdominal. por agir pelo estímulo, quebra a gordura por assim dizer e ativa a microcirculação que é um dos processos fundamentais. a pele tem uma excelente absorção, não deixando oleosa ou ressecada demais que poderia acabar sendo prejudicial. como diz o título passa a ter uma linda silhueta de hoje em diante, podendo arrasar com aquelas roupas mais justinhas para as ocasiões especiais.</p>
                        <br>
                        <h2>REDUZ ABDÔMEN E COXAS</h2>
                        <p class="text-justify">Além disso, de acordo com os testes feitos notou-se uma perda 2,8cm do abdômen em apenas 28 dias e 2,1cm nas coxas em mais de 30 dias. Não terá de gastar valores exorbitantes para alcançar o corpo perfeito e em sua própria casa realizará os procedimentos. Sentir estes e outros efeitos positivos faz com que tenha o aumento da auto-estima, que antes talvez tenha sido afetada por não se sentir bem consigo mesmo e para que tenha dias mais animados.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado.</p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>