<?php

    $title       = "Empresa";
    $description = ""; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php"; 
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "empresa"
    ));

?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container text-justify">
            <div class="sobre-nos">
            <div class="container">
                <h2>QUEM SOMOS</h2>
                <hr>
                <h3>Conheça um pouco mais sobre nossa empresa</h3>
                <p>Localizada em São Bernardo do Campo ha mais de 15 anos no mercado, a SPECIAL FÓRMULAS é uma farmácia magistral que produz e comercializa produtos e serviços para a promoção da saúde e bem estar. São formulações alopáticas, homeopáticas, fitoterápicas, ortomoleculares, dermatológicas, odontológicas e nutracêuticas, florais, produtos naturais e cosméticos, veterinários, dentre outros.</p>


                <p>A orientação farmacêutica é um dos grandes diferenciais da SPECIAL FÓRMULAS, que presta atendimento personalizado aos nossos clientes. A empresa obedece rigorosamente às boas práticas de manipulação em farmácia, com o objetivo de assegurar a excelência e a eficácia dos medicamentos.</p>


                <p>Inovadora em seus serviços e produtos, realiza entrega em domicílio para toda grande São Paulo. Ao longo do tempo, a SPECIAL FÓRMULAS vem se aprimorando, investindo em capacitação da equipe e modernização da sua estrutura fisíca, sem perder o respeito ao semelhante e ao meio ambiente.</p>


                <p>Nossos VALORES e COMPROMISSOS foram construídos ao longo do tempo e constituem a cultura da empresa. Nós transmitimos internamente nos nossos princípios de gestão e para o exterior nas relações que mantemos com os nossos clientes.</p>
                <br>
                <br>
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="content">
                            <a href="<?php echo $url; ?>" target="_blank">
                                <div class="content-overlay"></div>
                                <h3 class="missao">Visão</h3>
                                <div class="content-details fadeIn-top">
                                    <p>Manter-se como referência de excelência na qualidade de produtos e serviços na área farmacêutica.</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="content">
                            <a href="<?php echo $url; ?>" target="_blank">
                                <div class="content-overlay"></div>
                                <h3 class="missao">Missão</h3>
                                <div class="content-details fadeIn-top">
                                    <p>Investir continua e sustentávelmente em inovação farmacêutica  e atendimento, buscando sempre a melhor solução de modo a proporcionar aos nossos clientes uma melhor qualidade de vida e bem estar.</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="content">
                            <a href="<?php echo $url; ?>" target="_blank">
                                <div class="content-overlay"></div>
                                <h3 class="missao">Política da Qualidade</h3>
                                <div class="content-details fadeIn-top">
                                    <p>Atender e satisfazer os nossos clientes, através da conformidade dos produtos e serviços, pontualidade de entrega e melhoria contínua dos processos do Sistema da Gestão da Qualidade, obedecendo as exigências da legislação pertinente e os requisitos pré-estabelecidos.</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="content">
                            <a href="<?php echo $url; ?>" target="_blank">
                                <div class="content-overlay"></div>
                                <h3 class="missao">Ética na Manipulação Farmacêutica</h3>
                                <div class="content-details fadeIn-top">
                                    <p>Reforçamos nosso compromisso, responsabilidade e respeito junto a nossos clientes manipulando fórmulas em conformidade com os mais rígidos critérios estabelecidos pelos órgãos competentes; reproduzindo fielmente a prescrição realizada pela classe médica.</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="content">
                            <a href="<?php echo $url; ?>" target="_blank">
                                <div class="content-overlay"></div>
                                <h3 class="missao">Escuta dos Clientes</h3>
                                <div class="content-details fadeIn-top">
                                    <p>Escutar as necessidades de nossos clientes a fim de detectar oportunidades para nossa melhoraria contínua, de modo a estabelecer a SPECIAL FÓRMULAS como farmácia de manipulação eleita para o fornecimento de seus medicamentos.</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="content">
                            <a href="<?php echo $url; ?>" target="_blank">
                                <div class="content-overlay"></div>
                                <h3 class="missao">Inovação</h3>
                                <div class="content-details fadeIn-top" id="servicos">
                                    <p>Desenvolver em todos os domínios a inovação permanente com investimento contínuo.</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>