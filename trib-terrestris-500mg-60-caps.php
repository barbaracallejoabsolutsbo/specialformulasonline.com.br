<?php

    $title       = "Trib Terrestris 500mg 60 cáps";
    $description = "Beta-caroteno melhor conhecido como um precursor da vitamina A, é um importante antioxidante que ajuda a prevenir danos de radicais livres prejudiciais no corpo"; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Trib Terrestris 500mg 60 cáps</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/trib-terrestris-500mg-60-caps.png" alt="trib-terrestris-500mg-60-caps" title="trib-terrestris-500mg-60-caps">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>REDUÇÃO DOS RADICAIS LIVRES</h2>
                        <p class="text-justify">O Tribullus Terrestris é utilizado principalmente para potencializar o desempenho físico, pois aumenta a produção natural de testosterona. Turbina a força e o ganho de massa magra e alivia a sensação de fadiga e cansaço, além de reduzir os níveis de colesterol e os sintomas da menopausa.</p>
                    </div>
                </div>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>