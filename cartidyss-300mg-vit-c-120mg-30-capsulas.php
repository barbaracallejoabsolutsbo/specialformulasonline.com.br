<?php

    $title       = "Cartidyss 300mg + Vit C 120mg 30 Cápsulas";
    $description = "Beta-caroteno melhor conhecido como um precursor da vitamina A, é um importante antioxidante que ajuda a prevenir danos de radicais livres prejudiciais no corpo"; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Cartidyss 300mg + Vit C 120mg 30 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/cartidyss-300mg-vit-c-120mg-30-capsulas.png" alt="cartidyss-300mg-vit-c-120mg-30-capsulas" title="cartidyss-300mg-vit-c-120mg-30-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>Descrição</h2>
                        <p class="text-justify">Os benefícios da carqueja incluem a melhora da circulação sanguínea, da digestão, proteção do fígado e a facilitação do processo de emagrecimento. Cartidyss® é um extrato de cartilagem da arraia marinha composto por: 60% de Peptídeos de Colágeno Marinho Tipo II (baixo peso molecular 3.000 daltons, alta disponibilidade de 96% de peptídeos). Polissacarídeos 40% (Glicosaminoglicanos): Sulfato de Glucosamina 10%, Sulfato de Condroitina 25% e Ácido Hialurônico 5%. Minerais: Fosfato de Cálcio. Obs: A vitamina C associada à Cartidyss® age sinergicamente na síntese de colágeno</p>
                        <br>
                        <h2>AUTOPREENCHIMENTO CUTÂNEO</h2>
                        <p class="text-justify">Cartidyss pode estimular e manter a função da derme, contribuindo para firmeza, sustentação e hidratação para uma pele saudável. A derme é responsável pelas propriedades de resistência, elasticidade, sustentação e hidratação da pele. Contém fibroblastos que sintetizam colágeno, elastina, glicosaminoglicanos (GAG), entre outras moléculas. Com o envelhecimento, há uma diminuição na qualidade e quantidade de GAGs, contribuindo para o aparecimento de rugas, ressecamento e afinamento da pele.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>