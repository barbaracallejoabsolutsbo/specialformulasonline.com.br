<?php

    $title       = "Vitamina D3 2.000Ui 60 Doses";
    $description = "Conhecida como a vitamina do sol por ser formada no corpo pela ação dos raios solares na pele, a vitamina D3 é responsável por manter a saúde dos dentes..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Vitamina D3 2.000Ui 60 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/vitamina-d3-2000ui-60-doses.png" alt="vitamina-d3-2000ui-60-doses" title="vitamina-d3-2000ui-60-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>A VITAMINA DO SOL</h2>
                        <p class="text-justify">Conhecida como a “vitamina do sol" por ser formada no corpo pela ação dos raios solares na pele, a vitamina D3 é responsável por manter a saúde dos dentes e ossos, ajudar contra a fraqueza muscular, além de proteger o corpo de outras deficiências e doenças.</p>
                        <br>
                        <h2>SEM PROBLEMAS COM OS OSSOS</h2>
                        <p class="text-justify">O termo vitamina D engloba um grupo de moléculas secosteróides derivadas do 7- deidrocolesterol (7-DHC) interligadas através de uma cascata de reações fotolíticas e enzimáticas que acontecem em células de diferentes tecidos. À vitamina D3 é primariamente atribuído o papel de importante regulador da fisiologia osteomineral, em especial do metabolismo do cálcio.</p>
                    </div>
                </div>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>