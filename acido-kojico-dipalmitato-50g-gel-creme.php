<?php

    $title       = "Ácido Kójico Dipalmitato 50g Gel Creme";
    $description = "O Ácido kójico Dipalmitato é um agente clareador e protetor da pele com alta permeação. Produz excelentes efeitos na tonificação da pele."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Ácido Kójico Dipalmitato 50g Gel Creme</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/acido-kojico-dipalmitato-50g-gel-creme.png" alt="acido-kojico-dipalmitato-50g-gel-creme" title="acido-kojico-dipalmitato-50g-gel-creme">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>ÁCIDO KÓJICO DIPALMITATO</h2>
                        <p class="text-justify">O Ácido kójico Dipalmitato é um agente clareador e protetor da pele com alta permeação. Produz excelentes efeitos na tonificação da pele, combate manchas de idade, marcas de gravidez, sardas, assim como desordens da pele em geral, como pigmentação do rosto e do corpo.</p>
                        <p class="text-justify">O Ácido Kójico Dipalmitato é um clareador cutâneo que também previne o fotoenvelhecimento e a formação de rugas. Tem uma excelente propriedade de inibir a atividade da tirosinase através da quelação do cobre presente na pele humana, inibindo a formação de melanina.</p>
                        <p class="text-justify">O Ácido kójico Dipalmitato oferece excelente estabilidade no produto, sem problemas de instabilidade da cor. É um ativo com baixa potência se comparado a outros ácidos clareadores, porém, pode ser utilizado durante todo o ano e durante o dia.</p>
                    </div>
                </div>
                <p class="text-justify">Outra grande vantagem do Ácido Kójico Dipalmitato é que ele é indicado também para peles morenas e negras. Além disso, nosso produto pode ser usado em peles secas, mistas e oleosas.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>