<?php

    $title       = "Leave in Femme 60Ml - Fortalecedor de Fios SENKEDA";
    $description = "O Senkeda Leave-in Femme Oficialfarma é um sérum capilar sem enxágue que promove hidratação, reconstrução e proteção térmica aos fios."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Leave in Femme 60Ml - Fortalecedor de Fios SENKEDA</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/leave-in-femme-60ml-fortalecedor-de-fios.png" alt="leave-in-femme-60ml-fortalecedor-de-fios" title="leave-in-femme-60ml-fortalecedor-de-fios">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>SENKEDA LEAVE IN FEMME</h2>
                        <p class="text-justify">O Senkeda Leave-in Femme Oficialfarma é um sérum capilar sem enxágue que promove hidratação, reconstrução e proteção térmica aos fios. Sua fórmula contém Vitamina A e Vitamina E com alto poder de penetração nos fios, mantendo-os nutridos, acelerando o crescimento e retendo maior concentração de água para um efeito de alta hidratação.</p>
                        <br>
                        <h2>RENOVAÇÃO CELULAR</h2>
                        <p class="text-justify">Com tecnologia que sela as cutículas, pode permitir uma efetiva proteção da cor dos cabelos tingidos e tratados quimicamente. Ele também oferece proteção térmica, reposição de queratina, renovação celular e proteção contra reações oxidativas, garantindo um cabelo muito mais brilhante, macio, sem pontas duplas ou frizz.</p>
                    </div>
                </div>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>