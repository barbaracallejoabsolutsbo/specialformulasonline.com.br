<?php

    $title       = "Uc2 + Pqq 30 Doses";
    $description = "Beta-caroteno melhor conhecido como um precursor da vitamina A, é um importante antioxidante que ajuda a prevenir danos de radicais livres prejudiciais no corpo"; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Uc2 + Pqq 30 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/uc2-pqq-30-doses.png" alt="uc2-pqq-30-doses" title="uc2-pqq-30-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>UC-II</h2>
                        <p class="text-justify">O UC-II é um nutracêutico inovador que tem a função de diminuir o desgaste nas articulações. O ativo age juntamente com o sistema imunológico para reduzir a dor, o desconforto, a inflamação e assim melhorar a flexibilidade e a mobilidade articular.</p>
                        <br>
                        <h2>PQQ</h2>
                        <p class="text-justify">A Pirroloquinolina quinona (PQQ) é uma quinona triclíclica que funciona como uma coenzima em várias reações óxidos redutase bacterianas. É um nutriente essencial que o organismo não pode sintetizar, sendo um cofator de reações de óxi-redução, atuando como um potente antioxidante, com capacidade de neutralizar os radicais livres superóxidos e hidróxidos, resultando na redução do envelhecimento celular precoce, tendo como consequência a promoção do envelhecimento saudável. Melhora as funções cognitivas, promove a proteção mitocondrial, possui propriedades antioxidantes, protege o sistema imunológico e neurológico.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="https://web.whatsapp.com/send?phone=5511944930219&text=Ol%C3%A1%21%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20da%20Exaustyair%20-%20Sistemas%20de%20Exaust%C3%A3o%20e%20Ventila%C3%A7%C3%A3o%20Industrial"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>