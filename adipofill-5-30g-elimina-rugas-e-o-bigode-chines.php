<?php

    $title       = "Adipofill 5% 30g - Elimina Rugas E O Bigode Chinês";
    $description = "O Adipofill™ Special Fórmulas foi desenvolvido com propriedades que atuam contra o envelhecimento da pele, a perda de tecido adiposo e da elasticidade..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Adipofill 5% 30g - Elimina Rugas E O Bigode Chinês</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/adipofill-5-30g-elimina-rugas-e-o-bigode-chines.png" alt="adipofill-5-30g-elimina-rugas-e-o-bigode-chines" title="adipofill-5-30g-elimina-rugas-e-o-bigode-chines">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>ADIPOFILL 5%</h2>
                        <p class="text-justify">O Adipofill Special Fórmulas foi desenvolvido com propriedades que atuam contra o envelhecimento da pele, a perda de tecido adiposo e da elasticidade, formando uma camada na pele que uniformiza as linhas de expressão e o “bigode chines”.</p>
                        <br>
                        <h2>COMO FUNCIONA?</h2>
                        <p class="text-justify">Adipofill Special Fórmulas é um novo ativo desenvolvido com objetivo de mimetizar o lipopreenchimento. Composto pela L-Ornitina, um aminoácido obtido por biotecnologia a partir da fécula vegetal e encapsulado por Lonosome. O produto induz o preenchimento de células adiposas e aumenta o volume do tecido adiposo. Através de mecanismos não invasivos e mecanismos biocontrolados, melhora clinicamente os sinais associados à idade estrutural oferecendo um aspecto de firmeza e aparência mais jovem. O Adipofill Special Fórmulas foi desenvolvido com propriedades que atuam contra o envelhecimento da pele, a perda de tecido adiposo e da elasticidade, formando uma camada na pele que uniformiza as linhas de expressão e o “bigode chines”.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>