<?php

    $title       = "Nutricolin 300Mg 30 Cápsulas";
    $description = "o silício inteligente fundamental para manter a elasticidade e firmeza da pele e também cabelos e unhas fortes e saudáveis, pois estimula as proteínas da beleza"; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Nutricolin 300Mg 30 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/nutricolin-300mg-30-capsulas.png" alt="nutricolin-300mg-30-capsulas" title="nutricolin-300mg-30-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>BELEZA SEM SEGREDO</h2>
                        <p class="text-justify">NUTRICOLIN®, o silício inteligente fundamental para manter a elasticidade e firmeza da pele e também cabelos e unhas fortes e saudáveis, pois estimula as proteínas da beleza: colágeno, elastina e queratina. Com NUTRICOLIN® ajudando no cuidado da cabeça aos pés, você verá que não é tão difícil trazer de volta o brilho natural da pele, favorecendo a diminuição de rugas, linhas finas e, ainda por cima, contribuindo para cabelos mais espessos, unhas fortalecidas e mais resistentes. O código da beleza foi decifrado! O silício inteligente com todos os benefícios para pele, cabelos e unhas, agora sem segredo nenhum.</p>
                        <br>
                        <h2>DIVERSOS BENEFÍCIOS</h2>
                        <p class="text-justify">Na Pele ajuda a promover a síntese de colágeno e elastina, retenção de água e colabora para promover uma rede estrutural na derme trazendo inúmeros benefícios para a pele como: Alta hidratação, Síntese de colágeno tipo I, Ação antioxidante, Estabilização de GAC, Contribui na diminuição das rugas e linhas finas, auxilia na ação antiligante. Colabora para nutrição dos fios e a compactação da cutícula do cabelo. Ajuda a fortificar, hidratar e reestruturar os fios, auxilia na diminuição da quebra, contribui para aumentar a queratina, favorece resistência, elasticidade e volume. Nas Unhas, auxilia no aumento da queratina, ajuda a promover o fortalecimento, contribui para diminuir quebras, favorece a melhora da aparência geral (tonalidade e uniformidade).</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">O Uso do produto não é aconselhável para pessoas com problemas renais sem orientação médica. Imagens meramente ilustrativas. Pessoas com hipersensibilidade à substância não devem ingerir o produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Este produto não deve ser utilizado por gestantes e lactantes sem orientação médica. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. Todo medicamento deve ser mantido fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="https://web.whatsapp.com/send?phone=5511944930219&text=Ol%C3%A1%21%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20da%20Exaustyair%20-%20Sistemas%20de%20Exaust%C3%A3o%20e%20Ventila%C3%A7%C3%A3o%20Industrial"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>