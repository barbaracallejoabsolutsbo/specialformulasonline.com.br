<?php

    $title       = "Insea 2 250Mg 60 Cápsulas";
    $description = "Beta-caroteno melhor conhecido como um precursor da vitamina A, é um importante antioxidante que ajuda a prevenir danos de radicais livres prejudiciais no corpo"; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Insea 2 250Mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/insea-2-250mg-60-capsulas.png" alt="insea-2-250mg-60-capsulas" title="insea-2-250mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">InSea2 é um combinado dos polifenóis de Ascophyllum nodosum e Fucus vesiculosus, padronizado em florotaninos, que tem a capacidade de inibir a atividade da glucosidase e da amilase, reduzindo a digestão e assimilação de amido e açúcar, a resposta glicêmica normal que segue uma refeição, bem como o pico de insulina associada, e ajuda a manter a sensibilidade à insulina saudável. O InSea2® é o primeiro bloqueador de carboidratos com ação dupla, agindo nas duas principais etapas de absorção de carboidratos presentes no organismo, o que lhe garante ainda mais sucesso em sua função desejada.</p>
                        <br>
                        <h2>PREVENÇÃO DE DIABETES TIPO II</h2>
                        <p class="text-justify">O InSea2® age inibindo duas enzimas: a glucosidase e a amilase, o que o torna superior a bloqueadores simples de amido que agem inibindo somente a amilase. Uma vez inibidas ambas as enzimas, não haverá degradação dos carboidratos, reduzindo a absorção em sua maioria. Portanto, os carboidratos não conseguem ser digeridos e são enviados diretamente ao intestino para sua eliminação através das fezes. Esse mecanismo apresenta uma alternativa segura para auxiliar as dietas de emagrecimento e para diabéticos que precisam diminuir a quantidade de açúcar circulante.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>