<?php

    $title       = "Vitamina D3 + Vitamina K2 (Mk-7) 60 Cápsulas";
    $description = "Sem dúvidas o benefício mais conhecido da vitamina D3 é a sua função no desenvolvimento e manutenção dos ossos. Essa vitamina é capaz de estimular..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Vitamina D3 + Vitamina K2 (Mk-7) 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/vitamina-d3-vitamina-k2-mk-7-60-capsulas.png" alt="vitamina-d3-vitamina-k2-(mk-7)-60-capsulas" title="vitamina-d3-vitamina-k2-(mk-7)-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>VITAMINA D3</h2>
                        <p class="text-justify">Sem dúvidas o benefício mais conhecido da vitamina D3 é a sua função no desenvolvimento e manutenção dos ossos. Essa vitamina é capaz de estimular uma maior densidade óssea, colaborando para o crescimento saudável das crianças e também evitando a osteoporose em adultos e idosos. Contar com os níveis adequados de vitamina D3 no organismo está diretamente relacionada a uma boa saúde hormonal, já que estimula a produção adequada de estrogênio nas mulheres e de testosterona em homens.</p>
                        <br>
                        <h2>VITAMINA K2 (MK-7)</h2>
                        <p class="text-justify">Uma ingestão adequada de Vitamina K2 influencia positivamente o sistema cardiovascular. A Vitamina K2 ativa a Matrix Gla Protein (MGP), inibindo a deposição de cálcio na parede dos vasos. O cálcio é removido num sistema coordenado desativado: fatores solúveis, células e tecidos, mantendo as artérias saudáveis e flexíveis.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">"Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado" "Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado". "As indicações postas não se tratam de propaganda, e sim de descrição do produto". "Consulte sempre um especialista". "As imagens postas são meramente ilustrativas". "As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>