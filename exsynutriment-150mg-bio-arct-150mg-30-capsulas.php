<?php

    $title       = "Exsynutriment 150mg + Bio Arct 150mg 30 Cápsulas";
    $description = "A associação de Exsynutriment® e Bio-Arct® denominado como cápsula da Beleza, possui resultados visíveis no tecido conjuntivo pois reestrutura..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Exsynutriment 150mg + Bio Arct 150mg 30 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/exsynutriment-150mg-bio-arct-150mg-30-capsulas.png" alt="exsynutriment-150mg-bio-arct-150mg-30-capsulas" title="exsynutriment-150mg-bio-arct-150mg-30-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>EXSYNUTRIMENT + BIO ARCT</h2>
                        <p class="text-justify">A associação de Exsynutriment® e Bio-Arct® denominado como cápsula da Beleza, possui resultados visíveis no tecido conjuntivo pois reestrutura, firma, detoxifica e hidrata a pele. O Exsynutriment® é uma molécula original patenteada pela Exsymol em Mônaco, composta por ácido ortosílicico estabilizado em colágeno marinho hidrolisado que atua no tecido conjuntivo reestruturando as fibras de colágeno e elastina.</p>
                        <p class="text-justify">Sua reposição é essencial para regeneração dos tecidos danificados, degradação da membrana celular, aparecimento de rugas, envelhecimento precoce, desgaste ósseo, cabelos e unhas desvitalizados. O Bio-Arct® é um bioenergizante proveniente das profundezas do mar Artico, cuja composição é patenteada e única. Esta biomassa marinha tem cultivo altamente tecnológico e é riquíssima em bioativos padronizados: citrulyl arginina, taurina, floridosídeos e sais minerais. Exerce potente ação anti-inflamatória e pode ser recomendado para tratamentos pré e pós cirúrgicos, inclusive peelings.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado.</p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>