<?php

    $title       = "Keranat 150mg Por 2,5ml 150ML";
    $description = "O segredo contra a queda de cabelo e beleza dos fios. O cabelo bonito e saudável é um sinal de boa saúde e confere autoestima e bem-estar."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Keranat 150mg Por 2,5ml 150ML</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/keranat-150mg-por-25ml-150ml.png" alt="keranat-150mg-por-2.5ml-150ml" title="keranat-150mg-por-2.5ml-150ml">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>PODEROSO ALIADO</h2>
                        <p class="text-justify">O segredo contra a queda de cabelo e beleza dos fios. O cabelo bonito e saudável é um sinal de boa saúde e confere autoestima e bem-estar. O organismo deve ser alimentado com nutrientes para fornecer estrutura e funções fisiológicas saudáveis. A maioria das pessoas não associam o cabelo e seu crescimento como parte de uma estrutura viva do corpo. Para o cabelo crescer e ter a aparência saudável, brilhante e macia, é necessário manter hábitos saudáveis e evitar o estresse.</p>
                        <p class="text-justify">Keranat™ é um suplemento oral patenteado revolucionário, altamente eficaz, que fornece os nutrientes necessários para os cabelos, agindo diretamente no centro de crescimento dos fios no interior da raiz, favorecendo a diminuição da queda, melhora o brilho e maciez dos cabelos de dentro para fora. Keranat™ é um Nutracêutico, baseado em uma composição única de triterpenóides e fitoesteróis (Miliacine™); lipídeos polares como glicosilceramidas, fosfolipídeos e esfingolipídeos de origem vegetal (Lipowheat™), capaz de atuar na diminuição da queda do cabelo, facilitar o crescimento dos fios, prevenir a caspa, proteger o pigmento natural do cabelo (previne fios brancos), aumentar o volume e melhorar o aspecto saudável dos fios de dentro para fora.</p>
                    </div>
                <p class="text-justify">Keranat™ aumenta o metabolismo capilar, estimula a proliferação celular e a regeneração dos tecidos. Estudos científicos comprovam que o Keranat™ tem a capacidade de aumentar o crescimento dos cabelos, possui atividade anabólica (estimula fatores de crescimento na matriz extracelular), resultando em fios fortes, brilhantes e saudáveis. Além disso, os resultados científicos mostram um aumento do conforto do couro cabeludo, prevenindo assim o aparecimento de caspa e seborréia e diminuição dos fios na fase telógena.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>