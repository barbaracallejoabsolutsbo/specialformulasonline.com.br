<?php

    $title       = "Verisol 2,5g 30 Sachês";
    $description = "O Verisol Special Fórmulas é o único colágeno especialmente desenvolvido para a beleza da pele, que age de dentro para fora, atenuando e prevenindo..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Verisol 2,5g 30 Sachês</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/verisol-25g.png" alt="verisol-2,5g" title="verisol-2,5g">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>VERISOL</h2>
                        <p class="text-justify">O Verisol Special Fórmulas é o único colágeno especialmente desenvolvido para a beleza da pele, que age de dentro para fora, atenuando e prevenindo os sinais do tempo. É uma combinação única de Peptídeos Bioativos de Colágeno, que origina peptídeos específicos para atuar nas células dérmicas e assim, estimular e restaurar o metabolismo destas células.</p>
                        <br>
                        <h2>PELE JOVEM</h2>
                        <p class="text-justify">O Verisol® suaviza os sinais do tempo e melhora a elasticidade cutânea. Reduz o volume das rugas e aumenta o conteúdo de colágeno na pele a partir de 4 semanas de uso contínuo. Sua tecnologia recupera o equilíbrio necessário para contrabalancear os processos de envelhecimento da pele.</p>
                    </div>
                </div>
                <h2>COMBINAÇÃO DE PEPTÍDEOS</h2>
                <p class="text-justify">O Verisol® Special Fórmulas possui uma combinação única de Peptídeos Bioativos de Colágeno, desenvolvido com uma tecnologia especial para restaurar o metabolismo das células de dentro para fora. Obtido por meio de um processo de hidrólise tecnológico, que origina peptídeos específicos para atuar nas camadas mais profundas da pele, restabelecendo o metabolismo das células dérmicas com a administração de uma pequena dose diária. Verisol® Special Fórmulas é o único colágeno especialmente desenvolvido para a beleza da pele. Ele age de dentro para fora, atenuando e prevenindo os sinais do tempo. Melhora a elasticidade cutânea e reduz o volume das rugas.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. li>Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="https://web.whatsapp.com/send?phone=5511944930219&text=Ol%C3%A1%21%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20da%20Exaustyair%20-%20Sistemas%20de%20Exaust%C3%A3o%20e%20Ventila%C3%A7%C3%A3o%20Industrial"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>