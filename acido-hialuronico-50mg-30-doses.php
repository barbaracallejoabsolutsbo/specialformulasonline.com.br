<?php

    $title       = "Ácido Hialurônico 50Mg 30 Doses";
    $description = "O ÁCIDO HIALURÔNICO é um polissacarídeo da família das Glicosaminoglicanas (GAG’s), naturalmente presente na derme. Descoberto em 1934 no humor..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Ácido Hialurônico 50Mg 30 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/acido-hialuronico-50mg-30-doses.png" alt="acido-hialuronico-50mg-30-doses" title="acido-hialuronico-50mg-30-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>ÁCIDO HIALURÔNICO</h2>
                        <p class="text-justify">O ÁCIDO HIALURÔNICO é um polissacarídeo da família das Glicosaminoglicanas (GAG’s), naturalmente presente na derme. Descoberto em 1934 no humor vítreo dos olhos de vaca, despertou um grande interesse devido à sua excepcional capacidade hidratante. É um dos mais importantes componentes da derme envolvidos nesta função, devido à sua elevada capacidade de retenção de água. Aplicado topicamente, forma um filme hidratante sobre a epiderme, que ajuda a compensar a perda de água, melhorando as condições da pele e proporcionando desta forma elasticidade, suavidade e uma superfície mais homogênea.</p>
                        <p class="text-justify">Substância naturalmente presente no organismo humano, uma molécula de açúcar que atrai a água e pode atuar como um lubrificante e absorver impactos em partes móveis do corpo como as articulações. Do Ácido Hialurônico no nosso corpo, 56% dele está na pele, onde ele atua preenchendo o espaço entre as células, o que a mantém lisa, elástica e bem hidratada. Porém, com o tempo, sua concentração na pele diminui, o que causa o aparecimento de rugas e também seu ressecamento. O Ácido Hialurônico é indicado principalmente para melhorar o viço da pele, suavizando rugas e outras marcas da idade.</p>
                    </div>
                </div>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>