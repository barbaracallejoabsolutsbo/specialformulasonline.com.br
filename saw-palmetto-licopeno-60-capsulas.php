<?php

    $title       = "Saw Palmetto + Licopeno 60 Cápsulas";
    $description = "Associação fitoterápica para prevenir doenças da próstata e amenizar os sintomas relacionados à hiperplasia benigna de próstata (HPB)."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Saw Palmetto + Licopeno 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/saw-palmetto-licopeno-60-capsulas.png" alt="saw-palmetto-licopeno-60-capsulas" title="saw-palmetto-licopeno-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>SAW PALMETTO</h2>
                        <p class="text-justify">Associação fitoterápica para prevenir doenças da próstata e amenizar os sintomas relacionados à hiperplasia benigna de próstata (HPB). Também combate a obstrução do fluxo urinário e possui ação antioxidante. O Saw Palmetto é uma planta medicinal tradicionalmente utilizada como afrodisíaca, antiestrogênica, diurética, inibidora das células prostáticas, no tratamento de tumor benigno da próstata, impotência sexual e hiperplasia benigna da próstata. Alivia a micção noturna frequente e aumenta o jato urinário.</p>
                        <br>
                        <h2>LICOPENO</h2>
                        <p class="text-justify">O Licopeno é usado para neutralizar radicais livres, exercendo um efeito preventivo contra os riscos do câncer. Estudos conduzidos na Universidade de Eduard demonstraram sua eficácia na prevenção do câncer de próstata. Seu efeito antioxidante também protege o sistema cardiovascular. Vários estudos indicam a eficiência do licopeno como antioxidante, e há estudos que relacionam essa função com a prevenção de câncer, principalmente de próstata. Outros cânceres cujo risco está inversamente associado com o nível de licopeno no sangue e nos tecidos incluem mama, trato digestivo, bexiga e pele.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Não deve ser utilizado por pacientes com problemas epigástricos graves pré-existentes; Não administrar com o estômago vazio para não ocorrer náuseas; O Saw Palmetto possui ação hormonal oposta à da testosterona e poderá interagir com estrógenos presentes em terapia de reposição hormonal e em contraceptivos orais. Não é recomendada a administração conjunta com outras drogas que afetam os hormônios sexuais masculinos como finasterida ou flutamida; Saw Palmetto poderá aumentar o risco de sangramento quando administrado conjuntamente a fármacos como ácido acetilsalicílico, varfarina, heparina, clopidogrel, anti-inflamatórios não esteroidais como ibuprofeno ou naproxeno. Pode alterar a absorção de ferro no organismo. Imagens meramente ilustrativas. Pessoas com hipersensibilidade à substância não devem ingerir o produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Este produto não deve ser utilizado por gestantes e lactantes sem orientação médica. </p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. Todo medicamento deve ser mantido fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de </p>
                <hr>
                <br>
                <a class="compre-aqui" href="https://web.whatsapp.com/send?phone=5511944930219&text=Ol%C3%A1%21%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20da%20Exaustyair%20-%20Sistemas%20de%20Exaust%C3%A3o%20e%20Ventila%C3%A7%C3%A3o%20Industrial"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>