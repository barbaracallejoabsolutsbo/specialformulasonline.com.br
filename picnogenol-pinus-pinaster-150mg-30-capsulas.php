<?php

    $title       = "Picnogenol (Pinus Pinaster) 150MG 30 Cápsulas";
    $description = "O picnogenol, como também é conhecido, é na verdade uma combinação de 40 diferentes compostos que estão presentes na casca do pinheiro marítimo..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Picnogenol (Pinus Pinaster) 150MG 30 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/picnogenol-pinus-pinaster-150mg-30-capsulas.png" alt="picnogenol-(pinus-pinaster)-150mg-30-capsulas" title="picnogenol-(pinus-pinaster)-150mg-30-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">O picnogenol, como também é conhecido, é na verdade uma combinação de 40 diferentes compostos que estão presentes na casca do pinheiro marítimo, e que possuem diversos benefícios para a saúde. O pycnogenol pode ter diversas funções no organismo, mas sua principal característica é seu alto potencial antioxidante. Os antioxidantes são compostos químicos que têm a capacidade de combater os radicais livres, prevenindo ou diminuindo danos às células. Esses danos celulares causados pelos radicais livres estão associados a diversas doenças, como câncer, infarto e Alzheimer.</p>
                        <br>
                        <h2>BENEFÍCIOS</h2>
                        <p class="text-justify">Os antioxidantes atuam neutralizando ou diminuindo o efeito dos radicais livres, impedindo maiores prejuízos às células. O uso regular de pycnogenol está ligado à prevenção de doenças e também dos efeitos negativos das atividades físicas intensas. Estimula a produção de óxido nítrico (NO2), que por sua vez promove uma melhora na circulação sanguínea. Pode ter efeitos positivos no mecanismo de atuação da insulina no corpo. As substâncias antioxidantes presentes do pycnogenol seriam responsáveis por combater alguns dos principais mecanismos ligados ao envelhecimento: estresse oxidativo, danos à estrutura do DNA, inflamação e danos à membrana celular.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>