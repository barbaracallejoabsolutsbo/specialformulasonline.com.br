<?php
    $title       = "Remedios manipulados para presas alta";
    $description = "Todo suporte e atenção para seus remédios manipulados para pressão alta funcionarem com total eficácia. Consulte-nos para mais informações.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Conheça a Special Fórmulas. Somos uma farmácia magistral que trabalha na manipulação de fórmulas especiais, com uma grande variedade de produtos e formas de administração para o consumo de fármacos e fitoterápicos. Encontre todo suporte profissional e especializado para fabricar seus <strong>remédios manipulados para pressão alta</strong> prescritos pelo seu médico. Consulte seu profissional da saúde sempre antes de tomar qualquer tipo de medicamento, seja ele industrializado, manipulado, natural, entre outros.</p>
<p>Disponíveis para manipulação em diversas formas de consumo, nossas fórmulas são manipuladas com todas Boas Práticas de Manipulação em Farmácia, seguindo normas e regras da ANVISA para entregar <strong>remédios manipulados para pressão alta</strong>, para fins dermatológicos, estéticos, entre outros, da mais alta qualidade do mercado regional. Com toda agilidade e profissionalismo, desenvolvemos suas formulas com pontualidade, oferecendo produtos com alta capacidade de absorção e aproveitamento total das substâncias neles contidos.</p>
<p>Compre seus manipulados com total segurança na Special Fórmulas. Trabalhamos com equipamentos tecnológicos e modernos a fim de desenvolver <strong>remédios manipulados para pressão alta</strong> com substâncias como indapamida, clortalidona, furosemida, hidroclorotiazida, entre outros, que popularmente são receitados em doses recomendadas pelo seu médico consultado.</p>
<p>Os <strong>remédios manipulados para pressão alta</strong> podem funcionar da mesma forma como os remédios industrializados, com benefícios exclusivos como dosagem personalizada, formas de consumo mais viáveis e controláveis, preço mais baixo que o convencional e todo profissionalismo e atenção de uma farmácia magistral. Consulte seu médico e compre seus medicamentos manipulados com a Special Fórmulas e tenha preços, prazos e atendimento excelentes.</p>
<h2><strong>Faça suas receitas de remédios manipulados para pressão alta com a Special Fórmulas</strong></h2>
<p>Traga suas receitas para manipulação conosco. Não só para tratamento de pressão alta, mas também controle de apetite, cremes dermatológicos, estimulantes sexuais e muito mais. Uma infinidade de possibilidades de manipulação e compra fórmulas com a nossa farmácia de manipulação. Todo suporte e atenção para seus<strong> remédios manipulados para pressão alta </strong>funcionarem com total eficácia. Consulte-nos para mais informações.</p>
<h2><strong>Remédios manipulados para pressão alta e seus benefícios</strong></h2>
<p>Benefícios como personalização de dosagem de acordo com peso, idade e hábitos do dia a dia interferem totalmente na eficácia dos medicamentos receitados pelo seu médico. Ao comprar<strong> remédios manipulados para pressão alta </strong>você consegue todos esses benefícios a fim de gerir adequadamente as substâncias que seu médico deseja receitar, sem precisar pagar e consumir o que o seu corpo não precisa. Dessa forma você adquire seus medicamentos com preço justo, acessível e com maior eficiência para seu objetivo. </p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>