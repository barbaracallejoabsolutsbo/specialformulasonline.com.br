<?php

    $title       = "Vitamina B2 (Riboflavina) 250mg 60 Cápsulas";
    $description = "A Vitamina B2 (riboflavina) pertence ao Complexo B e tem como principais funções favorecer o metabolismo de gorduras, açúcares e proteínas..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Vitamina B2 (Riboflavina) 250mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/vitamina-b2-riboflavina-250mg-60-capsulas.png" alt="vitamina-b2-(riboflavina)-250mg-60-capsulas" title="vitamina-b2-(riboflavina)-250mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">A Vitamina B2 (riboflavina) pertence ao Complexo B e tem como principais funções favorecer o metabolismo de gorduras, açúcares e proteínas, estimular a produção de sangue e auxiliar na cicatrização e visão. Possui ação antioxidante e atua também no sistema respiratório e sistema nervoso. Não é produzida pelo organismo, portanto é indicada sua ingestão diária. As necessidades da suplementação aumentam simultaneamente com o crescimento, a gravidez e a lactação, e é indicada também para quem pratica esportes de alta performance.</p>
                        <br>
                        <h2 class="advertencias text-center">Advertências</h2>
                        <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                    </div>
                </div>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>