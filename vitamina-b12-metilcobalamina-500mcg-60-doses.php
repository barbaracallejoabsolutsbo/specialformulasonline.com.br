<?php

    $title       = "Vitamina B12 Metilcobalamina 500Mcg 60 Doses";
    $description = "A Vitamina B12 está envolvida em vários processos metabólicos do organismo e desempenha um papel importante no equilíbrio da saúde..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Vitamina B12 Metilcobalamina 500Mcg 60 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/vitamina-b12-metilcobalamina-500mcg-60-doses.png" alt="vitamina-b12-metilcobalamina-500mcg-60-doses" title="vitamina-b12-metilcobalamina-500mcg-60-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>MAIS ENERGIA</h2>
                        <p class="text-justify">A Vitamina B12 está envolvida em vários processos metabólicos do organismo e desempenha um papel importante no equilíbrio da saúde, pois ela colabora na construção estrutural do DNA, promove uma melhor absorção de nutrientes dos alimentos, ajuda o sistema imunológico, além de contribuir na produção de energia para o nosso organismo.</p>
                        <p class="text-justify">A vitamina B12 desempenha um papel importante na formação das células vermelhas do sangue, também é elemento chave para desenvolvimento e manutenção das funções do sistema nervoso. Com a ausência dessa vitamina em nosso organismo a mielina que recobre os nervos, como uma "capa" protetora, sofre um desgaste que recebe o nome de desmielinização, processo que ocorre tanto em neurônios de nervos periféricos, quanto naqueles da substância branca do cérebro.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>