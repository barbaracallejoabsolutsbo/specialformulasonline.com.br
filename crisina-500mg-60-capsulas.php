<?php

    $title       = "Crisina 500Mg 60 Cápsulas";
    $description = "Retirada da planta passiflora coerulea a crisina é ideal para atletas e fisiculturistas que buscam a hipertrofia (aumento do volume dos músculos)..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Crisina 500Mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/crisina-500mg-60-capsulas.png" alt="crisina-500mg-60-capsulas" title="crisina-500mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>CRISINA 500MG 60 CÁPSULAS - OFICIALFARMA</h2>
                        <p class="text-justify">Retirada da planta passiflora coerulea a crisina é ideal para atletas e fisiculturistas que buscam a hipertrofia (aumento do volume dos músculos). também tem como característica principal inibir o processo que transforma testosterona em estrogênios, principalmente EM HOMENS DE IDADE MAIS AVANÇADA.</p>
                        <br>
                        <h2>O QUE É CRISINA?</h2>
                        <p class="text-justify">As pessoas que buscam hipertrofia, como atletas e fisiculturistas, provavelmente já ouviram falar em CRISINA. Extraída da planta Passiflora coerulea (aparentada ao maracujá), também é conhecida pelo nome em inglês chrysin e está presente, principalmente, no mel e na própolis. O equilíbrio dos hormônios sexuais dos homens e mulheres é controlado pela enzima aromatase, que catalisa a conversão de testosterona, por exemplo, em estrogênio. Flavonoides são compostos presentes em grãos integrais, legumes, frutas e vegetais que são responsáveis pela proteção em doenças cardíacas, infartos e câncer.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado.</p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>