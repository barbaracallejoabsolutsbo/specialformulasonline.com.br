<?php

    $title       = "Bulbine 250Mg 60 Cápsulas";
    $description = "Bulbine aumenta a libido, a testosterona e melhora o desempenho sexual e físico de homens e mulheres. Estudos realizados comprovam que este extrato..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Bulbine 250Mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/bulbine-250mg-60-capsulas.png" alt="bulbine-250mg-60-capsulas" title="bulbine-250mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>ESTIMULA A PRODUÇÃO DE TESTOSTERONA</h2>
                        <p class="text-justify">Bulbine aumenta a libido, a testosterona e melhora o desempenho sexual e físico de homens e mulheres. Estudos realizados comprovam que este extrato diminui os níveis de estrogênio, aumenta os níveis de testosterona sérica e de hormônio luteinizante, reduzindo a ejaculação precoce.</p>
                        <br>
                        <h2>REDUZ MEDIDAS</h2>
                        <p class="text-justify">Em decorrência do aumento dos níveis de testosterona, o Bulbine estimula a síntese proteica do músculo, aumentando a massa muscular, ou seja, melhora a resistência, o desempenho e a força física. Além disso, a testosterona controla os níveis de gordura corporal, auxiliando a redução de medidas. Também previne a redução de massa magra e auxilia a recuperação muscular pós-exercício. Seu efeito sobre os níveis de testosterona não implica em alterações significativas das concentrações de prolactina, em ambos os sexos.</p>
                    </div>
                </div>
                <h2>AUXILIA O AUMENTO DA MASSA MUSCULAR E FORÇA FÍSICA</h2>
                <p class="text-justify">Devido ao estímulo da produção de testosterona, o Bulbine atua diretamente auxiliando o ganho de massa muscular, pois aumenta o desempenho físico e melhora também o desempenho sexual.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pode aumentar a contagem de glóbulos brancos, plaquetas e monócitos, enquanto diminui os níveis de neutrófilos, linfócitos e eosinófilos. As concentrações séricas de colesterol e triacilgliceróis podem ser aumentadas. Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Manter fora do alcance das crianças. Este produto não deve ser utilizado por mulheres grávidas. O uso do produto durante o período de amamentação sem orientação médica, também não é recomendado.</p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Nunca compre medicamentos sem orientação de um profissional habilitado. Imagens meramente ilustrativas. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>