<?php

    $title       = "Same 200Mg 60 Cápsulas";
    $description = "SAME (S-Adenosil-L-Metionina) é uma substância que atua como uma vitamina para impulsionar o seu humor e ajuda a manter a saúde das Articulações..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Same 200Mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/same-200mg-60-capsulas.png" alt="same-200mg-60-capsulas" title="same-200mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">SAME (S-Adenosil-L-Metionina) é uma substância que atua como uma vitamina para impulsionar o seu humor e ajuda a manter a saúde das Articulações e a função do fígado. Uma maneira natural para ajudar você a se sentir melhor, física e emocionalmente, same é uma substância segura e eficaz que existe em todas as células no corpo.</p>
                        <br>
                        <h2>BENEFÍCIOS</h2>
                        <p class="text-justify">Essencial para a produção dos químicos cerebrais, neurotransmissores e hormônios, same exerce um importante papel em como você se sente. Também como parte da cartilagem, e a parte que absorve o choque nas Articulações, ajudando a amortecer as Articulações para apoiar a mobilidade e a flexibilidade. Também ajuda na produção de um poderoso antioxidante chamado glutationa no corpo, usado para aumentar a capacidade do fígado de remover as toxinas.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>