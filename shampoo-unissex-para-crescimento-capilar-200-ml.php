<?php

    $title       = "Shampoo Unissex Para Crescimento Capilar 200 ml";
    $description = "O Shampoo Unissex, combina com todos os tipos de cabelos, ajudando a realçar a beleza dos fios e acelerar o crescimento capilar..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Shampoo Unissex Para Crescimento Capilar 200 ml</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/shampoo-unissex-para-crescimento-capilar-200-ml.png" alt="shampoo-unissex-para-crescimento-capilar-200-ml" title="shampoo-unissex-para-crescimento-capilar-200-ml">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>SHAMPOO UNISSEX</h2>
                        <p class="text-justify">O Shampoo Unissex combina com todos os tipos de cabelos, ajudando a realçar a beleza dos fios e acelerar o crescimento capilar.</p>
                        <br>
                        <h2>SAÚDE DOS CABELOS</h2>
                        <p class="text-justify">Sua fórmula ajuda a desobstruir os poros do couro cabeludo nos fios oleosos e evita a presença das pontas ressecadas nos cabelos secos, mantendo as madeixas sempre saudáveis.</p>
                    </div>
                </div>
                <h2 class="text-center">COMPOSIÇÃO SHAMPOO UNISSEX</h2>
                <h3>Chá verde 5%</h3>
                <p class="text-justify">Essencial para manter a saúde das células no couro cabeludo, evitando a queda capilar.</p>
                <h3>Capsicum 5%</h3>
                <p class="text-justify">Promove a vasodilatação, a oxigenação e estimula a circulação do couro cabeludo, evitando a queda de cabelo.</p>
                <h3>L Carnitina Tartarato 2%</h3>
                <p class="text-justify">Responsável por estimular o crescimento capilar no couro cabeludo.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>