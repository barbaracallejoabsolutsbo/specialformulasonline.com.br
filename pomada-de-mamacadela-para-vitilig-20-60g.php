<?php

    $title       = "Pomada De Mamacadela Para Vitiligo 20% 60g";
    $description = "A Mamacadela também pode ser chamada de mamica-de-cadela, algodão-do-campo, amoreira-do-campo, conduru ou fruta de cera. Ela é original do Cerrado..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Pomada De Mamacadela Para Vitiligo 20% 60g</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/pomada-de-mamacadela-para-vitilig-20-60g.png" alt="pomada-de-mamacadela-para-vitilig-20-60g" title="pomada-de-mamacadela-para-vitilig-20-60g">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>MAMACADELA</h2>
                        <p class="text-justify">A Mamacadela também pode ser chamada de mamica-de-cadela, algodão-do-campo, amoreira-do-campo, conduru ou fruta de cera. Ela é original do Cerrado, onde era usada como um remédio contra gripes e bronquites. Ela pode ser utilizada em casos de má circulação também. As substâncias encontradas nessa planta são conhecidas como fundamentais para tratamentos de doenças com sintomas na pele, hoje em dia ela é muito útil em várias doenças envolvendo a pele humana.</p>
                        <p class="text-justify">É uma planta medicinal que pode ser usada no tratamento de várias doenças como vitiligo, leucodermina, micose, psoríase, dermatite, entre outras. Os estudos ainda não estão totalmente prontos, mas tudo indica que pessoas afetadas por vitiligo podem voltar a ter sua coloração normal em certa área. Ela também pode ser muito útil contra gripes em casos de bronquite, úlcera gástrica e má circulação do sangue.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="https://web.whatsapp.com/send?phone=5511944930219&text=Ol%C3%A1%21%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20da%20Exaustyair%20-%20Sistemas%20de%20Exaust%C3%A3o%20e%20Ventila%C3%A7%C3%A3o%20Industrial"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>