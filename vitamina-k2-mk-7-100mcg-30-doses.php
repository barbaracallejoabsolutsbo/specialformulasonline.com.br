<?php

    $title       = "Vitamina K2 Mk-7 100Mcg 30 Doses";
    $description = "A Vitamina K2 é uma subclasse da vitamina K. Dentro de vitamina K2, existem diferentes variantes, sendo que a Vitamina K2 - MK-7, na forma de menaquinona..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Vitamina K2 Mk-7 100Mcg 30 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/vitamina-k2-mk-7-100mcg-30-doses.png" alt="vitamina-k2-mk-7-100mcg-30-doses" title="vitamina-k2-mk-7-100mcg-30-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>UM ALIADO PODEROSO</h2>
                        <p class="text-justify">A Vitamina K2 é uma subclasse da vitamina K. Dentro de vitamina K2, existem diferentes variantes, sendo que a Vitamina K2 - MK-7, na forma de menaquinona, é a única que tem sido considerada biodisponível, e tem uma meia vida longa na corrente sanguínea após a ingestão oral, proporcionando assim todos os benefícios relacionados a esta vitamina, como melhorar a saúde cardiovascular, aumentando simultaneamente a densidade mineral óssea.</p>
                        <p class="text-justify">Como um dos principais resultados dessa deficiência, o cálcio não é incorporado corretamente aos ossos, tornando-os fracos e quebradiços, enquanto há um acumulo nas artérias. Esse cálcio acumulado endurece e bloqueia as artérias. Atualmente a deficiência desta vitamina está se tornando um problema importante, pois está associada a diversos problemas de saúde como osteoporose, fraturas ósseas frequentes, e riscos cardiovasculares. Assim, é muito importante a suplementação da dieta com uma quantidade apropriada de Vitamina K2 - MK-7, para se obter ossos e artérias saudáveis.</p>
                    </div>
                </div>
                <p class="text-justify">Uma ingestão adequada de Vitamina K2 influencia positivamente o sistema cardiovascular. A Vitamina K2 ativa a Matrix Gla Protein (MGP), inibindo a deposição de cálcio na parede dos vasos. O cálcio é removido num sistema coordenado desativado: fatores solúveis, células e tecidos, mantendo as artérias saudáveis e flexíveis. No entanto, a deficiência de Vitamina K resulta em baixa carboxilação – ou ativação inadequada – e prejudica a função normal deste processo de remoção do cálcio. Assim, a deficiência de vitamina K2 indica um maior risco de calcificação arterial.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Não é recomendado o uso concomitante com varfarina ou outros anticoagulantes. Possui traços de lactose. Nunca compre medicamento sem orientação de um profissional habilitado. Imagens meramente ilustrativas. Pessoas com hipersensibilidade à substância não devem ingerir o produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o medicamento com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o medicamento se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Todo medicamento deve ser mantido fora do alcance das crianças. Este medicamento não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do medicamento durante o período de amamentação também não é recomendado.</p>
                <p class="advertencias text-justify">Este medicamento não deve ser utilizado por menores de 18 anos sem orientação médica. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>