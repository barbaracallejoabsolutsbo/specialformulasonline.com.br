<?php

    $title       = "Valeriana 100Mg 60 Cápsulas";
    $description = "A valeriana é uma alternativa natural e poderosa para acalmar os ânimos e a gula! Ela tem ação sedativa e combate ansiedade, agitação e insônia..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Valeriana 100Mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/valeriana-100mg-60-capsulas.png" alt="valeriana-100mg-60-capsulas" title="valeriana-100mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">A valeriana é uma alternativa natural e poderosa para acalmar os ânimos e a gula! Ela tem ação sedativa e combate ansiedade, agitação e insônia, sem causar dependência. Normalmente a Valeriana é muito usada em casos de insônia por seu efeito tranquilizante e relaxante, a planta possui efeito sedativo, calmante, antiespasmódico, sonífero, relaxante e anticonvulsionante. Para problemas relacionados ao sono, a planta é indicada em caso de ansiedade, estresse, epilepsia, insônia ou para quem está parando de fumar. Além de possuir propriedades que inibem a vontade de comer.</p>
                        <h2 class="advertencias text-center">Advertências</h2>
                        <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                    </div>
                </div>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>