<?php

    $title       = "Composto Indicado para o Aumento do Apetite MAIS APETITE 60 cápsulas";
    $description = "O principal efeito do medicamento é a estimulação do apetite, mas a presença da Cobamamida é um grande diferencial deste suplemento..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Composto Indicado para o Aumento do Apetite MAIS APETITE 60 cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/composto-indicado-para-o-aumento-do-apetite-mais-apetite-60-capsulas.png" alt="composto-indicado-para-o-aumento-do-apetite-mais-apetite-60-capsulas" title="composto-indicado-para-o-aumento-do-apetite-mais-apetite-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>COMPOSTO INDICADO PARA O AUMENTO DO APETITE</h2>
                        <p class="text-justify">COBAMAMIDA + BUCLISINA: O principal efeito do medicamento é a estimulação do apetite, mas a presença da Cobamamida é um grande diferencial deste suplemento: os outros não costumam ter um componente anabolizante em sua fórmula. A Cobamamida aumenta a eficácia do metabolismo proteico hepático e melhora a síntese de moléculas no corpo como um todo. Além disso, pela Cobamamida não ser um anabolizante esteroidal, não traz nenhum dos efeitos indesejados destes, como a virilização, aumento de pilificação, espermatogênese prejudicada e assim por diante.</p>
                        <p class="text-justify">Esse suplemento tem vários efeitos em nosso corpo. Ele é antiemético (evita o vômito e reduz enjoo ou náuseas), além de ser orexígeno (o contrário de anorexígeno, ou seja, estimula a sua fome) e anti-histamínico (antialérgico). Por sinal, medicamentos anti-histamínicos costumam ser a primeira escolha dos médicos para estimular o ganho de peso. O Cloridrato de Buclisina ainda age como hipoglicemiante, ou seja, retira o açúcar do seu sangue. Isso, em nosso corpo, estimula nosso “centro da fome” no cérebro, fazendo com que nosso apetite aumente.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado.</p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>