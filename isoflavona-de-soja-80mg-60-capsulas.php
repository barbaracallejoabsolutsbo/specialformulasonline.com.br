<?php

    $title       = "Isoflavona De Soja 80mg 60 Cápsulas";
    $description = "Indicado para o tratamento sintomático e prevenção de complicações da menopausa como doença cardíaca e osteoporose. Apresenta propriedades..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Isoflavona De Soja 80mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/isoflavona-de-soja-80mg-60-capsulas.png" alt="isoflavona-de-soja-80mg-60-capsulas" title="isoflavona-de-soja-80mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>ISOFLAVONA DE SOJA</h2>
                        <p class="text-justify">Indicado para o tratamento sintomático e prevenção de complicações da menopausa como doença cardíaca e osteoporose. Apresenta propriedades imunoestimulantes e antioxidantes. São substâncias (fitormônios) presentes na soja. Seus principais constituintes são Genisteína, Genistina e Daidzeína, Daidzina os quais tem uma forte afinidade pelo receptor Beta de estrogênios. Desta maneira podem reduzir os sintomas clássicos da menopausa. Tem ação preventiva contra cânceres hormônio dependente como o de mama. Apresenta propriedades imunoestimulantes e antioxidantes. Estudos mostram que Isoflavonas aumentam a quantidade de cálcio retida nos ossos e assim previne contra a osteoporose.</p>
                        <p class="text-justify">Ação na Aterosclerose Os flavonóides podem inibir vários estágios relacionados ao início da aterosclerose, como ativação de leucócitos, adesão, agregação e secreção de plaquetas além de atividades hipolipidêmicas e aumento dos receptores de colesterol LDL. Estudos demonstraram que cerca de 60mg por dia de Isoflavonas podem prover proteção oxidativa pela modificação do colesterol LDL. As propriedades antioxidantes das Isoflavonas podem reduzir a peroxidação lipídica, contribuindo para a diminuição de risco de doenças cardiovasculares. Princípio Ativo Isoflavona consiste de um conjunto de substâncias naturais retiradas da soja, chamadas isoflavonas. Sua atividade é semelhante aos hormônios femininos, dentre os quais o genisteíne e o daidzeíne são considerados vitais para a saúde da mulher.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar.</p>
                <p class="advertencias text-justify">Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>