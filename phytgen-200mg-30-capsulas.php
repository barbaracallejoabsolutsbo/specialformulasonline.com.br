<?php

    $title       = "PhyTgen 200mg 30 Cápsulas";
    $description = "PhyTgen é a combinação tecnológica perfeita do mais potente ácido graxo da romã e a fração ativa da Alga Undaria Pinnatifida padronizado em alto..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">PhyTgen 200mg 30 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/phytgen-200mg-30-capsulas.png" alt="phytgen-200mg-30-capsulas" title="phytgen-200mg-30-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>PROMOVE A QUEIMA DE GORDURA</h2>
                        <p class="text-justify">PhyTgen é a combinação tecnológica perfeita do mais potente ácido graxo da romã e a fração ativa da Alga Undaria Pinnatifida padronizado em alto teor de Fucoxantina. Devido ao seu potente efeito termogênico, uma dose de PhyTgen queima até 400 kcal/dia mesmo sem atividade física.</p>
                        <p class="text-justify">Além disso, diversas pesquisas clínicas demonstram a excelente capacidade de redução do peso corporal após 6 semanas de uso. Os mecanismos antiobesidade de PhyTgen incluem a competição pela absorção de lipídeos no enterócitos, aumenta a oxidação dos ácidos graxos e diminui o conteúdo lipídico hepático, inibe enzimas lipogênicas hepáticas, estimula a expressão da proteína de desacoplamento (UCP-1) no tecido adiposo branco, promove a expressão do receptor 3-andrenérgico (responsável pela lipólise termogênese), reduz níveis plasmáticos de leptina e ainda apresenta capacidade antioxidante e anti-inflamatória muito potente.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Nunca compre medicamento sem orientação de um profissional habilitado. Imagens meramente ilustrativas. Pessoas com hipersensibilidade à substância não devem ingerir o produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o medicamento com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o medicamento se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Todo medicamento deve ser mantido fora do alcance das crianças.</p>
                <p class="advertencias text-justify">Este medicamento não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do medicamento durante o período de amamentação também não é recomendado. Este medicamento não deve ser utilizado por menores de 18 anos sem orientação médica. "Se persistirem os sintomas, o médico deverá ser consultado". "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia."</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>