<?php

    $title       = "CT-2 Colágeno Tipo II 40mg 30 Cápsulas";
    $description = "Duas vezes mais ativo do que a condroitina associada à glucosamina, o colágeno tipo II é a principal proteína estrutural na cartilagem, responsável pela sua..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">CT-2 Colágeno Tipo II 40mg 30 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/ct-2-Colageno-tipo-ll-40mg-30-capsulas.png" alt="ct-2-Colageno-tipo-ll-40mg-30-capsulas" title="ct-2-Colageno-tipo-ll-40mg-30-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>MAIS FLEXIBILIDADE</h2>
                        <p class="text-justify">Duas vezes mais ativo do que a condroitina associada à glucosamina, o colágeno tipo II é a principal proteína estrutural na cartilagem, responsável pela sua resistência, tração e firmeza. Derivado de cartilagem de frango, consiste em colágeno tipo II não desnaturado, que age juntamente com o sistema imunológico para manter as articulações saudáveis, promovendo sua mobilidade e flexibilidade, e prevenindo e reduzindo casos de inflamações e lesões. As articulações são as conexões existentes entre os ossos, que permitem a mobilidade e flexibilidade dos movimentos, ou seja, quadril, joelho, ombro, cotovelo, tornozelo, dedos, punho, entre outras.</p>
                        <p class="text-justify">A suplementação diária de colágeno tipo II é indicada para prevenir as doenças articulares em atletas, obesos e pessoas que tenham histórico familiar de algum problema nas articulações. O colágeno tipo II é capaz de dessensibilizar o sistema imunológico. Desta forma previne o “ataque” autoimune às nossas cartilagens. Por consequência há um bloqueio da inflamação, sem a qual o paciente não apresenta mais o quadro de dor. Quando o organismo não ataca mais exacerbadamente as cartilagens, é reestabelecida a homeostase entre quebra e síntese do colágeno tipo II, devolvendo a mobilidade e o conforto ao paciente, promovendo melhora da qualidade de vida</p>
                    </div>
                </div>
                <p class="text-justify">À medida que envelhecemos, nosso organismo diminui a produção de colágeno. Estudos estimam que, a partir dos 30 anos de idade, ocorre uma redução de 1% na produção anual dessa proteína. Para mulheres que passaram pela menopausa, essa taxa pode atingir valores de até 2%. O colágeno tipo II é essencial no tratamento de doenças que atingem as articulações.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>