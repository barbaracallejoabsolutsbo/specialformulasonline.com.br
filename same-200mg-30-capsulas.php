<?php

    $title       = "Same 200Mg 30 Cápsulas";
    $description = "Visando garantir a qualidade dos produtos que necessitam de refrigeração, faremos envios exclusivamente de segunda-feira a quarta-feira..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Same 200Mg 30 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/same-200mg-30-capsulas.png" alt="same-200mg-30-capsulas" title="same-200mg-30-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>SAME 200MG 30 CÁPSULAS</h2>
                        <p class="text-justify">Visando garantir a qualidade dos produtos que necessitam de refrigeração, faremos envios exclusivamente de segunda-feira a quarta-feira. Agradecemos a compreensão. "O same é potencialmente um dos mais eficientes suplementos para tratamentos de artrite, doenças do fígado e uma série de outras doenças prevalentes na idade avançada, inclusive depressão. Pessoas que sofrem desses sintomas deveriam ter conhecimento sobre os múltiplos benefícios do S-adenosil-L-metionina"</p>
                        <p class="text-justify">SAME (S-Adenosil-L-Metionina) é uma substância que atua como uma vitamina para impulsionar o seu humor e ajuda a manter a saúde das Articulações e a função do fígado. Uma maneira natural para ajudar você a se sentir melhor, física e emocionalmente, same é uma substância segura e eficaz que existe em todas as células no corpo. Essencial para a produção dos químicos cerebrais, neurotransmissores e hormônios, same exerce um importante papel em como você se sente.</p>
                    </div>
                </div>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>