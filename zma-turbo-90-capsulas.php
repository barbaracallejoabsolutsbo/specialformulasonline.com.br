<?php

    $title       = "Zma Turbo 90 Cápsulas";
    $description = "O ZMA é um suplemento que tem em sua fórmula a combinação de zinco, magnésio e vitamina B6. Diferentes elementos químicos auxiliam na rotina..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Zma Turbo 90 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/zma-turbo-90-capsulas.png" alt="zma-turbo-90-capsulas" title="zma-turbo-90-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>ZMA 90 CÁPSULAS</h2>
                        <p class="text-justify">O ZMA é um suplemento que tem em sua fórmula a combinação de zinco, magnésio e vitamina B6. Diferentes elementos químicos auxiliam na rotina do nosso organismo. A demanda por eles aumenta ainda mais no caso de quem pratica exercícios intensos. Durante os treinos, o desgaste físico e a energia gasta para a execução das séries demandam ainda mais força do corpo.</p>
                        <p class="text-justify">O ZMA tem como objetivo, promover um aumento na produção de hormônios, que são precursores para a síntese de testosterona. O hormônio testosterona tem influência no sistema nervoso e no músculo esquelético, e quando produzido em boas quantidades tem efeito anabólico sobre os músculos, auxiliando diretamente no processo de hipertrofia, gera força, resistência e massa muscular. O ZMA é uma combinação perfeita, que difere dos produtos encontrados no mercado. Uma das carências nutricionais mais comuns entre adultos saudáveis é a falta de magnésio e de zinco.</p>
                    </div>
                </div>
                <h2>Composição</h2>
                <p class="text-justify">Cada 1 Cápsula contém:</p>
                <ul>
                    <li>Zinco* 10mg</li>
                    <li>Mágnésio Aspartato 150mg</li>
                    <li>Vitamina B6 3,5mg</li>
                    <li>Excipiente** q.s.p. 1 cápsula</li>
                    <li>Equivale a 50mg de zinco quelato</li>
                    <li>Amido, talco, estearato de magnésio, dióxido de silício coloidal</li>
                </ul>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>