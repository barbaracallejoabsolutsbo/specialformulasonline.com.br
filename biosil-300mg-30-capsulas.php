<?php

    $title       = "Biosil 300Mg 30 Cápsulas";
    $description = "BioSil™ é a marca comercial e registrada da patente da molécula ch-OSA®, um complexo único de ácido ortosilícico estabilizado em colina com ações benéficas"; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Biosil 300Mg 30 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/biosil-300mg-30-capsulas.png" alt="biosil-300mg-30-capsulas" title="biosil-300mg-30-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>BIOSIL 300MG</h2>
                        <p class="text-justify">BioSil é a marca comercial e registrada da patente da molécula ch-OSA®, um complexo único de ácido ortosilícico estabilizado em colina com ações benéficas, comprovadas em estudos, na saúde da pele, cabelos, unhas, ossos e articulações, através da ativação das vias de produção do colágeno. A estabilização por colina é a tecnologia mais avançada conhecida atualmente. Esse processo vai muito além de uma simples mistura de ingredientes; as moléculas de OSA e colina foram complexadas, de modo a garantir a estabilidade e a biodisponibilidade deste ingrediente inovador.</p>
                        <p class="text-justify">A complexação dessas moléculas dá origem a um produto líquido à temperatura ambiente, de difícil manipulação, pH extremamente ácido e baixa estabilidade. Ainda assim, a mistura deve ser mantida na forma líquida. Convertê-la para uma mistura sólida (pó) vai mudar sua estrutura química tornando-a ineficaz. Podemos assim afirmar que o ácido ortosilícico estabilizado em colina não pode ser apresentado na forma de pó. BioSil™, através da tecnologia ch-OSA® consegue transformar o produto líquido em beadlets, ou seja, um granulado estável, que mantém o líquido revestido, ideal para manipulação de cápsulas. Quando os beadlets são ingeridos, o líquido ch-OSA® será prontamente absorvido pelo trato gastrointestinal e aí sim terá total eficácia. Essa formulação é patenteada e exclusivamente comercializada sob a marca BioSil™, há mais de 20 anos!</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>