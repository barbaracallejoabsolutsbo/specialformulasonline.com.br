<?php

    $title       = "Passiflora (Maracujá) 200Mg 60 Cápsulas";
    $description = "A passiflora é mais conhecida por seu fruto, o maracujá. Ela possui boas quantidades de flavonoides. Eles apresentam vários efeitos biológicos..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Passiflora (Maracujá) 200Mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/passiflora-maracuja-200mg-60-capsulas.png" alt="passiflora-(maracuja)-200mg-60-capsulas" title="passiflora-(maracuja)-200mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">A passiflora é mais conhecida por seu fruto, o maracujá. Ela possui boas quantidades de flavonoides. Eles apresentam vários efeitos biológicos e farmacológicos, incluindo atividade antibacteriana, antiviral, anti-inflamatória, antialérgica e vasodilatadora. Além disso, estas substâncias inibem a peroxidação lípidica e reduzem o risco de doenças cardiovasculares, efeitos estes relacionados à sua atividade antioxidante, caracterizada pela capacidade de sequestrar radicais livres em organismos vivos.</p>
                        <br>
                        <h2>BENEFÍCIOS</h2>
                        <p class="text-justify">Ação calmante: A passiflora estimula a liberação de hormônios que levam ao relaxamento e sensação de bem-estar. Previne a depressão: A passiflora ajuda a evitar a depressão devido à sensação de bem-estar que ela proporciona. Melhora a concentração: Ao proporcionar maior sensação de bem-estar e relaxamento, a pessoa consegue alcançar maior estado de concentração. Boa contra infecções. Boa contra a doença de Parkinson. Controla a pressão arterial: As harmalas, substâncias presentes na passiflora inibem o consumo excessivo e desnecessário de oxigênio pelo cérebro.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>