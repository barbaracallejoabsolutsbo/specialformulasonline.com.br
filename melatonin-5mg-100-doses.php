<?php

    $title       = "Melatonin 5Mg 100 Doses";
    $description = "A Melatonin traz inúmeros benefícios à saúde, tendo como principal induzir ao sono e proporcionar um descanso tranquilo e revigorante..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Melatonin 5Mg 100 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/melatonin-5mg-100-doses.png" alt="melatonin-5mg-100-doses" title="melatonin-5mg-100-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>DURMA BEM</h2>
                        <p class="text-justify">A Melatonin traz inúmeros benefícios à saúde, tendo como principal induzir ao sono e proporcionar um descanso tranquilo e revigorante. Também possui atividade antioxidante, ou seja, combate os radicais livres e o envelhecimento precoce, além de ajudar na recuperação dos neurônios afetados pela doença de Alzheimer, episódios de isquemia cerebral e epilepsia.</p>
                    </div>
                </div>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>