<?php

    $title       = "Anti-Queda Capilar + Novos Fios 120Ml";
    $description = "O Sérum Anti-queda + Novos fios, da Special Fórmulas, possuem em sua composição o Cooper Peptídeo que estimula o crescimento capilar."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Anti-Queda Capilar + Novos Fios 120Ml</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/anti-queda-capilar-novos-fios-120ml.png" alt="anti-queda-capilar-novos-fios-120ml" title="anti-queda-capilar-novos-fios-120ml">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>ANTI-QUEDA CAPILAR + NOVOS FIOS</h2>
                        <p class="text-justify">O Sérum Anti-queda + Novos fios, da Special Fórmulas, possuem em sua composição o Cooper Peptídeo que estimula o crescimento capilar, a circulação sanguínea e o surgimento de novos fios de cabelo.</p>
                        <br>
                        <h2>COMO ATUA NO CORPO</h2>
                        <p class="text-justify">Os fatores de crescimento VEGF e AFGF aceleram o metabolismo folicular e o crescimento de novos vasos sanguíneos. O Follicusan age na prevenção das desordens funcionais do couro cabeludo e normaliza a secreção sebácea, evitando a formação de caspa. O Sérum Anti-queda + Novos fios, da Special Fórmulas, foi desenvolvido com ativos selecionados para auxiliar o crescimento capilar, estimulando novos fios de cabelo e a circulação sanguínea.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>