<?php

    $title       = "Testosterona";
    $description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elementum ultrices commodo. Donec quis dictum tortor."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "home",
        "produtos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="container">
            <section class="nossos-produtos catego-prod container">
                <h1 class="text-center">Testosterona</h1>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/tribulus-terrestris-500mg-120-capsulas-40-saponinas.png" alt="tribulus-terrestris-500mg-120-capsulas-(40-saponinas)" title="tribulus-terrestris-500mg-120-capsulas-(40-saponinas)" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Tribulus Terrestris 500Mg 120 Cápsulas (40% saponinas)</h3>
                            <p>O Tribulus Terrestris é utilizado principalmente...</p>
                            </div>
                            <a class="btn-entrectt" href="tribulus-terrestris-500mg-120-capsulas-40-saponinas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/tadalafil-10mg-60-doses.png" alt="tadalafil-10mg-60-doses" title="tadalafil-10mg-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Tadalafil 10Mg 60 Doses</h3>
                            <p>Definição da Disfunção Erétil: quando existe um estímulo sexual...</p>
                            </div>
                            <a class="btn-entrectt" href="tadalafil-10mg-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/tadalafil-com-arginina-60-doses.png" alt="tadalafil-com-arginina-60-doses" title="tadalafil-com-arginina-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Tadalafil Com Arginina 60 Doses</h3>
                            <p>O Tadalafil com Arginina irá lhe proporcionar um bombeamento muscular...</p>
                            </div>
                            <a class="btn-entrectt" href="tadalafil-com-arginina-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/tribulus-terrestris-500mg-60-doses-40-saponinas.png" alt="tribulus-terrestris-500mg-60-doses-(40-saponinas)" title="tribulus-terrestris-500mg-60-doses-(40-saponinas)" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Tribulus Terrestris 500Mg 60 Doses (40% saponinas)</h3>
                            <p>O Tribulus Terrestris é utilizado principalmente...</p>
                            </div>
                            <a class="btn-entrectt" href="tribulus-terrestris-500mg-60-doses-40-saponinas.php">Saiba +</a>
                        </div>
                    </div>
            </section>
        </div>    
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>