<?php

    $title       = "Morosil 500Mg 30 Doses";
    $description = "Obtido a partir do suco de laranjas vermelhas Moro, o Morosil 500mg 30 doses possui uma poderosa atividade antioxidante..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Morosil 500Mg 30 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/morosil-500mg-30-doses.png" alt="morosil-500mg-30-doses" title="morosil-500mg-30-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>MOROSIL 500MG 30 DOSES COM SELO DE AUTENTICIDADE</h2>
                        <p class="text-justify">Obtido a partir do suco de laranjas vermelhas Moro, o Morosil 500mg 30 doses possui uma poderosa atividade antioxidante, o que o torna um grande aliado a dietas emagrecedoras e, segundo estudos, é capaz de eliminar até 50% da gordura localizada em menos de três meses. Também diminui consideravelmente os níveis de triglicerídeos e colesterol, contribuindo para a redução da gordura no fígado e agindo em prol da saúde cardíaca.</p>
                        <p class="text-justify">Além da sua poderosa atividade antioxidante, o ativo contém uma elevada concentração de vitamina C, flavonoides e ácidos hidroxicinâmicos, que juntos trazem inúmeros benefícios para o organismo. A vitamina C é muito importante para o corpo humano, pois está relacionada com a produção de colágeno, absorção de ferro e ao aumento da imunidade. Os flavonoides tem suma importância no controle e prevenção de doenças cardiovasculares. Os ácidos hidroxicinâmicos são compostos fenólicos que modulam a defesa antioxidante em decorrência da inflamação. São encontrados em abundância em frutas e são associados à redução e prevenção do risco de problemas cardiovasculares e outras enfermidades crônicas.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO". "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>