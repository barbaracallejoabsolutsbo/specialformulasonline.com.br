<?php

    $title       = "Imune Of Multivitaminico 60 Cáps";
    $description = "Beta-caroteno melhor conhecido como um precursor da vitamina A, é um importante antioxidante que ajuda a prevenir danos de radicais livres prejudiciais no corpo"; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Imune Of Multivitaminico 60 Cáps</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/imune-of-multivitaminico-60-caps.png" alt="imune-of-multivitaminico-60-caps" title="imune-of-multivitaminico-60-caps">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>Descrição</h2>
                        <p class="text-justify">O Multivitamínico Imune OF da Oficial Farma tem uma fórmula abrangente de alta potência que fornece vitaminas e minerais essenciais necessários para a melhor saúde. É formulado com nutrientes cuidadosamente selecionados para maximizar seus benefícios. Combina uma série completa de nutriente, com a conveniência de um por dia. O multivitamínico Imune OF ajuda a ter mais energia pois possui vitaminas B2, B12, B6, niacina, biotina, ácido pantotênico e ferro, que ajudam o organismo a produzir energia e auxilia no fortalecimento de unhas e cabelos. Além disso, possui vitamina C, selênio e zinco que fortalece o sistema imunológico, Vitaminas D e K, além do Cálcio para a saúde dos ossos e vitamina A que contribui para a saúde da pele.</p>
                        <br>
                        <h2>COMO AGE NO CORPO</h2>
                        <p class="text-justify">Possui também minerais que são de extrema importância para o bom funcionamento de todo o nosso organismo como o manganês, o molibdênio e o magnésio.</p>
                    </div>
                </div>
                <h2>Composição</h2>
                <p class="text-justify">Cada 1 cápsula contém:</p>
                <ul>
                    <li>Cromo 18mcg</li>
                    <li>Ferro 8,1mg</li>
                    <li>Magnésio 50mg</li>
                    <li>Zinco 7mg</li>
                    <li>Iodo 33mcg</li>
                    <li>Cobre 450mcg</li>
                    <li>Selênio 20mcg</li>
                    <li>Molibdênio 23mcg</li>
                    <li>Manganês 1,2mcg</li>
                    <li>Vitamina A 400mcg</li>
                    <li>Vitamina C 45mg</li>
                    <li>Vitamina D3 5mcg</li>
                    <li>Vitamina E 6,7mg</li>
                    <li>Vitamina B1 1,2mg</li>
                    <li>Vitamina B2 1,3mg</li>
                    <li>Vitamina B6 1,3mg</li>
                    <li>Vitamina B12 2,4mcg</li>
                    <li>Niacina 16mg</li>
                    <li>Ácido pantotênico 5mg</li>
                    <li>Biotina 30mcg</li>
                    <li>Vitamina K 65mcg</li>
                    <li>Ácido fólico 240mcg</li>
                    <li>Cálcio carbonato 180mg</li>
                    <li>Excipiente* q.s.p. 1 cápsula</li>
                    <li>Amido, estearato de magnésio</li>
                </ul>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado.</p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>