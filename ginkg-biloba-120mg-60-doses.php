<?php

    $title       = "Ginkgo Biloba 120Mg 60 Doses";
    $description = "Ginkgo biloba é um derivado fitoterápico obtido da árvore Salisburia adiantifolia. Contém diversos princípios ativos como terpenos, pró-antocianidinas..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Ginkgo Biloba 120Mg 60 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/ginkg-biloba-120mg-60-doses.png" alt="ginkg-biloba-120mg-60-doses" title="ginkg-biloba-120mg-60-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>UM ALIADO PARA SUA SAÚDE</h2>
                        <p class="text-justify">Ginkgo biloba é um derivado fitoterápico obtido da árvore Salisburia adiantifolia. Contém diversos princípios ativos como terpenos, pró-antocianidinas e glicosídeos flavonídicos, que atuam no sistema circulatório e no metabolismo celular. Ginkgo Biloba tem ação preventiva e curativa contra as agressões endógenas e exógenas, tais como fenômeno de oxidação devido à presença de radicais livres; ação antiinflamatória e de prevenção do envelhecimento. Estimula a circulação sanguínea, atuando na circulação arterial, venosa e capilar, agindo na insuficiência vascular periférica. É protetora da barreira hematoencefálica.</p>
                        <p class="text-justify">Também diminui a hiper agregação plaquetária, atuando em processos trombóticos; diminui a agregação das hemácias e tem ainda uma ação protetora contra a lise de eritrócitos. Regulariza a permeabilidade capilar, age inibindo a hiper permeabilidade mediada pela bradicinina e histamina. A nível cerebral permite a diminuição das desordens da memória, distúrbios de atenção, diminuição de casos de vertigens, preservando por mais tempo autonomia e qualidade de vida, e também previne o edema cerebral.</p>
                    </div>
                </div>
                <p class="text-justify">Desta maneira, seu uso é indicado no tratamento de micro varizes, úlceras varicosas, artrite dos membros inferiores; tratamento de toda isquemia seja cerebral ou periférica; utilizado em vertigens, deficiências auditivas, perda de memória e dificuldade de concentração; e em tratamento nos processos vasculares degenerativos. Pela sua ação protetora contra radicais livres e pela inibição da destruição do colágeno, é utilizado também no tratamento profilático do envelhecimento celular e tratamento estético.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pode ocorrer alguns efeitos colaterais como distúrbios gastrintestinais, transtornos circulatórios incluindo queda de pressão arterial, cefaleia ou reações cutâneas. Imagens meramente ilustrativas. Pessoas com hipersensibilidade à substância não devem ingerir o produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Este produto não deve ser utilizado por gestantes e lactantes sem orientação médica. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Siga corretamente o modo de usar.</p>
                <p class="advertencias text-justify">Não desaparecendo os sintomas, procure orientação médica. Todo medicamento deve ser mantido fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>