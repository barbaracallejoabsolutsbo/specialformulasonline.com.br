<?php

    $title       = "Ácido D-Aspártico 1000Mg 30 Doses";
    $description = "O Ácido D-Aspártico 1000mg é um aminoácido, que tem papel importante na síntese de hormônios (testosterona e GH), pois atua no cérebro e no sistema endócrino..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Ácido D-Aspártico 1000Mg 30 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/acido-d-aspartico-1000mg-30-doses.png" alt="acido-d-aspartico-1000mg-30-doses" title="acido-d-aspartico-1000mg-30-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">O Ácido D-Aspártico 1000mg é um aminoácido, que tem papel importante na síntese de hormônios (testosterona e GH), pois atua no cérebro e no sistema endócrino. Ele foi desenvolvido com propriedades que aumentam a testosterona, e consequentemente aumenta o ganho de massa muscular e queima o excesso de gordura. Esse ácido também é indicado no tratamento da fadiga crônica e da depressão, pois promove o aumento da produção de energia e da excitação neuronal. Também estimula a imunidade, através do aumento da diferenciação celular.</p>
                        <br>
                        <h2>AÇÃO</h2>
                        <p class="text-justify">Ácido D-aspártico é um aminoácido endógeno, formado através da conversão do Ácido L-aspártico em Ácido D-aspártico pela enzima racemase D-aspartato, que realiza essa conversão. Este aminoácido tem papel importante na síntese de hormônios, pois atua no cérebro e no sistema endócrino diretamente como neurotransmissor e neuro secretor. Ácido D-aspártico é sintetizado pelo corpo e enviado para os locais em que se concentra melhor. Ele age sobre o sistema endocrinológico pela regulação, síntese e secreção hormonal da testosterona e do hormônio do crescimento. A substância é essencial na síntese de proteínas pelos músculos.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. O uso do produto durante o período de amamentação sem orientação médica, também não é recomendado.</p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. Nunca compre medicamentos sem orientação de um profissional habilitado. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>