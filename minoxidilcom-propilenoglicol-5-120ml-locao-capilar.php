<?php

    $title       = "MinoxidilCom Propilenoglicol 5% 120Ml - Loção Capilar";
    $description = "O Minoxidil 5% Com Propilenoglicol é uma fórmula desenvolvida pela Special Fórmulas que combina a eficiência do Minoxidil e a alta permeabilidade..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">MinoxidilCom Propilenoglicol 5% 120Ml - Loção Capilar</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/minoxidilcom-propilenoglicol-5-120ml-locao-capilar.png" alt="minoxidilcom-propilenoglicol-5-120ml-locao-capilar" title="minoxidilcom-propilenoglicol-5-120ml-locao-capilar">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>MINOXIDIL 5% COM PROPILENOGLICOL</h2>
                        <p class="text-justify">O Minoxidil 5% Com Propilenoglicol é uma fórmula desenvolvida pela Special Fórmulas que combina a eficiência do Minoxidil e a alta permeabilidade do Propilenoglicol. O Minoxidil 5% com Propilenoglicol é uma loção capilar que ajuda a revitalizar a raiz do cabelo e normalizar o ciclo do folículo, prolongando a fase anágena. Ele também tem a função de estimular a vascularização do couro cabeludo permitindo a oxigenação da área.</p>
                        <p class="text-justify">O Propilenoglicol permite que o Minoxidil aplicado seja distribuído uniformemente na área do couro cabeludo e seja absorvido facilmente pela pele. Recomenda-se passar o produto sempre nos cabelos secos. É importante salientar que o produto deve ser passado no couro cabeludo, e não nos fios. Ele deve ser absorvido pelos poros para agir na raiz dos cabelos. O tratamento deve ser contínuo. Uma vez interrompida a medicação, a calvície volta ao estado anterior ao tratamento em aproximadamente dois meses. A ação é principalmente na região parietal do couro cabeludo ou coroa, estabilizando a calvície, postergando sua evolução e estimulando cabelos que estão enfraquecidos ou finos transformando em cabelos mais grossos, em alopecias em estágio inicial.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="https://web.whatsapp.com/send?phone=5511944930219&text=Ol%C3%A1%21%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20da%20Exaustyair%20-%20Sistemas%20de%20Exaust%C3%A3o%20e%20Ventila%C3%A7%C3%A3o%20Industrial"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>