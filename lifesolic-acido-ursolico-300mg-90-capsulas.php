<?php

    $title       = "LifeSolic (Ácido Ursólico) 300mg 90 Cápsulas";
    $description = "O composto bioativo do Lifesolic é 100% natural, encontrado em alguns tipos de frutas, e ganhou fama depois de ter seus efeitos comprovados..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">LifeSolic (Ácido Ursólico) 300mg 90 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/lifesolic-acido-ursolico-300mg-90-capsulas.png" alt="lifesolic-(acido-ursolico)-300mg-90-capsulas" title="lifesolic-(acido-ursolico)-300mg-90-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>DIVERSOS BENEFÍCIOS</h2>
                        <p class="text-justify">O composto bioativo do Lifesolic é 100% natural, encontrado em alguns tipos de frutas, e ganhou fama depois de ter seus efeitos comprovados por pesquisadores americanos. Dentre esses efeitos podemos destacar a inibição do acúmulo de gordura corporal, a redução da resistência à ação da insulina por meio do Fator de crescimento, atenuação da atrofia muscular, ação anti-câncer, ação antioxidante, e efeitos anti-inflamatórios.</p>
                        <p class="text-justify">O composto bioativo do Lifesolic, o ácido ursólico, mostra-se promissor no combate ao câncer, devido a uma capacidade de suprimir o crescimento de novos vasos sanguíneos que geralmente levam as células de câncer a crescerem e se multiplicarem através da divisão celular. Devido esses efeitos do Lifesolic no favorecimento ao crescimento muscular, já encontramos uma forma saudável de tratar a atrofia muscular e para quem busca o remodelamento muscular.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>