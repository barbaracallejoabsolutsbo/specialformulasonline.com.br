<?php

    $title       = "Magnésio 200Mg 60 Doses";
    $description = "O Magnésio é um mineral essencial para os processos enzimáticos. Atua principalmente na produção de energia.
As principais fontes alimentares de Magnésio..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Magnésio 200Mg 60 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/magnesio-200mg-60-doses.png" alt="magnesio-200mg-60-doses" title="magnesio-200mg-60-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>MAGNÉSIO 200MG</h2>
                        <p class="text-justify">O Magnésio é um mineral essencial para os processos enzimáticos. Atua principalmente na produção de energia. As principais fontes alimentares de Magnésio são os cereais integrais, vegetais folhosos verdes, espinafre, nozes, frutas, legumes e tubérculos, como a batata. A suplementação de Magnésio contribui para muitos sistemas do organismo, em especial músculos e nervos. O Magnésio atua como cofator em mais de 300 reações metabólicas, desempenhando papel fundamental no metabolismo da glicose, na homeostase insulínica e glicêmica; na síntese de adenosina trifosfato, proteínas e ácidos nucleicos.</p>
                        <p class="text-justify">O Magnésio quelato (Bisglicinato) possui ligação com duas moléculas de glicina (aminoácido), o que potencializa seus benefícios proporcionando uma maior absorção e aproveitamento do Magnésio pelo organismo. O Magnésio é importante no processo de metabolização do Cálcio, da Vitamina C, Fósforo, Sódio e Potássio, sendo muito utilizado como suplemento por pessoas que não conseguem suprir suas necessidades diárias desse composto através da ingestão alimentar.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado.</p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>