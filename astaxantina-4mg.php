<?php

    $title       = "Astaxantina 4Mg 60 Cápsulas";
    $description = "Ao contrário de muitos outros antioxidantes, astaxantina não se torna pró-oxidante no corpo. Isso a torna um dos antioxidantes mais potentes e eficazes."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Astaxantina 4Mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/astaxantina-4mg.png" alt="astaxantina-4mg" title="astaxantina-4mg">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">Ao contrário de muitos outros antioxidantes, astaxantina não se torna pró-oxidante no corpo. Isso a torna um dos antioxidantes mais potentes e eficazes que se pode ingerir. A astaxantina exibe uma alta atividade de eliminação de radicais livres e protege as células, órgãos e tecidos do corpo dos danos oxidativos.</p>
                        <br>
                        <h2>ALIADO DO CORAÇÃO</h2>
                        <p class="text-justify">Essa função antioxidante da astaxantina acarreta uma impressionante variedade de benefícios para a saúde, incluindo proteção do sistema cardiovascular, estabilização do açúcar no sangue, melhora do sistema imunológico, redução de inflamações, proteção da saúde dos olhos e da pele, e pode até mesmo ter um futuro no tratamento do câncer.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>