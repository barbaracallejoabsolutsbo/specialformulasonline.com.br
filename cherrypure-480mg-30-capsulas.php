<?php

    $title       = "CherryPURE 480mg 30 Cápsulas";
    $description = "A CherryPure foi desenvolvida com propriedades antioxidantes, ou seja, contribui para a redução do estresse oxidativo, além de modular os níveis..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">CherryPURE 480mg 30 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/cherrypure-480mg-30-capsulas.png" alt="cherrypure-480mg-30-capsulas" title="cherrypure-480mg-30-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>REDUZ O ESTRESSE</h2>
                        <p class="text-justify">A CherryPure foi desenvolvida com propriedades antioxidantes, ou seja, contribui para a redução do estresse oxidativo, além de modular os níveis de cortisol, favorecendo a redução do catabolismo muscular. Também auxilia na recuperação e no aumento da resistência e na amenização dos danos sobre o tecido muscular.</p>
                        <br>
                        <h2>EXERCÍCIOS DE ALTA INTENSIDADE</h2>
                        <p class="text-justify">Rica em compostos fenólicos, antocianinas e flavonoides, a CherryPure ajuda a controlar a inflamação em exercícios de alta intensidade, sem comprometer o desenvolvimento e adaptação ao exercício. O produto também possui ação antioxidante, contribuindo para a redução do estresse oxidativo sobre o tecido muscular, além de modular os níveis de cortisol, favorecendo a redução do catabolismo.</p>
                    </div>
                </div>
                <h2>SONO</h2>
                <p class="text-justify">Além de todos benefícios voltados para o desempenho físico mencionados acima, O produto é um poderoso aliado do sono, pois através da ação de seus fitos componentes e de suas propriedades anti-inflamatórias e antioxidantes, reduz alguns dos fatores associados à insônia.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado.</p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>