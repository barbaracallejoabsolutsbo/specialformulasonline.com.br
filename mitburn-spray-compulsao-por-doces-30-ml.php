<?php

    $title       = "Mitburn® Spray Compulsão por Doces 30 ml";
    $description = "o spray compulsão por doces – mitburn foi desenvolvido com propriedades para diminuir a vontade de comer doces, ou seja, quando usado..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Mitburn® Spray Compulsão por Doces 30 ml</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/mitburn-spray-compulsao-por-doces-30-ml.png" alt="mitburn-spray-compulsao-por-doces-30-ml" title="mitburn-spray-compulsao-por-doces-30-ml">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>SPRAY COMPULSÃO POR DOCES – MITBURN®</h2>
                        <p class="text-justify">O spray compulsão por doces – mitburn foi desenvolvido com propriedades para diminuir a vontade de comer doces, ou seja, quando usado, reduz o efeito do sabor, sem interferir na sensação dos outros sabores, como salgado e o ácido. contribui também no aumento da metabolização, auxiliando na perda de gordura corporal.</p>
                        <br>
                        <h2>METABOLISMO ENERGÉTICO</h2>
                        <p class="text-justify">O MitBurn contribui para a lipólise, para o aumento do número mitocondrial, realiza beta-oxidação que ocorre no DNA mitocondrial, termogênese e conversão de lipídeos. Com o aumento na formação de novas mitocôndrias, o excesso de tecido adiposo que se acumula na circunferência abdominal diminui. Atua também na melhora do metabolismo energético, reduzindo a quantidade de gordura corporal.</p>
                    </div>
                </div>
                <h2>REDUZ O SABOR DOCE</h2>
                <p class="text-justify">A Tintura de Gymnema, presente na formulação, diminui a compulsão por doces, auxiliando no controle da glicemia e na perda de peso. Tem ação adstringente, estomáquica, tônica e refrescante. Possui também ação estimulante na produção de insulina. O efeito de reduzir o sabor doce dura de 1 a 2 horas, não interferindo na sensação de outros sabores como o salgado, o ácido e o adstringente. O Spray compulsão por doces – MitBurn foi desenvolvido com propriedades para diminuir a vontade de comer doces, ou seja, quando usado, reduz o efeito do sabor. Contribui também no aumento da metabolização, auxiliando na perda de gordura corporal.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado.</p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>