<?php

    $title       = "Colágeno Hidrolisado 500Mg 60 Doses";
    $description = "Um organismo saudável necessita de colágeno para a manutenção do tônus muscular e de uma pele firme. O colágeno é a principal proteína do corpo..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Colágeno Hidrolisado 500Mg 60 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/colageno-hidrolisado-500mg-60-doses.png" alt="colageno-hidrolisado-500mg-60-doses" title="colageno-hidrolisado-500mg-60-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>COLÁGENO HIDROLISADO</h2>
                        <p class="text-justify">Um organismo saudável necessita de colágeno para a manutenção do tônus muscular e de uma pele firme. O colágeno é a principal proteína do corpo que garante a coesão, elasticidade e regeneração da pele, cartilagem e ossos. O chamado colágeno hidrolisado é um tipo especial de gelatina. O que o diferencia da gelatina tradicional é a sua característica de não formar gel, isso porque aplica-se um maior grau de hidrólise na sua extração. Essa peculiaridade do colágeno hidrolisado o torna mais fácil de ser consumido, já que é solúvel a frio e poder ser misturado a vários tipos de alimentos, sem alterar sabor, odor ou textura.</p>
                        <p class="text-justify">O colágeno ingerido por via oral permite que o nosso organismo mantenha uma quantidade de massa muscular adequada, ajudando-o a utilizar eficientemente suas reservas lipídicas e de açúcar. Além disso, é um eficiente aliado contra processos de flacidez tecidual e, quando aliado à atividade física, torna-se uma excelente fonte proteica, capaz de sintetizar massa magra, mantendo assim o aspecto jovial do nosso corpo.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Imagens meramente ilustrativas. Pessoas com hipersensibilidade à substância não devem ingerir o produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Este produto não deve ser utilizado por gestantes e lactantes sem orientação médica. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Siga corretamente o modo de usar.</p>
                <p class="advertencias text-justify">Não desaparecendo os sintomas, procure orientação médica. Todo medicamento deve ser mantido fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>