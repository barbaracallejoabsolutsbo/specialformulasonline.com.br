<?php

    $title       = "Betacaroteno 50Mg 30 Cápsulas";
    $description = "O Ferro é um mineral encontrado fundamentalmente no sangue. O Ferro é responsável, em grande parte, pela assimilação do oxigênio pelas células..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Ferro 18Mg 90 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/ferro-18mg-90-capsulas.png" alt="ferro-18mg-90-capsulas" title="ferro-18mg-90-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>FERRO</h2>
                        <p class="text-justify">O Ferro é um mineral encontrado fundamentalmente no sangue. O Ferro é responsável, em grande parte, pela assimilação do oxigênio pelas células, promovendo sua utilização no processo vital das mesmas. Quase todo Ferro que existe no corpo está combinado com proteínas, seja no transporte, no armazenamento, nas enzimas ou nos componentes respiratórios. Para que ocorra a assimilação do Ferro, são necessárias as presenças de Cobalto, Cobre, Manganês e da Vitamina C. É importante na metabolização das vitaminas do complexo B, ajuda no crescimento, evita a fadiga e anemia, promove uma tonicidade de pele sadia.</p>
                        <h2 class="advertencias text-center">Advertências</h2>
                        <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar.</p>
                    </div>
                </div>
                <p class="advertencias text-justify">Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>