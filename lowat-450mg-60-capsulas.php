<?php

    $title       = "Lowat 450mg 60 Cápsulas";
    $description = "Lowat é um produto seguro e eficaz na perda de peso, derivado de duas plantas tradicionais: folhas de Piper betle e sementes de Dolichos biflorus..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Lowat 450mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/lowat-450mg-60-capsulas.png" alt="lowat-450mg-60-capsulas" title="lowat-450mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>LOWAT 450MG</h2>
                        <p class="text-justify">Lowat é um produto seguro e eficaz na perda de peso, derivado de duas plantas tradicionais: folhas de Piper betle e sementes de Dolichos biflorus. Lowat tem como alvo o tecido adiposo, diminui a formação de adipócitos e o acúmulo de gordura e estimula a queima de gordura. A obesidade está associada a um desequilíbrio entre ingestão alimentar e gasto energético, e é caracterizada pelo acúmulo excessivo de gordura corporal no indivíduo.</p>
                        <p class="text-justify">A obesidade é fator de risco para uma série de doenças como: hipertensão, diabetes tipo 2, problemas cardiovasculares, entre outras. Para diagnóstico em adultos, o parâmetro utilizado mais comumente é o de índice de massa corporal (IMC). O IMC é calculado dividindo-se o peso do paciente pela sua altura elevada ao quadrado.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado. </p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>