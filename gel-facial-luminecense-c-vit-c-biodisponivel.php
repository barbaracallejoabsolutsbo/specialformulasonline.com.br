<?php

    $title       = "Gel Facial LumineCense® C Vit C Biodisponível";
    $description = "LumineCense® é uma molécula desenvolvida no Japão única, que assegura uma vitamina C 100% estabilizada em solução aquosa extraída das geleiras do Monte Fuji..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Gel Facial LumineCense® C/ Vit C Biodisponível - 30g</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/gel-facial-luminecense-vit-c-biodisponivel-30g.png" alt="gel-facial-luminecense-vit-c-biodisponivel-30g" title="gel-facial-luminecense-vit-c-biodisponivel-30g">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>GEL FACIAL LUMINECENSE</h2>
                        <p class="text-justify">LumineCense® é uma molécula desenvolvida no Japão única, que assegura uma vitamina C 100% estabilizada em solução aquosa extraída das geleiras do Monte Fuji, que é considerada uma das águas mais puras do mundo. De acordo com estudos na pele, acelera a cicatrização, promove viço, auxilia na hidratação, equilibra o pH da pele, e também reconstitui a barreira de proteção cutânea, deixando a pele mais saudável e protegida de agressores externos como a poluição e o vento. Apresenta uma potente capacidade umectante, antibacteriana e hidratante, que não resseca a pele. Apresenta maior absorção na derme em comparação com a vitamina C. LumineCense® inibe os danos causados pela radiação UV, aumenta a síntese de colágeno na pele e reduz as rugas. É ainda indicada para rosácea, acne, fechamento dos poros, manchas e rugas.</p>
                        <br>
                        <h2>INDICAÇÕES E BENEFÍCIOS</h2>
                            <ul>
                                <li>Assegura uma vitamina C 100% estabilizada para a pele (Indicação principal);</li>
                                <li>Inibe os danos causados pela radiação UV;</li>
                                <li>Aumenta a síntese de colágeno na pele e reduz as rugas;</li>
                                <li>Fornece proteção para a pele;</li>
                                <li>Promove viço e hidratação da pele.</li>
                            </ul>
                    </div>
                </div>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>