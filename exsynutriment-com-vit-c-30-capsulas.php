<?php

    $title       = "Exsynutriment Com Vit C 30 Cápsulas";
    $description = "É uma associação do silício orgânico de Exsynutriment® com o ácido ascórbico da vitamina C que aumentam as defesas do organismo, embelezam..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Exsynutriment Com Vit C 30 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/exsynutriment-com-vit-c-30-capsulas.png" alt="exsynutriment-com-vit-c-30-capsulas" title="exsynutriment-com-vit-c-30-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>PREVINE O ENVELHECIMENTO PRECOCE</h2>
                        <p class="text-justify">É uma associação do silício orgânico de Exsynutriment® com o ácido ascórbico da vitamina C que aumentam as defesas do organismo, embelezam, renovam a pele e combatem as rugas. Exsynutriment® é uma forma estabilizada e concentrada de Silício Orgânico. Atua no tecido conjuntivo reestruturando as fibras de colágeno e elastina resultando na reestruturação e firmeza da pele de dentro para fora.</p>
                        <br>
                        <h2>AÇÃO ANTIOXIDANTE</h2>
                        <p class="text-justify">Vitamina C Facilita a absorção de ferro ajudando a prevenir a anemia ferropriva (falta de ferro). Participa de diversas funções fisiológicas: Síntese do Colágeno (rejuvenescimento da pele), auto poder antioxidante. Combate os radicais livres produzidos pelo organismo devido ao fumo, estresse, má alimentação e resíduos alimentares. Previne resfriado por estimular o sistema imunológico, distúrbios da coagulação sanguínea e lesões hepáticas.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Imagens meramente ilustrativas. Pessoas com hipersensibilidade à substância não devem ingerir o produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Este produto não deve ser utilizado por gestantes e lactantes sem orientação médica. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. Todo medicamento deve ser mantido fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>