<?php

    $title       = "Piperina (Pimenta Preta) 15mg 90 Cápsulas";
    $description = "O Piperina apresenta função digestiva e ativa o metabolismo. As substâncias da pimenta (capsaicina e piperina) melhoram a digestão..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Piperina (Pimenta Preta) 15mg 90 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/piperina-pimenta-preta-15mg-90-capsulas.png" alt="piperina-(pimenta-preta)-15mg-90-capsulas" title="piperina-(pimenta-preta)-15mg-90-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>PIPERINA (PIMENTA PRETA)</h2>
                        <p class="text-justify">O Piperina apresenta função digestiva e ativa o metabolismo. As substâncias da pimenta (capsaicina e piperina) melhoram a digestão, estimulando as secreções do estômago. Por apresentar uma ação termogênica, relacionada diretamente com o sistema nervoso autônomo, a piperina age como termo nutriente que aumenta o processo de absorção de certas substâncias no trato gastrointestinal.</p>
                        <p class="text-justify">A piperina foi encontrada para modificar a taxa de glucuronidação através da redução do teor de ácido UDP-glucurónico endógeno e também por inibição da atividade de transferase. Também foi encontrada para inibir a atividade de desidrogenase de UDP-glucose (UDP-GDH) em ambos no fígado e no intestino através da inibição não-competitiva. A glucuronidação é adição de ácido glucurónico a um substrato e que está envolvido no metabolismo xenobiótico. Glucuronídeos são mais solúveis em água e o corpo humano utiliza glucuronidação para eliminação de substâncias e /ou drogas a partir do corpo. A ação termogenica do organismo é regulado atravez do sistema nervoso autonomo e é representado por dois principais receptores no trato gastrointestinal, alfa e beta adrenergicos.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado.</p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>