<?php

    $title       = "Buclizina 25Mg 60 Cápsulas";
    $description = "A Buclizina possui ação orexígena (estimula o apetite), antiemético (evita o vômito e reduz o enjoo e a náusea) e anti-histamínica (anti-alérgico)..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Buclizina 25Mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/buclizina-25mg-60-capsulas.png" alt="buclizina-25mg-60-capsulas" title="buclizina-25mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">A Buclizina possui ação orexígena (estimula o apetite), antiemético (evita o vômito e reduz o enjoo e a náusea) e anti-histamínica (anti-alérgico). Também estimula o pâncreas a secretar insulina, que é o hormônio responsável por transportar a glicose (açúcar) para dentro das células. Assim, com menos açúcar no sangue (hipoglicemia), o cérebro envia o sinal de fome para a pessoa repor os níveis de glicose sanguínea.</p>
                        <br>
                        <h2>AJUDA A DIMINUIR O AÇÚCAR NO SANGUE</h2>
                        <p class="text-justify">Com benefícios poderosos, a Buclizina proporciona diversas ações para o organismo, como a capacidade de ajuda a controlar o açúcar no sangue, estimular o apetite e reduzir a náusea e o enjoo.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Em pacientes especialmente sensíveis, o leve efeito sedativo do dicloridrato de buclizina pode causar sonolência e potencializar a ação de substâncias depressoras do sistema nervoso central, inclusive bebidas alcoólicas. Pacientes diabéticos devem consumir com cautela. Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Manter fora do alcance das crianças. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. O uso do produto durante o período de amamentação sem orientação médica, também não é recomendado.</p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Nunca compre medicamentos sem orientação de um profissional habilitado. Imagens meramente ilustrativas. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>