<?php

    $title       = "Gaba 200Mg 60 Doses";
    $description = "O ácido gama aminobutírico, popularmente conhecido como GABA, se trata de um neurotransmissor que auxilia na atividade e regulação cerebral..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Gaba 200Mg 60 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/gaba-200mg-60-doses.png" alt="gaba-200mg-60-doses" title="gaba-200mg-60-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>GABA 200MG 60DOSES</h2>
                        <p class="text-justify">O ácido gama aminobutírico, popularmente conhecido como GABA, se trata de um neurotransmissor que auxilia na atividade e regulação cerebral. O aminoácido foi descoberto no final do século XIX, e sua atividade parece estar aliada a alguns mecanismos fisiológicos importantes. Estudos evidenciaram que ele pode estar diretamente relacionado com a tonificação de músculos e perda de peso. O que ocorre é que como se trata de um neurotransmissor, ele pode agir diretamente no bloqueio dos neurônios, evitando um excesso de excitação. Por isso, além do emagrecimento, pode também ser utilizado contra a ansiedade.</p>
                        <br>
                        <h2>POR QUE TOMAR?</h2>
                        <p class="text-justify">O GABA auxilia no relaxamento e contribuir para um boa noite de sono, também é importante para os atletas, tendo em vista que é durante a noite que são produzidos o hormônio do crescimento e a testosterona, que atuam no crescimento muscular. A ansiedade é um problema causado, em alguns casos, pela excitação exagerada no cérebro, que não permite que o indivíduo descanse ou relaxe, podendo causar até problemas relacionados à insônia.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado.</p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>