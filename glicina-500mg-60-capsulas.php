<?php

    $title       = "Glicina 500mg 60 Cápsulas";
    $description = "A glicina possui várias funções no nosso organismo. Dentre elas, temos a função de intermediário biossintético. Isso significa que o nosso corpo..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Glicina 500mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/glicina-500mg-60-capsulas.png" alt="glicina-500mg-60-capsulas" title="glicina-500mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>GLICINA</h2>
                        <p class="text-justify">A glicina possui várias funções no nosso organismo. Dentre elas, temos a função de intermediário biossintético. Isso significa que o nosso corpo precisa da glicina para fabricar outras substâncias úteis como as porfirinas (forma o grupo heme das hemoglobinas), purinas (base nitrogenada utilizada na formação da cadeia de DNA) e também as fosfocreatinas.</p>
                        <p class="text-justify">A glicina também atua como um neurotransmissor de função inibitória, principalmente na retina, medula espinhal e tronco cerebral. Ela é muito importante para que ocorra a diferença de potencial na célula e, desta forma, o impulso nervoso possa acontecer para levar as informações necessárias a diversas partes do corpo. Além dessas funções, a glicina também atua como elemento estrutural de várias substâncias, principalmente do colágeno. A glutationa, um antioxidante, essencial para o equilíbrio da oxidação celular causado, principalmente, pela ação dos radicais livres.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado.</p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>