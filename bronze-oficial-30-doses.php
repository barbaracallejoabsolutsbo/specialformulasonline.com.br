<?php

    $title       = "Bronze Oficial 30 Doses";
    $description = "Quando começa a esquentar as pessoas querem ostentar um corpo bronzeado que é sinônimo de status, sensualidade e saúde. O Bronze Oficial é uma fórmula rica..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Bronze Oficial 30 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/bronze-oficial-30-doses.png" alt="bronze-oficial-30-doses" title="bronze-oficial-30-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>Descrição</h2>
                        <p class="text-justify">Quando começa a esquentar as pessoas querem ostentar um corpo bronzeado que é sinônimo de status, sensualidade e saúde. O Bronze Oficial é uma fórmula rica em carotenoides que vão estimular a melanina de boa qualidade além de ter ativos com excelente atividade antioxidante que captam radicais livres e previnem assim, o dano ao DNA celular e consequentemente o envelhecimento precoce. Vale ressaltar que para um resultado duradouro, o produto deve ser ingerido pelo menos 30 dias antes da exposição solar ou outra forma de ativação do melanócito em produzir melanina de qualidade. Também vale a pena o lembrete de que não se trata de um bronzeador sem sol e que não é dispensado o uso de filtro solar em hipótese alguma mesmo esta fórmula apresentando ativos com ação de fotoproteção oral.</p>
                    </div>
                </div>                
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>