<?php

    $title       = "Queda Capilar";
    $description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elementum ultrices commodo. Donec quis dictum tortor."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "home",
        "produtos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="container">
            <section class="nossos-produtos catego-prod container">
                <h1 class="text-center">Queda Capilar</h1>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/suplemento-para-cabelo-femme-60-doses.png" alt="suplemento-para-cabelo-femme-60-doses" title="suplemento-para-cabelo-femme-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Suplemento para Cabelo Femme 60 Doses SENKEDA</h3>
                            <p>Complexo formado por vitaminas, minerais e outros ativos...</p>
                            </div>
                            <a class="btn-entrectt" href="suplemento-para-cabelo-femme-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/suplemento-para-cabelo-men-60-doses.png" alt="suplemento-para-cabelo-men-60-doses" title="suplemento-para-cabelo-men-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Suplemento para Cabelo Men 60 Doses</h3>
                            <p>Cada 1 Dose (2 Caps) Contém:Complexo B Hair 174mg Selênio...</p>
                            </div>
                            <a class="btn-entrectt" href="suplemento-para-cabelo-men-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/shampoo-regenerador-200ml.png" alt="shampoo-regenerador-200ml" title="shampoo-regenerador-200ml" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Shampoo Regenerador 200Ml</h3>
                            <p>O shampoo regenerador 200ML foi desenvolvido...</p>
                            </div>
                            <a class="btn-entrectt" href="shampoo-regenerador-200ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/minoxidil-tonico-capilar-men-50ml.png" alt="minoxidil-tonico-capilar-men-50ml" title="minoxidil-tonico-capilar-men-50ml" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Minoxidil Tônico Capilar Men 50Ml</h3>
                            <p>Complexo formado por vitaminas, minerais e outros ativos...</p>
                            </div>
                            <a class="btn-entrectt" href="minoxidil-tonico-capilar-men-50ml.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/shampoo-unissex-para-crescimento-capilar-200-ml.png" alt="shampoo-unissex-para-crescimento-capilar-200-ml" title="shampoo-unissex-para-crescimento-capilar-200-ml" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Shampoo Unissex Para Crescimento Capilar 200 ml</h3>
                            <p>O Shampoo Unissex combina com todos os tipos de cabelos...</p>
                            </div>
                            <a class="btn-entrectt" href="shampoo-unissex-para-crescimento-capilar-200-ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/trichoxidill-tonico-capilar-femme-50ml.png" alt="trichoxidill-tonico-capilar-femme-50ml" title="trichoxidill-tonico-capilar-femme-50ml" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Trichoxidill Tônico Capilar Femme 50Ml</h3>
                            <p>Tônico Capilar Femme Special Fórmulas é indicado no...</p>
                            </div>
                            <a class="btn-entrectt" href="trichoxidill-tonico-capilar-femme-50ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/anti-queda-capilar-novos-fios-120ml.png" alt="anti-queda-capilar-novos-fios-120ml" title="anti-queda-capilar-novos-fios-120ml" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Anti-Queda Capilar + Novos Fios 120Ml</h3>
                            <p>O Sérum Anti-queda + Novos fios, da Special Fórmulas...</p>
                            </div>
                            <a class="btn-entrectt" href="anti-queda-capilar-novos-fios-120ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/shampo-bomba-de-cafe-200-ml.png" alt="shampo-bomba-de-cafe-200-ml" title="shampo-bomba-de-cafe-200-ml" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Shampoo Bomba De Café 200 Ml</h3>
                            <p>Mistura caseira que auxilia o crescimento dos cabelos em até cinco vezes...</p>
                            </div>
                            <a class="btn-entrectt" href="shampo-bomba-de-cafe-200-ml.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/leave-in-femme-60ml-fortalecedor-de-fios.png" alt="leave-in-femme-60ml-fortalecedor-de-fios" title="leave-in-femme-60ml-fortalecedor-de-fios" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Leave in Femme 60Ml - Fortalecedor de Fios SENKEDA</h3>
                            <p>O Senkeda Leave-in Femme Oficialfarma é um sérum capilar sem enxágue...</p>
                            </div>
                            <a class="btn-entrectt" href="leave-in-femme-60ml-fortalecedor-de-fios.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/leave-in-men-100ml-controle-de-oleosidade.png" alt="leave-in-men-100ml-controle-de-oleosidade" title="leave-in-men-100ml-controle-de-oleosidade" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Leave in Men 100Ml - Controle de Oleosidade</h3>
                            <p>Com ativos que controlam a oleosidade e a seborreia...</p>
                            </div>
                            <a class="btn-entrectt" href="leave-in-men-100ml-controle-de-oleosidade.php">Saiba +</a>
                        </div>
                    </div>
            </section>
        </div>    
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>