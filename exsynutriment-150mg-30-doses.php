<?php

    $title       = "Exsynutriment 150Mg 30";
    $description = "O Exsynutriment é considerado um nutricosmético antiaging que possui propriedades antioxidantes, protegendo e restaurando a pele..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Exsynutriment 150Mg 30</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/exsynutriment-150mg-30-doses.png" alt="exsynutriment-150mg-30-doses" title="exsynutriment-150mg-30-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É EXSYNUTRIMENT?</h2>
                        <p class="text-justify">O Exsynutriment® é considerado um nutricosmético antiaging que possui propriedades antioxidantes, protegendo e restaurando a pele. Age no tecido conjuntivo, reestruturando as fibras de colágeno e elastina, promovendo uma firmeza da pele de dentro para fora, além de uma intensa hidratação cutânea.</p>
                        <p class="text-justify">De um modo geral, fazer a reposição da perda de silício orgânico através do Exsynutriment, melhora a sua saúde de “dentro para fora”, pois promove a melhor vascularização do sistema nervoso fazendo com que seu cérebro envelheça com mais qualidade, potencializa a fixação do cálcio no tecido ósseo, mantendo a densidade óssea e com isso prevenindo doenças, como a osteoporose. Tudo isso sem causar aumento de peso, pois não apresenta em sua composição nenhuma propriedade que faça aumentar o apetite, o peso ou até mesmo algum composto que cause inchaço.</p>
                    </div>
                </div>
                <p class="text-justify">Pele: Protege e restaura a pele contra o envelhecimento. Efeito lifting oral, promove a elasticidade da pele e ação “preenchedora” das rugas, eficiente no combate ao envelhecimento. Cabelos: Estimula a reposição de cabelos mais “espessos” e resistentes. A presença de silício no bulbo capilar possui ação antiqueda, fortalecendo o fio de cabelo. Unhas: Fortalece e estimula o crescimento, prevenindo danos causados pelo seu enfraquecimento e envelhecimento. A estrutura da unha é formada, sobretudo por queratina, a qual se liga ao silício, conferindo dureza e estabilidade das unhas. Cartilagem e Ossos: Prevenção de osteoporose e doenças causadas pela descalcificação óssea, pois o silício mantém a densidade óssea, fortalecendo o depósito de cálcio entre outros minerais presentes no tecido ósseo.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>