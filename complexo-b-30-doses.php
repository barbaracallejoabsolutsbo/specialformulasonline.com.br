<?php

    $title       = "Complexo B 30 DOSES";
    $description = "Composto por 8 vitaminas hidrossolúveis, ou seja, que dissolvem facilmente na água, o Complexo B é transportado para as células do corpo pela..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Complexo B 30 DOSES</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/complexo-b-30-doses.png" alt="complexo-b-30-doses" title="complexo-b-30-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>COMPLEXO B</h2>
                        <p class="text-justify">Composto por 8 vitaminas hidrossolúveis, ou seja, que dissolvem facilmente na água, o Complexo B é transportado para as células do corpo pela corrente sanguínea, proporcionando a melhora da saúde emocional, mental e ajudando no desempenho de praticantes de exercícios físicas.</p>
                        <br>
                        <h2>SAÚDE MENTAL E EMOCIONAL</h2>
                        <p class="text-justify">Essencial para a vida de qualquer ser humano, as vitaminas do Complexo B são as maiores responsáveis pela saúde emocional, mental e um grande aliado para quem pratica exercícios físicos. Essas vitaminas são fundamentais para impedir e tratar casos de depressão e ansiedade. Além desses benefícios comprovados, o Complexo B ajuda a manter a saúde dos nervos, pele, olhos, cabelos, fígado e boca, assim como a tonicidade muscular do aparelho gastrintestinal.</p>
                    </div>
                </div>
                <h2>COMPOSIÇÃO</h2>
                <p>Cada 1 Cápsula contém:</p>
                <ul>
                    <li>Vitamina B1 35mg</li>
                    <li>Vitamina B2 7mg</li>
                    <li>Vitamina B3 50mg</li>
                    <li>Vitamina B5 30mg</li>
                    <li>Vitamina B6 20mg</li>
                    <li>Vitamina B7 50mcg</li>
                    <li>Vitamina B9 (Metilfolato) 500mcg</li>
                    <li>Vitamina B12 500mcg</li>
                    <li>Excipiente* q.s.p. 1 dose</li>
                    <li>Amido, talco, dióxido de silício, estearato de magnésio</li>
                </ul>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>