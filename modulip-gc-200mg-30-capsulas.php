<?php

    $title       = "Modulip GC 200Mg 30 Cápsulas";
    $description = "É desejo de muita gente, emagrecer sem perder massa magra e sem flacidez, mas esse parece um objetivo difícil de ser alcançado. Pensando nisso..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Modulip GC 200Mg 30 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/modulip-gc-200mg-30-capsulas.png" alt="modulip-gc-200mg-30-capsulas" title="modulip-gc-200mg-30-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">É desejo de muita gente, emagrecer sem perder massa magra e sem flacidez, mas esse parece um objetivo difícil de ser alcançado. Pensando nisso, a Biotec (Exsymol) desenvolveu e a OFICIALFARMA disponibilizou o Modulip GC® Oral que é um modulador lipídico e neuroprotetor. Modulip GC® apresenta efeito neuroprotetor e antioxidante, além de estimular a lipólise.</p>
                        <br>
                        <h2>AÇÃO</h2>
                        <p class="text-justify">O cortisol, proveniente de um estresse crônico, pode afetar a inervação simpática do tecido adiposo branco (gordura), reduzindo o potencial lipolítico e aumentando, consequentemente, a deposição de gordura, principalmente na região visceral. Modulip GC inibe a ação do cortisol na inervação simpática mantendo o potencial de lipólise e estimula as terminações nervosas do tecido adiposo branco.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>