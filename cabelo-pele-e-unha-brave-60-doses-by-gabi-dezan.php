<?php

    $title       = "Cabelo, Pele e Unha Brave 60 Doses By Gabi Dezan+";
    $description = "O composto constitui em um mix de vitaminas e aminoácidos que fazem parte da estrutura dos cabelos, unhas e da pele. Zinco, sua ação é devida..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Cabelo, Pele e Unha Brave 60 Doses By Gabi Dezan+</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/cabelo-pele-e-unha-brave-60-doses-by-gabi-dezan.png" alt="cabelo-pele-e-unha-brave-60-doses-by-gabi-dezan" title="cabelo-pele-e-unha-brave-60-doses-by-gabi-dezan">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>AUMENTO DE COLÁGENO</h2>
                        <p class="text-justify">O composto constitui em um mix de vitaminas e aminoácidos que fazem parte da estrutura dos cabelos, unhas e da pele. Zinco, sua ação é devida à formação de uma fina película que, ao aderir com facilidade à superfície cutânea lesada, protege a pele de irritações, escoriações e coceiras. Vitamina C é uma vitamina hidrossolúvel encontrada naturalmente em frutas cítricas e em vegetais. Usada principalmente para à produção de vários suplementos alimentares. É essencial para a síntese de colágeno, do material intercelular e reparação de tecidos. Um potente antioxidante.</p>
                        <br>
                        <h2>AÇÃO ANTIOXIDANTE</h2>
                        <p class="text-justify">Vitamina E previne a oxidação de ácidos poli saturados por reagir com radicais livres que causam danos às membranas celulares. Está envolvida nos processos metabólicos de eliminação dos radicais livres (subproduto das reações de oxidação da mitocôndria), inibindo a formação dos mesmos e seus efeitos nocivos sobre o organismo. Biotina é essencial para produção de glicogênio, ácido nucleico e aminoácidos, tem participação na produção das células brancas do sangue, auxilia no fortalecimento da queda de cabelo e perda da pigmentação.</p>
                    </div>
                </div>
                <p class="text-justify">O composto constitui em um mix de vitaminas e aminoácidos que fazem parte da estrutura dos cabelos, unhas e da pele. Queratina tem efeito sobre o crescimento de cabelos e das unhas, indicado nos casos de perda difusa dos cabelos (perda de cabelo por razões desconhecidas), alterações degenerativas na estrutura do cabelo (cabelo enfraquecido, fino, não maleável, sem vida, opaco e sem cor), danificados pela luz do sol e radiação UV e nas desordens no crescimento das unhas (unhas quebradiças, rachadas e pouco maleáveis). Verisol® é o único colágeno especialmente desenvolvido para a beleza da pele, um alimento funcional inovador voltado aos cuidados com a pele, que age de dentro para fora, atenuando e prevenindo os sinais do tempo. Suaviza os sinais do tempo e melhora a elasticidade cutânea, reduz o volume das rugas e aumenta o conteúdo de colágeno na pele.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>