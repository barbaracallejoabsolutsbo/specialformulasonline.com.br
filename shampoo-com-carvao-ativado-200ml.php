<?php

    $title       = "Shampoo com Carvão Ativado 200ml";
    $description = "O Shampoo com Carvão Ativado 200ml Special Fórmulas conta com ativos responsáveis por proporcionar muito mais beleza e saúde para os cabelos."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Shampoo com Carvão Ativado 200ml</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/shampoo-com-carvao-ativado-200ml.png" alt="shampoo-com-carvao-ativado-200ml" title="shampoo-com-carvao-ativado-200ml">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>LIMPEZA PROFUNDA</h2>
                        <p class="text-justify">O Shampoo com Carvão Ativado 200ml Special Fórmulas conta com ativos responsáveis por proporcionar muito mais beleza e saúde para os cabelos, pois normaliza a permeabilidade capilar, hidratando e tornando-se um poderoso umectante. Também contribui para a renovação celular, além tonificar e restaurar os tecidos.O Shampoo com Carvão Ativado 200ml Special Fórmulas proporciona limpeza e saúde para os cabelos, pois normaliza a permeabilidade capilar, hidratando e tornando-se um poderoso umectante.</p>
                        <br>
                        <h2>AÇÃO ADSTRINGENTE</h2>
                        <p class="text-justify">Indicada para peles sensíveis e delicadas, a Calêndula possui ação emoliente, tonificante, cicatrizante, suavizante, refrescante, antialergênica, anti-inflamatória, protetora e restauradora de tecidos, antisséptica e bactericida. O Chá Verde possui ação estimulante, adstringente, antioxidante e antibacteriana. Também melhora a microcirculação periférica, normalizando a permeabilidade capilar.</p>
                    </div>
                </div>
                <h2>ESFOLIA O COURO CABELUDO</h2>
                <p class="text-justify">Charcoal Powder (carvão ativado) realiza uma esfoliação na raiz, estimulando o crescimento capilar. Durante a lavagem é necessário que se faça movimentos circulares para auxiliar a circulação e remover as células mortas. Possui uma ação única de fornecer hidratação para os fios, remover resíduos e combater a oleosidade.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. Nunca compre medicamentos sem orientação de um profissional habilitado. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>