<?php

    $title       = "Minoxidil Tônico Capilar Men 50Ml ";
    $description = "Beta-caroteno melhor conhecido como um precursor da vitamina A, é um importante antioxidante que ajuda a prevenir danos de radicais livres prejudiciais no corpo"; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Minoxidil Tônico Capilar Men 50Ml</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/minoxidil-tonico-capilar-men-50ml.png" alt="minoxidil-tonico-capilar-men-50ml" title="minoxidil-tonico-capilar-men-50ml">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>SUPLEMENTO PARA CABELO MEN</h2>
                        <p class="text-justify">Complexo formado por vitaminas, minerais e outros ativos, que juntos inibem a queda capilar, o Suplemento para Cabelo Men Special Fórmulas é um poderoso estimulante para o crescimento dos fios</p>
                        <br>
                        <h2>Trata a Calvície e Queda Capilar</h2>
                        <p class="text-justify">O Suplemento para Cabelo Men foi desenvolvido com ativos comprovadamente eficazes no tratamento da calvície e da queda capilar masculina.</p>
                    </div>
                </div>
                <h2>COMPOSIÇÃO SUPLEMENTO PARA CABELO MEN</h2>
                <h3>Complexo B Hair</h3>
                <p class="text-justify">Essencial para manter a saúde das células no couro cabeludo, evitando a queda capilar.</p>
                <h3>Selênio (como quelato)</h3>
                <p class="text-justify">Contém enzimas antioxidantes que previnem os danos aos folículos capilares.</p>
                <h3>Zinco (como quelato)</h3>
                <p class="text-justify">Fortalece os fios! O Zinco é um nutriente essencial para manter a saúde dos cabelos.</p>
                <h3>Saw palmeto</h3>
                <p class="text-justify">Trata-se de uma planta medicinal muito utilizada para tratar a calvície e a queda de cabelos.</p>
                <h3>Dutasterida</h3>
                <p class="text-justify">Segundo diversos estudos, é considerada uma das substâncias mais eficazes contra a alopecia.</p>
                <h3>Silício (como quelato)</h3>
                <p class="text-justify">Mineral essencial para a regeneração da pele e fortalecimento dos cabelos e unhas.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>