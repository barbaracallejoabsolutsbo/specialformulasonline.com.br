<?php

    $title       = "Vitamina K2 Mk-7 100Mcg 60 Cápsulas";
    $description = "A vitamina K-1, também conhecida como filoquinona, é encontrada nas plantas. A vitamina K2, chamada manaquinona (MK) é encontrada em animais ou bactérias."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Vitamina K2 Mk-7 100Mcg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/vitamina-k2-mk-7-100mcg-60-capsulas.png" alt="vitamina-k2-mk-7-100mcg-60-capsulas" title="vitamina-k2-mk-7-100mcg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>VITAMINA K2 MK-7 100MCG 60 CÁPSULAS</h2>
                        <p class="text-justify">A vitamina K-1, também conhecida como filoquinona, é encontrada nas plantas. A vitamina K2, chamada manaquinona (MK) é encontrada em animais ou bactérias. Em comparação com a vitamina K-1, a K2 possui benefícios comparáveis, mas possui concentrações mais elevadas na circulação em mais áreas no corpo. Resumindo, funciona melhor e fica mais tempo funcionando, em mais parte do corpo do que a K-1.</p>
                        <br>
                        <h2>O QUE É VITAMINA K2</h2>
                        <p class="text-justify">A vitamina K é essencial para formação e reparo ósseo saudáveis; faz parte da síntese da osteocalcina, a proteína no tecido ósseo usada na cristalização do cálcio. À medida que a vitamina K ajuda a desenvolver os ossos, também inibe a calcificação dos vasos sanguíneos. Esta função de “transporte de cálcio” remove, assim, os depósitos de cálcio dos vasos, onde não são necessários, e trabalhando junto com a vitamina D, aumenta os depósitos de cálcio nos ossos, onde o cálcio é mais necessário. A vitamina K também exerce uma importante função nos intestinos, ajudando a converter a glicose em glicogênio para armazenamento no fígado.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado.</p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>