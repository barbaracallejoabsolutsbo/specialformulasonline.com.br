<?php

    $title       = "Mousse de Limpeza Facial 2 em 1 Hidratação";
    $description = "A Mousse de Limpeza Facial 2 em 1 Hidratação e Regeneração da Pele 150ml da Oficialfarma, foi desenvolvido com ativos selecionados que auxiliam na limpeza..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Mousse de Limpeza Facial 2 em 1 Hidratação e Regeneração da Pele 150ml</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/mousse-de-limpeza-facial-2-em-1-hidratacao.png" alt="mousse-de-limpeza-facial-2-em-1-hidratacao" title="mousse-de-limpeza-facial-2-em-1-hidratacao">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>LIMPEZA PROFUNDA</h2>
                        <p class="text-justify">A Mousse de Limpeza Facial 2 em 1 Hidratação e Regeneração da Pele 150ml da Oficialfarma, foi desenvolvido com ativos selecionados que auxiliam na limpeza profunda da pele. Esses ativos possuem ação suavizante, refrescante e tonificante, melhorando a circulação de água na epiderme, além de restaurar a hidratação, controlando a oleosidade e mantendo a maciez e a renovação celular.</p>
                        <br>
                        <h2>NUTRIENTES</h2>
                        <p class="text-justify">O Extrato Glicólico de Hamamelis é um adstringente, com propriedade antiacneica, além de possuir potente ação contra a oleosidade excessiva da pele. Já o Extrato Glicólico de Calêndula tem ação emoliente, tonificante, lenitiva, cicatrizante, suavizante, refrescante, anti-alergênica, anti-inflamatória, antiacneica, antisséptica, bactericida, principalmente indicada para peles sensíveis e delicadas. Aquaporine são proteínas transmembranares que permitem o transporte de moléculas de água através da membrana plasmática, melhorando o transporte de água através das diferentes camadas (derme e epiderme), obtendo assim, uma pele mais hidratada, macia e flexível.</p>
                    </div>
                </div>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="https://web.whatsapp.com/send?phone=5511944930219&text=Ol%C3%A1%21%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20da%20Exaustyair%20-%20Sistemas%20de%20Exaust%C3%A3o%20e%20Ventila%C3%A7%C3%A3o%20Industrial"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>