<?php

    $title       = "Bio-Arct 100Mg 30 Cápsulas";
    $description = "Bio Arct é uma biomassa marinha, proveniente de algas vermelhas que age favorecendo os cuidados com a pele, e a protegendo contra agressores externos."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Bio-Arct 100Mg 30 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/bio-arct-100mg-30-capsulas.png" alt="bio-arct-100mg-30-capsulas" title="bio-arct-100mg-30-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>Descrição</h2>
                        <p class="text-justify">Bio Arct é uma biomassa marinha, proveniente de algas vermelhas que age favorecendo os cuidados com a pele, e a protegendo contra agressores externos. Age em diferentes pontos, desde a hidratação até a melhora cutânea. Um dos seus principais constituintes é o dipeptídeo citrulyl-arginina, que é capaz de aumentar a energia e estimular o crescimento celular para a biossintese de proteínas e em particular do colágeno. Possui uma ação antioxidante única, que além de ocasionar uma pele mais jovial e iluminada, previne o envelhecimento precoce da pele.</p>
                        <br>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>