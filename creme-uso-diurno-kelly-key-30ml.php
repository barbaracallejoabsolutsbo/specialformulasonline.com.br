<?php

    $title       = "Creme Uso Diurno (Kelly Key) 30Ml";
    $description = "Beta-caroteno melhor conhecido como um precursor da vitamina A, é um importante antioxidante que ajuda a prevenir danos de radicais livres prejudiciais no corpo"; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Creme Uso Diurno (Kelly Key) 30Ml</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/creme-uso-diurno-kelly-key-30ml.png" alt="creme-uso-diurno-(kelly-key)30ml" title="creme-uso-diurno-(kelly-key)30ml">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>ÁCIDO HIALURÔNICO</h2>
                        <p class="text-justify">Conheça a substância que combate o envelhecimento da pele. O ácido hialurônico é indicado principalmente para melhorar o viço da pele, suavizando rugas e outras marcas da idade. Quando usado de forma injetável, ele pode ser usado no contorno de face, lábios (contorno e volume), sulcos naso-labiais, sulco naso-jugal (olheiras), rugas faciais, e para repor volume em face, mãos e algumas regiões corporais.</p>
                        <br>
                        <h2>PCA (ÁCIDO PIRROLIDONA CARBOXÍLICO)</h2>
                        <p class="text-justify">PCA-Na tem alto poder umectante e é um dos componentes do fator natural de hidratação (NMF).É extremamente seguro e, devido a sua biodegradabilidade, é considerado um produto compatível ecologicamente. Ao contrário dos polióis, não é pegajoso ao toque. Tem alta capacidade de retenção de umidade,mesmo em ambientes com umidade relativa baixa. Pode reter umidade em queratina,mais eficientemente que a glicerina,contribuindo para a maciez da pele.</p>
                    </div>
                </div>
                <h2>BENEFÍCIOS</h2>
                <ul>
                    <li><strong>Rica em antioxidantes:</strong> a berinjela apresenta uma boa atividade antioxidante pela presença do ácido clorogênico, cafeíco e flavonóides que atuam combatendo o envelhecimento celular e protegendo a oxidação das gorduras nas membranas celulares.</li>
                    <li><strong>Benefícios para o coração:</strong> devido a presença de fibras solúveis e antioxidantes, existe um controle do colesterol e consequentemente uma melhora do fluxo sanguíneo, prevenindo também o estresse oxidativo.</li>
                    <li><strong>Perda de peso:</strong> 1 xícara de berinjela apresenta em torno de 8% de fibra dietética, sendo uma excelente fonte de fibras, o que por sua vez auxilia na perda de peso e redução do colesterol. Além disso, esse fruto tem a vantagem de conter poucas calorias.</li>
                    <li><strong>Rica em vitaminas do complexo B:</strong> a berinjela é rica em vitaminas B1, B2 e B6 que são essenciais para o funcionamento adequado do sistema nervoso central, para produção de energia, equilíbrio hormonal e função hepática.</li>
                    <li><strong>Fonte de cálcio, magnésio e potássio:</strong> esses minerais são essenciais para o adequado funcionamento do organismo atuando na construção, manutenção dos ossos e melhora da contração muscular.</li>
                    <li><strong>Pele saudável:</strong> a presença de vitamina C é um dos nutrientes que ajudam a manter a pele saudável, macia e hidratada, tendo também função antioxidante. Há outros antioxidantes na berinjela, como a antocianina. Todos esses ajudam a combater o excesso dos radicais livres no organismo, que podem causar envelhecimento precoce e rugas.</li>
                    <li><strong>Prevenção de diversos tipos de cânceres:</strong> a casca da berinjela apresenta um potente antioxidante chamado Nasunin que atua no organismo, estimulando a formação de novos vasos e a melhora do suprimento sanguíneo, o que por sua vez contribui para prevenção de diversos tipos de cânceres.</li>
                </ul>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>