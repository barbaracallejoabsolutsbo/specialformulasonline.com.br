<?php

    $title       = "Faseolamina + Cassiolamina + Glucomannan 60 Doses";
    $description = "De maneira resumida, pode-se dizer que a faseolamina bloqueia a alfa-amilase, uma enzima que está naturalmente presente em nosso corpo..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Faseolamina + Cassiolamina + Glucomannan 60 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/faseolamina-cassiolamina-glucomannan-60-doses.png" alt="faseolamina-cassiolamina-glucomannan-60-doses" title="faseolamina-cassiolamina-glucomannan-60-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>FASEOLAMINA</h2>
                        <p class="text-justify">De maneira resumida, pode-se dizer que a faseolamina bloqueia a alfa-amilase, uma enzima que está naturalmente presente em nosso corpo. Como a principal função da alfa-amilase é exatamente “quebrar” os carboidratos e permitir a liberação de glicose para as células, a inibição da enzima acaba por reduzir a quantidade de energia disponível na circulação.</p>
                        <br>
                        <h2>CASSIOLAMINA</h2>
                        <p class="text-justify">A Cassia nomame é uma planta medicinal nativa na China, mas atualmente cresce em muitas regiões em todo o mundo. O principal uso de cássia nomame na medicina tradicional, é para aumentar o fluxo de urina. O Hama-cha, um popular chá diurético japonês tem como base o extrato dessa planta.</p>
                    </div>
                </div>
                <h2>GLUCOMANNAN</h2>
                <p class="text-justify">O glucomannan é uma fibra solúvel, tipo que absorve água e se torna um gel durante a digestão. Usado para constipação, diabetes tipo 2, controle do açúcar no sangue e para baixar o colesterol, o glucomannan emagrece também e pode ser encontrado nas formas de comprimido, cápsula e em pó. Usado há muito tempo na medicina tradicional chinesa, hoje em dia o glucomannan é visto principalmente como um suplemento de perda de peso. Ele age no estômago e nos intestinos, absorvendo água para formar uma massa de fibras. Ele também pode desacelerar a absorção de açúcar e colesterol nos intestinos.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>