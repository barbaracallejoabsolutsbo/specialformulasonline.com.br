<?php
    $title       = "Remedios manipulados dermatológicos";
    $description = "Remédios manipulados dermatológicos, remédios manipulados para linha estética, fitoterápica, prescrita, veterinária, fitness, nutrição e muito mais.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Conheça toda nossa linha de <strong>remédios manipulados dermatológicos</strong>. Diversos produtos especiais para você. Fórmulas com benefícios e composições especiais, saiba mais falando com nosso atendimento. Aqui é possível encomendar diversas fórmulas de remédios manipulados receitados por médicos após consultas, com toda agilidade e pontualidade. Com prazos exclusivos, encontre preços acessíveis e todo comprometimento para produtos de qualidade.</p>
<p><strong>Remédios manipulados dermatológicos</strong>, remédios manipulados para linha estética, fitoterápica, prescrita, veterinária, fitness, nutrição e muito mais. Conheça todos nossos serviços de manipulação e tenha fórmulas feitas com cuidado e atenção seguindo todos os padrões da ANVISA. Diversos produtos prontos para o consumo disponíveis em nosso catálogo, confira nossas ofertas e compre com a maior farmácia magistral de São Paulo.</p>
<p>Existem muitas vantagens em adquirir seus <strong>remédios manipulados dermatológicos</strong>, isso porque dessa forma você pode atender de forma individualizada todas as necessidades de cada paciente. Dessa forma fica mais fácil prescrever fármacos de forma eficiente e direta para cada caso. Os medicamentos manipulados conosco contam com toda rotulagem com dosagem e composição, seguindo todas normas de Boas Práticas de Manipulação determinadas pelo Ministério da Saúde.</p>
<p>A Special Fórmulas faz uso apenas de matérias primas com controle rigoroso de qualidade, certificados, controlados e adequadamente estocados, com total segurança nos nossos produtos manipulados. Aqui é possível encontrar diversos medicamentos muito importantes para tratamentos específicos, a fim de manipular substâncias dificilmente encontradas nessas formas e dosagens no mercado farmacêutico convencional. Compre <strong>remédios manipulados dermatológicos</strong> com a Special Fórmulas.</p>
<h2><strong>Diferenças entre remédios industrializados e remédios manipulados dermatológicos</strong></h2>
<p>Medicamentos magistrais são conhecidos como remédios manipulados, podem ser encontrados diversos modelos no mercado de manipulação, fórmulas em cápsulas, comprimidos, sachês, entre outras vias de consumo, a fim de personalizar fórmulas que não são encontradas no mercado convencional de fármacos industrializados nas dosagens e composições que são precisas como necessário em algumas receitas. Para isso existem farmácias de manipulação, a fim de atender as necessidades pessoais de cada paciente. Com a Special Fórmulas você encontra<strong> remédios manipulados dermatológicos </strong>para clarear a pele, evitar o envelhecimento e o aparecimento de rugas.</p>
<h2><strong>É confiável utilizar remédios manipulados dermatológicos?</strong></h2>
<p>Sim, sua utilização é 100% confiável, e quando consultado um médico antes uso desses tipos de medicamentos e<strong> remédios manipulados dermatológicos </strong>você encontra todo suporte profissional para usar adequadamente com total eficácia. Consulte seu profissional da saúde e traga suas receitas para manipulação com a Special Fórmulas.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>