<?php

    $title       = "Tônico Facial Antioleosidade 100Ml";
    $description = "agente umectante natural, conhecido por ser abundante na pele humana, ele ajuda a pele e os cabelos a se manterem com uma aparência saudável e viçosa"; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Tônico Facial Antioleosidade 100Ml</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/tonico-facial-antioleosidade.png" alt="tonico-facial-antioleosidade" title="tonico-facial-antioleosidade">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>PCA-NA</h2>
                        <p class="text-justify">PCA-NA: agente umectante natural, conhecido por ser abundante na pele humana, ele ajuda a pele e os cabelos a se manterem com uma aparência saudável e viçosa. PCA-Na, além de um excelente efeito hidratante (superior ao da Glicerina), é um produto seguro, que pode ser usado na pele e em mucosas, o que o torna especialmente indicado em praticamente qualquer tipo de formulação dermocosmética. Ele previne o ressecamento da pele mesmo em condições de baixa umidade.</p>
                        <br>
                        <h2>ALOE VERA</h2>
                        <p class="text-justify">Extrato Glicólico de Aloe Vera: Tem ação emoliente, cicatrizante, tonificante, antiinflamatória, suavizante, lenitiva, refrescante, hidratante, protetora e restauradora de tecidos. Usado em preparações lenitivas para peles delicadas, sensíveis, irritadiças e/ou secas. Também é indicado para tratamento da acne, psoríase, coceiras, eczemas, erisipela, picadas de insetos e de pequenos ferimentos (como cicatrizante). Poderá ser incorporado em cremes, loções cremosas, hidroalcóolicas ou tônicas, em shampoos, géis, cremes para banho, loção de limpeza, filtros solares e outros produtos cosméticos.</p>
                    </div>
                </div>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <br>
                <a class="compre-aqui" href="https://web.whatsapp.com/send?phone=5511944930219&text=Ol%C3%A1%21%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20da%20Exaustyair%20-%20Sistemas%20de%20Exaust%C3%A3o%20e%20Ventila%C3%A7%C3%A3o%20Industrial"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>