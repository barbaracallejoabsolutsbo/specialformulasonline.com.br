<?php

    $title       = "Goji Berry 500Mg Concentrado 60 Cápsulas ";
    $description = "Goji Berry 500mg da Special Fórmulas, foi desenvolvida com propriedades coadjuvantes que auxiliam o organismo contra radicais livres..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Goji Berry 500Mg Concentrado 60 Cápsulas </h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/goji-berry-500mg-concentrado-60-capsulas.png" alt="goji-berry-500mg-concentrado-60-capsulas" title="goji-berry-500mg-concentrado-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>POTENTE AÇÃO ANTIOXIDANTE</h2>
                        <p class="text-justify">Goji Berry 500mg da Special Fórmulas, foi desenvolvida com propriedades coadjuvantes que auxiliam o organismo contra radicais livres, potente ação antioxidante e antienvelhecimento, além de atuar na melhora cutânea de dentro para fora.</p>
                        <br>
                        <h2>AÇÃO CONTRA RADICAIS LIVRE</h2>
                        <p class="text-justify">Goji Berry com um grande número de aminoácidos, vitaminas, minerais, ácidos graxos insaturados, antioxidantes e sendo especialmente rico em polissacarídeos. Os antioxidantes presentes no Goji Berry têm uma importância fundamental no combate ao envelhecimento da pele, estimulando a renovação celular. Também são responsáveis por auxiliar no emagrecimento.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Advertências Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="https://web.whatsapp.com/send?phone=5511944930219&text=Ol%C3%A1%21%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20da%20Exaustyair%20-%20Sistemas%20de%20Exaust%C3%A3o%20e%20Ventila%C3%A7%C3%A3o%20Industrial"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>