<?php

    $title       = "Thiomucase 400 UTR 90 Doses";
    $description = "A Thiomucase se tornou uma grande aliada das pessoas que procuram se livrar das formas que parecem inchadas e da celulite. Os efeitos benéficos da Thimucase..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Thiomucase 400 UTR 90 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/thiomucase-400-utr-90-doses.png" alt="thiomucase-400-utr-90-doses" title="thiomucase-400-utr-90-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">A Thiomucase se tornou uma grande aliada das pessoas que procuram se livrar das formas que parecem inchadas e da celulite. Os efeitos benéficos da Thimucase nos tratamentos de problemas relacionados com o acumulo de líquidos e a celulite foram comprovados em laboratório e a substância é largamente utilizada em tratamentos médicos.</p>
                        <p class="text-justify">Age na quebra das células proteicas conhecidas por reter uma grande quantidade de água. Sua ação promove principalmente o aumento na viscosidade dos tecidos, aumentado sua permeabilidade e proporcionado a reabsorção dos líquidos. É muito indicada para pessoas que possuem problemas com a retenção de água. É importante lembrar que a Thiomucase não age como um emagrecedor, pois não elimina gordura, isto acontece, na verdade, por conta da perda de líquidos. A eficiência deste tratamento está muito mais relacionada com problemas relativos ao inchaço, celulite, cólicas menstruais e saúde da pele em geral.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>