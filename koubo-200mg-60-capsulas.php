<?php

    $title       = "Koubo 200Mg 60 Cápsulas";
    $description = "Koubo 200mg foi desenvolvido com propriedades moduladoras do apetite, possui ação diurética, antioxidante, além de reduzir níveis de colesterol..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Koubo 200Mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/koubo-200mg-60-capsulas.png" alt="koubo-200mg-60-capsulas" title="koubo-200mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>MODERADOR DE APETITE</h2>
                        <p class="text-justify">Koubo 200mg foi desenvolvido com propriedades moduladoras do apetite, possui ação diurética, antioxidante, além de reduzir níveis de colesterol. Estudos apontam que o ativo inibe o apetite, pois possui boas quantidades de tiramina, que exerce ação sacietogênica, capaz de diminuir a vontade por doces.</p>
                        <br>
                        <h2>ATIVOS</h2>
                        <p class="text-justify">Conhecido como doce do deserto, o Koubo é uma fruta popular brasileira. Além de tiramina, que ajuda no controle do apetite, o ativo também possui betalaína, que conta com uma poderosa ação antioxidante. Já a indicaxantina elimina os líquidos e as toxinas, exercendo atividade diurética. O fato de ajudar no controle do colesterol se deve ao ômega 6 e 9, que também combatem os radicais livres.</p>
                    </div>
                </div>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>