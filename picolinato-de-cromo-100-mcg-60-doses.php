<?php

    $title       = "Picolinato De Cromo 100 Mcg 60 Doses";
    $description = "O Picolinato de cromo é considerado como a melhor fonte de cromo. O mineral foi identificado como um componente ativo na nutrição humana..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Picolinato De Cromo 100 Mcg 60 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/picolinato-de-cromo-100-mcg-60-doses.png" alt="picolinato-de-cromo-100-mcg-60-doses" title="picolinato-de-cromo-100-mcg-60-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">O Picolinato de cromo é considerado como a melhor fonte de cromo. O mineral foi identificado como um componente ativo na nutrição humana. Sabemos que o cromo é um parceiro essencial para uma eficiente ação da insulina. Entretanto, esta pequena quantia exerce papéis importantes na melhoria da eficácia da insulina, regulação dos níveis de açúcar no sangue, e a ativação de várias enzimas para produção de energia e também ajuda a reduzir a compulsão por doces.</p>
                        <br>
                        <h2 class="advertencias text-center">Advertências</h2>
                        <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                    </div>
                </div>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>