<?php

    $title       = "Adifyline 2% Loção Aumento Seios E Glúteos 60gr";
    $description = "Adifyline 2% é indicado para os seios e glúteos. Ajuda a proporcionar firmeza e aumento do volume no local aplicado. Essas duas regiões do corpo..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Adifyline 2% Loção Aumento Seios E Glúteos 60gr</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/adifyline-2-locao-aumento-seios-e-gluteos-60gr.png" alt="adifyline-2-locao-aumento-seios-e-gluteos-60gr" title="adifyline-2-locao-aumento-seios-e-gluteos-60gr">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">Adifyline 2% é indicado para os seios e glúteos. Ajuda a proporcionar firmeza e aumento do volume no local aplicado. Essas duas regiões do corpo costumam perder a sustentação com o passar do tempo e o produto foi desenvolvido para combater esse processo. Com ação direta no tecido adiposo, estimula o aumento do volume local e auxilia a firmeza destas áreas, pois, produz de forma benéfica, um preenchimento nas áreas desejadas.</p>
                        <br>
                        <h2>COMBATE A FLACIDEZ</h2>
                        <p class="text-justify">Adifyline 2% estimula o aumento do volume e a firmeza dos seios e dos glúteos. Melhora também a elasticidade e textura da pele.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. O uso do produto durante o período de amamentação sem orientação médica, também não é recomendado.</p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. Nunca compre medicamentos sem orientação de um profissional habilitado. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>