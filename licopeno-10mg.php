<?php

    $title       = "Licopeno 10Mg 30 Cápsulas";
    $description = "Licopeno é um composto da família dos carotenoides, como o betacaroteno. É o pigmento natural responsável pela cor do tomate, da melancia e da goiaba."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Licopeno 10Mg 30 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/licopeno-10mg.png" alt="licopeno-10mg" title="licopeno-10mg">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>AÇÃO ANTIOXIDANTE</h2>
                        <p class="text-justify">Licopeno é um composto da família dos carotenoides, como o betacaroteno. É o pigmento natural responsável pela cor do tomate, da melancia e da goiaba. Apesar de não ser considerado um nutriente essencial, ajuda o organismo a reverter os danos causados pela exposição excessiva ao fumo, poluição e radiação solar. Licopeno age como antioxidante nas células, formando uma camada de proteção contra o envelhecimento, o câncer e as doenças cardiovasculares. Consumido regularmente, pode se tornar um grande aliado à saúde.</p>
                        <br>
                        <h2>PREVINE A DEGENERAÇÃO CELULAR</h2>
                        <p class="text-justify">O Licopeno ajuda a prevenir doenças degenerativas e atua neutralizando radicais livres antes que possam danificar o material celular. Ao estudar a pele exposta à radiação ultravioleta (UV), cientistas descobriram que o Licopeno é superior ao betacaroteno ao proteger o tecido da pele de danos oxidativos. Também age exercendo um efeito preventivo contra os riscos do câncer de próstata e de doenças cardiovasculares. É utilizado na cosmecêutica para o tratamento de prevenção dos antirradicais livres em produtos antiaging.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="https://web.whatsapp.com/send?phone=5511944930219&text=Ol%C3%A1%21%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20da%20Exaustyair%20-%20Sistemas%20de%20Exaust%C3%A3o%20e%20Ventila%C3%A7%C3%A3o%20Industrial"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>