<?php

    $title       = "Raspberry Ketone 100Mg 60 Cápsulas";
    $description = "Beta-caroteno melhor conhecido como um precursor da vitamina A, é um importante antioxidante que ajuda a prevenir danos de radicais livres prejudiciais no corpo"; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Raspberry Ketone 100Mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/raspberry-ketone-100mg-60-capsulas.png" alt="raspberry-ketone-100mg-60-capsulas" title="raspberry-ketone-100mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                         <h2>O QUE É?</h2>
                        <p class="text-justify">Desde os tempos antigos, as framboesas são objeto de pesquisas na área da medicina, e com isso já foi comprovado o seu efeito antioxidante e como ela ajuda a relaxar os vasos sanguíneos. Porém, nos últimos anos passou-se a pesquisar se a cetona de framboesa emagrece. Cetona de framboesa (raspberry ketone), cientificamente conhecida como 4-(4-hidroxifenil) butano-2-ona é um composto fenólico, ou seja, é o responsável pelo aroma presente em alguns frutos silvestres, dentre eles a framboesa, e são responsáveis por dar a cor avermelhada e o aroma das frutas. Normalmente elas são utilizadas para dar cor, sabor ou fragrância para refrigerantes, sorvetes ou cosméticos, mas este composto está sendo vendido hoje em dia como um suplemento alimentar. O que fez com que as cetonas de framboesa chamassem a atenção dos especialistas na área e público em geral, é que suplementos alimentares feitos a partir delas poderiam ajudar a queima a gordura corporal, ajudando assim a pessoa a emagrecer, mesmo que mantendo uma dieta rica em gordura.Ultimamente, os suplementos feitos a partir desde composto estão sendo apontados como o próximo “milagre para a perda de peso” já que os fabricantes alegam que as cetonas de framboesa ajudam o seu corpo a quebrar a gordura rapidamente.</p>
                        <br>
                    </div>
                </div>
                <h2>RASPBERRY KETONE 100MG</h2>
                <p class="text-justify">Um dos maiores benefícios dos suplementos de cetonas de framboesa é a rapidez com que produz efeitos. Tomada em doses de dois comprimidos por dia com um copo de água, e combinado com uma dieta saudável e exercício físico regular, a cetona de framboesa pode produzir resultados de emagrecimento em apenas 2 semanas.</p>
                <h2>MECANISMO DE AÇÃO</h2>
                <p class="text-justify">Estudos recentes dizem que a estrutura da cetona de framboesa (raspberry ketone) é similar à da capsaicina, que é um químico natural que devido ao seu efeito termogênico aumenta o metabolismo, ajudando a pessoa a emagrecer. Elas também atuam com a enzima lipase, que é responsável pela quebra das moléculas de lipídios (gorduras), e no sistema digestivo ela tem a função de basicamente transformar lipídeos (gorduras) em ácidos graxos e glicerol, ajudando na perda de peso.Atuando na inibição da lipase pancreática, que é uma enzima produzida pelo pâncreas que intervém na digestão das gorduras alimentares, elas fazem com que essa gordura não seja digerida, sendo eliminada nas fezes sem que sejam absorvidas pelo organismo.</p>
                <br>
                <h2 class="advertencias text-center">CONTRA INDICAÇÕES</h2>
                <p class="advertencias text-center">Grávidas e lactantes.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="https://web.whatsapp.com/send?phone=5511944930219&text=Ol%C3%A1%21%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20da%20Exaustyair%20-%20Sistemas%20de%20Exaust%C3%A3o%20e%20Ventila%C3%A7%C3%A3o%20Industrial"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>