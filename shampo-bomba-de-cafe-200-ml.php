<?php

    $title       = "Shampoo Bomba De Café 200 Ml";
    $description = "Mistura caseira que auxilia o crescimento dos cabelos em até cinco vezes mais rápido do que o natural, o xampu bomba de café virou febre..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Shampoo Bomba De Café 200 Ml</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/shampo-bomba-de-cafe-200-ml.png" alt="shampo-bomba-de-cafe-200-ml" title="shampo-bomba-de-cafe-200-ml">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>SHAMPOO BOMBA DE CAFÉ</h2>
                        <p class="text-justify">Mistura caseira que auxilia o crescimento dos cabelos em até cinco vezes mais rápido do que o natural, o xampu bomba de café virou febre e se espalhou pela internet. Fácil, este produto eficiente; leva apenas dois ingredientes em sua fórmula: um xampu sem sal e três colheres de sopa de café em pó. A vantagem anunciada pelas defensoras do método é que a fórmula não contém agentes químicos nocivos à saúde, como o xampu bomba original, que é feito de Monovin A, uma vitamina de uso veterinário para animais de grande porte, prejudicial não só aos cabelos, mas também ao fígado.</p>
                        <p class="text-justify">A mania de usar pó de café como tratamento capilar ganhou força após um estudo da Universidade Lübeck, na Alemanha, divulgar que a cafeína pode auxiliar no tratamento da queda de cabelo estimulando o crescimento de novos fios. "Quando fizemos o experimento de aplicar uma solução com cafeína nos folículos capilares fragilizados, notamos que a velocidade de crescimento aumentou 25%", explica Tobias U. Fischer, dermatologista do centro de estudos alemão. O estudo explica que a cafeína ativa um fator de crescimento que ajuda a prevenir que as células capilares morram ou entrem em estado de dormência.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado.</p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>