<?php

    $title       = "Superox-C 5% (Kakadu Plum) Creme Facial 30G";
    $description = "O Creme Superox – C Oficialfarma tem propriedades exclusivas e ativos selecionados que apresentam ação anti-aging, antirrugas e antioxidante."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Superox-C 5% (Kakadu Plum) Creme Facial 30G</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/superox-c-5-kakadu-plum-creme-facial-30g.png" alt="superox-c-5-(kakadu-plum)-creme-facial-30g" title="superox-c-5-(kakadu-plum)-creme-facial-30g">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>AÇÃO ANTI-AGING</h2>
                        <p class="text-justify">O Creme Superox – C Oficialfarma tem propriedades exclusivas e ativos selecionados que apresentam ação anti-aging, antirrugas e antioxidante. Ao diminuir os sinais da idade, aumenta também a luminosidade da pele. O ativo botânico da formulação é extraído da ameixa de Kakadu, especialmente desenvolvido para diminuir os efeitos do envelhecimento causados pelos radicais livres. Aumenta também a síntese de pró-colágeno I, melhorando a luminosidade e a uniformidade do tom da pele.</p>
                        <br>
                        <h2>REDUZ RUGAS E LINHAS DE EXPRESSÃO</h2>
                        <p class="text-justify">Outro ativo é o Pró-TG3 que atua como precursor do fator de crescimento de transformação TGF-beta. Além disso, devido à presença das vitaminas lipossolúveis A e E em sua composição, o Pró-TG3 atua também como um potente agente antioxidante, aumentando sua eficácia anti-aging e reduzindo o processo inflamatório cutâneo.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="https://web.whatsapp.com/send?phone=5511944930219&text=Ol%C3%A1%21%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20da%20Exaustyair%20-%20Sistemas%20de%20Exaust%C3%A3o%20e%20Ventila%C3%A7%C3%A3o%20Industrial"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>