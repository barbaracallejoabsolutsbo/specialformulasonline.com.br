<?php

    $title       = "Haired-X 50 ml – Éclaire";
    $description = "Haired-X Special Fórmulas é o primeiro tônico de crescimento capilar à base de minoxidil."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Haired-X 50 ml – Éclaire</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/haired-x-50mg.png" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>Haired-X</h2>
                        <p class="text-justify"><strong>Composição: TrichoSol</strong> <br>Tussilago Farfara (Coltsfoot) Flower Extract (and) Achillea Millefolium Extract (and) Cinchona Succirubra Bark Extract (and) Alcohol (and) Water Aqua (and) Alcohol Denat. (and) Panthenyl Ethyl Ether (and) Inositol (and) Milk Protein (and) Lactose (and) Acetyl Cysteine (and) Acetyl Methionine (and) Sodium Citrate (and) Citric Acid 6-(1-Piperidinyl)-2,4-pyrimidinediamine3-oxide sulfato Panthenol</p>
                        <h2>Descrição</h2>
                        <p class="text-justify">Haired-X Special Fórmulas é o primeiro tônico de crescimento capilar à base de minoxidil desenvolvido por um médico Tricologista referência internacional no Estudo dos Cabelos. Dr Lucas Fustinoni é um dos maiores e mais conhecidos especialistas em cabelos do Brasil e se destacou por produzir conteúdo sobre Doenças Capilares que é compartilhado por milhões de brasileiros nas redes sociais.</p>
                    </div>
                </div>
                <h2>RESULTADO EFICAZ E GARANTIDO</h2>
                <p class="text-justify">O Haired-X Special Fórmulas trata a calvície e a queda capilar. Estimula o crescimento dos fios e preenche as falhas de cabelo.</p>
                <h2>ATIVOS POTENCIALIZADOS</h2>
                <p class="text-justify">A formulação do Haired-X Special Fórmulas alia a eficácia comprovada do Minoxidil com um blend de ativos poderosos. Indicado para quem já teve resultados com o Minoxidil, mas ainda percebe que alguns fios não conseguiram se desenvolver e estão fracos e finos.</p>
                <h2>PREVINE O EFEITO REBOTE</h2>
                <p class="text-justify">Haired-X SPecial Fórmulas controla a secreção sebácea no couro cabeludo, prevenindo assim a formação da caspa e o reengorduramento acelerado dos fios (efeito rebote). Se você sofre com cabelos oleosos, experimente o Shampoo Greasy Block para combater a oleosidade diretamente de dentro da glândula sebácea.</p>
                <h2>PRODUTOS PARA USAR JUNTO</h2>
                <p class="text-justify">Para atingir o máximo de benefícios, o Haired-X Special Fórmulas pode ser utilizado com outros produtos, como o Upill Men ou Femina e o Factorhair Special Fórmulas.</p>
                <h2>RESULTADOS</h2>
                <p class="text-justify">O tratamento com Upill Men apresenta resultados após cerca de 2 a 3 meses. Após 6 meses de uso contínuo a diferença se torna visível e satisfatório. Algumas pessoas que não sofrem de calvície avançada podem notar a eficiência do produto antes deste período.</p>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. O uso do produto durante o período de amamentação sem orientação médica, também não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. Nunca compre o produto sem orientação de um profissional habilitado. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>