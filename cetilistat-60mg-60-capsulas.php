<?php

    $title       = "Cetilistat 60mg 60 Cápsulas";
    $description = "O cetilistat é um medicamento desenvolvido para tratamento da obesidade. Ele age da mesma forma que o Orlistat ao inibir a ação da lípase pancreática..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Cetilistat 60mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/cetilistat-60mg-60-capsulas.png" alt="cetilistat-60mg-60-capsulas" title="cetilistat-60mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">O cetilistat é um medicamento desenvolvido para tratamento da obesidade. Ele age da mesma forma que o Orlistat ao inibir a ação da lípase pancreática, uma enzima que quebra triglicerídeos (um tipo de gordura) no intestino. Sem essa enzima, os triglicerídeos da dieta não são absorvidos e são excretados sem serem digeridos.</p>
                        <br>
                        <h2>AÇÃO</h2>
                        <p class="text-justify">Ao contrário de muitos remédios para emagrecer disponíveis no mercado, o Cetilistat não atua no sistema nervoso inibindo o apetite. O mecanismo de ação do Cetilistat se dá através do bloqueio de uma determinada enzima, a Lipase. Nosso corpo tem inúmeras enzimas que atuam o tempo todo para permitir o funcionamento adequado de todas nossas funções vitais, entre elas a digestão e absorção de nutrientes.</p>
                    </div>
                </div>
                <h2>COMBATE A OBESIDADE</h2>
                <p class="text-justify">No caso das gorduras, a enzima que permite sua absorção é a lipase, que atua para que as gorduras (lipídios) se transformem em ácidos graxos e glicerol. É na forma de glicerol que as gorduras são absorvidas. O Cetilistat atua impedindo o funcionamento da lipase, o que leva a uma absorção de até 30% menos de gordura pela parede do intestino. Com uma menor absorção de gordura, gera-se um déficit calórico, ou seja, é possível absorver menos calorias do que de fato foi ingerido.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>