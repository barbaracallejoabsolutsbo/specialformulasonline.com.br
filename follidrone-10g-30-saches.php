<?php

    $title       = "Follidrone 10G 30 Sachês";
    $description = "Você treina diariamente e se alimenta de forma saudável, mas ainda não conseguiu atingir a forma física desejada? Pode ser que você precise de um auxílio..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Follidrone 10G 30 Sachês</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/follidrone-10g-30-saches.png" alt="follidrone-10g-30-saches" title="follidrone-10g-30-saches">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">O Follidrone é um suplemento proteico natural que reduz a ação da miostatina e por isso ajuda quem deseja elevar a massa muscular e melhorar o desempenho físico. A miostatina é um fator de crescimento (TGF-8) que está presente no nosso organismo e consegue inibir a produção de fibras musculares, ou seja, ela diminui muito o crescimento muscular. Algumas pessoas têm mais facilidade em desenvolver músculos porque naturalmente possuem menos miostatina do que outras. Aliás, os níveis de miostatina vão aumentando com o passar do tempo, por isso é mais difícil para pessoas mais velhas manterem os tecidos musculares.</p>
                        <br>
                        <h2>BENEFÍCIOS</h2>
                        <p class="text-justify">Melhora a construção muscular: Ao reduzir a miostatina, o suplemento permite que o organismo fique livre para produzir mais tecidos musculares de acordo com seu progresso nos treinos. Grande ação termogênica: O Follidrone age na queima de calorias para produzir calor, ou seja, ele te emagrece e ao mesmo tempo gera mais energia. Boa função ergogênica: Consumir o suplemento pode melhorar a performance física do atleta e aumentar a sua força muscular. Aumenta a definição muscular: Ao queimar gorduras e inibir a miostatina, o Follidrone auxilia a pessoa a desenvolver uma definição muito melhor dos músculos.</p>
                    </div>
                </div>
                <p class="text-justify">Reduz a fadiga: A creatina proporciona mais energia ao corpo, dessa forma, o tempo de intervalo entre cada série de exercícios físicos diminui bastante, o que gera um rendimento bem maior e um desempenho mais eficaz. Ganho de massa magra: A folistatina, presente na composição do Follidrone, ajuda a combater a atrofia muscular, assim como facilita o aumento da massa magra (músculos) no corpo.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>