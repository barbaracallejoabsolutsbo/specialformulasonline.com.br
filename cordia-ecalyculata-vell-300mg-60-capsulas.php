<?php

    $title       = "Cordia Ecalyculata Vell 300mg 60 Cápsulas";
    $description = "Rica em cafeína, alantoína, potássio, taninos e óleos essenciais, tem sido usada como diurético, coadjuvante natural para o tratamento da obesidade..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Cordia Ecalyculata Vell 300mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/cordia-ecalyculata-vell-300mg-60-capsulas.png" alt="cordia-ecalyculata-vell-300mg-60-capsulas" title="cordia-ecalyculata-vell-300mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">Rica em cafeína, alantoína, potássio, taninos e óleos essenciais, tem sido usada como diurético, coadjuvante natural para o tratamento da obesidade, ajudando a eliminar a barriga, queimando a gordura e permitindo o bem-estar cardíaco. Este fitoterapêutico é perfeito para quem quer seguir uma dieta de emagrecimento saudável, sem recorrer a produtos nocivos para a saúde.</p>
                        <br>
                        <h2>PROPRIEDADES</h2>
                        <p class="text-justify">A Cordia ecalyculata Vell ou Cordia salicifolia Cham é popularmente conhecida pelos nomes porangaba, cafezinho, café-do-mato, chá-de-frade, louro-salgueiro e louro-mole. Constitui uma forma natural e saudável para perda de peso. As propriedades terapêuticas dessa planta podem estar atribuídas à presença de ativos como a cafeína, potássio, taninos, alantoína e ácidos graxos. Pesquisas comprovam a ação da cafeína no sistema nervoso central como supressor do apetite e estimulante para o aumento da queima de gorduras localizadas, principalmente na região do abdômen. Os taninos auxiliam no combate aos radicais livres, prevenindo contra o envelhecimento precoce da pele.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>