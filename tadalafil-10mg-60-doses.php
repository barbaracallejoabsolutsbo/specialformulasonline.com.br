<?php

    $title       = "Tadalafil 10Mg 60 Doses";
    $description = "Definição da Disfunção Erétil: quando existe um estímulo sexual, o pênis se enche de sangue e ocorre a ereção. Se o homem tiver disfunção erétil (DE)..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Tadalafil 10Mg 60 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/tadalafil-10mg-60-doses.png" alt="tadalafil-10mg-60-doses" title="tadalafil-10mg-60-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>AUXILIA NA DISFUNÇÃO ERÉTIL</h2>
                        <p class="text-justify">Definição da Disfunção Erétil: quando existe um estímulo sexual, o pênis se enche de sangue e ocorre a ereção. Se o homem tiver disfunção erétil (DE), o pênis não se enche de sangue de forma adequada e a ereção não é suficiente para ocorrer uma relação sexual. A disfunção erétil, também chamada de impotência sexual, pode ter diversas causas. É importante consultar um especialista para descobrir a natureza desse problema. Tadalafil é um fármaco sintético que pode auxiliar o homem com DE a obter e manter uma ereção quando estiver sexualmente excitado.</p>
                        <p class="text-justify">Tadalafil ajuda a aumentar o fluxo de sangue no pênis e pode auxiliar homens com disfunção erétil a obterem e manterem uma ereção satisfatória para a atividade sexual. Uma vez completa a relação sexual, o fluxo sanguíneo diminui e a ereção termina. Para Tadalafil funcionar, é necessária uma excitação sexual. O homem não terá uma ereção apenas por tomar uma dose de Tadalafil, sem estimulação sexual. Tadalafil age, em média, a partir de 30 minutos, podendo ter ação por até 36 horas. Tadalafil é indicado no tratamento de homens com disfunção erétil (impotência sexual) e para os tratamentos dos sinais e sintomas de Hiperplasia Prostática Benigna (HPB), melhorando os problemas causados pelo fluxo de urina nestes pacientes. O Tadalafil também é frequentemente utilizado para retardar a ejaculação em conjunto com fármacos antidepressivos.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="text-justify">Em estudos clínicos, o Tadalafil mostrou aumentar os efeitos hipotensivos dos nitratos. Supõe-se que isto seja resultado dos efeitos combinados dos nitratos e Tadalafil na via óxido nítrico/GMPc. Portanto, a administração de Tadalafil a pacientes que estão usando qualquer forma de nitrato orgânico é contraindicada. É contra indicado para homens com problemas cardiovasculares e com restrição de atividade sexual. O Tadalafil não deve ser usada em pacientes com conhecida hipersensibilidade ao tadalafil ou a qualquer componente da formulação. O tadalafil não é indicado para homens que não apresentam disfunção erétil e/ou sinais e sintomas de Hiperplasia Prostática Benigna (HPB). O uso de Tadalafil não é indicado para mulheres. Não é indicado para pessoas com Insuficiência renal grave. Evite a automedicação. Medicamentos mesmo livres de obrigação de prescrição médica, merecem cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Nunca compre medicamento sem orientação de um profissional habilitado. Imagens meramente ilustrativas. Pessoas com hipersensibilidade à substância não devem ingerir o produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico.</p>       
                <p class="advertencias text-justify">Não use o medicamento com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o medicamento se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Todo medicamento deve ser mantido fora do alcance das crianças. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. Este medicamento não deve ser utilizado por menores de 18 anos sem orientação médica. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO". "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>