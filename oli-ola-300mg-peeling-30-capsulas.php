<?php

    $title       = "Oli Ola 300Mg Peeling 30 Cápsulas";
    $description = "Ao promover uma ação de “peeling em cápsulas” - devido à estimulação da produção de colágeno e aumento do tempo de vida útil dos fibroblastos "; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Oli Ola 300Mg Peeling 30 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/oli-ola-300mg-peeling-30-capsulas.png" alt="oli-ola-300mg-peeling-30-capsulas" title="oli-ola-300mg-peeling-30-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>OLI OLA 300MG PEELING 30 CÁPSULAS COM SELO DE AUTENTICIDADE</h2>
                        <p class="text-justify">Ao promover uma ação de “peeling em cápsulas” - devido à estimulação da produção de colágeno e aumento do tempo de vida útil dos fibroblastos - o Oli Ola® Special Fórmulas melhora a firmeza e elasticidade cutânea, reduz a perda e as alterações funcionais da elastina dérmica, proporciona a diminuição da hiperpigmentação - tornando a tonalidade da pele mais uniforme. Também estimula a renovação celular e modula a taxa de crescimento e apoptose de queratinócitos.</p>
                        <br>
                        <h2>INDICADO PARA TODOS OS TIPOS DE PELE</h2>
                        <p class="text-justify">Pode ser utilizado em todos os tipos de pele e associado a peelings químicos, o Oli Ola Special Fórmulas auxilia na diminuição da inflamação ocasionada por procedimentos estéticos. Possui ação complementar cardioprotetora comprovada, auxiliando na redução do colesterol total, LDL e triglicerídeos e no aumento do HDL.</p>
                    </div>
                </div>
                <h2>MAIS SEGURO QUE OUTROS TRATAMENTOS CUTÂNEOS</h2>
                <p class="text-justify">Os benefícios do Oli-Ola frente a outros procedimentos de peeling químicos efísicos são: não causar desconfortos cutâneos e apresentar boa adesão ao tratamento, podendo ser usado em todos os foto tipos cutâneos.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="https://web.whatsapp.com/send?phone=5511944930219&text=Ol%C3%A1%21%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20da%20Exaustyair%20-%20Sistemas%20de%20Exaust%C3%A3o%20e%20Ventila%C3%A7%C3%A3o%20Industrial"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>