<?php

    $title       = "Leave in Men 100Ml - Controle de Oleosidade";
    $description = "Com ativos que controlam a oleosidade e a seborreia, o Leave-in Men controla a queda capilar e pode ser aplicado nos cabelos secos ou molhados."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Leave in Men 100Ml - Controle de Oleosidade</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/leave-in-men-100ml-controle-de-oleosidade.png" alt="leave-in-men-100ml-controle-de-oleosidade" title="leave-in-men-100ml-controle-de-oleosidade">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>LEAVE IN MEN</h2>
                        <p class="text-justify">Com ativos que controlam a oleosidade e a seborreia, o Leave-in Men controla a queda capilar e pode ser aplicado nos cabelos secos ou molhados.</p>
                        <br>
                        <h2>CUIDA DO CABELO</h2>
                        <p class="text-justify">Solução sem enxágue, o Leave-in Men possui ativos capazes de controlar a oleosidade do couro cabeludo, melhorar a circulação no local e estimular o crescimento de novos fios. Proporciona também limpeza e sensação de frescor.</p>
                    </div>
                </div>
                <h2 class="text-center">COMPOSIÇÃO LEAVE IN MEN</h2>
                <h3>Piritionato de zinco</h3>
                <p class="text-justify">Poderoso agente anti-seborreico, antifúngico e antibacteriano, utilizado para tratar a caspa e dermatite.</p>
                <br>
                <h3>Biotina</h3>
                <p class="text-justify">É a base de Vitamina B7, que estimula a produção de queratina, além de promover hidratação.</p>
                <br>
                <h3>Auxina tricogena</h3>
                <p class="text-justify">É um blend de nutrientes que fortalecem da raiz dos cabelos, proporcionando tonicidade.</p>
                <br>
                <h3>Tintura de menta</h3>
                <p class="text-justify">Propriedades antisséptica, adstringente e refrescante.</p>
                <br>
                <h3>Solução Hidroalcóolica</h3>
                <p class="text-justify">Solução antisséptica e antimicrobiana, capaz de remover as impurezas e o excesso de oleosidade.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>