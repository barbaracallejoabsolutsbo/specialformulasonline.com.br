<?php

    $title       = "Vitamina B1 250Mg (Tiamina) 100 Cápsulas";
    $description = "A Vitamina B1, também chamada Tiamina ou aneurina, é uma das vitaminas de complexo B. A Tiamina existe em plantas e animais e é essencial..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Vitamina B1 250Mg (Tiamina) 100 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/vitamina-b1-250mg-tiamina-100-capsulas.png" alt="vitamina-b1-250mg-(tiamina)-100-capsulas" title="vitamina-b1-250mg-(tiamina)-100-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">A Vitamina B1, também chamada Tiamina ou aneurina, é uma das vitaminas de complexo B. A Tiamina existe em plantas e animais e é essencial em algumas reações metabólicas. A maioria dos alimentos contém pequenas quantidades de tiamina. Grandes quantidades existem em carnes de porco e órgãos. Ajuda a fortalecer o sistema imunológico e pode ajudar na redução dos problemas digestivos, inclusive a falta de apetite.</p>
                        <br>
                        <h2>AÇÃO</h2>
                        <p class="text-justify">A Vitamina B1 ajuda na conversão de alimentos, especificamente os carboidratos em combustível (glicose), que é "queimada" para produzir energia. Necessária para o melhor funcionamento do cérebro, a Vitamina B1 é considerada uma Vitamina "antiestresse" devido à sua habilidade de melhorar a capacidade do corpo de se adaptar aos agentes estressantes mentais e físicos. Precursor de um neurotransmissor chamado acetilcolina, envolvido na comunicação entre os nervos. Desta forma, a Vitamina B1 promove uma função adequada dos músculos e do sistema nervoso.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>