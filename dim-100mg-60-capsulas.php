<?php

    $title       = "Dim 100Mg 60 Cápsulas";
    $description = "Di-indol Metano (DIM) é um composto natural formado durante a quebra autolítica da glucobrassicina presente em plantas alimentícias do gênero Brassica..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Dim 100Mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/dim-100mg-60-capsulas.png" alt="dim-100mg-60-capsulas" title="dim-100mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>Descrição</h2>
                        <p class="text-justify">Di-indol Metano (DIM) é um composto natural formado durante a quebra autolítica da glucobrassicina presente em plantas alimentícias do gênero Brassica, incluindo brócolis, repolho, couve de bruchelas, couve-flor e couve. Para esta reação catalítica é requerido uma enzima chamada mirosinase que está presente endogenamente nesses alimentos e que são liberadas através da quebra das paredes celulares.</p>
                        <br>
                        <h2>CARACTERÍSTICAS</h2>
                        <p class="text-justify">Sendo um componente fitoquímico, DIM é capaz de modificar o metabolismo do estrogênio. Ele promove o equilíbrio do estrogênio aumentando sua ação benéfica e segurança; e desta forma previne câncer e envelhecimento precoce em homens e mulheres.</p>
                    </div>
                </div>
                <h2>MECANISMO DE AÇÃO</h2>
                <p class="text-justify">Embora seja claro que DIM induz a apoptose em células tumorais in vitro, o mecanismo exato de ação para o efeito anti-neoplásico in vivo ainda é desconhecido. O mecanismo de ação imunomodulatório de DIM é estimular a transcrição do receptorgama-interferon bem como a produção de gama-interferon. DIM parece ainda mostrar sinergia com gama-interferon na potencialização de MHC-I Complex (células que mediam interações com leucócitos).</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>