<?php

$h1      	 = "Home";
$title    	 = "";
    $description = "MUDAR"; // Manter entre 130 a 160 caracteres
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "tools/fancybox",
        "tools/nivo-slider",
        "tools/slick",
        "home"
    ));
    
    ?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="banner-home">
            <div class="theme-default-nivo-slider">
                <div id="slider" class="nivoSlider"> 
                    <img src="<?php echo $url; ?>imagens/banner/background0.jpg" alt="Imagem">
                    <img src="<?php echo $url; ?>imagens/banner/background1.jpg" alt="Imagem">
                    <img src="<?php echo $url; ?>imagens/banner/background2.jpg" alt="Imagem">
                    <img src="<?php echo $url; ?>imagens/banner/background3.jpg" alt="Imagem">
                    <img src="<?php echo $url; ?>imagens/banner/background4.jpg" alt="Imagem">
                    <img src="<?php echo $url; ?>imagens/banner/background5.jpg" alt="Imagem">
                    <img src="<?php echo $url; ?>imagens/banner/background6.jpg" alt="Imagem">
                </div>
            </div>
        </div>
        <div class="nossos-produtos catego-prod">
            <div class="container">
                <div class="row">
                    <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <img src="<?php echo $url; ?>imagens/betacaroteno-50mg.png" alt="betacaroteno-50mg" title="betacaroteno-50mg" class="img-responsive">
                        <div class="descricao-prod">
                            <h3>Betacaroteno 50Mg</h3>
                            <p>Beta-caroteno melhor conhecido como um precursor da vitamina A, é um importante antioxidante que ajuda...</p>
                        </div>
                        <a class="btn-entrectt" href="betacaroteno50mg.php">Saiba +</a>
                    </div>
                    <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <img src="<?php echo $url; ?>imagens/betacaroteno-25000ui.png" alt="betacaroteno-25000ui" title="betacaroteno-25000ui" class="img-responsive">
                        <div class="descricao-prod">
                            <h3>Betacaroteno 25000Ui</h3>
                            <p>Betacaroteno, melhor conhecido como um precursor da vitamina A, é um importante antioxidante que ajuda...</p>
                        </div>
                        <a class="btn-entrectt" href="betacaroteno-25000ui.php">Saiba +</a>
                    </div>
                    <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <img src="<?php echo $url; ?>imagens/cavalinha100mg.png" alt="cavalinha100mg" title="cavalinha100mg" class="img-responsive">
                        <div class="descricao-prod">
                            <h3>Cavalinha 100Mg 60 Cápsulas</h3>
                            <p>A cavalinha é uma planta medicinal que pode trazer vários benefícios para a saúde. Ela auxilia na redução de peso...</p>
                        </div>
                        <a class="btn-entrectt" href="cavalinha-100mg.php">Saiba +</a>
                    </div>
                    <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <img src="<?php echo $url; ?>imagens/gel-creme-antiacne-azeloglicina.png" alt="gel-creme-antiacne-azeloglicina" title="gel-creme-antiacne-azeloglicina" class="img-responsive">
                        <br>
                        <div class="descricao-prod">
                            <h3>Gel Creme Antiacne Com Azeloglicina </h3>
                            <p>Azeloglicina® é um ativo derivado da condensação do ácido azeláico ao aminoácido glicina, que garante total eficácia e segurança ao tratamento...</p>
                        </div>
                        <a class="btn-entrectt" href="gel-creme-antiacne-azeloglicina-60g.php">Saiba +</a>
                    </div>
                </div>
                <div class="row">
                    <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <img src="<?php echo $url; ?>imagens/gel-noturno-para-acne-30g.png" alt="gel-noturno-para-acne-30g" title="gel-noturno-para-acne-30g" class="img-responsive">
                        <br>
                        <br>
                        <div class="descricao-prod">
                            <h3>Gel Noturno Para Tratamento Da Acne </h3>
                            <p>O peróxido de benzoíla é um dos medicamentos tópicos mais antigos usados no tratamento da acne. Trata-se de uma substância...</p>
                        </div>
                        <a class="btn-entrectt" href="gel-noturno-para-acne-30g.php">Saiba +</a>
                    </div>
                    <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <img src="<?php echo $url; ?>imagens/oleo-de-linhaca-500mg.png" alt="oleo-de-linhaca-500mg" title="oleo-de-linhaca-500mg" class="img-responsive">
                        <div class="descricao-prod">
                            <h3>Óleo De Linhaça 500Mg</h3>
                            <p>O organismo humano tem necessidades próprias que precisam ser mantidas em constante equilíbrio...</p>
                        </div>
                        <a class="btn-entrectt" href="oleo-de-linhaca-500mg.php">Saiba +</a>
                    </div>
                    <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <img src="<?php echo $url; ?>imagens/sabonete-auxiliar-no-tratamento-antiacne.png" alt="sabonete-auxiliar-no-tratamento-antiacne" title="sabonete-auxiliar-no-tratamento-antiacne" class="img-responsive">
                        <br>
                        <br>
                        <div class="descricao-prod">
                            <h3>Sabonete Auxiliar No Tratamento Antiacne</h3>
                            <p>Esse ácido possui propriedades esfoliantes e antimicrobianas, o que significa que afina a camada espessada da pele...</p>
                        </div>
                        <a class="btn-entrectt" href="sabonete-auxiliar-no-tratamento-antiacne.php">Saiba +</a>
                    </div>
                    <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <img src="<?php echo $url; ?>imagens/sabonete-auxiliar-no-tratamento-de-oleosidade.png" alt="sabonete-auxiliar-no-tratamento-de-oleosidade" title="sabonete-auxiliar-no-tratamento-de-oleosidade" class="img-responsive">
                        <br>
                        <br>
                        <div class="descricao-prod">
                            <h3>Sabonete Auxiliar No Tratamento De Oleosidade</h3>
                            <p>O Extrato Glicólico de Hamamélis tem ação adstringente, vasoprotetora, vasoconstritora, descongestionante...</p>
                        </div>
                        <a class="btn-entrectt" href="sabonete-auxiliar-no-tratamento-de-oleosidade.php">Saiba +</a>
                    </div>
                </div>
                <div class="row">
                    <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <img src="<?php echo $url; ?>imagens/tonico-facial-antioleosidade.png" alt="tonico-facial-antioleosidade" title="tonico-facial-antioleosidade" class="img-responsive">
                        <br>
                        <br>
                        <div class="descricao-prod">
                            <h3>Tônico Facial Antioleosidade</h3>
                            <p>agente umectante natural, conhecido por ser abundante na pele humana, ele ajuda a pele e os cabelos a se manterem com uma aparência...</p>
                        </div>
                        <a class="btn-entrectt" href="tonico-facial-antioleosidade.php">Saiba +</a>
                    </div>
                    <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <img src="<?php echo $url; ?>imagens/vitamina-oral-para-acne.png" alt="vitamina-oral-para-acne" title="vitamina-oral-para-acne" class="img-responsive">
                        <div class="descricao-prod">
                            <h3>Vitamina Oral Para Acne</h3>
                            <p>Vitamina A: A forma mais pura da vitamina A é o retinol, que possui alto poder antioxidante...</p>
                        </div>
                        <a class="btn-entrectt" href="vitamina-oral-para-acne.php">Saiba +</a>
                    </div>
                </div>
            </div>
        </div>
        
         
        
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.nivo",
        "tools/jquery.fancybox",
        "tools/contador-clientes",
        "tools/jquery.slick"
    )); ?>

    <script>
        $(function(){
            $(".counter").counterUp({
                delay: 100,
                time:7000
            });
        });
    </script>

    <script>
        $(function(){
            $(".autoplay").slick({
                slidesToShow: 5,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000000
            });
        });
    </script>

    <script>
        $(function(){
            $("#slider").nivoSlider({
                animSpeed: 500,
                pauseTime: 20000,
            });
            //effect: "random",
            //slices: 15,
            //boxCols: 8,
            //boxRows: 4,
            //animSpeed: 500,
            //pauseTime: 3000,
            //startSlide: 0,
            //directionNav: true,
            //controlNav: true,
            //controlNavThumbs: false,
            //pauseOnHover: true,
            //manualAdvance: false,
            //prevText: 'Prev',
            //nextText: 'Next',
            //randomStart: false,
            //beforeChange: function(){},
            //afterChange: function(){},
            //slideshowEnd: function(){},
            //lastSlide: function(){},
            //afterLoad: function(){}
        });

        $(function(){
         function scrollTopMenu(a){
             var id_element = $(a).attr("data-id");
             var top = $("#"+id_element).offset().top -150;
             if(id_element === "home")
             {
                 var top = 0;
             }
             $("html, body").animate({
                 scrollTop: top
             }, 700);
         }
         $("ul li a").click(function(){
             scrollTopMenu($(this));
         });
         $("a.click-second").click(function(){
             scrollTopMenu($(this));
         });
     });
 </script>

</body>
</html>