<?php

    $title       = "Vitamina B5 (Ácido Pantotênico) 500Mg 60 Doses";
    $description = "Beta-caroteno melhor conhecido como um precursor da vitamina A, é um importante antioxidante que ajuda a prevenir danos de radicais livres prejudiciais no corpo"; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Vitamina B5 (Ácido Pantotênico) 500Mg 60 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/vitamina-b5-acido-antotenico-500Mg-60-doses.png" alt="vitamina-b5-(acido-antotenico)-500Mg-60-doses" title="vitamina-b5-(acido-antotenico)-500Mg-60-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>UMA VITAMINA VERSÁTIL</h2>
                        <p class="text-justify">A vitamina b5, também chamada de ácido pantotênico, pertence ao grupo das vitaminas b solúveis em água. Seu o nome vem da palavra grega "pantos", que significa "em todos os lugares", já que está presente em todas as células vivas. Uma ingestão adequada de vitamina B5 (ácido pantotênico) é importante, uma vez que ajuda o corpo: converter alimentos em glicose, usado para produzir energia; quebrar gorduras, carboidratos e proteínas para gerar energia; sintetizar o colesterol; formar glóbulos vermelhos, bem como hormônios sexuais e hormônios relacionados ao estresse.</p>
                        <br>
                        <h2>REDUZ O RISCO DE DOENÇAS</h2>
                        <p class="text-justify">Cicatrização de feridas: Estudos, principalmente em tubos de ensaio e animais e apenas alguns com humanos, sugerem que A suplementação de vitamina B5 pode acelerar a cicatrização de feridas, especialmente após uma operação. Colesterol alto e triglicerídeos: Vários pequenos estudos sugerem que a vitamina B5 (pantetina) pode ajudar a reduzir o colesterol e triglicerídeos no sangue de pessoas com alto teor de gordura no sangue. Artrite reumatóide: Há evidências muito preliminares sugerindo que os suplementos de ácido pantotênico podem ajudar reduzir os sintomas da artrite reumatóide.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="https://web.whatsapp.com/send?phone=5511944930219&text=Ol%C3%A1%21%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20da%20Exaustyair%20-%20Sistemas%20de%20Exaust%C3%A3o%20e%20Ventila%C3%A7%C3%A3o%20Industrial"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>