<?php

    $title       = "Vitamina B6 (Piridoxina) 100Mg 30 Cápsulas";
    $description = "A Vitamina B6 também conhecida como cloridrato de piridoxina, é utilizada nos tratamentos e na prevenção dos estados de carência da vitamina no organismo..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Vitamina B6 (Piridoxina) 100Mg 30 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/vitamina-b6-piridoxina-100mg-30-capsulas.png" alt="vitamina-b6-(piridoxina)-100mg-30-capsulas" title="vitamina-b6-(piridoxina)-100mg-30-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">A Vitamina B6 também conhecida como cloridrato de piridoxina, é utilizada nos tratamentos e na prevenção dos estados de carência da vitamina no organismo, no tratamento de certos distúrbios metabólicos, na depressão e outros sintomas associados a STPM (Síndrome da Tensão Pré Menstrual) e ao uso de anticoncepcionais. É uma vitamina hidrossolúvel, envolvida principalmente no metabolismo dos aminoácidos e também no metabolismo glicídico e lipídico, necessária também para a formação da hemoglobina. A deficiência de piridoxina em humanos é rara, mas pode ocorrer em determinadas situações, como por exemplo nos tratamentos com isoniazida.</p>
                        <p class="text-justify">A Vitamina B6 é convertido em sua forma fisiologicamente ativa, o pirodoxal fosfato, por ação de quinases, em presença de magnésio. O pirodoxal atua como coenzima no metabolismo protéico, glicídico e lipídico. Uma função extremamente importante no tratamento da tensão pré-menstrual é sua ação sobre o ácido glutâmico. O pirodoxal fosfato promove a descarboxilação deste aminoácido formado, posteriormente, o ácido gama amino butírico (GABA), que é considerado um neurotransmissor inibidor. A diminuição na concentração de GABA no SNC leva ao quadro de excitação e irritabilidade.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>