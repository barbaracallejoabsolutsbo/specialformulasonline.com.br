<?php

    $title       = "Glycoxil + Exsynutriment + Bio-Arct 30 Caps";
    $description = "A associação de Glycoxil, Exsynutriment e Bio-Arct, possui resultados visíveis no tecido conjuntivo pois reestrutura, firma, detoxifica e hidrata a pele..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Glycoxil + Exsynutriment + Bio-Arct 30 Caps</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/glycoxil-exsynutriment-bio-arct-30-caps.png" alt="glycoxil-exsynutriment-bio-arct-30-caps" title="glycoxil-exsynutriment-bio-arct-30-caps">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>“CÁPSULA DA BELEZA”</h2>
                        <p class="text-justify">A associação de Glycoxil®, Exsynutriment® e Bio-Arct®, possui resultados visíveis no tecido conjuntivo pois reestrutura, firma, detoxifica e hidrata a pele, além de prevenir o envelhecimento precoce pelo seu alto poder antioxidante. O Glycoxil® apresenta atividade antiglicante, assim como outros derivados peptídicos da carcinina, previne a peroxidação lipídica, protegendo as membranas do estresse oxidativo. A inibição da peroxidação lipídica se dá pela combinação da propriedade varredora de radicais livres e da propriedade quelante dos metais.</p>
                        <br>
                        <h2>EXSYNUTRIMENT</h2>
                        <p class="text-justify">O Exsynutriment® é uma molécula original patenteada pela Exsymol em Mônaco, composta por ácido ortosílicico estabilizado em colágeno marinho hidrolisado que atua no tecido conjuntivo reestruturando as fibras de colágeno e elastina. Sua reposição é essencial para regeneração dos tecidos danificados, degradação da membrana celular, aparecimento de rugas, envelhecimento precoce, desgaste ósseo, cabelos e unhas desvitalizados.</p>
                    </div>
                </div>
                <h2>BIO-ARCT</h2>
                <p class="text-justify">O Bio-Arct® é um bioenergizante proveniente das profundezas do mar Artico, cuja composição é patenteada e única. Esta biomassa marinha tem cultivo altamente tecnológico e é riquíssima em bioativos padronizados: citrulyl arginina, taurina, floridosídeos e sais minerais. Exerce potente ação anti-inflamatória e pode ser recomendado para tratamentos pré e pós cirúrgicos, inclusive peelings. Possui um enorme poder de combate ao estresse oxidativo, reduzindo, portanto, o envelhecimento.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Imagens meramente ilustrativas. Pessoas com hipersensibilidade à substância não devem ingerir o produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Este produto não deve ser utilizado por gestantes e lactantes sem orientação médica. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. Todo medicamento deve ser mantido fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="https://web.whatsapp.com/send?phone=5511944930219&text=Ol%C3%A1%21%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20da%20Exaustyair%20-%20Sistemas%20de%20Exaust%C3%A3o%20e%20Ventila%C3%A7%C3%A3o%20Industrial"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>