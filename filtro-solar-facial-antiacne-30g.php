<?php

    $title       = "Filtro Solar Facial Antiacne 30G";
    $description = "O Filtro Solar Facial Antiacne, da Special Fórmulas, foi desenvolvido cuidadosamente para proteger a pele acneica. Sua formulação exclusiva contém..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Filtro Solar Facial Antiacne 30G</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/filtro-solar-facial-antiacne-30g.png" alt="filtro-solar-facial-antiacne-30g" title="filtro-solar-facial-antiacne-30g">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>FILTRO SOLAR FACIAL ANTIACNE</h2>
                        <p class="text-justify">O Filtro Solar Facial Antiacne, da Special Fórmulas, foi desenvolvido cuidadosamente para proteger a pele acneica. Sua formulação exclusiva contém ativos para melhorar a aparência da pele e diminuir a oleosidade, além de proteger contra os raios UVA e UVB</p>
                        <br>
                        <h2>CONTROLE DA OLEOSIDADE</h2>
                        <p class="text-justify">Desenvolvido especialmente para o cuidado da acne, uma das propriedades do Filtro Solar Antiacne é fechar os poros dilatados. Além de uma associação de princípios ativos de última geração afim de se obter um amplo espectro de atividades. Favorece a normalização da oleosidade, inibe o processo inflamatório, controla a hiper queratinização e restaura o equilíbrio biológico da pele.</p>
                    </div>
                </div>
                <br>
                <h2 class="advertencias text-center">Filtro Solar Facial Antiacne</h2>
                <p class="advertencias text-justify">O Filtro Solar Facial Antiacne, da Special Fórmulas, atua para melhorar a aparência da pele acneica e diminuir a oleosidade, além de proteger contra os raios UVA e UVB. Controla também o brilho da pele e dá um efeito mate (toque seco).</p>
                <br>
                <h2 class="text-center">TOMAR SOL SEM PREOCUPAÇÕES</h2>
                <div class="row img-vantagens">
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 vantagens-1">
                        <img class="text-center" src="imagens/vantagens/protecao-solar-uva.png" alt="protecao-solar-uva" title="protecao-solar-uva">
                        <h3 class="text-center">Proteção Solar UVA/UVB</h3>
                        <p class="text-justify">Essencial contra os raios UVA e UVB, o Filtro Solar Facial Antiacne, da Oficialfarma, protege e melhora o aspecto da pele.</p>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 vantagens-2">
                        <img src="imagens/vantagens/acao-sebo-regulatoria.png" alt="acao-sebo-regulatoria" title="acao-sebo-regulatoria">
                        <h3 class="text-center">Ação sebo-regulatória</h3>
                        <p class="text-justify">O Filtro Solar Facial Antiacne Oficialfarma possui fórmula com ativos eficazes na regulação do sebo, controlando a oleosidade da pele.</p>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 vantagens-3">
                        <img src="imagens/vantagens/efeito-toque-seco.png" alt="efeito-toque-seco"
                        title="efeito-toque-seco">
                        <h3 class="text-center">Efeito toque seco</h3>
                        <p class="text-justify">O Filtro Solar Facial Antiacne, da Oficialfarma, possui toque seco, controlando o brilho da pele.</p>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 vantagens-4">
                        <img src="imagens/vantagens/absorção-da-oleosidade-sem-ressecamento-da-pele.png" alt="absorção-da-oleosidade-sem-ressecamento-da-pele" title="absorção-da-oleosidade-sem-ressecamento-da-pele">
                        <h3 class="text-center">Absorção da oleosidade sem ressecamento da pele</h3>
                        <p class="text-justify">Sua fórmula permite redução visível da oleosidade e imperfeições da pele, sendo um adstringente natural para pele oleosa e sensível, sem causar ressecamento da pele.</p>
                    </div>   
                </div>
                <hr>
                <br>
                <a class="compre-aqui" href="https://web.whatsapp.com/send?phone=5511944930219&text=Ol%C3%A1%21%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20da%20Exaustyair%20-%20Sistemas%20de%20Exaust%C3%A3o%20e%20Ventila%C3%A7%C3%A3o%20Industrial"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>