<?php

    $title       = "Dimpless 40Mg 30 Cápsulas";
    $description = "Dimpless 40mg, foi desenvolvido com propriedades para auxiliar no tratamento da celulite, uma condição multifatorial de hábitos alimentares inadequados..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Dimpless 40Mg 30 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/dimpless-40mg-30-capsulas.png" alt="dimpless-40mg-30-capsulas" title="dimpless-40mg-30-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>PODEROSO ALIADO</h2>
                        <p class="text-justify">Dimpless 40mg, foi desenvolvido com propriedades para auxiliar no tratamento da celulite, uma condição multifatorial de hábitos alimentares inadequados, sedentarismo, estresse, medicamentos e gravidez. A junção destes fatores facilita o acúmulo de gordura, edema, inflamação e consequentemente a formação da fibrose, dificultando a circulação local e provocando o aspecto de “casca de laranja” característico na celulite.</p>
                        <br>
                        <h2>O QUE É DIMPLESS?</h2>
                        <p class="text-justify">O Dimpless ativo de uso oral com ação específica na fibrose e na lipogênese, que apresenta melhora no aspecto da celulite, rico em superóxido desmutase (SOD), outros antioxidantes primários, como a catalase (CAT) e glutationa peroxidase (GPx), e secundários, como a coenzima Q10, ácido lipóico, carotenoides, vitamina A, E e C.</p>
                    </div>
                </div>
                <h2>PODEROSO ALIADO</h2>
                <p class="text-justify">O Dimpless favorece a reconstrução da matriz extra celular, reduz nódulos e o acúmulo de gorduras, colaborando na melhora do aspecto da celulite, além de ajudar na prevenção de fibroses contribuindo com ação anti-inflamatória.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado.</p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>