<?php

    $title       = "Menopausa";
    $description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elementum ultrices commodo. Donec quis dictum tortor."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "home",
        "produtos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="container">
            <section class="nossos-produtos catego-prod container">
                <h1 class="text-center">Menopausa</h1>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/composto-super-libido-unissex-60-doses.png" alt="composto-super-libido-unissex-60-doses" title="composto-super-libido-unissex-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Composto Super Libido Unissex 60 Doses</h3>
                            <p>Os chineses consideram o Epimedium Icariin...</p>
                            </div>
                            <a class="btn-entrectt" href="composto-super-libido-unissex-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/oleo-de-primula-500mg-60-capsulas.png" alt="oleo-de-primula-500mg-60-capsulas" title="oleo-de-primula-500mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Óleo De Primula 500Mg 60 Cápsulas</h3>
                            <p>O Óleo de Prímula é extraído das sementes da Evening Primrose...</p>
                            </div>
                            <a class="btn-entrectt" href="oleo-de-primula-500mg-60-capsulas.php">Saiba +</a>                        
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/isoflavona-de-soja-80mg-60-capsulas.png" alt="isoflavona-de-soja-80mg-60-capsulas" title="isoflavona-de-soja-80mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Isoflavona De Soja 80mg 60 Cápsulas</h3>
                            <p>Indicado para o tratamento sintomático e prevenção de...</p>
                            </div>
                            <a class="btn-entrectt" href="isoflavona-de-soja-80mg-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/acido-hialuronico-1-gel-creme-30g.png" alt="acido-hialuronico-1-gel-creme-30g" title="acido-hialuronico-1-gel-creme-30g" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Ácido Hialurônico 1% Gel Creme 30g</h3>
                            <p>O ÁCIDO HIALURÔNICO é um polissacarídeo da família...</p>
                            </div>
                            <a class="btn-entrectt" href="acido-hialuronico-1-gel-creme-30g.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/acido-hialuronico-50mg-60-doses.png" alt="acido-hialuronico-50mg-60-doses" title="acido-hialuronico-50mg-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Ácido Hialurônico 50Mg 60 Doses</h3>
                            <p>O ÁCIDO HIALURÔNICO é um polissacarídeo da família...</p>
                            </div>
                            <a class="btn-entrectt" href="acido-hialuronico-50mg-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/acido-hialuronico-50mg-30-doses.png" alt="acido-hialuronico-50mg-30-doses" title="acido-hialuronico-50mg-30-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Ácido Hialurônico 50Mg 30 Doses</h3>
                            <p>O ÁCIDO HIALURÔNICO é um polissacarídeo da família...</p>
                            </div>
                            <a class="btn-entrectt" href="acido-hialuronico-50mg-30-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/oli-ola-300mg-peeling-30-capsulas.png" alt="oli-ola-300mg-peeling-30-capsulas" title="oli-ola-300mg-peeling-30-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Oli Ola 300Mg Peeling 30 Cápsulas</h3>
                            <p>Ao promover uma ação de “peeling em cápsulas”...</p>
                            </div>
                            <a class="btn-entrectt" href="oli-ola-300mg-peeling-30-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/pill-food-turbinado-120-capsulas.png" alt="pill-food-turbinado-120-capsulas" title="pill-food-turbinado-120-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Pill Food Turbinado 120 Cápsulas</h3>
                            <p>Pill Food Turbinado é um complexo alimentar formado...</p>
                            </div>
                            <a class="btn-entrectt" href="pill-food-turbinado-120-capsulas.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/oleo-de-rosa-mosqueta-30ml.png" alt="oleo-de-rosa-mosqueta-30ml" title="oleo-de-rosa-mosqueta-30ml" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Óleo de Rosa Mosqueta 30ml</h3>
                            <p>A Rosa Mosqueta é uma planta silvestre do grupo...</p>
                            </div>
                            <a class="btn-entrectt" href="oleo-de-rosa-mosqueta-30ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/colageno-hidrolisado-500mg-60-doses.png" alt="colageno-hidrolisado-500mg-60-doses" title="colageno-hidrolisado-500mg-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Colágeno Hidrolisado 500Mg 60 Doses</h3>
                            <p>Um organismo saudável necessita de colágeno para a manutenção...</p>
                            </div>
                            <a class="btn-entrectt" href="colageno-hidrolisado-500mg-60-doses.php">Saiba +</a>  
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/pill-food-turbinado-60-doses.png" alt="pill-food-turbinado-60-doses" title="pill-food-turbinado-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Pill Food Turbinado 60 Doses</h3>
                            <p>Pill Food Turbinado é um complexo alimentar formado...</p>
                            </div>
                            <a class="btn-entrectt" href="pill-food-turbinado-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/adifyline-2-locao-aumento-seios-e-gluteos-60gr.png" alt="adifyline-2-locao-aumento-seios-e-gluteos-60gr" title="adifyline-2-locao-aumento-seios-e-gluteos-60gr" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Adifyline 2% Loção Aumento Seios E Glúteos 60gr</h3>
                            <p>Adifyline 2% é indicado para os seios...</p>
                            </div>
                            <a class="btn-entrectt" href="adifyline-2-locao-aumento-seios-e-gluteos-60gr.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/actigym-5-50g.png" alt="actigym-5-50g" title="actigym-5-50g" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Actigym 5% 50G</h3>
                            <p>O actigym foi obtido pela biotecnologia, através de microorganismo...</p>
                            </div>
                            <a class="btn-entrectt" href="actigym-5-50g.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/actigym-5-100g.png" alt="actigym-5-100g" title="actigym-5-100g" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Actigym 5% 100G</h3>
                            <p>Actigym é um ativo marinho obtido por biotecnologia a partir...</p>
                            </div>
                            <a class="btn-entrectt" href="actigym-5-100g.php">Saiba +</a>                        
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/locao-colo-e-mamas-200ml.png" alt="locao-colo-e-mamas-200ml" title="locao-colo-e-mamas-200ml" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Loção Colo E Mamas 200Ml</h3>
                            <p>O lactato de amônio é responsável em parte pelo ator de hidratação cutânea...</p>
                            </div>
                            <a class="btn-entrectt" href="locao-colo-e-mamas-200ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/magnesio-dimalato-300mg-90-doses.png" alt="magnesio-dimalato-300mg-90-doses" title="magnesio-dimalato-300mg-90-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Magnésio Dimalato 300Mg 90 Doses</h3>
                            <p>O Magnésio é um mineral essencial para os todos os processos...</p>
                            </div>
                            <a class="btn-entrectt" href="magnesio-dimalato-300mg-90-doses.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/vitamina-b6-piridoxina-100mg-30-capsulas.png" alt="vitamina-b6-(piridoxina)-100mg-30-capsulas" title="vitamina-b6-(piridoxina)-100mg-30-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Vitamina B6 (Piridoxina) 100Mg 30 Cápsulas</h3>
                            <p>A Vitamina B6 também conhecida como cloridrato de piridoxina...</p>
                            </div>
                            <a class="btn-entrectt" href="vitamina-b6-piridoxina-100mg-30-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/l-teanina-200mg-60-capulas.png" alt="l-teanina-200mg-60-capulas" title="l-teanina-200mg-60-capulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>L-Teanina 200Mg 60 Cápulas</h3>
                            <p>L-Teanina é um aminoácido não comum na nossa dieta...</p>
                            </div>
                            <a class="btn-entrectt" href="l-teanina-200mg-60-capulas.php">Saiba +</a>  
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/oleo-de-primula-500mg-60-capsulas.png" alt="oleo-de-primula-500mg-60-capsulas" title="oleo-de-primula-500mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Óleo De Primula 500Mg 60 Cápsulas</h3>
                            <p>O Óleo de Prímula é extraído das sementes da Evening Primrose...</p>
                            </div>
                            <a class="btn-entrectt" href="oleo-de-primula-500mg-60-capsulas.php">Saiba +</a>
                        </div>
                    </div>
            </section>
        </div>    
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>