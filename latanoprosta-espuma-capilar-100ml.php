<?php

    $title       = "Latanoprosta Espuma Capilar 100mL";
    $description = "Visando garantir a qualidade dos produtos que necessitam de refrigeração, faremos envios exclusivamente de segunda-feira a quarta-feira."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Latanoprosta Espuma Capilar 100mL</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/latanoprosta-espuma-capilar-100ml.png" alt="latanoprosta-espuma-capilar-100ml" title="latanoprosta-espuma-capilar-100ml">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">Visando garantir a qualidade dos produtos que necessitam de refrigeração, faremos envios exclusivamente de segunda-feira a quarta-feira. Agradecemos a compreensão. Latanoprosta é um análogo direto da prostaglandina F2-alfa, comumente utilizado sob a forma de colírio no tratamento de glaucoma e hipertensão ocular, que apresenta como efeito adverso o aumento da densidade ciliar.</p>
                        <p class="text-justify">Com base nesta evidência, estudos avaliaram sua ação no tratamento de alopecia e foi observado o aumento significativo da densidade capilar, com o aumento tanto de pelos terminais quanto de pelos velus. Sua ação ocorre principalmente através do estímulo aos folículos capilares, prolongando a fase anágena e promovendo a conversão da fase telógena à fase anágena. OBS: APÓS O RECEBIMENTO, MANTER O PRODUTO SOB REFRIGERAÇÃO 2ºC A 8ºC. O PRODUTO É ENVIADO EM EMBALAGEM TÉRMICA AFIM DE GARANTIR A ESTABILIDADE.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>