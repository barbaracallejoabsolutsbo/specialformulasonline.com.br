<?php

    $title       = "Shampoo Regenerador 200Ml";
    $description = "O shampoo regenerador 200ML foi desenvolvido para uma aplicação em áreas específicas de queda ou falta de cabelo. Lave o cabelo com o shampoo..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Shampoo Regenerador 200Ml</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/shampoo-regenerador-200ml.png" alt="shampoo-regenerador-200ml" title="shampoo-regenerador-200ml">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">O shampoo regenerador 200ML foi desenvolvido para uma aplicação em áreas específicas de queda ou falta de cabelo. Lave o cabelo com o shampoo convencional e enxague, depois aplique o SHAMPOO OF nas áreas que deseja crescimento de fios, massageie, espere 5 minutos e enxague. O resultado é potencializado se após o enxague, secar o cabelo bem e aplicar a loção de Minoxidil com Propilenoglicol.</p>
                        <br>
                        <h2>ATIVOS</h2>
                        <p class="text-justify">Tintura de Jaborandi: o mais famoso fim terapêutico desta planta, pelo qual ganhou o apelido "remédio milagroso", é o fato de a tintura de jaborandi ser extremamente eficaz no tratamento da calvície. Loções capilares contendo esta planta em sua fórmula possuem ação tônica que estimula o crescimento e chega até interromper a queda de cabelos. Tintura de Capsicum: a capsaicina, principal componente da tintura de cápsicum, é responsável pela vasodilatação e oxigenação, estimulando a circulação do couro cabeludo, evitando a queda de cabelo.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="https://web.whatsapp.com/send?phone=5511944930219&text=Ol%C3%A1%21%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20da%20Exaustyair%20-%20Sistemas%20de%20Exaust%C3%A3o%20e%20Ventila%C3%A7%C3%A3o%20Industrial"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>