<?php

    $title       = "Orlistat 120Mg 60 Cápsulas";
    $description = "Orlistat é um fármaco direcionado para o tratamento da obesidade e do excesso de peso, e atua através da modificação da absorção das gorduras no intestino..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Orlistat 120Mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/orlistat-120mg-60-capsulas.png" alt="orlistat-120mg-60-capsulas" title="orlistat-120mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O MELHOR BLOQUEADOR DE GORDURA</h2>
                        <p class="text-justify">Orlistat é um fármaco direcionado para o tratamento da obesidade e do excesso de peso, e atua através da modificação da absorção das gorduras no intestino, acelerando a perda de peso, bem como a manutenção do peso perdido nas dietas de emagrecimento. Como se sabe, as gorduras que ingerimos são moléculas grandes que, para dissolver-se, necessitam ser rompidas e transformadas em moléculas menores por ação enzimática. Orlistat não é considerado um supressor do apetite, pois atua de modo diferente, atingindo as enzimas pancreáticas e gástricas responsáveis pela digestão da gordura (Sizer, et al., 2003).</p>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Este produto é contraindicado em indivíduos que apresentam colestase, síndrome da má absorção crônica, Crohn ou que possuem hipersensibilidade conhecida aos componentes presentes na formulação. Perdas ou evacuações oleosas, flatulência com perdas oleosas, urgência para evacuar, aumento das evacuações, desconforto/dor abdominal, flatulência, fezes líquidas, infecções do trato respiratório superior, gripe, cefaleia e hipoglicemia são reações adversas mais comum. Nunca compre medicamento sem orientação de um profissional habilitado. Pessoas com hipersensibilidade à substância não devem ingerir o produto. Imagens meramente ilustrativas. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o medicamento com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o medicamento se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem.</p>
                <p class="advertencias text-justify">Todo medicamento deve ser mantido fora do alcance das crianças. Este medicamento não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do medicamento durante o período de amamentação também não é recomendado. Este medicamento não deve ser utilizado por menores de 18 anos sem orientação médica. Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>