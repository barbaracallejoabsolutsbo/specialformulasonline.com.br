<?php

    $title       = "Minoxidil + D Pantenol + Auxina Tricógena 120ml";
    $description = "O Minoxidil + D Pantenol + Auxina Tricogena é uma fórmula desenvolvida pela Oficialfarma que combina a eficiência do Minoxidil, a hidratação proporcionada..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Minoxidil + D Pantenol + Auxina Tricógena 120ml</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/minoxidil-d-pantenol-auxina-tricogena-120ml.png" alt="minoxidil-d-pantenol-auxina-tricogena-120ml" title="minoxidil-d-pantenol-auxina-tricogena-120ml">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>MINOXIDIL + D PANTENOL + AUXINA TRICÓGENA</h2>
                        <p class="text-justify">O Minoxidil + D Pantenol + Auxina Tricogena é uma fórmula desenvolvida pela Oficialfarma que combina a eficiência do Minoxidil, a hidratação proporcionada pelo D Pantenol e o sinergismo entre extratos vegetais para o combate a queda capilar da Auxina Tricogena.</p>
                        <p class="text-justify">O Minoxidil + D Pantenol + Auxina Tricogena é uma loção capilar completa que normaliza o ciclo do folículo, prolongando a fase anágena e mantendo a saúde dos fios nas demais fases. Ele também tem a função de estimular a vascularização do couro cabeludopermitindo a oxigenação da área. A ação da loção capilar de Minoxidil + D Pantenol + Auxina Tricogena se dá pelo incremento dos processos de redox, normalizando as trocas metabólicas e respiratórias da raiz capilar, assim como a circulação sanguínea da fase anagênica.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>