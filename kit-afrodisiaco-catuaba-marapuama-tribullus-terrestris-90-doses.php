<?php

    $title       = "Kit Afrodisíaco - Catuaba + Marapuama + Tribullus Terrestris - 90 doses";
    $description = "A Catuaba, também conhecida como Alecrim-do-campo, Catuaba-verdadeira, Catuabinha, Catuíba, Catuaba-pau, Caramuru ou Tatuaba, é uma planta medicinal..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Kit Afrodisíaco - Catuaba + Marapuama + Tribullus Terrestris - 90 doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/kit-afrodisiaco-Catuaba-marapuama-tribullus-terrestris-90-doses.png" alt="kit-afrodisiaco-Catuaba-marapuama-tribullus-terrestris-90-doses" title="kit-afrodisiaco-Catuaba-marapuama-tribullus-terrestris-90-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>CATUABA</h2>
                        <p class="text-justify">A Catuaba, também conhecida como Alecrim-do-campo, Catuaba-verdadeira, Catuabinha, Catuíba, Catuaba-pau, Caramuru ou Tatuaba, é uma planta medicinal muito utilizada para fazer remédios afrodisíacos para problemas de impotência masculina. O nome científico da Catuaba é Anemopaegma mirandum e pode ser comprada em lojas de produtos naturais, farmácias de manipulação e alguns mercados e supermercados, na forma de pó, cápsulas, bebida alcoólica ou na sua forma natural.</p>
                        <br>
                        <h2>TRIBULUS TERRESTRIS</h2>
                        <p class="text-justify">Hoje no mercado existem uma infinidade de produtos naturais utilizados para promover o ganho de massa magra ou perda de gordura. O Tribulus Terrestris é um deles na verdade, é uma erva afrodisíaca usada também no aumento da massa muscular. O fato é que o extrato do Tribulus Terrestris vem sendo bastante consumido por praticantes de atividades físicas, em especial a musculação, com o intuito de estimular o desenvolvimento dos músculos.</p>
                    </div>
                </div>
                <h2>MARAPUAMA</h2>
                <p class="text-justify">A Marapuama, também conhecida como muirapuama, muiratã e Viagra da Amazônia é uma planta nativa da floresta amazônica, muito utilizada pelos índios nativos do Brasil. Essa planta, de nome científico Ptychopetalum uncinatum, possui muitos ácidos, como o lignocerico, campesterol, behenico, araquídidco, além de óleos essenciais e ésteres.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>