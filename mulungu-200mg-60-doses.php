<?php

    $title       = "Mulungu 200Mg 60 Doses";
    $description = "O mulungu tem longo uso na medicina popular brasileira, sendo uma planta medicinal muito encontrada na Floresta Amazônica. O mulungo é usado na medicina..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Mulungu 200Mg 60 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/mulungu-200mg-60-doses.png" alt="mulungu-200mg-60-doses" title="mulungu-200mg-60-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">O mulungu tem longo uso na medicina popular brasileira, sendo uma planta medicinal muito encontrada na Floresta Amazônica. O mulungo é usado na medicina natural como um sedativo e calmante natural para problemas do sistema nervoso, incluindo o estresse, ansiedade e depressão. Como sonífero natural, possui efeito hepatônico, ou seja, tonifica, equilibra e fortalece o fígado, além de combater hepatite, obstruções, alto níveis de enzima presentes no fígado e esclerose.</p>
                        <p class="text-justify">O mulungu também alivia a tosse e os sintomas do stress, ansiedade, depressão, histeria, ataques de pânico e transtornos compulsivos, sendo útil ainda para pessoas que sofrem de insônia e agitação durante o sono. O efeito calmante da planta também pode beneficiar algumas pessoas que sofrem de abstinência de nicotina (cigarro) ou uso de drogas.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>