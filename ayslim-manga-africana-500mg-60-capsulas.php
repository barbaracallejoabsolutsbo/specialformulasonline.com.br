<?php

    $title       = "Ayslim Manga Africana 500Mg 60 Cápsulas";
    $description = "O O Ayslim Manga Africana, desenvolvido com propriedades adjuvantes que reduz os níveis de colesterol, ajuda na taxa glicêmica e tem efeito laxativo..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Ayslim Manga Africana 500Mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/ayslim-manga-africana-500mg-60-capsulas.png" alt="ayslim-manga-africana-500mg-60-capsulas" title="ayslim-manga-africana-500mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>Descrição</h2>
                        <p class="text-justify">O O Ayslim Manga Africana, desenvolvido com propriedades adjuvantes que reduz os níveis de colesterol, ajuda na taxa glicêmica e tem efeito laxativo.</p>
                        <p class="text-justify">É adjuvante também no tratamento da obesidade, diabetes e doenças relacionadas, com o propósito de controlar a absorção de lipídeos da dieta e o peso corporal. Promove a saciedade, tem efeito laxativo e controle a glicemia e o colesterol.</p>
                        <p class="text-justify">O Ayslim Manga Africana foi desenvolvido com propriedades adjuvantes que reduz os níveis de colesterol, ajuda na taxa glicêmica e tem efeito laxativo. Auxilia também no tratamento da obesidade e da diabete.</p>
                        <h2>Composição</h2>
                        <h3>Cada 1 cápsula contém:</h3>
                        <ul>
                            <li>Ayslim Manga Africana (Irvingia gabonesis) 500mg</li>
                            <li>Excipiente q.s.p 1 cápsula</li>
                            <li>Amido, talco, dióxido de silício, estearato de magnésio</li>
                        </ul>
                    </div>
                </div>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>