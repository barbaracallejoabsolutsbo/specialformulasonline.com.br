<?php
    $title       = "Tratamento de queda de cabelo";
    $description = "Medicamentos como Minoxidil, Finasterida, Espironolactona, Alfaestradiol, entre outros, são utilizados na composição de fórmulas para tratamento de queda de cabelo.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Hoje existem diversas opções de <strong>tratamento de queda de cabelo.</strong> A alimentação, descanso, rotina, estresse, entre outros fatores, influenciam diretamente na saúde dos seus fios capilares. O uso exacerbado ou contínuo de medicamentos também pode afetar os fios do cabelo, por isso, tomar apenas o que o seu médico receitar é a melhor opção.</p>
<p>Bons hábitos de higiene, um bom descanso, boa hidratação e alimentação, sempre irão potencializar o seu <strong>tratamento de queda de cabelo</strong>. Caso seu médico receite algum tipo de remédio ou fórmula, a Special Fórmulas oferece todo suporte para manipulação de seus fármacos sob dosagens especificadas pelo seu profissional da saúde.</p>
<p>Com formas de administração em shampoos, condicionadores e tônicos capilares, cremes hidratantes, e muitas outras opções, a Special Fórmulas trabalha com primor para oferecer os manipulados de melhor qualidade de absorção e aproveitamento para seu <strong>tratamento de queda de cabelo</strong>. Confira nossas condições e compre com a maior farmácia de manipulados da região.</p>
<p>Medicamentos como Minoxidil, Finasterida, Espironolactona, Alfaestradiol, entre outros, são utilizados na composição de fórmulas para <strong>tratamento de queda de cabelo</strong>. Para obter suas receitas prescritas pelo médico, fale conosco pelo nosso atendimento e envie seu receituário escaneado. Assim você pode comprar o conforto da sua casa e receber seus medicamentos manipulados com toda qualidade, atenção, segurança e profissionalismo.</p>
<h2><strong>Procure seu médico e encontre o melhor tratamento de queda de cabelo</strong></h2>
<p>Apenas seu médico poderá lhe oferecer as melhores opções de<strong> tratamento de queda de cabelo. </strong>Consulte-o e traga sua receita para manipulação conosco. A Special Fórmulas trabalha com matérias-primas exclusivas de fontes certificadas e controladas com todo rigor. Trabalhamos com maquinário e profissionais especializados para o ramo farmacêutico magistral, manipulando seus fármacos com toda atenção minuciosa e segurança. Compre manipulados da melhor qualidade do mercado com a Special Fórmulas.</p>
<h2><strong>Tratamento de queda de cabelo com Minoxidil, fortaleça e estimule o crescimento </strong></h2>
<p>O <strong>tratamento de queda de cabelo </strong>com o Minoxidil é o que mais estimula o crescimento dos fios capilares. Fortalece e estimula os bulbos capilares melhorando os fios que crescem muito mais nutridos, evitando quedas. Compre seus medicamentos manipulados para tratamentos em geral com a Special Fórmulas. </p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>