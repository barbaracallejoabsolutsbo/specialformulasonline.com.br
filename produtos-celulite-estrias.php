<?php

    $title       = "Celulite e Estrias";
    $description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elementum ultrices commodo. Donec quis dictum tortor."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "home",
        "produtos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="container">
            <section class="nossos-produtos catego-prod container">
                <h1 class="text-center">Celulite e Estrias</h1>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/centella-asiatica-500mg-60-capsulas.png" alt="centella-asiatica-500mg-60-capsulas" title="centella-asiatica-500mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Centella Asiática 500Mg 60 Cápsulas</h3>
                            <p>Centella Asiatica 500mg da linha conhecida como gotu kola...</p>
                            </div>
                            <a class="btn-entrectt" href="centella-asiatica-500mg-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/dimpless-40mg-30-capsulas.png" alt="dimpless-40mg-30-capsulas" title="dimpless-40mg-30-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Dimpless 40Mg 30 Cápsulas</h3>
                            <p>Dimpless 40mg, foi desenvolvido com propriedades para auxiliar...</p>
                            </div>
                            <a class="btn-entrectt" href="dimpless-40mg-30-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/drenagem-linfatica-em-capsulas-500mg-60-capsulas.png" alt="drenagem-linfatica-em-capsulas-500mg-60-capsulas" title="drenagem-linfatica-em-capsulas-500mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Drenagem Linfática Em Cápsulas 500mg 60 Cápsulas</h3>
                            <p>Cactin é o extrato seco obtido do fruto do cacto Opuntia...</p>
                            </div>
                            <a class="btn-entrectt" href="drenagem-linfatica-em-capsulas-500mg-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/pholia-magra-300mg-60-capsulas.png" alt="pholia-magra-300mg-60-capsulas" title="pholia-magra-300mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Pholia Magra 300Mg 60 Cápsulas</h3>
                            <p>É um tipo de suplemento produzido a partir da erva chamada...</p>
                            </div>
                            <a class="btn-entrectt" href="pholia-magra-300mg-60-capsulas.php">Saiba +</a>
                        </div>
                    </div>
            </section>
        </div>    
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>