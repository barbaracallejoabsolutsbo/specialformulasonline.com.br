<?php

    $title       = "Óleo De Coco 1000mg 120 Cápsulas";
    $description = "O Óleo de Coco é rico em Ácidos Graxos de Cadeia Média (triglicerídeos de cadeia média), cerca de 65%. O Óleo de Coco tem sido usado no Pacífico durante séculos"; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Óleo De Coco 1000mg 120 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/oleo-de-coco-1000mg-120-capsulas.png" alt="oleo-de-coco-1000mg-120-capsulas" title="oleo-de-coco-1000mg-120-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>ÓLEO DE COCO</h2>
                        <p class="text-justify">O Óleo de Coco é rico em Ácidos Graxos de Cadeia Média (triglicerídeos de cadeia média), cerca de 65%. O Óleo de Coco tem sido usado no Pacífico durante séculos como uma parte importante da dieta no uso diário, e recentes pesquisas confirmam as crenças tradicionais de que o coqueiro é a "Árvore da Vida" e que fruto e óleo de coco têm um papel importante a desempenhar em uma dieta equilibrada, nutritiva. O Óleo de Coco Extra Virgem é obtido por prensagem a frio da carne do coco desidratada.</p>
                        <p class="text-justify">O Óleo de Coco é distinguido por sua cadeia curta de ácidos graxos e seu ponto de fusão relativamente baixo. O Óleo de Coco Extra Virgem é usado na indústria alimentícia como um ingrediente gorduroso em muitos produtos como margarina, chocolate, sopas instantâneas e outros. Na indústria farmacêutica e cosmética é usado como base e veículo de substâncias, em pomadas e protetores solar.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>