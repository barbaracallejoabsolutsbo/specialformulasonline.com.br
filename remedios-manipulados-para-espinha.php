<?php
    $title       = "Remedios manipulados para espinha";
    $description = "Todos medicamentos possuem possíveis efeitos colaterais, por esse motivo, saiba sempre a composição de seus remédios manipulados para espinha e consulte seu médico.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Encontre <strong>remédios manipulados para espinha</strong> para uso tópico, fórmulas manipuladas para auxílio no combate a acne e muito mais. A Special Fórmulas é uma farmácia magistral, especializada na manipulação de fármacos. Para comprar produtos manipulados conosco, consulte seu médico e traga suas receitas prescritas por ele. Manipule fórmulas com a maior farmácia de manipulação da região.</p>
<p>Os <strong>remédios manipulados para espinha</strong> podem agir de diversas formas, tudo depende do que for prescrito pelo seu dermatologista. Algumas fórmulas prontas estão disponíveis em todo mercado de fármacos industrializados do Brasil, mas com a Special Fórmulas você manipula de forma personalizada dosagens específicas de cada substância receitada. Com opções de manipulação para uso oral, tópico, anticoncepcionais da indústria farmacêutica, entre outros, consulte seu médico especialista e tenha o suporte profissional para obter sua prescrição de forma adequada.</p>
<p>Todos medicamentos possuem possíveis efeitos colaterais, por esse motivo, saiba sempre a composição de seus <strong>remédios manipulados para espinha</strong> e consulte seu médico. Disponíveis para manipulação em cremes, pomadas, loções, cápsulas, comprimidos, entre muitas outras opções, a Special Fórmulas atua utilizando de todas Boas Práticas de Manipulação em Farmácia para fabricar seus manipulados.</p>
<p>Um dos remédios mais populares no mercado farmacêutico é o Roacutan. Com isotretinoína na composição, age reduzindo o tamanho das glândulas sebosas e sua atividade, sendo uma das substâncias do mercado de fármacos industrializados mais utilizadas para tratamento de acne. Como alternativa, dermatologistas passaram a substituir tratamentos com Roacutan com cremes e <strong>remédios manipulados para espinha</strong> para via oral com substâncias alternativas e menos nocivas para o organismo.</p>
<h2><strong>Substitua Roacutan por remédios manipulados para espinha receitados pelo seu médico</strong></h2>
<p>Consulte seu dermatologista e tenha alternativas melhores de tratamento para acne com<strong> remédios manipulados para espinha. </strong>Para potencializar os efeitos dos remédios para acne, evite o uso de esfoliantes, tratamentos a laser, hidratar e proteger a pele e os lábios, se hidratar adequadamente, entre outros bons hábitos de rotina que devem ser adotados. Alguns desses remédios para espinha não devem ser utilizados durante a gravidez ou amamentação como no caso da clindamicina e medicamentos retinóides, mas hoje já existem opções e alternativas para mulheres lactantes e grávidas, consulte seu profissional dermatologista e tenha suporte adequado.</p>
<h2><strong>Remédios manipulados para espinha fazem mal?</strong></h2>
<p><strong>Remédios manipulados para espinha</strong> não fazem mal desde que prescritos e recomendados por uma consulta a um profissional da saúde. Dessa forma, os medicamentos podem ser prescritos e manipulados de forma correta, de acordo com dosagem e substâncias para os objetivos de cada paciente.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>