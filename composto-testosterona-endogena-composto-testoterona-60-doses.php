<?php

    $title       = "Composto Testosterona Endógena";
    $description = "Considerado o hormônio mais importante para o ganho de massa muscular, a testosterona age por meio de reações bioquímicas. Ela é considerada um dos hormônios..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Composto Testosterona Endógena "COMPOSTO TESTOSTERONA" 60 doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/composto-testosterona-endogena-composto-testoterona-60-doses.png" alt="composto-testosterona-endogena-composto-testoterona-60-doses" title="composto-testosterona-endogena-composto-testoterona-60-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>GANHO DE MASSA MAGRA</h2>
                        <p class="text-justify">Considerado o hormônio mais importante para o ganho de massa muscular, a testosterona age por meio de reações bioquímicas. Ela é considerada um dos hormônios anabólicos mais importantes do corpo e está relacionada à força física, resistência e desempenho. Por isso, está diretamente ligada ao desenvolvimento muscular. Podendo ser usado tanto por homens quanto por mulheres, o Composto Testosterona Endógena tem a poderosa capacidade de potencializar a produção natural de testosterona.</p>
                        <br>
                        <h2>PERDA DE GORDURA CORPORAL</h2>
                        <p class="text-justify">Além de potencializar o ganho de massa magra, a testosterona ajuda a reduzir o índice de gordura corporal. Com os níveis de testosterona adequados, o corpo passa a consumir mais energia e, como consequência, queima muito mais calorias. Principal hormônio andrógeno e anabólico, a testosterona é produzida naturalmente e em grandes quantidades no organismo masculino e feminino.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Embora raros, podem ser apresentados eritema local, descamação, prurido, hipotensão arterial, náuseas, fadiga, erupção cutânea, cefaleia. Retenção hidrossalina com edemas, hipertricose. Pode causar efeito Shedding (os fios que cairiam aos poucos, caem todos de uma única vez). Nunca compre medicamento sem orientação de um profissional habilitado. Imagens meramente ilustrativas. Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o medicamento com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o medicamento se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Todo medicamento deve ser mantido fora do alcance das crianças. Este medicamento não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do medicamento durante o período de amamentação também não é recomendado.</p>
                <p class="advertencias text-justify">Este medicamento não deve ser utilizado por menores de 18 anos sem orientação médica. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO”. "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>