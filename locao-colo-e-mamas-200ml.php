<?php

    $title       = "Loção Colo E Mamas 200Ml";
    $description = "Beta-caroteno melhor conhecido como um precursor da vitamina A, é um importante antioxidante que ajuda a prevenir danos de radicais livres prejudiciais no corpo"; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Loção Colo E Mamas 200Ml</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/locao-colo-e-mamas-200ml.png" alt="locao-colo-e-mamas-200ml" title="locao-colo-e-mamas-200ml">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>ATIVOS</h2>
                        <p class="text-justify"><b>Lactato de Amônio:</b> O lactato de amônio é responsável em parte pelo ator de hidratação cutânea, ou seja, pela capacidade da pele de reter água. Tem um forte poder hidratante promovendo maior turgor e brilho da pele. Em diversos estudos realizados com aplicação do Lactato de Amônio constatou-se que, comprovadamente, aumenta a quantidade de glicosaminoglicanas presentes na derme que são substâncias importantes, pois: promovem a renovação celular, restauram a função da pele e intensificam os mecanismos de proteção e atividades imunoestimulantes.</p>
                        <p class="text-justify"><b>Alantoína:</b> Alantoína é um produto atoxico, não irritante, com reconhecidas propriedades dermatológicas anti-idade, renovador celular e melhora a umectação da pele.</p>
                        <p class="text-justify"><b>Ácido Láctico:</b> O Ácido Láctico é um alfa-hidroxiácido que ocorre naturalmente na pele e é conhecido pelo seu alto poder umectante. Aplicado na pele, o ácido láctico e seus sais (lactatos) atuam como agente antimicrobiano, regulador de pH, hidratante, umectante, agente rejuvenescedor e clareador da pele.</p>
                    </div>
                </div>
                <p class="text-justify"><b>MATRIXYL:</b> 45% menos rugas em dois meses. O Matrixyl age sinergicamente para reparar os danos do envelhecimento cutâneo. São mensageiras da reestruturação e do reparo cutâneo. Elas ativam a neo-síntese das macromoléculas da matriz extracelular, proporcionando ao Matrixyl™3000 uma eficácia visível anti-rugas.</p>
                <p class="text-justify"><b>DMAE Glicolato:</b> Manter a pele jovem atualmente está cada vez mais fácil e ao alcance de todos. Vitamina C, filtros solares, peelings, tratamentos com ácidos e agora mais uma novidade: DMAE. De acordo com alguns estudos apresentados no Meeting da Academia Americana de Dermatologia demonstraram que o Dimetilaminoetanol, conhecido com DMAE, exerce efeito positivo no combate à flacidez e na melhoria do aspecto da pele.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>