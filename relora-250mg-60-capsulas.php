<?php

    $title       = "Relora 250Mg 60 Cápsulas";
    $description = "Relora é a combinação patenteada de constituintes ativos de plantas, o honokiol e a berberina, com eficácia comprovada na diminuição da ansiedade..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Relora 250Mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/relora-250mg-60-capsulas.png" alt="relora-250mg-60-capsulas" title="relora-250mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">Relora é a combinação patenteada de constituintes ativos de plantas, o honokiol e a berberina, com eficácia comprovada na diminuição da ansiedade, agitação do sono, níveis de cortisol e compulsão alimentar relacionada ao estresse. Berberina também possui efeitos ansiolíticos e antidepressivos. Relora é eficaz na redução do estresse e ansiedade e capaz de aumentar os sentimentos de bem-estar. Pela ação sinérgica dos seus dois ativos, Relora funciona por meio do equilíbrio do hipotálamo no cérebro, que regula funções importantes como níveis hormonais, peso e o ciclo do sono. Ao promover efeito calmante e ansiolítico, Relora ajuda a diminuir os níveis de cortisol e restabelecer o equilíbrio do eixo HPA.</p>
                        <h2 class="advertencias text-center">Advertências</h2>
                        <p class="advertencias text-justify">Nunca compre medicamento sem orientação de um profissional habilitado. Imagens meramente ilustrativas. Pessoas com hipersensibilidade à substância não devem ingerir o produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o medicamento com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC).</p>
                    </div>
                </div>
                <p class="advertencias text-justify">Proteger da luz, do calor e da umidade. Nestas condições, o medicamento se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Todo medicamento deve ser mantido fora do alcance das crianças. Este medicamento não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do medicamento durante o período de amamentação também não é recomendado. Este medicamento não deve ser utilizado por menores de 18 anos sem orientação médica. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>