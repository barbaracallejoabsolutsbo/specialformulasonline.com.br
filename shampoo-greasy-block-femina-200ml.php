<?php

    $title       = "SHAMPOO GREASY BLOCK FEMINA 200ML";
    $description = "Beta-caroteno melhor conhecido como um precursor da vitamina A, é um importante antioxidante que ajuda a prevenir danos de radicais livres prejudiciais no corpo"; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">SHAMPOO GREASY BLOCK FEMINA 200ML</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/shampoo-greasy-block-femina-200ml.png" alt="shampoo-greasy-block-femina-200ml" title="shampoo-greasy-block-femina-200ml">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>Descrição</h2>
                        <p class="text-justify">Seu couro cabeludo é muito oleoso e necessita lavar os cabelos todos os dias? O Shampoo Greasy Block Femina vem para controlar essa oleosidade através de ativos que garantem o bom funcionamento das células. Ao contrário da maioria dos ingredientes dos shampoos comuns, Greasy Block Femina atua profundamente nos poros, evitando o acúmulo de oleosidade e ajudando a desobstruir os folículos capilares.</p>
                        <p class="text-justify">Oleosidade excessiva não é um problema de higiene ou falta de lavagem do couro cabeludo, mas sim um distúrbio da glândula sebácea. Certas pessoas apresentam uma ativação muito elevada dessa glândula, produzindo sebo e oleosidade em excesso. Isso pode acontecer por distúrbios hormonais, genética, calor excessivo, stress, alimentação inadequada entre outros motivos.</p>
                    </div>
                </div>
                <h2>EXTRATO NATURAIS</h2>
                <p class="text-justify">Os taninos são responsáveis por tornar o extrato de hamamelis um adstringente natural, pois removem o excesso de óleo do couro cabeludo. Esta é uma das razões pela qual o hamamélis é um dos tratamentos naturais mais populares. No cabelo ajuda a reduzir o acúmulo de óleo, o que faz com que os fios fiquem mais brilhantes, saudáveis e mais volumosos. Também é ótimo para remover odores capilares e ajudar no couro cabeludo irritado. Os benefícios do chá verde para os cabelos se dão através dos nutrientes e antioxidantes presentes nele.</p>
                <p class="text-justify">Um deles são as vitaminas do complexo B, pois auxiliam na saúde dos fios, ajuda a controlar as pontas duplas, amacia e fortalece as raízes capilares. Os ativos naturais presentes em Greasy Block Femina agem em sinergia reduzindo a oleosidade e estimulando o crescimento capilar!</p>
                <p class="text-justify">O Greasy Block Femina age com eficiência contra diversos problemas capilares que podem levar à queda e à calvície. O shampoo conta com substâncias em sua composição que atuam profundamente nos poros, evitando o acúmulo de óleo, ajudando a desobstruir os folículos capilares e reduzindo a caspa.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. O uso do produto durante o período de amamentação sem orientação médica, também não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. Nunca compre o produto sem orientação de um profissional habilitado. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>