<?php

    $title       = "BioBlanc 2% Serum Com Oliva Francesa Para Clareamento Cutâneo";
    $description = "As oliveiras cultivadas para a fabricação do Bioblanc® crescem em meio a plantações de lavanda (lavandula officinalis) no Sul da França."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">BioBlanc 2% Serum Com Oliva Francesa Para Clareamento Cutâneo - 30g</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/bioblanc-2-serum-com-oliva-francesa-para-clareamento-cutaneo-30g.png" alt="bioblanc-2-serum-com-oliva-francesa-para-clareamento-cutaneo-30g" title="bioblanc-2-serum-com-oliva-francesa-para-clareamento-cutaneo-30g">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">As oliveiras cultivadas para a fabricação do Bioblanc® crescem em meio a plantações de lavanda (lavandula officinalis) no Sul da França. Os polifenóis que são encontrados em abundância no Bioblanc são antioxidantes extremamente eficientes. Os polifenóis são excepcionalmente estáveis e protetores. O membro mais poderoso do grupo de polifenóis presentes na oliva é o hidroxitirosol. Estudos recentes mostram que o hidroxitirosol protege os melanócitos humanos de danos causados pela radiação UV e atua no combate a inflamação cutânea.</p>
                        <br>
                        <h2>AÇÃO</h2>
                        <p class="text-justify">A emulsão com BioBlanc® 2% possui toque seco e suave, sendo recomendado para todos os tipos de pele. Apresenta eficácia em relação ao clareamento de manchas cerca de 10 vezes maior que o ácido kójico e não causa ardência e nem irritações. Devido a alta concentração de hidroxitirosol presente no BioBlanc®, atua fortemente na respiração celular cutânea e na manutenção das funções fisiológicas da pele, combatendo o envelhecimento. Hidroxitirosol possui o mais alto nível de atividade de absorção de radicais livres já registrado por um antioxidante natural. É absorvido prontamente devido a sua afinidade com a pele.</p>
                    </div>
                </div>
                <h2>PODEROSO ALIADO</h2>
                <p class="text-justify">Ele estimula a síntese de novos fibroblastos e consequentemente aumentam a síntese de colágeno de modo a prevenir e combater os sinais do envelhecimento. BioBlanc® é o princípio ativo disponível no mercado com maior concentração de hidroxitirosol (6%) por isso mais efetivo em termos de ação despigmentante, antioxidante e antimicrobiana.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>