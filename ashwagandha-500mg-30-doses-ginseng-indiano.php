<?php

    $title       = "Ashwagandha 500Mg 30 Doses Ginseng indiano";
    $description = "Ashwagandha é uma planta fitoterápica indiana usada por séculos por sua capacidade de aumentar a energia mental e física. A ashwagandha é amplamente..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Ashwagandha 500Mg 30 Doses Ginseng indiano</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/ashwagandha-500mg-30-doses-ginseng-indiano.png" alt="ashwagandha-500mg-30-doses-ginseng-indiano" title="ashwagandha-500mg-30-doses-ginseng-indiano">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É ASHWAGANDHA?</h2>
                        <p class="text-justify">Ashwagandha é uma planta fitoterápica indiana usada por séculos por sua capacidade de aumentar a energia mental e física. A ashwagandha é amplamente usada no herbalismo ocidental moderno como um adaptogênico, uma substância que pode ajudar o corpo a se adaptar de maneira saudável ao estresse fisiológico e psicológico, aumentando, assim, a resistência aos agentes estressantes. A Ashwagandha também é muito escolhida por contribuir com efeito energético, pois pode diminuir também a produção em excesso do hormônio Cortisol.</p>
                        <p class="text-justify">A Ashwagandha é indicada para aumentar o desejo sexual, tratar impotência, aumentar a força muscular, reduzir a fraqueza, aumentar os níveis de energia no organismo, estimular o sistema imune, estabilizar os níveis de açúcar no sangue, reduzir o colesterol e tratar a insônia. Também pode ser usada no tratamento do câncer, pois tem propriedades que tornam as células cancerígenas mais sensíveis à radioterapia ou quimioterapia. Pode ajudar a reduzir as reações de estresse, inclusive a ansiedade e a tensão, enquanto também oferece uma melhoria em termos de energia física durante momentos de estresse. Ajuda o corpo a desenvolver resistência contra diversas condições de saúde, a desenvolver resistência contra a pressão e ajudar a desacelerar o processo de envelhecimento.</p>
                    </div>
                </div>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>