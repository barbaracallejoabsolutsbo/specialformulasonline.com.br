<?php

    $title       = "Lisina + Prolina - 60 Cápsulas";
    $description = "Lisina é um aminoácido essencial e indispensável para o organismo. Ajuda a enriquecer a alimentação e possui relevantes benefícios para a saúde."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Lisina + Prolina - 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/lisina-prolina-60-capsulas.png" alt="lisina-prolina-60-capsulas" title="lisina-prolina-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>LISINA</h2>
                        <p class="text-justify">Lisina é um aminoácido essencial e indispensável para o organismo. Ajuda a enriquecer a alimentação e possui relevantes benefícios para a saúde, uma vez que aumenta a imunidade, prevenindo a gripe e a herpes. Ajuda a aumentar a apetite e a excreção do suco gástrico, para uma melhor digestão. Auxilia no crescimento das crianças. Além disso, a suplementação é muito importante por repor a carência deste aminoácido que só pode ser obtido através da alimentação, prevenindo a fadiga, anemia, náusea, desordens visuais e atordoamento.</p>
                        <br>
                        <h2>PROLINA</h2>
                        <p class="text-justify">Também conhecida como L-prolina por sua estrutura molecular, a prolina é um aminoácido indispensável para a síntese de colágeno. Entre outras funções, o colágeno é essencial para a saúde da pele, ossos, tendões e também dá suporte para o tecido muscular. A prolina tem ainda atuação preventiva nas lesões e doenças do sistema cardiovascular.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="https://web.whatsapp.com/send?phone=5511944930219&text=Ol%C3%A1%21%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20da%20Exaustyair%20-%20Sistemas%20de%20Exaust%C3%A3o%20e%20Ventila%C3%A7%C3%A3o%20Industrial"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>