<?php

    $title       = "Gamma Oryzanol 300Mg 60 Cápsulas";
    $description = "É conhecido como óleo de arroz. o metabólito ativo é o ácido ferúlico que circula pela corrente sanguínea e pode ser absorvido por tecidos no corpo..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Gamma Oryzanol 300Mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/gamma-oryzanol-300mg-60-capsulas.png" alt="gamma-oryzanol-300mg-60-capsulas" title="gamma-oryzanol-300mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>GAMMA ORYZANOL 300MG</h2>
                        <p class="text-justify">É conhecido como óleo de arroz. o metabólito ativo é o ácido ferúlico que circula pela corrente sanguínea e pode ser absorvido por tecidos no corpo. o ácido ferúlico tem um efeito na liberação de hormônios, especialmente em aumentar os níveis de testosterona. gamma oryzanol é também conhecido por estimular o hipotálamo a liberar o hormônio do crescimento (gh), que estimula o organismo a produzir o (hgh). ambos, testosterona e gh humano possuem efeitos de crescimento muscular. gama oryzanol tem sido cientificamente sugerido para aumentar a força, reduzir o excesso de gordura corporal, melhorar a energia durante os treinos e reduzir a dor pós-treino. também mantêm uma libido saudável.</p>
                        <h2>BENEFÍCIOS</h2>
                        <ul>
                            <li>Alternativa natural para esteroides anabólicos;</li>
                            <li>Aumenta a força muscular, tamanho e definição;</li>
                            <li>Reduz a sensação de dor;</li>
                            <li>Reduz os níveis de colesterol;</li>
                            <li>Protege contra câncer;</li>
                            <li>Diminui a fadiga muscular;</li>
                            <li>Diminui os níveis de colesterol e triglicerídeos;</li>
                            <li>Melhora a performance física.</li>
                        </ul>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado.</p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>