<?php
    $title       = "Remedios manipulados para academia";
    $description = "Encontre o melhor suporte para manipulação de remédios manipulados para academia, controlados, composições especiais e dosagens específicas para cada caso.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Conheça a maior farmácia de manipulação que conta com suporte total para fabricar <strong>remédios manipulados para academia</strong>. Entre fórmulas convencionais e personalizadas, trabalhamos com primor para oferecer produtos confeccionados a partir de matérias primas exclusivas, certificadas, controladas e de alto padrão de qualidade. Consulte nosso atendimento para mais informações de como encomendar seus manipulados.</p>
<p>Encontre o melhor suporte para manipulação de <strong>remédios manipulados para academia</strong>, controlados, composições especiais e dosagens específicas para cada caso. Com manipulação farmacêutica seguindo criteriosamente todas as informações de prescrição médica.</p>
<p>O melhor custo x benefício do mercado, você também encontra em nosso site fórmulas fitness para uso de atletas e praticantes de musculação, academia e esportes. Fórmulas seguras, extremamente eficazes e com preço altamente acessível. Consulte sempre seu médico antes de qualquer tipo de uso medicamentoso. Mas quais são os melhores <strong>remédios manipulados para academia?</strong></p>
<p>A verdade é que não existe o melhor, e sim o mais eficiente para cada objetivo, por esse motivo consultar seu profissional da saúde é a melhor opção. Encontre substâncias que não são encontradas no mercado farmacêutico industrializado nas dosagens necessárias para uso no dia a dia e tenha remédios manipulados exclusivos com a Special Fórmulas. Os <strong>remédios manipulados para academia</strong> contam com personalização total, dessa forma, você pode ter de forma isolada e controlada cada substância prescrita pelo seu médico, em uma fórmula ou em várias. Consulte-nos para mais informações e orçamentos.</p>
<h2><strong>Porque comprar remédios manipulados para academia?</strong></h2>
<p>Todas as farmácias convencionais contam com uma infinidade de medicamentos e fórmulas que servem também para o ramo fitness. No entanto, na maioria dos casos, não são encontradas em fórmulas e dosagens personalizadas para serem ministrados de forma mais precisa e isolada. Cada pessoa possui particularidades nos objetivos e necessidades que devem ser analisados por um profissional a fim de que este prescreva adequadamente o que é mais aconselhável e efetivo para o paciente. Dessa forma você consegue fabricar seus <strong>remédios manipulados para academia</strong> conosco. Traga sua receita e manipule-os com nossa farmácia magistral especializada em diversos meios de manipulação com todas Boas Práticas de Manipulação em Farmácia.</p>
<h2><strong>Remédios manipulados para academia e variedades disponíveis em nosso catálogo</strong></h2>
<p>Encontre artigos para homeopatia, fitoterápicos, fórmulas nutricionais para o ramo da saúde, fitness, natural, florais, manipulações prescritas com diversas formas de consumo e uso, entre outras opções e artigos do ramo farmacêutico. Somos uma farmácia de manipulação especializada e atendemos fabricando <strong>remédios manipulados para academia</strong>, seja para performance, ganho de massa, inibidores de apetite, entre outras opções.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>