<?php

    $title       = "Serum Intensificador De Brilho";
    $description = "O D-pantenol é uma espécie de álcool hidrossolúvel e rico em vitamina B5, que é um nutriente essencial para cuidar da pele e dos cabelos. Ao ser aplicado..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Serum Intensificador De Brilho</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/serum-intensificador-de-brilho.png" alt="serum-intensificador-de-brilho" title="serum-intensificador-de-brilho">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>Serum Aumentador de Brilho</h2>
                        <h3>D Pantenol</h3>
                        <p class="text-justify">O D-pantenol é uma espécie de álcool hidrossolúvel e rico em vitamina B5, que é um nutriente essencial para cuidar da pele e dos cabelos. Ao ser aplicado em qualquer uma dessas regiões e nas unhas, o produto ativa o ácido pantotênico, presente naturalmente nos fios e na derma, proporcionando mais hidratação e saúde.</p>
                        <br>
                        <h3>Indicações e Benefícios:</h3>
                        <p class="text-justify">O D-pantenol é um elemento rico em agentes hidratantes, e atua como um restaurador dos cabelos ao ativar o ácido pantotênico na raiz capilar. Os benefícios serão de maior maleabilidade dos fios, proporcionando a diminuição de pontas duplas, menos frizz e mais hidrataçãoCom o uso do D-pantenol, as madeixas ficam protegidas da quebra e queda, além de crescerem de forma saudável, fortes e brilhosos. Portanto, o produto é indicado para mulheres com cabelos tingidos, danificados ou quebradiços.</p>
                    </div>
                </div>
                <h3>Ext Glic de Aloe Vera</h3>
                <p class="text-justify">O Extrato de Aloe Vera é extraído do interior da filha da babosa, certificada como orgânica biodinâmica. O produto traz inúmeros benefícios para a pele, o cabelo e o corpo, agindo como hidratante, emoliente, refrescante, dando mais brilho e fortalecendo os cabelos. O Extrato de Aloe Vera possui naturalmente vitaminas, polissacarídeos, alantoína, ácido salicílico e outras substâncias extremamente benéficas à saúde.</p>
                <br>
                <h3>Benefícios para os Cabelos:</h3>
                <p class="text-justify">Contribui para o crescimento dos cabelos e condicionamento do couro cabeludo. Abre os poros do couro cabeludo, assim permitindo melhor absorção de nutrientes e vitaminas e potencializando brilho e beleza. Por seus efeitos na reconstrução capilar, é também conhecido como uma “queratina vegetal”. Esse efeito “queratina vegetal” aumenta a flexibilidade dos fios, evitando quebra e dando aspecto saudável. Seus aminoácidos nutrem, revitalizam e promovem brilho extremo aos cabelos. Promove limpeza profunda do cabelo, eliminando impurezas e resíduos.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>