<?php

    $title       = "Vitamina D3 5.000Ui 60 Doses";
    $description = "Conhecida como a vitamina do sol por ser formada no corpo pela ação dos raios solares na pele, a vitamina D3 é responsável por manter a saúde dos dentes..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Vitamina D3 5.000Ui 60 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/vitamina-d3-5000ui-60-doses.png" alt="vitamina-d3-5000ui-60-doses" title="vitamina-d3-5000ui-60-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>A VITAMINA DO SOL</h2>
                        <p class="text-justify">Conhecida como a “vitamina do sol" por ser formada no corpo pela ação dos raios solares na pele, a vitamina D3 é responsável por manter a saúde dos dentes e ossos, ajudar contra a fraqueza muscular, além de proteger o corpo de outras deficiências e doenças.</p>
                        <br>
                        <h2>SEM PROBLEMAS COM OS OSSOS</h2>
                        <p class="text-justify">O termo vitamina D engloba um grupo de moléculas secosteróides derivadas do 7- deidrocolesterol (7-DHC) interligadas através de uma cascata de reações fotolíticas e enzimáticas que acontecem em células de diferentes tecidos. À vitamina D3 é primariamente atribuído o papel de importante regulador da fisiologia osteomineral, em especial do metabolismo do cálcio.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado.</p>
                <p class="advertencias text-justify"> Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>