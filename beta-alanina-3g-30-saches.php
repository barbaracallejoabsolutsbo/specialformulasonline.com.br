<?php

    $title       = "Beta Alanina 3G 30 Sachês";
    $description = "Ingestão desse suplemento pode promover maior resistência à fadiga no treinamento, seja ele de endurance ou de musculação. A beta-alanina é precursora..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Beta Alanina 3G 30 Sachês</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/beta-alanina-3g-30-saches.png" alt="beta-alanina-3g-30-saches" title="beta-alanina-3g-30-saches">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>CONCLUSÃO</h2>
                        <p class="text-justify">Ingestão desse suplemento pode promover maior resistência à fadiga no treinamento, seja ele de endurance ou de musculação. A beta-alanina é precursora da carnosina; maiores níveis de carnosina em nossos músculos auxilia na manutenção do pH muscular, evitando sua queda em virtude do acúmulo de ácido lático.</p>
                        <br>
                        <h2>PROPRIEDADES E FUNÇÃO DA BETA ALANINA</h2>
                        <p class="text-justify">A Beta-Alanina é um aminoácido BETA (outro tipo de aminoácido, não participante da síntese de proteínas, exclusividade dos aminoácidos alfa) que juntamente com a histidina é responsável pela síntese de um importante tamponante para a fadiga localizada comum nos exercícios físicos de alta intensidade, a Carnosina. Em situações de exercícios intensos e de duração média a prolongada, como uma aula de spinning, depois do treino de musculação, ou uma corrida de 10k, uma maior quantidade de Carnosina dentro do músculo é capaz de evitar ou atrasar a formação de um tradicional “vilão” dos exercícios vigorosos responsável pelo surgimento da fadiga, o ácido lático. A suplementação com Beta-Alanina é capaz de promover um aumento real, demonstrado científicamente, na concentração intramuscular de Carnosina.</p>
                    </div>
                </div>
                <h2>QUAIS AS INDICAÇÕES DA BETA ALANINA?</h2>
                <ul>
                    <li>Aumento de massa muscular;</li>
                    <li>Aumento de desempenho em atividade física;</li>
                    <li>Potencializa os efeitos da carnosina;</li>
                    <li>Tratamento de menopausa;</li>
                    <li>Indivíduos com dieta baixa de proteínas.</li>
                </ul>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>