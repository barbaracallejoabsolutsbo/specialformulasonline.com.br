<?php

    $title       = "5 Htp 100Mg 60 Doses (Grifonia Simplicifolia)";
    $description = "Beta-caroteno melhor conhecido como um precursor da vitamina A, é um importante antioxidante que ajuda a prevenir danos de radicais livres prejudiciais no corpo"; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">5 Htp 100Mg 60 Doses (Grifonia Simplicifolia)</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/5-htp-100mg-60-doses-grifonia-simplicifolia.png" alt="5-htp-100mg-60-doses-(grifonia-simplicifolia)" title="5-htp-100mg-60-doses-(grifonia-simplicifolia)">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>AÇÃO ANSIOLÍTICA, ANTIDEPRESSIVA E REGULADORA DO HUMOR</h2>
                        <p class="text-justify">O 5-HTP é um precursor direto da serotonina e derivado da planta Griffonia simplicifolia. Esta planta tem sido usada tradicionalmente na medicina africana para inibir diarreia, vômito e constipação, e também como um afrodisíaco. A suplementação diária com L-5-Hidroxitriptofano possui ação no equilíbrio das desordens do SNC, tais como o sono, a memória, o aprendizado e a regulação da temperatura e do humor.</p>
                        <br>
                        <h2>AUXILIA NO COMBATE AO ESTRESSE</h2>
                        <p class="text-justify">Também auxilia no comportamento sexual, nas funções cardiovasculares, contrações musculares, na regulação endócrina e na depressão. No estresse crônico, há um aumento na concentração de cortisol plasmático. Isto pode inibir a conversão do triptofano – ingerido através dos alimentos – em L-5-Hidroxitriptofano, mas não inibe a conversão do 5-HTP em 5-HT (serotonina). Por essa razão, o 5 HTP se tornou um poderoso aliado no tratamento ao estresse.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>