<?php

    $title       = "Padinactive Nutri 100mg 60 Cápsulas";
    $description = "PADINACTIVE® NUTRI traz um novo conceito de tratamento anti-rugas: estimula a proliferação e a atividade dos fibroblastos na produção de matriz extra-celular..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Padinactive Nutri 100mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/padinactive-nutri-100mg-60-capsulas.png" alt="padinactive-nutri-100mg-60-capsulas" title="padinactive-nutri-100mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>LIFTING ORAL E PÓS PEELING</h2>
                        <p class="text-justify">PADINACTIVE® NUTRI traz um novo conceito de tratamento anti-rugas: estimula a proliferação e a atividade dos fibroblastos na produção de matriz extra-celular, especialmente Glicosaminoglicanas (GAGs). Sendo rico em minerais, comporta-se como um suplemento de minerais específico para a pele, atendendo o conceito de “mineralização cutânea”.</p>
                        <br>
                        <h2>COMPOSIÇÃO</h2>
                        <p class="text-justify">Maltanedienol: princípio ativo patenteado da alga Padina pavonica que proporciona a fixação de cálcio e aumenta a expressão de desmossomas na pele. Cálcio: forma orgânica. Aminoácidos essenciais: Isoleucina, Leucina, Triptofano e Valina. Vitaminas: Vitamina A, Vitamina B12, Vitamina C e Vitamina E</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="https://web.whatsapp.com/send?phone=5511944930219&text=Ol%C3%A1%21%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20da%20Exaustyair%20-%20Sistemas%20de%20Exaust%C3%A3o%20e%20Ventila%C3%A7%C3%A3o%20Industrial"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>