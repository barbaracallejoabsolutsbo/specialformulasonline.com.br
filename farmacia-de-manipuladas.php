<?php
    $title       = "Farmacia de manipuladas";
    $description = "A Special Fórmulas é uma farmácia de manipulados que existe há mais de 15 anos no mercado, produzimos e comercializamos fórmulas manipuladas magistrais que promovem saúde e bem estar geral.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>O processo de manipulação de fármacos e fitoterápicos para diversos fins na Special Fórmulas é feito de forma totalmente profissional e moderna. Com equipamentos de alto padrão e última geração, realizamos procedimentos de manipulação, com micronização para grande ganho de qualidade do medicamento manipulado, diminuindo partículas em até 2 micras. <strong>Farmácia de manipulados</strong> é com a Special Fórmulas.</p>
<p>Nossa <strong>farmácia de manipulados</strong> é especializada e conta com muita variedade de opções para manipulação, sob prescrição médica e com substanciais exclusivas no mercado farmacológico.  Garantindo comprimidos feitos com toda uniformidade granulométrica do conteúdo interno, garantem maior distribuição dos princípios ativos do manipulado com maior permeabilidade, velocidade de dissolução no organismo com ganhos significativos na eficácia dos medicamentos manipulados aqui.</p>
<p>Profissionais altamente capacitados e responsáveis para proporcionar sempre serviços seguros e de alta qualidade para todo ramo de <strong>farmácia de manipulados</strong>. Aqui você encontra todo suporte para manipulação de sólidos, semissólidos e líquidos, hormônios, homeopatia, florais, entre outras opções.</p>
<p>Nosso laboratório trabalha na confecção de manipulados em sachês, cápsulas em geral, sólidos, semissólidos, fórmulas magistrais, xaropes, cremes, shampoos, loções, sabonetes líquidos e muito mais. Nossa <strong>farmácia de manipulados</strong> trabalha com fórmulas manipuladas de forma segura seguindo normas de Boas Práticas de Manipulação em Farmácias (BPMF).</p>
<h2><strong>Conheça nossa Farmácia de Manipulados Special Fórmulas</strong></h2>
<p>A Special Fórmulas é uma<strong> farmácia de manipulados </strong>que existe há mais de 15 anos no mercado, produzimos e comercializamos fórmulas manipuladas magistrais que promovem saúde e bem estar geral. Entre substâncias fitoterápica, homeopáticas, alopáticas, ortomoleculares, dermatológicos, odontológicos, nutracêuticas e veterinárias, aqui você encontra inclusive florais, cosméticos e produtos naturais. Confira nosso catálogo e encontre opções exclusivas para você.</p>
<h2><strong>Farmácia de manipulados com segurança e alta tecnologia de manipulação</strong></h2>
<p>Nossa <strong>farmácia de manipulados </strong>sempre preza pela qualidade de ponta na produção de nossas fórmulas manipuladas. Nossos valores e compromissos constituem a cultura de desenvolvimento da empresa, transmitindo diretamente todos nossos princípios de gestão e relações com a clientela da forma mais profissional, pontual e respeitosa. Entregamos a domicílio para toda Grande São Paulo, a consulta para o resto do país. Fórmulas terapêuticas com atendimento personalizado sempre utilizando de boas práticas de manipulação em farmácia, produzindo produtos excelentes e eficientes.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>