<?php

    $title       = "Cactin Drenagem Linfática 500mg 60 Cápsulas";
    $description = "Cactin é o extrato seco obtido do fruto do cacto Opuntia ficus-indica. Possui uma composição única em vitaminas, minerais, lipídeos, aminoácidos como..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Cactin Drenagem Linfática 500mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/cactin-drenagem-linfatica-500mg-60-capsulas.png" alt="cactin-drenagem-linfatica-500mg-60-capsulas" title="cactin-drenagem-linfatica-500mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>“ELIMINA O INCHAÇO!”</h2>
                        <p class="text-justify">Cactin é o extrato seco obtido do fruto do cacto Opuntia ficus-indica. Possui uma composição única em vitaminas, minerais, lipídeos, aminoácidos como cisteína e taurina, antioxidantes como glutationa, flavonoides, entre outros compostos fenólicos, Apresenta propriedades diuréticas, eliminando o excesso de líquidos sem o desequilíbrio eletrolítico, aliado no gerenciamento do peso na redução da gordura corporal, auxilia na manutenção da saúde óssea, na função renal e na redução dos níveis plasmáticos de triacilgliceróis e LDL, coadjuvante no cuidado da esteatose hepática. Cactin possui efeito antioxidante e diurético sem a perda de minerais, contribuindo para a redução da gordura corporal e do edema.</p>
                        <br>
                        <h2>EFEITO REDUTOR DE MEDIDAS DE CACTIN:</h2>
                        <p class="text-justify">Um estudo duplo-cego placebo controlado foi realizado em voluntários do sexo feminino com idades entre 40 a 50 anos e IMC normal, avaliando os efeitos de Cactin a 2g/dia na redução da circunferência do quadril durante 28 dias. Resultados: Ao término do estudo, foi observado que a utilização de Cactin promoveu redução de até 2%, cerca de 1,9 cm da circunferência do quadril das voluntárias quando comparados com o grupo placebo.</p>
                    </div>
                </div>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>