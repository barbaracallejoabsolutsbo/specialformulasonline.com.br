<?php

    $title       = "Silício Orgânico 150Mg 60 Cápsulas";
    $description = "O Óleo de Semente de Abóbora possui propriedades antioxidantes por ser abundante em vitamina E."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Silício Orgânico 150Mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/silicio-organico-150mg.60.capsulas.png" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>Silício Orgânico </h2>
                        <p class="text-justify"><strong>Composição:</strong> Cada 1 Cápsula contém: </p>
                        <ul>
                            <li>Silício Orgânico- Nutricolin® 150mg</li>
                            <li>Excipiente* q.s.p. 1 cápsula</li>
                            <li>*carbonato de magnésio, talco, dióxido de silício, cálcio fosfato bibasico.</li>
                        </ul>
                        <h3>O VERDADEIRO NUTRICOLIN®</h3>
                        <p class="text-justify">O Silício Orgânico - Nutricolin® é um complexo formado entre o ácido ortosilícico que garante alta biodisponibilidade por ser estabilizado em colina e responsável por aumentar a síntese das “Proteínas da Beleza”, como colágeno, elastina e queratina. É um mineral extremamente importante, presente em vários tecidos do corpo humano como a pele, cabelos e unhas. Na pele os estudos apontam o papel essencial na formação estrutural da derme através do aumento da síntese de colágeno, elastina e estabilização de glicosaminoglicanos. No cabelo e na unha, o silício aumenta a síntese e a compactação de queratina.</p>
                        <p class="text-justify">O Silício está presente na forma de ácido ortosilícico em vários alimentos e bebidas, como aveia e cerveja, mas não é a forma mais adequada para a devida absorção no intestino, e com o envelhecimento ocorre diminuição da capacidade do organismo em absorver este mineral. Há vários estudos científicos demonstrando que a colina é o estabilizador ideal do ácido ortosilícico, garantindo sua absorção. Através destas comprovações científicas foi desenvolvido o Silício Orgânico, com uma tecnologia comprovada, proporcionando inúmeros benefícios à saúde.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">O uso do produto não é aconselhável para pessoas com problemas renais sem orientação médica. Imagens meramente ilustrativas. Pessoas com hipersensibilidade à substância não devem ingerir o produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Este produto não deve ser utilizado por gestantes e lactantes sem orientação médica. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. Todo medicamento deve ser mantido fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>