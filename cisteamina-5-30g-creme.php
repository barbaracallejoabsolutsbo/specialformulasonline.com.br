<?php

    $title       = "Cisteamina 5% 30g Creme";
    $description = "Cisteamina é o novo ativo indicado para desordens de hiperpigmentação cutânea, atuando como corretor de pigmentos. Naturalmente presente no corpo..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Cisteamina 5% 30g Creme</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/cisteamina-5-30g-creme.png" alt="cisteamina-5%-30g-creme" title="cisteamina 5% 30g creme">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>CISTEAMINA 5% 30G</h2>
                        <p class="text-justify">Cisteamina é o novo ativo indicado para desordens de hiperpigmentação cutânea, atuando como corretor de pigmentos. Naturalmente presente no corpo, a Cisteamina reduz fortemente a melanina na epiderme. A Cisteamina 5% 30g Special Fórmulas trata o melasma com eficácia, uniformiza os tons de pele com irregularidades e previne o aparecimento de novas manchas.</p>
                        <br>
                        <h2>EFICÁCIA CONTRA O MELASMA</h2>
                        <p class="text-justify">O melasma se caracteriza pelo surgimento de manchas escuras na pele, mais comumente na face, mas também pode ser de ocorrência extra facial, com acometimento dos braços, pescoço e colo. Não é uma doença contagiosa. Afeta mais frequentemente as mulheres, podendo ser vista também em homens. Não há uma causa definida, mas muitas vezes está relacionada ao uso de anticoncepcionais.</p>
                    </div>
                </div>
                <h2>CISTEAMINA: POTENTE E SEGURA</h2>
                <p class="text-justify">Pesquisas demonstram que a Cisteamina uso tópico é muito mais potente e segura que a hidroquinona sendo indicada para pacientes com melasma resistente ou com desordens de hiperpigmentação causadas pelo sol ou pós gestacional.</p>
                <br>
                <h2 class="advertencias text-center">Cisteamina 5% 30g</h2>
                <p class="advertencias text-justify">Eficaz contra o melasma e hiperpigmentação, a Cisteamina 5% 30g Special Fórmulas uniformiza e pode ser utilizada em todos tipos de pele, além de possuir ação antioxidante. A eficácia da Cisteamina tópica no tratamento do melasma foi recentemente demonstrada em ensaios clínicos publicados nas mais renomadas revistas científicas e presente nos principais congressos de dermatologia do mundo.</p>
                <br>
                <h2 class="text-center">PARA UMA PELE SEM IMPRESSÕES</h2>
                <div class="row img-vantagens">
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 vantagens-1">
                        <img class="text-center" src="imagens/vantagens/protecao-solar-uva.png" alt="pode-ser-utilizado-em-todos-os-tons-de-pele" title="Pode ser utilizado em todos os tons de pele">
                        <h3 class="text-center">Pode ser utilizado em todos os tons de pele</h3>
                        <p class="text-justify">A Cisteamina 5% 30g Oficialfarma tem a vantagem de ser um produto tópico e eficaz, que pode ser utilizado em todos os tipos de pele</p>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 vantagens-2">
                        <img src="imagens/vantagens/uniformiza-o-tom-de-peles-com-irregularidades.png" alt="uniformiza-o-tom-de-peles-com-irregularidades" title="uniformiza-o-tom-de-peles-com-irregularidades">
                        <h3 class="text-center">Uniformiza o tom de peles com irregularidades</h3>
                        <p class="text-justify">Eficaz contra o melasma e hiperpigmentação, a Cisteamina 5% 30g Oficialfarma uniformiza e pode ser utilizada em todos os tipos de pele.</p>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 vantagens-3">
                        <img src="imagens/vantagens/efeito-toque-seco.png" alt="tratamento-eficaz-contra-melasma-e-hiperpigmentacao"
                        title="Tratamento eficaz contra melasma e hiperpigmentação">
                        <h3 class="text-center">Tratamento eficaz contra melasma e hiperpigmentação</h3>
                        <p class="text-justify">A eficácia da Cisteamina tópica no tratamento do melasma foi recentemente demonstrada em ensaios clínicos publicados nas mais renomada revistas científicas e presente nos principais congressos de der</p>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 vantagens-4">
                        <img src="imagens/vantagens/previne-contra-o-aparecimento-de-novas-manchas.png" alt="previne-contra-o-aparecimento-de-novas-manchas" title="previne-contra-o-aparecimento-de-novas-manchas">
                        <h3 class="text-center">Previne contra o aparecimento de novas manchas</h3>
                        <p class="text-justify">Além de combater a hiperpigmentação de forma eficaz, a Cisteamina 5% 30g Oficialfarma também previne o aparecimento de novas manchas.</p>
                    </div>   
                </div>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>