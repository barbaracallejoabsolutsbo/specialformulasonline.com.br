<?php

    $title       = "Composto Auxiliar no Tratamento Contra Insônia";
    $description = "O composto  auxiliar para tratamento contra insônia é destinado à aquelas pessoas que precisam de um relaxamento extra na hora de dormir ou relaxar..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Composto Auxiliar no Tratamento Contra Insônia "DORMIR BEM" 30 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/composto-auxiliar-no-tratamento-contra-insonia-dormir-bem-30-capsulas.png" alt="composto-auxiliar-no-tratamento-contra-insonia-dormir-bem-30-capsulas" title="composto-auxiliar-no-tratamento-contra-insonia-dormir-bem-30-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">O composto  auxiliar para tratamento contra insônia é destinado à aquelas pessoas que precisam de um relaxamento extra na hora de dormir ou relaxar. Ela também ajuda no controle de ansiedade causado pelo estresse do dia a dia. Sua fôrmula é composta por três nutrientes essenciais para seus resultados, veremos a seguir quais são.</p>
                        <br>
                        <h2>MELISSA</h2>
                        <p class="text-justify">Melissa também conhecida como Erva Cidreira tem ação sedativa, eupéptica e espasmolítica. Auxiliar no tratamento dos casos de insônia, depressões e tensões nervosas. Tem ação digestiva e é coadjuvante no tratamento de afecções do estômago, gases e cólicas intestinais.</p>
                    </div>
                </div>
                <h2>PASSIFLORA</h2>
                <p class="text-justify">A passiflora é mais conhecida por seu fruto, o maracujá. Ela possui boas quantidades de flavonoides. Eles apresentam vários efeitos biológicos e farmacológicos, incluindo atividade antibacteriana, antiviral, anti-inflamatória, antialérgica e vasodilatadora. Além disso, estas substâncias inibem a peroxidação lípidica e reduzem o risco de doenças cardiovasculares, efeitos estes relacionados à sua atividade antioxidante, caracterizada pela capacidade de sequestrar radicais livres em organismos vivos.</p>
                <br>
                <h2>KAWA KAWA</h2>
                <p class="text-justify">A kawa kawa, de nome científico Piper methysticum, é uma planta originária das ilhas da Oceania (Polinésia, Indonésia e Melanésia), também conhecida por outras denominações como cava cava, awa-irai, kawa e outras. A kawa kawa possui várias propriedades medicinais bastante eficientes e, por ser de fácil cultivo, pode ser plantada no jardim de casa, assim como outras ervas medicinais. A propriedade medicinal mais comum da kawa kawa é a de calmante e/ou sedativo, devido à presença do Waka, um relaxante ósseo e muscular que acalma todo o sistema nervoso central. Dentre as propriedades medicinais desta planta, podemos destacar as seguintes: Ansiolítica; Espasmolítica; Calmante; Anticonvulsionante; Analgésica; Antisséptica; Sedativa; Tranquilizante.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>