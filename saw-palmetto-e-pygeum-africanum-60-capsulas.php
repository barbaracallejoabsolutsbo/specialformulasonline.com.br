<?php

    $title       = "Saw Palmetto e Pygeum Africanum 60 Cápsulas";
    $description = "Associação fitoterápica para prevenir doenças da próstata e amenizar os sintomas relacionados à hiperplasia benigna de próstata (HPB)..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Saw Palmetto e Pygeum Africanum 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/saw-palmetto-e-pygeum-africanum-60-capsulas.png" alt="saw-palmetto-e-pygeum-africanum-60-capsulas" title="saw-palmetto-e-pygeum-africanum-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>SAW PALMETTO + PYGEUM AFRICANUM</h2>
                        <p class="text-justify">Associação fitoterápica para prevenir doenças da próstata e amenizar os sintomas relacionados à hiperplasia benigna de próstata (HPB). Também combate a obstrução do fluxo urinário e infecções do trato urinário pois possui ação anti-inflamatória, estimulando e anti-edema. Possui ação no aumento da capacidade erétil.</p>
                        <br>
                        <h2>COMO AGE NO CORPO</h2>
                        <p class="text-justify">O Pygeum africanum é um potente fitoterápico que atua contra infecções do trato urinário. O extrato de Pygeum possui também uma função anti-inflamatória, um efeito estimulante da secreção e um efeito antiedematoso e um aumento da capacidade erétil, o qual se pode traduzir como um incremento na função sexual masculina sem que seja considerado como afrodisíaco.</p>
                    </div>
                </div>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>