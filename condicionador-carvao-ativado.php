<?php

    $title       = "Condicionador carvão Ativado";
    $description = "Beta-caroteno melhor conhecido como um precursor da vitamina A, é um importante antioxidante que ajuda a prevenir danos de radicais livres prejudiciais no corpo"; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Condicionador carvão Ativado</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/condicionador-carvao-ativado.png" alt="condicionador-carvao-ativado" title="condicionador-carvao-ativado">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>LIMPEZA PROFUNDA</h2>
                        <p class="text-justify">O Condicionador com Carvão Ativado Special Fórmulas foi desenvolvido com propriedades para manter o equilíbrio fisiológico dos cabelos, proporcionando condicionamento, maciez e suavidade. Combate também a oleosidade, proporcionando limpeza profunda contra resíduos.</p>
                        <br>
                        <h2>TRATA CABELOS DANIFICADOS</h2>
                        <p class="text-justify">Com ativos selecionados, o Aloe Vera é responsável pela ação emoliente, cicatrizante, tonificante, anti-inflamatória, suavizante, hidratante e restauradora de tecidos. Já o colágeno aumenta a resistência dos fios em cabelos danificados por processos químicos, descoloração pelo sol e poluição em excesso. O Charcoal Powder (carvão ativado) fornece hidratação e limpeza profunda aos fios, removendo os resíduos.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. Nunca compre medicamentos sem orientação de um profissional habilitado. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>