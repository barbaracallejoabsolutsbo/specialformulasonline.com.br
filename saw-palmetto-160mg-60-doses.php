<?php

    $title       = "Saw Palmetto 160Mg 60 Doses";
    $description = "Saw Palmetto, de nome científico Serenoa repens, é uma palmeira de pequeno porte de hastes espinhosas e serreadas, que alcança no máximo 4 metros de altura..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Saw Palmetto 160Mg 60 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/saw-palmetto-160mg-60-doses.png" alt="saw-palmetto-160mg-60-doses" title="saw-palmetto-160mg-60-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>SAW PALMETTO 160MG 60 CÁPSULAS</h2>
                        <p class="text-justify">Saw Palmetto, de nome científico Serenoa repens, é uma palmeira de pequeno porte de hastes espinhosas e serreadas, que alcança no máximo 4 metros de altura. Também conhecida como sabal, esta pequena palmeira é originária dos Estados Unidos, mais precisamente da região que vai do Texas até a Carolina do Sul. O Saw Palmetto possui várias propriedades medicinais advindas de seus pequenos frutos de cor preto-azulados, semelhantes a amoras. Conheça melhor as propriedades e os benefícios à saúde proporcionados por esta pequena palmeira.</p>
                        <p class="text-justify">Saw Palmetto está indicado em casos de alopecia androgênica, prostatite, hiperplasia benigna de próstata (HBP), em adenomas benignos da próstata, e nos processos inflamatórios associados a sintomas urológicos, tais como: retenção urinária, mictúria, e alteração do fluxo urinário, que são características clínicas da HBP. Alguns estudos tem demonstrado que o extrato de Serenoa repens pode inibir a atividade da enzima 5a-redutase e, que muito provavelmente, os compostos responsáveis por este efeito são os fitoesteróis, em particular o ß-sitosterol e stigmasterol presentes nesta planta. Este fitoterápico apresenta compostos que agem inibindo a atividade da 5a-redutase impedindo a formação da di-hidrotestosterona (DHT).</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. </p>
                <p class="advertencias text-justify">Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>