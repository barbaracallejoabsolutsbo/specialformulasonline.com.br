<?php

    $title       = "Exsynutriment Com In Cell 30 Cápsulas";
    $description = "In Cell é um ingrediente funcional preparado a partir da gema do ovo esterilizada, com alta concentração de proteínas essenciais importantes..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Exsynutriment Com In Cell 30 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/exsynutriment-com-in-cell-30-capsulas.png" alt="exsynutriment-com-in-cell-30-capsulas" title="exsynutriment-com-in-cell-30-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>PODEROSA COMBINAÇÃO</h2>
                        <p class="text-justify">In Cell é um ingrediente funcional preparado a partir da gema do ovo esterilizada, com alta concentração de proteínas essenciais importantes para a síntese de colágeno e elastina e, alta concentração de ômega 3, 7 e 9, substâncias fundamentais para a nutrição, função e metabolismo celular. In Cell pode ser considerado como coadjuvante à terapia com Exsynutriment, pois aumenta os níveis de colágeno na pele, por fornecer os aminoácidos essenciais. Fortalece cabelos e unhas, por fornecer aminoácidos sulfurados e lipídeos funcionais.</p>
                        <br>
                        <h2>FORTALECE UNHAS E CABELO</h2>
                        <p class="text-justify">O Exsynutriment® promete diminuir a flacidez e promover a firmeza da pele, retardar o envelhecimento, fortalecer os cabelos e unhas, e estimular a produção de colágeno e elastina no corpo. O Exsynutriment® restaura as fibras de colágeno e elastina que vão se degradando com o passar dos anos. É uma forma estabilizada e concentrada de Silício Orgânico que age no tecido conjuntivo, reestruturando as fibras de colágeno e elastina, promovendo uma firmeza da pele de dentro para fora, além de uma intensa hidratação cutânea.</p>
                    </div>
                </div>
                <p class="text-justify">O Exsynutriment® é considerado um nutricosmético anti-aging que possui propriedades antioxidantes, protegendo e restaurando a pele. De um modo geral, fazer a reposição da perda de silício orgânico através do Exsynutriment®, melhora a sua saúde de “dentro para fora”, pois promove a melhor vascularização do sistema nervoso fazendo com que seu cérebro envelheça com mais qualidade, potencializa a fixação do cálcio no tecido ósseo, mantendo a densidade óssea e com isso prevenindo doenças, como a osteoporose.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Imagens meramente ilustrativas. Pessoas com hipersensibilidade à substância não devem ingerir o produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Este produto não deve ser utilizado por gestantes e lactantes sem orientação médica. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. Todo medicamento deve ser mantido fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>