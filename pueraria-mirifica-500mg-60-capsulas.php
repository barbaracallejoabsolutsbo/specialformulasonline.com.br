<?php

    $title       = "Pueraria Mirífica 500mg 60 Cápsulas";
    $description = "A Pueraria Mirífica 500mg 60 cápsulas Special Fórmulas apresentam resultados estéticos relevantes, pois possui ação antienvelhecimento..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Pueraria Mirífica 500mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/pueraria-mirifica-500mg-60-capsulas.png" alt="pueraria-mirifica-500mg-60-capsulas" title="pueraria-mirifica-500mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE FAZ?</h2>
                        <p class="text-justify">A Pueraria Mirífica 500mg 60 cápsulas Special Fórmulas apresentam resultados estéticos relevantes, pois possui ação antienvelhecimento, melhora a aparência da pele, ajuda a aumentar o tamanho dos seios e combate a queda capilar. Sua atividade, porém, vai muito além da estética. O ativo é responsável por cuidar da saúde cardiovascular e minimizar as desordens provenientes da menopausa.</p>
                        <br>
                        <h2>AÇÃO</h2>
                        <p class="text-justify">A Pueraria Mirífica é composta por formas glicosídicas “Puerarin”, seu constituinte principal, e “Miroesterol”. Tais elementos auxiliam o funcionamento dos hormônios femininos promovendo uma atividade estrogênica maior que a isoflavona da soja. Ajuda a aumentar os seios por alongamento e ramificação dos dutos que se ligam ao mamilo. Também aumenta o tecido adiposo e ligamentos em volta dos seios que fornecem o suporte e a forma. A eficácia contra desordens relacionadas à menopausa se deve aos tubérculos da planta, que são ricos em fitoestrogênios, que tem ação estrogênica semelhante aos estrógenos conjugados.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pueraria mirífica não deve ser utilizada por mulheres que fazem uso de anticoncepcionais ou terapia de reposição hormonal, mulheres com cistos de mama, ovário ou útero ou com câncer. Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado.</p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="https://web.whatsapp.com/send?phone=5511944930219&text=Ol%C3%A1%21%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20da%20Exaustyair%20-%20Sistemas%20de%20Exaust%C3%A3o%20e%20Ventila%C3%A7%C3%A3o%20Industrial"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>