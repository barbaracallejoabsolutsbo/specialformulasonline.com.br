<?php

    $title       = "Minoxidil Turbinado 120Ml";
    $description = "O Minoxidil Turbinado é uma fórmula desenvolvida pela Special Fórmulas que combina a eficiência do Minoxidil e da Vitamina H que é fundamental para a saúde."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Minoxidil Turbinado 120Ml</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/minoxidil-turbinado-120ml.png" alt="minoxidil-turbinado-120ml" title="minoxidil-turbinado-120ml">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>Descrição</h2>
                        <p class="text-justify">O Minoxidil Turbinado é uma fórmula desenvolvida pela Special Fórmulas que combina a eficiência do Minoxidil e da Vitamina H que é fundamental para a saúde dos cabelos. Além disso, a fórmula do Minoxidil Turbinado possui ingredientes que aumentam a permeabilidade dos ativos no couro cabeludo, garantindo melhores resultados e alta hidratação aos fios. O Minoxidil Turbinado é uma loção capilar que ajuda a revitalizar a raiz do cabelo e normalizar o ciclo do folículo, prolongando a fase anágena (fase de crescimento ativo do fio). Ele também tem a função de estimular a vascularização do couro cabeludo permitindo oxigenação da área.</p>
                        <p class="text-justify">O tratamento deve ser contínuo. Uma vez interrompido o tratamento, a calvície volta ao estado anterior em aproximadamente dois meses. A ação é principalmente na região parietal do couro cabeludo ou coroa, estabilizando a calvície, postergando sua evolução e estimulando cabelos que estão enfraquecidos ou finos transformando em cabelos mais grossos, em alopecias em estágio inicial. A testosterona e suas derivações se prendem a seus receptores nos pelos e isso causa uma diminuição desses pelos. A variação da testosterona que mais causa isso é a hidrotestosterona, também conhecido por sua sigla DHT, é um derivado cinco vezes mais poderoso que a própria testosterona.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Embora raros, podem ser apresentados eritema local, descamação, prurido, hipotensão arterial, náuseas, fadiga, erupção cutânea, cefaleia. Retenção hidrossalina com edemas, hipertricose. Pode causar efeito Shedding (os fios que cairiam aos poucos, caem todos de uma única vez). Nunca compre medicamento sem orientação de um profissional habilitado. Imagens meramente ilustrativas. Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o medicamento com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o medicamento se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Todo medicamento deve ser mantido fora do alcance das crianças.</p>
                <p class="advertencias text-justify">Este medicamento não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do medicamento durante o período de amamentação também não é recomendado. Este medicamento não deve ser utilizado por menores de 18 anos sem orientação médica. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO”. "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="https://web.whatsapp.com/send?phone=5511944930219&text=Ol%C3%A1%21%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20da%20Exaustyair%20-%20Sistemas%20de%20Exaust%C3%A3o%20e%20Ventila%C3%A7%C3%A3o%20Industrial"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>