<?php

    $title       = "Óleo de Rosa Mosqueta 30ml";
    $description = "A Rosa Mosqueta é uma planta silvestre do grupo das rosáceas da região do sul dos Andes Chilenos. De sua semente é extraído um óleo..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Óleo de Rosa Mosqueta 30ml</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/oleo-de-rosa-mosqueta-30ml.png" alt="oleo-de-rosa-mosqueta-30ml" title="oleo-de-rosa-mosqueta-30ml">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>ÓLEO DE ROSA MOSQUETA 30ML</h2>
                        <p class="text-justify">A Rosa Mosqueta é uma planta silvestre do grupo das rosáceas da região do sul dos Andes Chilenos. De sua semente é extraído um óleo rico em nutrientes essenciais aos processos fisiológicos e bioquímicos responsáveis pela formação e manutenção da pele.</p>
                        <p class="text-justify">O Óleo de Rosa Mosqueta é utilizado na conservação da textura da pele, deixando-a macia e mantendo a hidratação natural. Seus componentes auxiliam na manutenção de uma pele bonita e saudável. Por hidratar, lubrificar e nutrir a pele, é um cosmético indicado para peles muito secas, ásperas e sem vida. Tem ação regeneradora dos tecidos, cicatrizante e emoliente.</p>
                    </div>
                </div>
                <h2>PARA QUE SERVE O ÓLEO DE ROSA MOSQUETA?</h2>
                <p class="text-justify">Há muito tempo, os povos andinos do Chile usam o óleo de rosa mosqueta para tratar a pele e os cabelos. A indústria o utiliza na produção de produtos cosméticos, como cremes e sprays.</p>
                <p class="text-justify">O óleo é composto majoritariamente pelos ácidos graxos do tipo ômega 6 (ácido linoleico) e ômega 3 (ácido linolênico), eles representam 80% de seus componentes. Esses ácidos graxos essenciais, ajudam a reparar a pele e evitam a sua desidratação, pois formam uma barreira impermeável à água.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado.</p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>