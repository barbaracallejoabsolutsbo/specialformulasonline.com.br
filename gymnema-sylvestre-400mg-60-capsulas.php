<?php

    $title       = "Gymnema Sylvestre 400Mg 60 Cápsulas";
    $description = "Gymnema Sylvestre é uma erva indiana reconhecida por séculos. Usa um extrato puro de Gymnema sylvester (5:1), que é a forma mais desejável..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Gymnema Sylvestre 400Mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/gymnema-sylvestre-400mg-60-capsulas.png" alt="gymnema-sylvestre-400mg-60-capsulas" title="gymnema-sylvestre-400mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">Gymnema Sylvestre é uma erva indiana reconhecida por séculos. Usa um extrato puro de Gymnema sylvester (5:1), que é a forma mais desejável. Pullulan é um polissacarídeo que fornece melhoria sinergística das qualidades do Gymnema sylvester. Um membro da família da serralha, a gymnema sylvestre é nativa das regiões tropicais da Índia. Apelidado de “destruidor de açúcar” porque as folhas bloqueiam com eficácia os sabores doces na boca quando mastigados, a erva é frequentemente promovida como um supressor de apetite e agente de perda de peso.</p>
                        <br>
                        <h2>AÇÃO</h2>
                        <p class="text-justify">Em pessoas com diabete tipo 1, gimnema parece aumentar a ação da insulina e controla os níveis de açúcar no sangue. No caso do tipo 2 de diabete mais prevalecente, também conhecido como diabete não dependente de insulina, constatações de pesquisas indicam que o uso de gimnema pode melhorar o controle do açúcar no sangue e resultar na necessidade de doses menores de medicamentos orais para de diabete para controlar a doença.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>