<?php

    $title       = "Vitis Vinifera (Semente De Uva) 150mg 60 Cápsulas";
    $description = "Antioxidante, anti-inflamatória, antimicrobiana e anticarcinogênica, hipolipemiante, protetor cardiovascular, protetor renal, preventivo da arteriosclerose..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Vitis Vinifera (Semente De Uva) 150mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/vitis-vinifera-semente-de-uva-150mg-60-capsulas.png" alt="vitis-vinifera-(semente-de-uva)-150mg-60-capsulas" title="vitis-vinifera-(semente-de-uva)-150mg-60-capsulas-50mg">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>BENEFÍCIOS</h2>
                        <p class="text-justify">Antioxidante, anti-inflamatória, antimicrobiana e anticarcinogênica, hipolipemiante, protetor cardiovascular, protetor renal, preventivo da arteriosclerose e no rejuvenescimento da pele. Semente de uva é um potente antioxidante e seus componentes ativos constituem flavonóides e proantocianidinas que, além de atuarem como sequestradores de radicais livres, promovem a vasodilatação, inibem fosfolipases, ciclooxigenases e lipoxogenases, bem como reduzem a peroxidação lipídica.</p>
                        <br>                
                    </div>
                    <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="https://web.whatsapp.com/send?phone=5511944930219&text=Ol%C3%A1%21%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20da%20Exaustyair%20-%20Sistemas%20de%20Exaust%C3%A3o%20e%20Ventila%C3%A7%C3%A3o%20Industrial"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>