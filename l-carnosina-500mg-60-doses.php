<?php

    $title       = "L-Carnosina 500Mg 60 Doses";
    $description = "A l-carnosina é um pequeno peptídeo que contém dois aminoácidos, beta-alanina e histidina. É encontrada em concentrações relativamente altas..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">L-Carnosina 500Mg 60 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/l-carnosina-500mg-60-doses.png" alt="l-carnosina-500mg-60-doses" title="l-carnosina-500mg-60-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>L-CARNOSINA 500MG</h2>
                        <p class="text-justify">A l-carnosina é um pequeno peptídeo que contém dois aminoácidos, beta-alanina e histidina. É encontrada em concentrações relativamente altas em vários tecidos do corpo, notavelmente nos músculos do esqueleto, músculos do coração, e no cérebro. O mais notável é que estes dois aminoácidos trabalham muito mais eficazmente do que trabalhariam se não fossem combinados para formar a l-carnosina, primeiramente. Enquanto ainda estão combinados, possuem a capacidade exclusiva de rejuvenescer as células – fazer com que as células antigas rejuvenesçam e prolongar o ciclo de vida das mesmas. A l-carnosina é a única combinação comercialmente disponível que possui esta capacidade muito rara e exclusiva.</p>
                        <br>
                        <h2>BENEFÍCIOS</h2>
                        <ul>
                            <li>Aumentar a imunidade e reduzir a inflamação;</li>
                            <li>Produz efeitos anticancerígenos no corpo;</li>
                            <li>Protege contra danos de radiação e síndrome reversa pós-radiação;</li>
                        </ul>
                    </div>
                </div>
                <ul>
                    <li>Protege contra a formação de úlceras gástricas, e ajuda a curar as úlceras existentes;</li>
                    <li>Inibe (ou inverte) a glicosilação e desacelera os efeitos prejudiciais – e pró-envelhecimento – do consumo de carboidratos;</li>
                    <li>Aumenta a potência e resistência muscular.</li>
                </ul>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="https://web.whatsapp.com/send?phone=5511944930219&text=Ol%C3%A1%21%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20da%20Exaustyair%20-%20Sistemas%20de%20Exaust%C3%A3o%20e%20Ventila%C3%A7%C3%A3o%20Industrial"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>