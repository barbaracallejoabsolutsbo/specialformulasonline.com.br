<?php

    $title       = "Serum Minoxidil C/ Fator De Crescimento P/ Sobrancelhas 5Ml";
    $description = "Desenvolvido pela equipe Special Fórmulas, para ajudar a melhorar a aparência da sobrancelha, o Sérum com Minoxidil 2% e Fator de Crescimento..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Serum Minoxidil C/ Fator De Crescimento P/ Sobrancelhas 5Ml</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/serum-minoxidil-com-fator-de-crescimento-para-sobrancelhas-5ml.png" alt="serum-minoxidil-com-fator-de-crescimento-para-sobrancelhas-5ml" title="serum-minoxidil-com-fator-de-crescimento-para-sobrancelhas-5ml">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">Desenvolvido pela equipe Special Fórmulas, para ajudar a melhorar a aparência da sobrancelha, o Sérum com Minoxidil 2% e Fator de Crescimento age normalizando e estimulando o Crescimento dos pelos na região da sobrancelha. Com sua fórmula especial que não agride a pele e estimula o Crescimento das sobrancelhas.</p>
                        <p class="text-justify">Uma sobrancelha bem feita e volumosa é considerada muito importante para a aparência da face, por isso a sobrancelha é cada vez mais almejada, especialmente pelas mulheres. Dessa forma, o MINOXIDIL em loção auxilia quem busca uma sobrancelha mais volumosa e espessa, longe das falhas. Além de estimular o CRESCIMENTO da sobrancelha, a loção apresenta praticidade e conforto na hora da aplicação. O tratamento deve ser contínuo. Uma vez interrompida a medicação, a calvície volta ao estado anterior ao tratamento em aproximadamente dois meses.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>