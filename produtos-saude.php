<?php

    $title       = "Saúde";
    $description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elementum ultrices commodo. Donec quis dictum tortor."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "home",
        "produtos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="container">
            <section class="nossos-produtos catego-prod container">
                <h1 class="text-center">Saúde</h1>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/ct-2-Colageno-tipo-ll-40mg-30-capsulas.png" alt="ct-2-Colageno-tipo-ll-40mg-30-capsulas" title="ct-2-Colageno-tipo-ll-40mg-30-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>CT-2 Colágeno Tipo II 40mg 30 Cápsulas</h3>
                            <p>Duas vezes mais ativo do que a condroitina associada...</p>
                            </div>
                            <a class="btn-entrectt" href="ct-2-Colageno-tipo-ll-40mg-30-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/uc-ll-40mg-30-capsulas-colageno-tipo-2.png" alt="uc-ll-40mg-30-capsulas-(colageno-tipo-2)" title="uc-ll-40mg-30-capsulas-(colageno-tipo-2)" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Uc II® 40Mg 30 Cápsulas (Colágeno Tipo 2)</h3>
                            <p>O UC-II® é um nutracêutico inovador que tem a função...</p>
                            </div>
                            <a class="btn-entrectt" href="uc-ll-40mg-30-capsulas-colageno-tipo-2.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/uc-ll-colageno-tipo-2-40mg-60-capsulas.png" alt="uc-ll-(colageno-tipo-2)-40mg-60-capsulas" title="uc-ll-(colageno-tipo-2)-40mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Uc II (Colágeno Tipo 2) 40Mg 60 Cápsulas</h3>
                            <p>O uc-ii é um nutracêutico inovador que tem a função...</p>
                            </div>
                            <a class="btn-entrectt" href="uc-ll-colageno-tipo-2-40mg-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/diacereina-50mg-60-capsulas.png" alt="diacereina-50mg-60-capsulas" title="diacereina-50mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Diacereína 50mg 60 Cápsulas</h3>
                            <p>Eficaz no combate de problemas articulares, a Diacereína...</p>
                            </div>
                            <a class="btn-entrectt" href="diacereina-50mg-60-capsulas.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/vitamina-d3-10000ui-60-doses.png" alt="vitamina-d3-10000ui-60-doses" title="vitamina-d3-10000ui-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Vitamina D3 10.000Ui 60 Doses</h3>
                            <p>Conhecida como a “vitamina do sol" por ser formada no corpo...</p>
                            </div>
                            <a class="btn-entrectt" href="vitamina-d3-10000ui-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/vitamina-c-500mg-60-doses.png" alt="vitamina-c-500mg-60-doses" title="vitamina-c-500mg-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Vitamina C 500Mg 60 Doses</h3>
                            <p>A vitamina C é uma vitamina que deve estar presente em nossas vidas...</p>
                            </div>
                            <a class="btn-entrectt" href="vitamina-c-500mg-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/vitamina-c-500mg-zinco-15mg-30-doses.png" alt="vitamina-c-500mg-zinco-15mg-30-doses" title="vitamina-c-500mg-zinco-15mg-30-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Vitamina C 500mg + Zinco 15mg 30 Doses</h3>
                            <p>Vitamina C + Zinco, é uma excelente combinação...</p>
                            </div>
                            <a class="btn-entrectt" href="vitamina-c-500mg-zinco-15mg-30-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/vitamina-d3-50000ui-12-doses.png" alt="vitamina-d3-50000ui-12-doses" title="vitamina-d3-50000ui-12-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Vitamina D3 50.000Ui 12 Doses</h3>
                            <p>Conhecida como a “vitamina do sol" por ser formada...</p>
                            </div>
                            <a class="btn-entrectt" href="vitamina-d3-50000ui-12-doses.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/oleo-de-rosa-mosqueta-30ml.png" alt="oleo-de-rosa-mosqueta-30ml" title="oleo-de-rosa-mosqueta-30ml" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Óleo de Rosa Mosqueta 30ml</h3>
                            <p>A Rosa Mosqueta é uma planta silvestre do grupo das rosáceas...</p>
                            </div>
                            <a class="btn-entrectt" href="oleo-de-rosa-mosqueta-30ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/zinco-15mg-120-doses.png" alt="zinco-15mg-120-doses" title="zinco-15mg-120-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Zinco 15Mg 120 Doses</h3>
                            <p>Zinco é um oligoelemento essencial para nosso organismo...</p>
                            </div>
                            <a class="btn-entrectt" href="zinco-15mg-120-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/centella-asiatica-500mg-60-capsulas.png" alt="centella-asiatica-500mg-60-capsulas" title="centella-asiatica-500mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Centella Asiática 500Mg 60 Cápsulas</h3>
                            <p>Centella Asiatica 500mg da linha conhecida como gotu...</p>
                            </div>
                            <a class="btn-entrectt" href="centella-asiatica-500mg-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/polypodium-leucotomos-250mg-30-capsulas.png" alt="polypodium-leucotomos-250mg-30-capsulas" title="polypodium-leucotomos-250mg-30-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Polypodium Leucotomos 250Mg 30 Cápsulas</h3>
                            <p>Proteção da estrutura cutânea: PL apresenta efeitos anti-aging...</p>
                            </div>
                            <a class="btn-entrectt" href="polypodium-leucotomos-250mg-30-capsulas.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/vitamina-b12-metilcobalamina-500mcg-60-doses.png" alt="vitamina-b12-metilcobalamina-500mcg-60-doses" title="vitamina-b12-metilcobalamina-500mcg-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Vitamina B12 Metilcobalamina 500Mcg 60 Doses</h3>
                            <p>A Vitamina B12 está envolvida em vários processos...</p>
                            </div>
                            <a class="btn-entrectt" href="vitamina-b12-metilcobalamina-500mcg-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/picolinato-de-cromo-350mcg-90-capsulas.png" alt="picolinato-de-cromo-350mcg-90-capsulas" title="picolinato-de-cromo-350mcg-90-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Picolinato De Cromo 350Mcg 90 Cápsulas</h3>
                            <p>O picolinato de cromo é especialmente indicado no caso...</p>
                            </div>
                            <a class="btn-entrectt" href="picolinato-de-cromo-350mcg-90-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/ginkg-biloba-120mg-60-doses.png" alt="ginkg-biloba-120mg-60-doses" title="ginkg-biloba-120mg-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Ginkgo Biloba 120Mg 60 Doses</h3>
                            <p>Ginkgo biloba é um derivado fitoterápico obtido da árvore...</p>
                            </div>
                            <a class="btn-entrectt" href="ginkg-biloba-120mg-60-doses.php">Saiba +</a>
                        </div>
                    </div>
            </section>
        </div>    
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>