<?php

    $title       = "Loção Para O Corpo (Kelly Key) 200Ml";
    $description = "Desenvolvida com ativos e nutrientes que garantem uma hidratação intensa, a Loção Para o Corpo Kelly Key Oficialfarma também estimula a renovação..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Loção Para O Corpo (Kelly Key) 200Ml</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/locao-para-o-corpo-kelly-key-200ml.png" alt="locao-para-o-corpo-(kelly-key)-200ml" title="locao-para-o-corpo-(kelly-key)-200ml">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>SUA PELE RENOVADA</h2>
                        <p class="text-justify">Desenvolvida com ativos e nutrientes que garantem uma hidratação intensa, a Loção Para o Corpo Kelly Key Oficialfarma também estimula a renovação celular, sendo assim, capaz de melhorar a aparência da pele como um todo e agindo até nas camas mais profundas da epiderme. Com uma poderosa ação antioxidante, ou seja, capaz de combater os radicais livres, a loção também tem ação antienvelhecimento – devolvendo, assim, a elasticidade da pele.</p>
                        <br>
                        <h2>ATIVOS</h2>
                        <p class="text-justify">Aquaporine: são proteínas transmembranares que possibilitam um melhor transporte de água através das diferentes camadas derme e epiderme. Vitamina E: conta com potente ação antioxidante, ou seja, combate os radicais livres promovendo um efeito antienvelhecimento, que hidrata a de dentro para fora. Vitamina A: é essencial para o desenvolvimento da pele. Estimula a renovação celular, promove a síntese de e é muito eficiente na prevenção e no tratamento de fotoenvelhecimento porque aumenta a elasticidade e tonicidade dos tecidos cutâneos prevenindo a formação de linhas de expressão e protegendo a pele contra agressões exteriores.</p>
                    </div>
                </div>
                <p class="text-justify">O óleo de amêndoas: amacia e tonifica a pele, além de possuir propriedades rejuvenescedoras, regeneradoras, hidratantes, amaciantes e nutritivas. Ômega Oil: atua como um emoliente lipofílico, que lubrifica e oferece uma suave sensação emoliente. PCA-Na: proporciona um toque macio e sedoso garantindo uma hidratação intensa.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. Nunca compre medicamentos sem orientação de um profissional habilitado. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="https://web.whatsapp.com/send?phone=5511944930219&text=Ol%C3%A1%21%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20da%20Exaustyair%20-%20Sistemas%20de%20Exaust%C3%A3o%20e%20Ventila%C3%A7%C3%A3o%20Industrial"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>