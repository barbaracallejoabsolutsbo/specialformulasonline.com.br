<?php

    $title       = "Betacaroteno 50Mg 30 Cápsulas";
    $description = "O Pantoficial é um suplemento alimentar que serve para tratar do cabelo e das unhas em caso de queda, cabelo frágil, fino ou quebradiço, prevenção..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Betacaroteno 50Mg 30 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/pantoficial-60-doses.png" alt="pantoficial-60-doses" title="pantoficial-60-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É PANTOFICIAL?</h2>
                        <p class="text-justify">O Pantoficial é um suplemento alimentar que serve para tratar do cabelo e das unhas em caso de queda, cabelo frágil, fino ou quebradiço, prevenção do aparecimento de cabelos brancos e também em caso de unhas fracas, quebradiças ou rachadas. Este suplemento tem na sua composição alguns nutrientes importantes como cálcio, cistina e vitaminas, que são benéficos para o cabelo e para as unhas, e também contém queratina, um dos principais componentes do cabelo. Indicado também em caso de cabelos danificados e sem vida devido ao uso de alisamentos, chapinha, secador de cabelo, tinturas ou queimados pelo sol.</p>
                        <p class="text-justify">Com eficácia comprovada no tratamento contra o enfraquecimento capilar, o Pantoficial apresenta uma combinação poderosa de substâncias que são formuladas da maneira ideal para devolver a saúde aos cabelos. Sua ação fortalece a estrutura dos fios, interrompendo a queda, estimulando o crescimento e preenchendo as áreas com falhas. Além disso, promove a redução do frizz e das pontas duplas, hidrata profundamente e garante um brilho visível.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Nunca compre medicamento sem orientação de um profissional habilitado. Imagens meramente ilustrativas. Pessoas com hipersensibilidade à substância não devem ingerir o produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o medicamento com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o medicamento se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Todo medicamento deve ser mantido fora do alcance das crianças. Este medicamento não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do medicamento durante o período de amamentação também não é recomendado. Este medicamento não deve ser utilizado por menores de 18 anos sem orientação médica. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO". "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="https://web.whatsapp.com/send?phone=5511944930219&text=Ol%C3%A1%21%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20da%20Exaustyair%20-%20Sistemas%20de%20Exaust%C3%A3o%20e%20Ventila%C3%A7%C3%A3o%20Industrial"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>