<?php

    $title       = "5 Htp 50Mg 30 Doses (Grifonia Simplicifolia)";
    $description = "o 5-htp é um precursor direto da serotonina e derivado da planta griffonia simplicifolia. esta planta tem sido usada tradicionalmente na medicina..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">5 Htp 50Mg 30 Doses (Grifonia Simplicifolia)</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/5-htp-50mg-30-doses-grifonia-simplicifolia.png" alt="5-htp-50mg-30-doses-(grifonia-simplicifolia)" title="5-htp-50mg-30-doses-(grifonia-simplicifolia)">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>5 HTP 50MG</h2>
                        <p class="text-justify">O 5-htp é um precursor direto da serotonina e derivado da planta griffonia simplicifolia. esta planta tem sido usada tradicionalmente na medicina africana para inibir diarreia, vômito e constipação, e também como um afrodisíaco. a suplementação diária com l-5-hidroxitriptofano possui ação no equilíbrio das desordens do snc, tais como o sono, a memória, o aprendizado e a regulação da temperatura e do humor.</p>
                        <br>
                        <h2>BENEFÍCIOS</h2>
                        <ul>
                            <li>Auxilia no combate ao estresse;</li>
                            <li>Possui ação ansiolítica, antidepressiva e reguladora do humor;</li>
                            <li>Melhora o sono;</li>
                            <li>Auxiliar na saúde sexual;</li>
                            <li>Auxilia na saúde cardiovascular;</li>
                            <li>Auxilia no emagrecimento.</li>
                        </ul>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar.</p>
                <p class="advertencias text-justify">Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>