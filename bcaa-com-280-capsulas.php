<?php

    $title       = "Bcaa Com 280 Cápsulas";
    $description = "Os BCAAs trabalham como transportadores de nitrogênio, nutriente que atua na síntese de outros aminoácidos necessários para a produção de novas..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Bcaa Com 280 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/bcaa-com-280-capsulas.png" alt="bcaa-com-280-capsulas" title="bcaa-com-280-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>BCAA 280 CÁPSULAS</h2>
                        <p class="text-justify">Os BCAAs trabalham como transportadores de nitrogênio, nutriente que atua na síntese de outros aminoácidos necessários para a produção de novas fibras musculares (anabolismo). Outra função dos BCAAs é estimular a produção de insulina, hormônio que promove a entrada de glicose e aminoácidos nas células. Esses efeitos do BCAA ajudam a diminuir os danos musculares resultantes da prática esportiva, aceleram a recuperação muscular, estimulam a síntese de proteínas e reduzem a fadiga.</p>
                        <br>
                        <h2>COMO AGE?</h2>
                        <p class="text-justify">Após um treino de resistência intenso, até mesmo atletas experientes começam a sofrer com o efeito do catabolismo muscular. Esse é o momento em que as reservas de glicogênio estão lá embaixo, o que obriga o fígado a utilizar o aminoácido L-alanina para sintetizar glicose. Cerca de metade dos aminoácidos liberados pelos músculos durante o treino é composta de L-alanina.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado.</p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>