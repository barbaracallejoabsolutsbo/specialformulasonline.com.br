<?php

    $title       = "Cabelo Homem & Mulher";
    $description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elementum ultrices commodo. Donec quis dictum tortor."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "home",
        "produtos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="container">
            <section class="nossos-produtos catego-prod container">
                <h1 class="text-center">Cabelo Homem & Mulher</h1>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/minoxidil-turbinado-120ml.png" alt="minoxidil-turbinado-120ml" title="minoxidil-turbinado-120ml" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Minoxidil Turbinado 120Ml</h3>
                            <p>O Minoxidil Turbinado é uma fórmula desenvolvida pela Special Fórmulas...</p>
                            </div>
                            <a class="btn-entrectt" href="minoxidil-turbinado-120ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/pill-food-turbinado-120-capsulas.png" alt="pill-food-turbinado-120-capsulas" title="pill-food-turbinado-120-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Pill Food Turbinado 120 Cápsulas</h3>
                            <p>Pill Food Turbinado é um complexo alimentar formado por vitaminas...</p>
                            </div>
                            <a class="btn-entrectt" href="pill-food-turbinado-120-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/biotina-5mg-60-capsulas.png" alt="biotina-5mg-60-capsulas" title="biotina-5mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Biotina 5Mg 60 Cápsulas</h3>
                            <p>A Biotina é uma vitamina hidrossolúvel do complexo B essencial...</p>
                            </div>
                            <a class="btn-entrectt" href="biotina-5mg-60-capsulas.php">Saiba +</a>
                        </div>
                    </div>
            </section>
        </div>    
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>