<?php

    $title       = "Shampoo Hidratante com babosa";
    $description = "O shampoo hidratante com Babosa 200ml Special Fórmulas, foi desenvolvido com ativos que proporcionam a regeneração dos fios, deixando os cabelos..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Shampoo Hidratante com babosa</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/shampoo-hidratante-com-babosa.png" alt="shampoo-hidratante-com-babosa" title="shampoo-hidratante-com-babosa">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>PROMOVE BRILHO E MACIEZ AOS FIOS</h2>
                        <p class="text-justify">O shampoo hidratante com Babosa 200ml Special Fórmulas, foi desenvolvido com ativos que proporcionam a regeneração dos fios, deixando os cabelos mais hidratados e brilhosos proporcionando um toque sedoso e macio.</p>
                        <br>
                        <h2>RECONSTRUÇÃO DOS FIOS</h2>
                        <p class="text-justify">O Extrato Glicólico de Aloe Vera tem ação emoliente, cicatrizante, tonificante, anti-inflamatória, calmante, suavizante, refrescante, hidratante, protetora e restauradora de tecidos. Rico em compostos antracênicos (aloína, aloe-emodina, aloinase), mucilagem, carboidratos, polissacarídeos, ácido crisofânico; enzimas (celulose, carboxipeptidase, catalase, amilase, oxidase), aminoácidos; vitaminas B, C e E; e sais minerais.</p>
                    </div>
                </div>
                <h2>AÇÃO HIDRATANTE</h2>
                <p class="text-justify">D-Pantenol atua especialmente como condicionador, auxiliando na retenção de umidade, espessamento e intumescimento dos fios, evitando a formação de pontas bipartidas e conferindo facilidade de penteado. Testes mostram que o D-Pantenol tem a propriedade de reparar danos causados por tinturas, permanentes e outros agentes, sem deixar um filme pesado sobre os cabelos, conferindo brilho e maciez. Vitamina E atua como hidratante e antioxidante, possui ação antirradicais livres. Proporciona aos fios maciez e hidratação.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. O uso do produto durante o período de amamentação sem orientação médica, também não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. Nunca compre o produto sem orientação de um profissional habilitado. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>