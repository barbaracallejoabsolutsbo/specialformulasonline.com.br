<?php

    $title       = "Gel Creme Antiacne Com Azeloglicina 60g";
    $description = "Azeloglicina® é um ativo derivado da condensação do ácido azeláico ao aminoácido glicina, que garante total eficácia e segurança ao tratamento"; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Gel Creme Antiacne Com Azeloglicina 60g</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/gel-creme-antiacne-azeloglicina.png" alt="gel-creme-antiacne-azeloglicina" title="gel-creme-antiacne-azeloglicina">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>GEL CREME ANTIACNE COM AZELOGLICINA</h2>
                        <p class="text-justify">A pele pode ser acometida por algumas desordens que geram sinais e marcas indesejadas, como as manchas e a acne. Azeloglicina® é um ativo derivado da condensação do ácido azeláico ao aminoácido glicina, que garante total eficácia e segurança ao tratamento que requeira propriedades:</p>
                        <br>
                        <ul>
                            <li class="text-justify">Clareadora: no processo de despigmentação da pele inibe a tirosinase e promove resposta clareadora em 21 dias de aplicação contínua;</li>
                            <li class="text-justify">Sebonormalizadora: reduz os níveis de ácidos graxos livres no sebo pela inibição competitiva da 5-alfa-redutase, bloqueando a conversão da testosterona a diidrotestosterona (DHT);</li>
                            <li class="text-justify">Antiacnéica: pelas propriedades bacteriostáticas contra uma extensa variedade de aeróbios e em altas concentrações, agindo como bactericida contra S. epidermidis e S. aureus;</li>
                            <li class="text-justify">Anti-queratinizante: devido ao seu efeito citostático antiproliferativo sobre os queratinócitos;</li>
                            <li class="text-justify">Hidratante: dá mais elasticidade à pele porque está ligada a duas moléculas de glicina.</li>
                        </ul>
                    </div>
                </div>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="https://web.whatsapp.com/send?phone=5511944930219&text=Ol%C3%A1%21%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20da%20Exaustyair%20-%20Sistemas%20de%20Exaust%C3%A3o%20e%20Ventila%C3%A7%C3%A3o%20Industrial"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>