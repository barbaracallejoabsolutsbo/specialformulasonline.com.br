<?php

    $title       = "L-Arginina Com L-Ornitina 750Mg 90 Cápsulas";
    $description = "A arginina tem atuação importantíssima para a síntese proteica no organismo. Portanto é um grande agente no ganho de massa magra e perda de gordura..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">L-Arginina Com L-Ornitina 750Mg 90 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/l-arginina-com-l-ornitin-750mg-90-capsulas.png" alt="l-arginina-com-l-ornitin-750mg-90-capsulas" title="l-arginina-com-l-ornitin-750mg-90-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>L-ARGININA</h2>
                        <p class="text-justify">A arginina tem atuação importantíssima para a síntese proteica no organismo. Portanto é um grande agente no ganho de massa magra e perda de gordura. Tudo isso devido à melhora da retenção de nitrogênio. Esse suplemento trabalha no desenvolvimento da massa magra em detrimento da gorda, através da chamada NO sintase. Com isso, o atleta pode contar com um aliado suplementar para ganhar progressão nos treinos de curta duração e alta intensidade. Assim funciona também aos que pararam por algum tempo de treinar e pretendem recuperar mais rapidamente a capacidade que possuíam antes da pausa.</p>
                        <br>
                        <h2>L-ORNITINA</h2>
                        <p class="text-justify">A ornitina, também chamada de l-ornitina, é um aminoácido não essencial, o que quer dizer que ele é produzido pelo corpo, e é derivado da quebra de arginina durante o ciclo do ácido cítrico. Ela ajuda a criar músculos e reduzir a gordura do corpo, principalmente quando é combinada com arginina e carnitina. Ela também é necessária para a formação de citrulina, prolina e ácido glutâmico, aminoácidos que ajudam a fornecer energia às células do corpo. A ornitina é frequentemente vendida como um suplemento para fisiculturistas, em combinação com a arginina, já que estudos laboratoriais indicam que ela aumenta os níveis de insulina e do hormônio do crescimento, além de prevenir a perda de músculos.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>