<?php

    $title       = "Trans Resveratrol 100Mg 60 Doses";
    $description = "O Resveratrol, é um composto fenólico, do tipo estilbeno, da classe dos polifenóis não flavonoides encontrados na uva e seus derivados, e tem despertado..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Trans Resveratrol 100Mg 60 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/trans-resveratrol-100mg-60-doses.png" alt="trans-resveratrol-100mg-60-doses" title="trans-resveratrol-100mg-60-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">O Resveratrol, é um composto fenólico, do tipo estilbeno, da classe dos polifenóis não flavonoides encontrados na uva e seus derivados, e tem despertado interesse devido a seus efeitos à saúde. O Resveratrol é sintetizado naturalmente na planta sob duas formas isômeras: trans-resveratrol (trans-3,5,4’-trihidroxiestilbeno) e cis-resveratrol (cis-3,5,4’-trihidroxiestilbeno). A forma isômera trans- Resveratrol é mais estável que a forma cis- Resveratrol.</p>
                        <p class="text-justify">As moléculas do Resveratrol são produzidas pelas plantas em situações de estresse leve, e quando esta substância é levada para o organismo humano atua como antioxidante natural protegendo o corpo do envelhecimento celular e aumentando a longevidade. O estresse oxidativo é causa de grandes alterações fisiológicas e patológicas no organismo humano, ocasiona fatores desencadeantes de doenças crônicas e degenerativas, envelhecimento e câncer.</p>
                    </div>
                </div>
                <p class="text-justify">Por ser um composto fenólico com atuações funcionais como agente antioxidante, anti-inflamatório e anti-fadiga, tem sido aplicado em várias doenças desempenhando efeito positivo na prevenção e tratamento de desordens metabólicas e degenerativas.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="https://web.whatsapp.com/send?phone=5511944930219&text=Ol%C3%A1%21%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20da%20Exaustyair%20-%20Sistemas%20de%20Exaust%C3%A3o%20e%20Ventila%C3%A7%C3%A3o%20Industrial"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>