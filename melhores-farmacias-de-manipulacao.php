<?php
    $title       = "Melhores farmacias de manipulacao";
    $description = "As melhores farmácias de manipulação do Brasil são conhecidas por utilizar técnicas e tecnologias modernas para manipulação de fórmulas prescritas para você mesmo quando bem específicas.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>As <strong>melhores farmácias de manipulação</strong> do Brasil são conhecidas por utilizar técnicas e tecnologias modernas para manipulação de fórmulas prescritas para você mesmo quando bem específicas. Com a Special Fórmulas você encontra todo suporte profissional de um laboratório com mão de obra especializada para manipulações seguras e eficientes.</p>
<p>A Special Fórmulas está no mercado nacional há mais de uma década oferecendo serviços que só se encontram nas melhores <strong>farmácias de manipulação do país</strong>. Utilizando de todas boas práticas de manipulação em farmácia, produzimos fórmulas e manipulados personalizados com receita para muitos fins. Encontre as melhores ofertas para soluções e fórmulas homeopáticas, alopáticas, fitoterápicas, entre outras.</p>
<p>Procure sempre orientação médica para prescrição e manipulação de quaisquer que sejam as substâncias. Encontre suporte completo inclusive para manipular fórmulas veterinárias, nutracêuticas, ortomoleculares, florais, produtos dermatológicos, entre outros. A Special Fórmulas atua sempre com primor a fim de entregar manipulados de qualidade e eficiência excelentes, com ótima absorção pelas tecnologias de manipulação que utilizamos, além de equipamentos modernos e especiais que só as <strong>melhores farmácias de manipulação</strong> oferecem.</p>
<p>Encontre suporte profissional para manipulação de cremes dermatológicos, pomadas, cosméticos e muito mais com nossos serviços especiais disponíveis aqui. <strong>Melhores farmácias de manipulação</strong> utilizam de técnicas e equipamentos inovadores para formular manipulados, dessa forma, nossos produtos contam com investimento contínuo em melhorias, com tecnologias para melhor manipulação existente no mercado.</p>
<h2><strong>A maior variedade de fórmulas das melhores farmácias de manipulação do mercado</strong></h2>
<p>Encontre o suporte mais completo de uma das<strong> melhores farmácias de manipulação </strong>do Brasil. A Special Fórmulas atua no ramo farmacêutico há mais de 15 anos e com todo profissionalismo e comprometimento realiza a manipulação de fórmulas magistrais sob prescrição médica com toda pontualidade e atenção possível. Com entrega a domicílio para toda Grande São Paulo, consulte nossas condições de envio e faça toda compra na segurança e conforto da sua residência.</p>
<h2><strong>Principais serviços e objetivos das melhores farmácias de manipulação, Special Fórmulas</strong></h2>
<p>Escolha a melhor das<strong> melhores farmácias de manipulação</strong> de São Paulo. A Special Fórmulas oferece serviços especiais de manipulação de fórmulas que necessitam de cuidados e atenção. Faça fórmulas e receitas personalizadas para diversos fins, nossos serviços de manipulação seguem todos os padrões de certificação da Anvisa com toda atenção para Boas Práticas de Manipulação em Farmácias. Equipamentos de ponta que diminuem partículas em até 2 micras aumentando absorção e dando ganho na performance da qualidade do medicamento manipulado.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>