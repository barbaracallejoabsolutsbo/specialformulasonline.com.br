<?php

    $title       = "Vitamina E 400Ui 60 Doses";
    $description = "A Vitamina E é uma vitamina lipossolúvel essencial na nutrição. É absorvida no trato gastrintestinal (20 a 80%), une-se a beta lipoproteínas no sangue..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Vitamina E 400Ui 60 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/vitamina-e-400ui-60-doses.png" alt="vitamina-e-400ui-60-doses" title="vitamina-e-400ui-60-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">A Vitamina E é uma vitamina lipossolúvel essencial na nutrição. É absorvida no trato gastrintestinal (20 a 80%), une-se a beta lipoproteínas no sangue e o seu armazenamento ocorre em todos os tecidos, especialmente no adiposo. Está envolvida nos processos metabólicos de eliminação dos radicais livres (subproduto das reações de oxidação da mitocôndria), inibindo a formação dos mesmos e seus efeitos nocivos sobre o organismo, além de proteger os eritrócitos de sofrerem a hemólise. Os tocoferóis apresentam quatro isômeros com diferentes atividades farmacológicas. Dos quatro tocoferóis (alfa, beta, gama, delta), o alfa é o mais eficaz.</p>
                        <br>
                        <h2>ALIADA DO CORAÇÃO</h2>
                        <p class="text-justify">A Vitamina E intervém em diferentes fases da síntese do ácido araquidônico que atua no metabolismo das prostaglandinas. A incorporação plaquetária de Vitamina E, obtida através de suplementação oral, está associada a uma inibição da agregação plaquetária. Em prematuros colocados em incubadora, a Vitamina E impede a formação da fibroplasia retro lenticular e displasia bronco pulmonar. Alguns estudos demonstram que, após a administração de Vitamina E, ocorre uma redistribuição dos lipídios sanguíneos, possivelmente devido à estimulação da hidrólise do colesterol esterificado. Em caso de dislipoproteinemia (índices baixos de HDL, com nítido aumento do índice de LDL), a Vitamina E provocaria uma redistribuição do colesterol no sentido de aumento da fração HDL colesterol antiaterogênico e diminuição do LDL, colesterol aterogênico.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">O uso concomitante a ácido acetilsalicílico (AAS) pode aumentar o risco de sangramento na gengiva. Doses muito elevadas de vitamina E reduzem a absorção das vitaminas A e K. O uso concomitante a antiácidos contendo hidróxido de alumínio diminui a absorção das vitaminas lipossolúveis. O uso simultâneo com anticoagulantes derivados da cumarina pode levar à hipoprotrombinemia. O uso simultâneo com suplementos de ferro altera a resposta hematológica em pacientes com anemia por deficiência de ferro. Possui traços de lactose. Nunca compre medicamento sem orientação de um profissional habilitado. Imagens meramente ilustrativas. Pessoas com hipersensibilidade à substância não devem ingerir o produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico.</p>
                <p class="advertencias text-justify">Não use o medicamento com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o medicamento se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Todo medicamento deve ser mantido fora do alcance das crianças. Este medicamento não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do medicamento durante o período de amamentação também não é recomendado. Este medicamento não deve ser utilizado por menores de 18 anos sem orientação médica. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>