<?php

    $title       = "Composto Super Libido Unissex 60 Doses";
    $description = "Os chineses consideram o Epimedium Icariin, também conhecido como Ying Yang Huo, o principal elemento de aumento da libido para homens e mulheres..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Composto " Super Libido Unissex " 60 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/composto-super-libido-unissex-60-doses.png" alt="composto-super-libido-unissex-60-doses" title="composto-super-libido-unissex-60-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>COMPOSTO " SUPER LIBIDO UNISSEX " 60 DOSES</h2>
                        <p class="text-justify">Os chineses consideram o Epimedium Icariin, também conhecido como Ying Yang Huo, o principal elemento de aumento da libido para homens e mulheres, e ajuda principal na função erétil dos homens. Apesar do modo exato do funcionamento do Epimediium ser desconhecido, a planta tem sido usada por muito tempo para restabelecer a potência sexual, impulsionar a função erétil, acalmar fadiga e até mesmo aliviar o desconforto da menopausa.</p>
                        <br>
                        <h2>COMPOSIÇÃO</h2>
                        <p class="text-justify">O QUE É MACA?</p>
                        <p class="text-justify">Maca (Lepidium meyenii) é um vegetal similar a uma raiz com o formato de rabanete que cresce em um clima severo dos Andes na América do Sul em elevações de até 15.000 pés.</p>
                        <p class="text-justify">O QUE É SAW PALMETTO?</p>
                        <p class="text-justify">A saw palmetto, de nome científico Serenoa repens, é uma palmeira de pequeno porte de hastes espinhosas e serreadas, que alcança no máximo 4 metros de altura.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado.</p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>