<?php
    $title       = "Remedio manipulado para pele";
    $description = "O remédio manipulado para pele age de forma inovadora removendo manchas na pele, ou melhorando cicatrizes, tudo depende do objetivo e do remédio que você procura.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A Special Fórmulas disponibiliza para você diversas fórmulas e manipulados que funcionam como <strong>remédio manipulado para pele</strong>. Aqui você pode trazer sua receita feita pelo médico e adquirir suporte total para manipulação. Encontre serviços especiais de manipulação para cápsulas, sachês, cremes, soluções, entre outros produtos.</p>
<p>Entre os mais comuns ingredientes para o <strong>remédio manipulado para pele</strong>, ácido retinóico e ácido kójico, vitanol-A, klassis entre outras substâncias são utilizadas na manipulação. Devendo sempre consultar um dermatologista antes para qualquer tipo de manipulação deste tipo, tenha todo o suporte profissional de um laboratório especializado que faz uso das Boas Práticas de Manipulação em Farmácia para oferecer sempre o melhor para a clientela.</p>
<p>Existem fórmulas especiais que são mais seguras para o uso rotineiro que também podem ser encontradas conosco. Ainda assim, consultar um profissional é sempre necessário. O <strong>remédio manipulado para pele </strong>age de forma inovadora removendo manchas na pele, ou melhorando cicatrizes, tudo depende do objetivo e do remédio que você procura. Caso o seu dermatologista receite alguma fórmula específica para manipulação, encontre todo suporte para isso aqui, com a Special Fórmulas, líder no mercado de manipulação em São Paulo.</p>
<p>A eficácia de qualquer <strong>remédio manipulado para pele</strong> vai depender muito do quando você cuida da sua pele diariamente, hidratando, lavando e sempre protegendo do sol com protetores solares e evitando longos períodos expostos. Dessa forma, o manipulado age de forma muito mais eficiente clareando e removendo manchas. Encontre fórmulas em creme para pele com ação anti-inflamatória, hidratante, clareador e para tratamento de acne.</p>
<h2><strong>Consulte um médico para usar qualquer remédio manipulado para pele</strong></h2>
<p>Antes de utilizar qualquer tipo de medicamento, procure sempre um profissional. Qualquer tipo de medicamento possui contra indicações e restrições para o uso. Consulte a bula do seu<strong> remédio manipulado para pele. </strong></p>
<h2><strong>Como aumentar a eficácia do remédio para pele</strong></h2>
<p>Sempre mantenha a pele bem higienizada e hidratada. Em casos de necessidade de se expor ao sol, sempre utilize protetor solar indicado pelo seu dermatologista antes de sair a luz solar. Compre qualquer tipo de<strong> remédio manipulado para pele </strong>com receita ou com substâncias para uso sem receita com a Special Fórmulas. Acesse nosso catálogo e encontre as melhores opções de fórmulas e manipulados de São Paulo.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>