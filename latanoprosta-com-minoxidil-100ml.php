<?php

    $title       = "Latanoprosta Com Minoxidil 100Ml";
    $description = "Latanoprosta Fagron é um análogo direto da prostaglandina F2-alfa, comumente utilizado sob a forma de colírio no tratamento de glaucoma e hipertensão ocular..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Latanoprosta Com Minoxidil 100Ml</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/latanoprosta-com-minoxidil-100ml.png" alt="latanoprosta-com-minoxidil-100ml" title="latanoprosta-com-minoxidil-100ml">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>LATANOPROSTA COM MINOXIDIL 100ML</h2>
                        <p class="text-justify">Latanoprosta Fagron é um análogo direto da prostaglandina F2-alfa, comumente utilizado sob a forma de colírio no tratamento de glaucoma e hipertensão ocular, que apresenta como efeito adverso o aumento da densidade ciliar. Com base nesta evidência, estudos avaliaram sua ação no tratamento de alopecia e foi observado o aumento significativo da densidade capilar, com o aumento tanto de pelos terminais quanto de pelos velus. Sua ação ocorre principalmente através do estímulo aos folículos capilares, prolongando a fase anágena e promovendo a conversão da fase telógena à fase anágena.
</p>
                        <br>
                        <h2>BENEFÍCIOS</h2>
                        <ul>
                            <li>Estímulo à fase anágena</li>
                            <li>Redução da queda capilar</li>
                            <li>Aumento da conversão de pelos velus em pelos terminais</li>
                            <li>Aumento da densidade e pigmentação capilar</li>
                            <li>Efeitos mais potentes, rápidos e duradouros que os tratamentos atualmente disponíveis</li>
                        </ul>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>