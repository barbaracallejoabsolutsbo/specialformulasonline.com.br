<?php

    $title       = "Loção Auxiliar No Tratamento De Acne 100Ml";
    $description = "Acnebiol é uma formulação especialmente desenvolvida para o tratamento da pele com acne e oleosa, seja jovem ou adulta. Os princípios ativos do Acnebiol..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Loção Auxiliar No Tratamento De Acne 100Ml</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/locao-auxiliar-no-tratamento-de-acne-100ml.png" alt="locao-auxiliar-no-tratamento-de-acne-100ml" title="locao-auxiliar-no-tratamento-de-acne-100ml">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>Descrição</h2>
                        <p class="text-justify">Acnebiol é uma formulação especialmente desenvolvida para o tratamento da pele com acne e oleosa, seja jovem ou adulta. Os princípios ativos do Acnebiol atuam complementarmente:</p>
                        <ul>
                            <li><strong>Ácido salicílico</strong>: um hidroxiácido com função esfoliante na superfície da pele, removendo as células mortas e refinando a textura da pele. Eficaz em reduzir a formação de acnes e comedões (os famosos “cravos”).</li>
                            <li><strong>Salicilato de Dimetilsilanodiol</strong>: ação queratolítica (quebra da queratina, minimização de cicatrizes), anti-inflamatória e anti-edema. Atua como estabilizador da membrana celular e no processo da imunomodulação cutânea, prevenindo e reparando a membrana em casos de danos por estresse oxidativo e auxiliando na manutenção da integridade da estrutura da pele.</li> 
                        </ul>
                    </div>
                </div>
                <ul>
                    <li><strong>Acetilmetionato de zinco</strong>: o zinco ligado ao aminoácido acetilmetionina favorece a solubilização e permeação cutânea. Este ativo tem ação sebo-reguladora e microbicida.</li>
                    <li><strong>Extratos vegetais (Aloe, Lúpulo, Pepino e Limão)</strong>: os extratos de Aloe e Pepino apresentam função hidratante e cicatrizante; o extrato de Lúpulo atua na oleosidade excessiva; o extrato de Limão tem propriedades antisséptica, adstringente e antioleosidade.</li>
                    <li><strong>Proteínas estruturais</strong>: restauração do equilíbrio da pele, através da hidratação e retenção da umidade e, consequentemente, melhoria da elasticidade. Prevenção contra a formação de queloides, que poderiam resultar em cicatrizes.</li>
                </ul>
                <br>
                <p class="text-justify">Acnebiol então pode atuar em algumas frentes: normalizando a oleosidade excessiva da pele; inibindo o processo inflamatório que leva à formação de comedões; removendo as células mortas da superfície da pele, controlando também a hiperqueratinização, melhorando a textura da pele. Testes em voluntários demonstraram que o uso de Acnebiol tem efeito secativo em 48h de aplicação, e após 30 dias de tratamento melhora lesões leves de ACNE.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>