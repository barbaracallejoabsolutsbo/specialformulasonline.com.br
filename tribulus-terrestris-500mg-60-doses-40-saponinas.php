<?php

    $title       = "Tribulus Terrestris 500Mg 60 Doses (40% saponinas)";
    $description = "Beta-caroteno melhor conhecido como um precursor da vitamina A, é um importante antioxidante que ajuda a prevenir danos de radicais livres prejudiciais no corpo"; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Tribulus Terrestris 500Mg 60 Doses (40% saponinas)</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/tribulus-terrestris-500mg-60-doses-40-saponinas.png" alt="tribulus-terrestris-500mg-60-doses-(40-saponinas)" title="tribulus-terrestris-500mg-60-doses-(40-saponinas)">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>REDUÇÃO DOS RADICAIS LIVRES</h2>
                        <p class="text-justify">O Tribulus Terrestris é utilizado principalmente para potencializar o desempenho físico, pois aumenta a produção natural de testosterona. Turbina a força e o ganho de massa magraealiviaa sensação de fadiga e cansaço. Ele também é capaz de melhorar os momentos de intimidade, aumentando a libido e auxiliando na ereção.</p>
                        <p class="text-justify">Tribulus terrestris é indicado como regulador hormonal e para aumento da espermatogênese em pacientes que apresentam alterações das funções sexuais devido a uma baixa concentração do hormônio dehidroepiandrosterona (DHEA) no organismo. Além da elevação da testosterona, há um aumento da libido, frequência e força das ereções e recuperação da atividade sexual. Reduz os níveis de colesterol e melhora o humor. Em mulheres, diminui os sintomas da frigidez sexual, aumenta a libido e reduz os sintomas da menopausa.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Portadores de hiperplasia benigna de próstata somente devem utilizar este produto após avaliação médica. Nunca compre medicamento sem orientação de um profissional habilitado. Imagens meramente ilustrativas. Não é recomendado para mulheres com problemas hormonais. Não é recomendado o uso prolongado. Pode causa gastrite e refluxo. Pessoas com hipersensibilidade à substância não devem ingerir o produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o medicamento com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o medicamento se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Todo medicamento deve ser mantido fora do alcance das crianças. </p>
                <p class="advertencias text-justify">Este medicamento não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do medicamento durante o período de amamentação também não é recomendado. Este medicamento não deve ser utilizado por menores de 18 anos sem orientação médica. "se persistirem os sintomas, o médico deverá ser consultado". "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>