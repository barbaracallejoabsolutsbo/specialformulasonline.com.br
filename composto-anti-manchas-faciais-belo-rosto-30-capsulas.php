<?php

    $title       = "Composto Anti-Manchas Faciais BELO ROSTO";
    $description = "O Oli Ola, cujo nome científico é Olea Europaea Fruit Extract, foi descoberto por pesquisadores na Tunísia, no sul do Mediterrâneo. Trata-se de um extrato 100%"; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Composto Anti-Manchas Faciais "BELO ROSTO" 30 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/composto-anti-manchas-faciais-belo-rosto-30-capsulas.png" alt="composto-anti-manchas-faciais-belo-rosto-30-capsulas" title="composto-anti-manchas-faciais-belo-rosto-30-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <ul>
                            <li><strong>OLI-OLA:</strong> O Oli Ola, cujo nome científico é Olea Europaea Fruit Extract, foi descoberto por pesquisadores na Tunísia, no sul do Mediterrâneo. Trata-se de um extrato 100% natural retirado de um fruto de oliveira típica da região. O ativo natural Oli Ola atua como um nutricosmético que serve para clarear a pele, estimular a renovação celular, inclusive a produção de elastina e colágeno da tez, mantendo-a sempre bela. Oli Ola é poderoso e proporciona resultados perceptíveis a partir da quarta semana de tratamento. Mas é preciso sempre levar em conta que cada organismo reage de forma diferente a um medicamento, por isso o tempo de resposta e ação pode variar e ser um pouco maior entre os pacientes.</li>
                            <li><strong>PYCNOGENOL:</strong> O picnogenol, como também é conhecido, é na verdade uma combinação de 40 diferentes compostos que estão presentes na casca do pinheiro marítimo, e que possuem diversos benefícios para a saúde. O pycnogenol pode ter diversas funções no organismo, mas sua principal característica é seu alto potencial antioxidante. Os antioxidantes são compostos químicos que têm a capacidade de combater os radicais livres, prevenindo ou diminuindo danos às células. Esses danos celulares causados pelos radicais livres estão associados a diversas doenças, como câncer, infarto e Alzheimer.</li>
                        </ul>
                    </div>
                </div>
                <ul>
                    <li><strong>ROMÃ EXTRATO SECO(POMOGRANATE):</strong> A romã é conhecida como a ? super fruta? por sua alta concentração de anti-oxidantes, sendo uma das frutas mais nutritivas.O óleo da semente de Romã é derivado da semente da espécie Punica granatum. A romã é rica em vitamina A, E do complexo B e vitamina C, além de flavonóides e polifenóis que proporcionam a capacidade antioxidante do fruto. A romã contém cálcio, potássio, ferro e fitonutrientes que ajudam a proteger o corpo contra doenças cardíacas, diabetes, artrite reumatóide e câncer. Pode ser utilizado na forma de extrato seco ou óleo.</li>
                </ul>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>