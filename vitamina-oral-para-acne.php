<?php

    $title       = "Vitamina Oral Para Acne 60 Cápsulas";
    $description = "Vitamina A: A forma mais pura da vitamina A é o retinol, que possui alto poder antioxidante e é muito utilizado na área cosmética."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Vitamina Oral Para Acne - 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/vitamina-oral-para-acne.png" alt="vitamina-oral-para-acne" title="vitamina-oral-para-acne">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>VITAMINA ORAL PARA ACNE</h2>
                        <p class="text-justify">Vitamina A: A forma mais pura da vitamina A é o retinol, que possui alto poder antioxidante e é muito utilizado na área cosmética, participando da composição de diferentes produtos como cremes e óleos corporais.</p>
                        <p class="text-justify">Vitamina C: Muito provavelmente, você já deve ter ouvido falar sobre o quanto a vitamina C pode fazer bem à saúde, especialmente à da pele. Várias pessoas a utilizam no tratamento dos sinais do envelhecimento, como rugas e linhas de expressão, na retirada de manchas, como nas axilas e virilhas, ou até mesmo para deixar a pele mais limpa.</p>
                        <p class="text-justify">Vitamina B6: A vitamina B6 exerce diversas funções no organismo. Atua como coenzima (substância responsável por ativar determinadas enzimas, fazendo com que exerçam sua função) em várias reações enzimáticas, estando envolvida no metabolismo de aminoácidos e glicogênio;</p>
                    </div>
                </div>
                <p class="text-justify">Zinco: O zinco é um dos tratamentos naturais mais estudados para a acne. Com um bom número de estudos disponíveis, sabemos como o zinco reduz acne. Tem sido demonstrado que ele reduz quase todas as causas conhecidas de acne: Estudos de proveta mostram que o zinco mata bactérias que causam a acne. Não é tão eficaz quanto os antibióticos, mas ainda pode ser útil.</p>
                <p class="text-justify">Cobre: Tem ação antibiótica local e estimula o processo de defesa do organismo, além de aumentar a resistência a infecções tanto virais como bacterianas. O Cobre é um importante nutriente que desempenha um importante papel na síntese de hemoglobina, mielina, melanina e colágeno.</p>
                
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="https://web.whatsapp.com/send?phone=5511944930219&text=Ol%C3%A1%21%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20da%20Exaustyair%20-%20Sistemas%20de%20Exaust%C3%A3o%20e%20Ventila%C3%A7%C3%A3o%20Industrial"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>