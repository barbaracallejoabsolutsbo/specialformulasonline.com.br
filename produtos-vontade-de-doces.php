<?php

    $title       = "Vontade de Doces";
    $description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elementum ultrices commodo. Donec quis dictum tortor."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "home",
        "produtos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="container">
            <section class="nossos-produtos catego-prod container">
                <h1 class="text-center">Vontade de Doces</h1>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/5-htp-100mg-60-doses-grifonia-simplicifolia.png" alt="5-htp-100mg-60-doses-(grifonia-simplicifolia)" title="5-htp-100mg-60-doses-(grifonia-simplicifolia)" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>5 Htp 100Mg 60 Doses (Grifonia Simplicifolia)</h3>
                            <p>O 5-HTP é um precursor direto da serotonina e derivado da...</p>
                            </div>
                            <a class="btn-entrectt" href="5-htp-100mg-60-doses-grifonia-simplicifolia.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/termogenico-abelhinha-120-capsulas.png" alt="termogenico-abelhinha-120-capsulas" title="termogenico-abelhinha-120-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Termogênico Abelhinha 120 Cápsulas</h3>
                            <p>Aumenta o gasto calórico durante a atividade física...</p>
                            </div>
                            <a class="btn-entrectt" href="termogenico-abelhinha-120-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/picolinato-de-cromo-350mcg-90-capsulas.png" alt="picolinato-de-cromo-350mcg-90-capsulas" title="picolinato-de-cromo-350mcg-90-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Picolinato De Cromo 350Mcg 90 Cápsulas</h3>
                            <p>O picolinato de cromo é especialmente indicado no caso...</p>
                            </div>
                            <a class="btn-entrectt" href="picolinato-de-cromo-350mcg-90-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/tribulus-terrestris-500mg-60-doses-40-saponinas.png" alt="tribulus-terrestris-500mg-60-doses-(40-saponinas)" title="tribulus-terrestris-500mg-60-doses-(40-saponinas)" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Tribulus Terrestris 500Mg 60 Doses (40% saponinas)</h3>
                            <p>O Tribulus Terrestris é utilizado principalmente para potencializar...</p>
                            </div>
                            <a class="btn-entrectt" href="tribulus-terrestris-500mg-60-doses-40-saponinas.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/garcinia-cambogia-500mg-60-doses.png" alt="garcinia-cambogia-500mg-60-doses" title="garcinia-cambogia-500mg-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Garcinia Cambogia 500Mg 60 Doses</h3>
                            <p>A Garcinia Cambogia é uma fruta nativa do sul da Ásia...</p>
                            </div>
                            <a class="btn-entrectt" href="garcinia-cambogia-500mg-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/picolinato-de-cromo-100-mcg-60-doses.png" alt="picolinato-de-cromo-100-mcg-60-doses" title="picolinato-de-cromo-100-mcg-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Picolinato De Cromo 100 Mcg 60 Doses</h3>
                            <p>O Picolinato de cromo é considerado como a melhor fonte de cromo...</p>
                            </div>
                            <a class="btn-entrectt" href="picolinato-de-cromo-100-mcg-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/picnogenol-pinus-pinaster-150mg-30-capsulas.png" alt="picnogenol-(pinus-pinaster)-150mg-30-capsulas" title="picnogenol-(pinus-pinaster)-150mg-30-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Picnogenol (Pinus Pinaster) 150MG 30 Cápsulas</h3>
                            <p>O picnogenol, como também é conhecido, é na verdade uma...</p>
                            </div>
                            <a class="btn-entrectt" href="picnogenol-pinus-pinaster-150mg-30-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/5-htp-50mg-30-doses-grifonia-simplicifolia.png" alt="5-htp-50mg-30-doses-(grifonia-simplicifolia)" title="5-htp-50mg-30-doses-(grifonia-simplicifolia)" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>5 Htp 50Mg 30 Doses (Grifonia Simplicifolia)</h3>
                            <p>O 5-htp é um precursor direto da serotonina e derivado da planta griffonia...</p>
                            </div>
                            <a class="btn-entrectt" href="5-htp-50mg-30-doses-grifonia-simplicifolia.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/5-htp-100mg-30-doses-grifonia-simplicifolia.png" alt="5-htp-100mg-30-doses-(grifonia-simplicifolia)" title="5-htp-100mg-30-doses-(grifonia-simplicifolia)" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>5 Htp 100Mg 30 Doses (Grifonia Simplicifolia)</h3>
                            <p>O 5-HTP é um precursor direto da serotonina e derivado da...</p>
                            </div>
                            <a class="btn-entrectt" href="5-htp-100mg-30-doses-grifonia-simplicifolia.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/trib-terrestris-500mg-60-caps.png" alt="trib-terrestris-500mg-60-caps" title="trib-terrestris-500mg-60-caps" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Trib Terrestris 500mg 60 cáps</h3>
                            <p>O Tribullus Terrestris é utilizado principalmente para...</p>
                            </div>
                            <a class="btn-entrectt" href="trib-terrestris-500mg-60-caps.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/5-htp-200mg-30-capsulas-grifonia-simplicifolia.png" alt="5-htp-200mg-30-capsulas-(grifonia-simplicifolia)" title="5-htp-200mg-30-capsulas-(grifonia-simplicifolia)" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>5 Htp 200Mg 30 Cápsulas (Grifonia Simplicifolia)</h3>
                            <p>O 5-HTP é um precursor direto da serotonina e derivado da...</p>
                            </div>
                            <a class="btn-entrectt" href="5-htp-200mg-30-capsulas-grifonia-simplicifolia.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/trans-resveratrol-100mg-60-doses.png" alt="trans-resveratrol-100mg-60-doses" title="trans-resveratrol-100mg-60-dosestrans-resveratrol-100mg-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Trans Resveratrol 100Mg 60 Doses</h3>
                            <p>O Resveratrol, é um composto fenólico, do tipo estilbeno...</p>
                            </div>
                            <a class="btn-entrectt" href="trans-resveratrol-100mg-60-doses.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/termogenico-sem-cafeina-fortermo-120-capsulas.png" alt="termogenico-sem-cafeina-fortermo-120-capsulas" title="termogenico-sem-cafeina-fortermo-120-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Termogênico Sem Cafeína Fortermo 120 Cápsulas</h3>
                            <p>O fortermo é um dos principais termogênicos do mercado...</p>
                            </div>
                            <a class="btn-entrectt" href="termogenico-sem-cafeina-fortermo-120-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/metformina-500mg-60-capsulas.png" alt="metformina-500mg-60-capsulas" title="metformina-500mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Metformina 500mg 60 Cápsulas</h3>
                            <p>É um medicamento usado especialmente para o tratamento de diabetes...</p>
                            </div>
                            <a class="btn-entrectt" href="metformina-500mg-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/saffrin-88-25mg-60-capsulas.png" alt="saffrin-88-25mg-60-capsulas" title="saffrin-88-25mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Saffrin 88,25mg 60 Cápsulas</h3>
                            <p>Saffrin estimula a sensação de saciedade, inibe a recaptação...</p>
                            </div>
                            <a class="btn-entrectt" href="saffrin-88-25mg-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/pholia-negra-100mg-60-capsulas.png" alt="pholia-negra-100mg-60-capsulas" title="pholia-negra-100mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Pholia Negra 100Mg 60 Cápsulas</h3>
                            <p>PHOLIA NEGRA é um extrato padronizado de Ilex p....</p>
                            </div>
                            <a class="btn-entrectt" href="pholia-negra-100mg-60-capsulas.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/gymnema-sylvestre-400mg-60-capsulas.png" alt="gymnema-sylvestre-400mg-60-capsulas" title="gymnema-sylvestre-400mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Gymnema Sylvestre 400Mg 60 Cápsulas</h3>
                            <p>Gymnema Sylvestre é uma erva indiana reconhecida por séculos...</p>
                            </div>
                            <a class="btn-entrectt" href="gymnema-sylvestre-400mg-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/mitburn-spray-compulsao-por-doces-30-ml.png" alt="mitburn-spray-compulsao-por-doces-30-ml" title="mitburn-spray-compulsao-por-doces-30-ml" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Mitburn® Spray Compulsão por Doces 30 ml</h3>
                            <p>O spray compulsão...</p>
                            </div>
                            <a class="btn-entrectt" href="mitburn-spray-compulsao-por-doces-30-ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/ayslim-manga-africana-500mg-60-capsulas.png" alt="ayslim-manga-africana-500mg-60-capsulas" title="ayslim-manga-africana-500mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Ayslim Manga Africana 500Mg 60 Cápsulas</h3>
                            <p>O O Ayslim Manga Africana, desenvolvido com propriedades...</p>
                            </div>
                            <a class="btn-entrectt" href="ayslim-manga-africana-500mg-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/koubo-200mg-60-capsulas.png" alt="koubo-200mg-60-capsulas" title="koubo-200mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Koubo 200Mg 60 Cápsulas</h3>
                            <p>Koubo 200mg foi desenvolvido com propriedades moduladoras...</p>
                            </div>
                            <a class="btn-entrectt" href="koubo-200mg-60-capsulas.php">Saiba +</a>
                        </div>
                    </div>
            </section>
        </div>    
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>