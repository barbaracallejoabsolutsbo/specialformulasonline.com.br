<?php

    $title       = "Óleo de Semente de Abóbora 1000mg 80 Cápsulas";
    $description = "O Óleo de Semente de Abóbora possui propriedades antioxidantes por ser abundante em vitamina E, que age prevenindo a propagação das reações..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Óleo de Semente de Abóbora 1000mg 80 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/oleo-de-semente-de-abobora-1000mg.png" alt="oleo-de-semente-de-abobora-1000mg" title="oleo-de-semente-de-abobora-1000mg">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>PREVINE O CÂNCER DE PRÓSTATA</h2>
                        <p class="text-justify">O Óleo de Semente de Abóbora possui propriedades antioxidantes por ser abundante em vitamina E, que age prevenindo a propagação das reações dos radicais livres nas membranas biológicas. Os fito-químicos da abóbora foram associados à prevenção de câncer de próstata e destacados para vários estudos em campos de pesquisa relacionados à doença. Além disso, sabe-se que os ácidos graxos essenciais presentes no óleo, contribui para melhora dos níveis de LDL (Colesterol ruim) e aumenta os níveis de HDL (Colesterol bom) no organismo (AL-ZUHAIR H, 1997). Os antioxidantes nas formas de vitamina A e E, também são úteis na manutenção da saúde da pele.</p>
                        <p class="text-justify">O Óleo de semente de abóbora revelou-se útil para reduzir os efeitos da BHP induzida pela testosterona. Além dos benefícios relacionados à próstata, as dietas ricas em sementes de abóbora também têm sido associadas a níveis inferiores de câncer de estômago, mama, pulmão e colorretal. O Óleo de semente de abóbora é um agente seguro e eficaz no tratamento da alopécia androgenética leve a moderada, estimulando o crescimento e aumentando o volume e espessura dos cabelos. Devido as suas propriedades antiinflamatórias e antioxidantes, o Óleo de Semente de Abóbora tem sido utilizado no alívio dos sintomas de pacientes que sofrem de artrite reumatoide.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas que fazem uso de anticoagulantes orais (Warfarina, etc.) devem fazer o uso deste produto sob supervisão do médico e/ou nutricionista. Não contém glúten. Nunca compre medicamento sem orientação de um profissional habilitado. Imagens meramente ilustrativas. Pessoas com hipersensibilidade à substância não devem ingerir o produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Todo medicamento deve ser mantido fora do alcance das crianças. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. Este medicamento não deve ser utilizado por menores de 18 anos sem orientação médica. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="https://web.whatsapp.com/send?phone=5511944930219&text=Ol%C3%A1%21%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20da%20Exaustyair%20-%20Sistemas%20de%20Exaust%C3%A3o%20e%20Ventila%C3%A7%C3%A3o%20Industrial"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>