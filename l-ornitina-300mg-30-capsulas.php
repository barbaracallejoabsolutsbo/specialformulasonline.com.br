<?php

    $title       = "L-Ornitina 300Mg 30 Cápsulas";
    $description = "A Ornitina, também chamada de L-Ornitina, é um aminoácido não essencial, o que quer dizer que ele é produzido pelo corpo, e é derivado..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">L-Ornitina 300Mg 30 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/l-ornitina-300mg-30-capsulas.png" alt="l-ornitina-300mg-30-capsulas" title="l-ornitina-300mg-30-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">A Ornitina, também chamada de L-Ornitina, é um aminoácido não essencial, o que quer dizer que ele é produzido pelo corpo, e é derivado da quebra de arginina durante o ciclo do ácido cítrico. Ela ajuda a criar músculos e reduzir a gordura do corpo, principalmente quando é combinada com arginina e carnitina. Ela também é necessária para a formação de citrulina, prolina e ácido glutâmico, aminoácidos que ajudam a fornecer energia às células do corpo.</p>
                        <br>
                        <h2>BENEFÍCIOS</h2>
                        <p class="text-justify">Diminuição dos níveis de amônia: Em uma das ligações e conversões da ornitina, é necessária a presença de amônia, o que faz com que a ornitina ajude a reduzir as concentrações de amônia no sangue, além de aumentar a concentração de ureia, que é um subproduto dessa ligação. Ganho de músculos: A maior parte dos suplementos para ganho de músculos contém uma mistura de ornitina e arginina. A ornitina é responsável por elevar os níveis do hormônio do crescimento (GH) e de insulina.</p>
                    </div>
                </div>
                <p class="text-justify">Recuperação: Níveis saudáveis de ornitina no corpo ajudam na recuperação de traumas, queimaduras, infecções e até câncer. Em um estudo, foi observado que pacientes que tomavam de 10 a 30 gramas de uma forma de l-ornitina por dia curavam-se mais rápido, fazendo com que ficassem menos tempo no hospital. Isso porque ela ajuda a manter os tecidos e músculos do corpo. Ansiedade: Um estudo com animais publicado em 2011 na Nutrition and Neuroscience descobriu que a ornitina reduziu comportamentos ansiosos nos animais, comparado ao grupo de controle. Embora os resultados sejam promissores, eles ainda precisam ser confirmados em estudos com humanos.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>