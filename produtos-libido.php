<?php

    $title       = "Libido e Estimulante Sexuais";
    $description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elementum ultrices commodo. Donec quis dictum tortor."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "home",
        "produtos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="container">
            <section class="nossos-produtos catego-prod container">
                <h1 class="text-center">Libido e Estimulante Sexuais</h1>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/ioimbina-yohimbine-5mg-60-doses.png" alt="ioimbina-(yohimbine)-5mg-60-doses" title="ioimbina-(yohimbine)-5mg-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Ioimbina (Yohimbine) 5Mg 60 Doses</h3>
                            <p>Ao ser consumida, a Ioimbina promove o aumento da excitabilidade...</p>
                            </div>
                            <a class="btn-entrectt" href="ioimbina-yohimbine-5mg-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/tribulus-terrestris-500mg-120-capsulas-40-saponinas.png" alt="tribulus-terrestris-500mg-120-capsulas-(40-saponinas)" title="tribulus-terrestris-500mg-120-capsulas-(40-saponinas)" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Tribulus Terrestris 500Mg 120 Cápsulas (40% saponinas)</h3>
                            <p>O Tribulus Terrestris é utilizado...</p>
                            </div>
                            <a class="btn-entrectt" href="tribulus-terrestris-500mg-120-capsulas-40-saponinas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/tadalafil-10mg-60-doses.png" alt="tadalafil-10mg-60-doses" title="tadalafil-10mg-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Tadalafil 10Mg 60 Doses</h3>
                            <p>Definição da Disfunção Erétil: quando existe um estímulo sexual...</p>
                            </div>
                            <a class="btn-entrectt" href="tadalafil-10mg-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/tadalafil-com-arginina-60-doses.png" alt="tadalafil-com-arginina-60-doses" title="tadalafil-com-arginina-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Tadalafil Com Arginina 60 Doses</h3>
                            <p>O Tadalafil com Arginina irá lhe proporcionar um bombeamento...</p>
                            </div>
                            <a class="btn-entrectt" href="tadalafil-com-arginina-60-doses.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/tribulus-terrestris-500mg-60-doses-40-saponinas.png" alt="tribulus-terrestris-500mg-60-doses-(40-saponinas)" title="tribulus-terrestris-500mg-60-doses-(40-saponinas)" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Tribulus Terrestris 500Mg 60 Doses (40% saponinas)</h3>
                            <p>O Tribulus Terrestris é utilizado...</p>
                            </div>
                            <a class="btn-entrectt" href="tribulus-terrestris-500mg-60-doses-40-saponinas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/ioimbina-yohimbine-10mg-90-doses.png" alt="ioimbina-(yohimbine)-10mg-90-doses" title="ioimbina-(yohimbine)-10mg-90-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Ioimbina (Yohimbine) 10Mg 90 Doses</h3>
                            <p>Ao ser consumida, a Ioimbina promove o aumento da excitabilidade...</p>
                            </div>
                            <a class="btn-entrectt" href="ioimbina-yohimbine-10mg-90-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/saw-palmetto-160mg-60-doses.png" alt="saw-palmetto-160mg-60-doses" title="saw-palmetto-160mg-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Saw Palmetto 160Mg 60 Doses</h3>
                            <p>Saw Palmetto, de nome científico Serenoa repens...</p>
                            </div>
                            <a class="btn-entrectt" href="saw-palmetto-160mg-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/saw-palmetto-e-pygeum-africanum-60-capsulas.png" alt="saw-palmetto-e-pygeum-africanum-60-capsulas" title="saw-palmetto-e-pygeum-africanum-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Saw Palmetto e Pygeum Africanum 60 Cápsulas</h3>
                            <p>Associação fitoterápica para prevenir doenças da próstata...</p>
                            </div>
                            <a class="btn-entrectt" href="saw-palmetto-e-pygeum-africanum-60-capsulas.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/long-jack-400mg-30-capsulas.png" alt="long-jack-400mg-30-capsulas" title="long-jack-400mg-30-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Long Jack 400Mg 30 Cápsulas</h3>
                            <p>O Longjack é popularmente conhecido devido a sua propriedade...</p>
                            </div>
                            <a class="btn-entrectt" href="long-jack-400mg-30-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/tadalafil-5mg-60-doses.png" alt="tadalafil-5mg-60-doses" title="tadalafil-5mg-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Tadalafil 5Mg 60 Doses</h3>
                            <p>Tadalafil é um fármaco sintético que pode auxiliar o homem...</p>
                            </div>
                            <a class="btn-entrectt" href="tadalafil-5mg-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/tadalafil-10mg-60-doses.png" alt="tadalafil-10mg-60-doses" title="tadalafil-10mg-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Tadalafil 10Mg 60 Doses</h3>
                            <p>Definição da Disfunção Erétil: quando existe um estímulo sexual...</p>
                            </div>
                            <a class="btn-entrectt" href="tadalafil-10mg-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/testofen-300mg-60-capsulas.png" alt="testofen-300mg-60-capsulas" title="testofen-300mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Testofen 300mg 60 Cápsulas</h3>
                            <p>Aumento de performance nos exercícios. Pertencente ao...</p>
                            </div>
                            <a class="btn-entrectt" href="testofen-300mg-60-capsulas.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/maca-peruana-500mg-120-capsulas.png" alt="maca-peruana-500mg-120-capsulas" title="maca-peruana-500mg-120-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Maca Peruana 500Mg 120 Cápsulas</h3>
                            <p>A maca é utilizada tradicionalmente como afrodisíaco e como...</p>
                            </div>
                            <a class="btn-entrectt" href="maca-peruana-500mg-120-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/maca-peruana-500mg-60-doses.png" alt="maca-peruana-500mg-60-doses" title="maca-peruana-500mg-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Maca Peruana 500Mg 60 Doses</h3>
                            <p>A maca é utilizada tradicionalmente como afrodisíaco...</p>
                            </div>
                            <a class="btn-entrectt" href="maca-peruana-500mg-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/composto-testosterona-endogena-composto-testoterona-60-doses.png" alt="composto-testosterona-endogena-composto-testoterona-60-doses" title="composto-testosterona-endogena-composto-testoterona-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Composto Testosterona Endógena 60 doses</h3>
                            <p>Considerado o hormônio mais importante...</p>
                            </div>
                            <a class="btn-entrectt" href="composto-testosterona-endogena-composto-testoterona-60-doses.php">Saiba +</a>
                        </div>
                    </div>
            </section>
        </div>    
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>