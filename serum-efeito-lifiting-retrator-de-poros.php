<?php

    $title       = "Sérum Efeito Lifiting Retrator De Poros 30ML";
    $description = "O Sérum Efeito Lifting Retrator De Poros Oficialfarma foi desenvolvido com um blend de ativos especiais que possuem propriedades hidratantes e atuam..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Sérum Efeito Lifiting Retrator De Poros 30ML</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/serum-efeito-lifiting-retrator-de-poros-30ml.png" alt="serum-efeito-lifiting-retrator-de-poros-30ml" title="serum-efeito-lifiting-retrator-de-poros-30ml">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>Descrição</h2>
                        <p class="text-justify">O Sérum Efeito Lifting Retrator De Poros Oficialfarma foi desenvolvido com um blend de ativos especiais que possuem propriedades hidratantes e atuam no fechamento dos poros.</p>
                        <p class="text-justify">O produto também auxilia no clareamento de manchas faciais, reduz a oleosidade e aumenta a queratinização. Possui efeito lifting uniformizando a pele, suavizando rugas e linhas de expressão.</p>
                        <p class="text-justify">Nicotinamida é a forma amida da vitamina B3, que atua no tratamento de acnes leves a moderada.</p>    
                    </div>
                </div>
                <ul>
                    <li><strong>Miniporyl</strong> induz a contração do colágeno permitindo uma redução do tamanho dos poros e regula a produção do excesso de sebo.</li>
                    <li><strong>TGP-2</strong> diminui a expressão da melanogênese, ou seja, reduzir a pigmentação da pele.</li>
                    <li><strong>Aquaporine</strong> melhora o transporte de água através das diferentes camadas (derme e epiderme), obtendo assim, uma pele mais hidratada, macia e flexível.</li>
                    <li><strong>Multi Action-5</strong> possui minerais ortomoleculares obtidos por biotecnologia: Zn, Fe, Mg, Si, Cu, que atuam no desenvolvimento do tecido conjuntivo e na uniformização da coloração da pele.</li>
                    <li><strong>Idealift</strong> ativo desenvolvido para reverter a flacidez cutânea. Também possui ação hidratante.</li>
                </ul>
                <br>

                <p class="text-justify">Desenvolvido com um blend de ativos com propriedades hidratantes e clareadoras, o Sérum Efeito Lifting Retrator De Poros Oficialfarma fecha os poros, regula a oleosidade e suaviza rugas, marcas e linhas rede expressão.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>