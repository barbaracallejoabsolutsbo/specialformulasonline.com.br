<?php

    $title       = "Ômega 1g DHA 50% (ultra) - 60 Cápsulas";
    $description = "O Ômega 3 é essencial para a saúde do cérebro e memória. Constituído por frações do ácido docosahexaenóico (DHA), são indicados principalmente para a função..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Ômega 1g DHA 50% (ultra) - 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/omega-1g-dha-50-ultra-60-capsulas.png" alt="omega-1g-dha-50-(ultra)-60-capsulas" title="omega-1g-dha-50-(ultra)-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>ÔMEGA 1G DHA 50% (ULTRA)</h2>
                        <p class="text-justify">O Ômega 3 é essencial para a saúde do cérebro e memória. Constituído por frações do ácido docosahexaenóico (DHA), são indicados principalmente para a função neurológica. As cápsulas do Ômega 1g DHA 50% (ultra), possuem 50% de DHA.</p>
                        <br>
                        <h2>ÁCIDO DOCOSAHEXAENÓICO (DHA)</h2>
                        <p class="text-justify">O ácido docosahexaenóico (DHA) pode impedir a formação de substâncias deletérias para o cérebro e aumentar a produção de substâncias anti-inflamatórias e neuro protetoras, melhorando a concentração, a memória e as habilidades motoras. Aumentando também a motivação e a velocidade de reação.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>