<?php

    $title       = "TrichoXidil 2,5% Loção Capilar 60ml";
    $description = "Indicado para combater a queda de cabelo, a Loção Capilar com TrichoXidil ™ 2,5% da Special Formulas é uma fórmula 100% natural, livre de álcool..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">TrichoXidil 2,5% Loção Capilar 60ml</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/trichoxidil-25-locao-capilar-60ml.png" alt="trichoxidil-25-locao-capilar-60ml" title="trichoxidil-25-locao-capilar-60ml">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>TRICHOXIDIL 2,5% LOÇÃO CAPILAR 60ML</h2>
                        <p class="text-justify">Indicado para combater a queda de cabelo, a Loção Capilar com TrichoXidil ™ 2,5% da Special Formulas é uma fórmula 100% natural, livre de álcool, parabenos e propilenoglicol. Não causa reações alérgicas e garante ótimos resultados.Em estudo clínico realizado com homens e mulheres com alopecia androgenética, TrichoXidil™ alcançou resultados surpreendentes.</p>
                        <p class="text-justify">O TrichoXidil™ é um fito complexo vegano, desenvolvido a partir de frações específicas de óleos essenciais para o crescimento capilar. Sua ação se dá pela estimulação dos 3 fatores de crescimento: KGF, IGF-1 e VEGF. Atua também na expressão gênica dos fatores de crescimento KGF, IGF-1 e VEGF, envolvidos na proliferação de células na matriz, da papila dérmica e sistema vascular, aumentando a quantidade de matriz extracelular na papila dérmica e mantendo o folículo na fase anágena (fase de produção dos fios).Utilizado no veículo TrichoSol™, promoveu em apenas 90 dias uma redução de 37% dos fios em fase telógena (fase de repouso dos fios) e o aumento de 29% dos fios em fase anágena (fase de produção dos fios).</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o uso, respeitando o prazo de validade indicado na embalagem. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. Todo medicamento deve ser mantido fora do alcance das crianças. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>