<?php

    $title       = "Composto Ganho De Peso 60 Cápsulas";
    $description = "O Composto Ganho de Peso possui ativos, que além de estimular o apetite, atuam na absorção de nutrientes e no aumento da massa muscular..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Composto Ganho De Peso 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/composto-ganho-de-peso-60-capsulas.png" alt="composto-ganho-de-peso-60-capsulas" title="composto-ganho-de-peso-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>MASSA MUSCULAR</h2>
                        <p class="text-justify">O Composto Ganho de Peso possui ativos, que além de estimular o apetite, atuam na absorção de nutrientes e no aumento da massa muscular. Essa combinação faz com que o produto seja um grande aliado no aumento do peso corporal e no ganho de massa muscular de forma ordenada e otimizada.</p>
                        <br>
                        <h2>EFEITO ANABÓLICO</h2>
                        <p class="text-justify">Possui ação anabolizante não hormonal e auxilia no tratamento de quadros de anemia. Além disso, o Composto Ganho de Peso não causa sonolência, o que é comum aos produtos para estimulação de apetite. O Composto Ganho de Peso conta com nutrientes essenciais para quem busca ganhar peso de uma forma saudável. Atua estimulando o apetite e ajudando na absorção dos nutrientes ingeridos.</p>
                    </div>
                </div>
                 <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pode causar dores de cabeça, náuseas e secura nas mucosas e casos raros. Imagens meramente ilustrativas. Pessoas com hipersensibilidade à substância não devem ingerir o produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o medicamento se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado. </p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Todo medicamento deve ser mantido fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>