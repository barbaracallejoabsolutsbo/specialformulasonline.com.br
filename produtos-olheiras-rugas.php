<?php

    $title       = "Produtos";
    $description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elementum ultrices commodo. Donec quis dictum tortor."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "home",
        "produtos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="container">
            <section class="nossos-produtos catego-prod container">
                <h1 class="text-center">Olheiras e Rugas</h1>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/acido-kojico-dipalmitato-50g-gel-creme.png" alt="acido-kojico-dipalmitato-50g-gel-creme" title="acido-kojico-dipalmitato-50g-gel-creme" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Ácido Kójico Dipalmitato 50g Gel Creme</h3>
                            <p>O Ácido kójico Dipalmitato é um agente clareador...</p>
                            </div>
                            <a class="btn-entrectt" href="acido-kojico-dipalmitato-50g-gel-creme.php">Saiba +</a>
                        </div>
                    </div>
            </section>
        </div>    
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>