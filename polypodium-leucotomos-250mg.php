<?php

    $title       = "Polypodium Leucotomos 250Mg 30 Cápsulas";
    $description = "Proteção da estrutura cutânea: PL apresenta efeitos anti-aging. Previne mudanças na pele associadas ao estresse oxidativo (o excesso de radicais livres..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Polypodium Leucotomos 250Mg 30 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/polypodium-leucotomos-250mg.png" alt="polypodium-leucotomos-250mg" title="polypodium-leucotomos-250mg">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>PREVINE O ENVELHECIMENTO PREMATURO DA PELE</h2>
                        <p class="text-justify">Proteção da estrutura cutânea: PL apresenta efeitos anti-aging. Previne mudanças na pele associadas ao estresse oxidativo (o excesso de radicais livres e a defesa antioxidante insuficiente), inibe a expressão de colagenases (enzimas que quebram o nosso colágeno), regulam a produção da elastina e do próprio colágeno. Por fim, foi demonstrado também que PL previne o dano à membrana celular causado pela peroxidação lipídica.</p>
                        <br>
                        <h2>REFORÇA AS DEFESAS DA PELE EM EXPOSIÇÃO AO SOL</h2>
                        <p class="text-justify">Proteção efetiva contra o câncer cutâneo. Administrado oralmente na forma de cápsulas, o PL inibe o dano ao DNA e a sua mutação. Previne a formação de ciclobutamo pirimidina (que é um dano celular induzido pela radiação ultravioleta) e reduz o dano ao DNA mitocondrial (avaliado pela “deleção comum”, que mede o quanto este DNA foi afetado pela radiação UVA).</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="https://web.whatsapp.com/send?phone=5511944930219&text=Ol%C3%A1%21%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20da%20Exaustyair%20-%20Sistemas%20de%20Exaust%C3%A3o%20e%20Ventila%C3%A7%C3%A3o%20Industrial"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>