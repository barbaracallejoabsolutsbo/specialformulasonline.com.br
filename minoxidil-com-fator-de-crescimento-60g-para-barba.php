<?php

    $title       = "Minoxidil Com Fator De Crescimento 60G";
    $description = "O Minoxidil 5% Com Fatores de Crescimento é uma fórmula desenvolvida que combina a eficiência do Minoxidil com um Blend de Fatores de Crescimento..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Minoxidil Com Fator De Crescimento 60G P/ Barba</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/minoxidil-com-fator-de-crescimento-60g-para-barba.png" alt="minoxidil-com-fator-de-crescimento-60g-para-barba" title="minoxidil-com-fator-de-crescimento-60g-para-barba">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">O Minoxidil 5% Com Fatores de Crescimento é uma fórmula desenvolvida que combina a eficiência do Minoxidil com um Blend de Fatores de Crescimento que potencializam os efeitos do produto. Ajuda a combater as falhas de crescimento na barba, normalizando o ciclo do folículo, prolongando a fase anágena. Fatores de Crescimento são proteínas (citoquinas) produzidas por células do tecido e são responsáveis pelo fenômeno conhecido por “Comunicação Celular”.</p>
                        <p class="text-justify">Graças a esta “comunicação química” que existe entre as células que o tecido desempenha a sua função. Através do envelhecimento e por decorrência de algumas doenças, a produção de Fatores de Crescimento é diminuída e, com ela, a fisiologia do tecido fica comprometida. O Blend de Fatores de Crescimento reverte a atrofia folicular induzida pela DHT, aumenta o tamanho dos folículos, e fortalece os fios. O tratamento deve ser contínuo. Uma vez interrompido o tratamento, as falhas voltam ao estado anterior ao tratamento em aproximadamente dois meses.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="text-justify">Embora raros, podem ser apresentados eritema local, descamação, prurido, hipotensão arterial, náuseas, fadiga, erupção cutânea, cefaleia. Retenção hidrossalina com edemas, hipertricose. Pode causar efeito Shedding (os fios que cairiam aos poucos, caem todos de uma única vez). Nunca compre medicamento sem orientação de um profissional habilitado. Imagens meramente ilustrativas. Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o medicamento com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o medicamento se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Todo medicamento deve ser mantido fora do alcance das crianças. Este medicamento não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar.</p>
                <p class="advertencias text-justify">Não desaparecendo os sintomas, procure orientação médica. O uso do medicamento durante o período de amamentação também não é recomendado. Este medicamento não deve ser utilizado por menores de 18 anos sem orientação médica. "se persistirem os sintomas, o médico deverá ser consultado". "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>