<?php

    $title       = "Betacaroteno 50Mg 30 Cápsulas";
    $description = "Beta-caroteno melhor conhecido como um precursor da vitamina A, é um importante antioxidante que ajuda a prevenir danos de radicais livres prejudiciais no corpo"; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Betacaroteno 50Mg 30 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/betacaroteno-50mg.png" alt="betacaroteno-50mg" title="betacaroteno-50mg">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">Beta-caroteno, melhor conhecido como um precursor da vitamina A, é um importante antioxidante que ajuda a prevenir danos de radicais livres prejudiciais no corpo. O beta-caroteno pertence a um grupo de compostos de plantas chamados carotenóides. Estes são os pigmentos que fornecem a coloração amarela, laranja e vermelha nas frutas e vegetais.</p>
                        <br>
                        <h2>POR QUE TOMAR?</h2>
                        <p class="text-justify">Saúde ocular: Uma dieta com alto consumo de beta-caroteno, vitaminas C e E, e zinco é associada com um risco substancialmente reduzido de degeneração macular relacionada à idade em pessoas mais velhas. Degeneração macular relacionada à idade (AMD) é um transtorno degenerativo da macula, a parte central da retina, e é a causa mais comum de cegueira irreversível em países desenvolvidos, de acordo com as informações históricas no artigo.</p>
                    </div>
                </div>
                <h2>AÇÃO</h2>
                <p class="text-justify">O beta-caroteno é um poderoso antioxidante, que protege as células da pele contra danos oxidantes causados por radicais livres. Também é considerado como um dos carotenóides que aumenta a função do sistema imunológico. Prevenção anticancerígena: Além da atividade de melhoria de antioxidante e imunológica, os carotenóides, incluindo beta-caroteno, demonstraram a capacidade de estimular a comunicação entre células. Ao promover a comunicação adequada entre as células, os carotenóides podem exercer um papel na prevenção do câncer.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se trata de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>