<?php

    $title       = "Maca Peruana 500Mg 60 Doses";
    $description = "A maca é utilizada tradicionalmente como afrodisíaco e como suplemento nutricional energético. Recentemente os atletas estão encontrando na Maca..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Maca Peruana 500Mg 60 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/maca-peruana-500mg-60-doses.png" alt="maca-peruana-500mg-60-doses" title="maca-peruana-500mg-60-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>AUMENTA A RESISTÊNCIA FÍSICA E MENTAL</h2>
                        <p class="text-justify">A maca é utilizada tradicionalmente como afrodisíaco e como suplemento nutricional energético. Recentemente os atletas estão encontrando na Maca uma alternativa excelente para substituir os anabolizantes. Apresenta numerosas indicações como energético e restaurador físico e psicológico, melhora a memória e a concentração, fortalece o sistema imunológico, reduz os sintomas da TPM e aumenta a espermatogênese.</p>
                        <br>
                        <h2>MAIS ENERGIA</h2>
                        <p class="text-justify">Além de oferecer diversos benefícios para a saúde, a maca peruana também é vantajosa para quem pratica atividades físicas e deseja potencializar os resultados dos treinos. Realizando a suplementação do fitoterápico, você garante muito mais energia e disposição, o que pode aumentar a resistência física e retardar a fadiga. Mais do que isso, as fibras presentes na composição da maca peruana colaboram para o aumento da saciedade, fator com atuação indireta no controle de peso. A maca peruana também é uma alternativa aos esteroides anabolizantes, pois é rica em esteróis.</p>
                    </div>
                </div>
                <h2>COMBATE A DIABETES</h2>
                <p class="text-justify">A maca peruana diminui a absorção de glicose no organismo devido a grande quantidade de fibras em sua composição. Além disso, o seu consumo regular inibe a ação de uma enzima que age no processo de digestão e reduz a inflamação associada a doenças crônicas, como diabetes tipo 2. Sendo assim, evita que a insulina seja liberada no organismo em grandes quantidades e aumenta o controle do diabetes.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Não é recomendado para mulheres com problemas hormonais. Não é recomendado o uso prolongado. Pode causa gastrite e refluxo. Nunca compre medicamento sem orientação de um profissional habilitado. Imagens meramente ilustrativas. Pessoas com hipersensibilidade à substância não devem ingerir o produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o medicamento com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o medicamento se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Todo medicamento deve ser mantido fora do alcance das crianças. Este medicamento não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do medicamento durante o período de amamentação também não é recomendado.</p>
                <p class="advertencias text-justify">Este medicamento não deve ser utilizado por menores de 18 anos sem orientação médica. "Se persistirem os sintomas, o médico deverá ser consultado". "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>