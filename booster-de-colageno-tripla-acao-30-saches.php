<?php

    $title       = "Booster de Colágeno Tripla Ação 30 Sachês";
    $description = "Três tipos de colágeno no mesmo produto! O Verisol, o UC-II e o Colágeno Hidrolisado juntos têm seus benefícios potencializados, ou seja..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Booster de Colágeno Tripla Ação 30 Sachês</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/booster-de-colageno-tripla-acao-30-saches.png" alt="booster-de-colageno-tripla-acao-30-saches" title="booster-de-colageno-tripla-acao-30-saches">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>PODEROSO ALIADO</h2>
                        <p class="text-justify">Três tipos de colágeno no mesmo produto! O Verisol, o UC-II e o Colágeno Hidrolisado juntos têm seus benefícios potencializados, ou seja, devolvem a firmeza e elasticidade da pele com muito mais eficácia. Também protege os ossos e age contra o envelhecimento.</p>
                        <br>
                        <h2>ATIVOS</h2>
                        <p class="text-justify">Verisol® é o único colágeno especialmente desenvolvido para a beleza da pele, um alimento funcional inovador voltado aos cuidados com a pele, que age de dentro para fora, atenuando e prevenindo os sinais do tempo. O ativo suaviza os sinais do tempo e melhora a elasticidade cutânea, reduz o volume das rugas e aumenta o conteúdo de colágeno na pele a partir de 4 semanas de uso contínuo. UC-II® é um colágeno do tipo II, não desnaturado, derivado da cartilagem de frango. Ele é a principal proteína estrutural responsável pela tração e firmeza do tecido cartilaginoso.</p>
                    </div>
                </div>
                <p class="text-justify">Colágeno Hidrolisado é a principal proteína do corpo que garante a coesão, elasticidade e regeneração da pele, cartilagem e ossos. O colágeno ingerido por via oral permite que o nosso organismo mantenha uma quantidade de massa muscular adequada, além disso, é um eficiente aliado contra processos de flacidez tecidual e, quando aliado à atividade física, torna-se uma excelente fonte proteica, capaz de sintetizar massa magra, mantendo assim o aspecto jovial do corpo. A Vitamina C é necessária para a formação de colágeno e reparação de tecidos corporais e pode estar envolvido em algumas reações de oxidação e redução de radicais livres, estimula a produção de fibras na derme, além de possuir ação antioxidante.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. O uso do produto durante o período de amamentação sem orientação médica, também não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. Nunca compre medicamentos sem orientação de um profissional habilitado. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>