<?php

    $title       = "Manteiga de Karité 30g";
    $description = "Indicado pra hidratar pele e cabelos, a Manteiga de Karité Special Fórmulas, previne o ressecamento e proporciona toque aveludado e agradável sensação..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Manteiga de Karité 30g</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/manteiga-de-karite-30g.png" alt="manteiga-de-karite-30g" title="manteiga-de-karite-30g">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>Descrição</h2>
                        <p class="text-justify">Indicado pra hidratar pele e cabelos, a Manteiga de Karité Special Fórmulas, previne o ressecamento e proporciona toque aveludado e agradável sensação de emoliência à pele. Rica em Ácido Cinâmico, atua como um filtro solar natural, auxiliando na proteção da pele e dos cabelos contra a radiação UV.</p>
                        <p class="text-justify">É composto por uma mistura de ácidos graxos (Ácido Oleico; Ácido Esteárico; Ácido Palmítico; Ácido Linoleico) e por triterpenos, esteroides e hidrocarbonetos. Contém também tocoferóis, que possuem propriedades antioxidantes.</p>
                        <p class="text-justify">A Manteiga de Karité, da Special fórmulas, é um excelente emoliente e exerce ação protetora sobre a pele e cabelos, prevenindo o ressecamento. Atua também como um filtro solar natural, auxiliando na proteção contra a radiação UV.</p>
                    </div>
                </div>
                <h2>BENEFÍCIOS:</h2>
                <ul>
                    <h3>Hidratação da pele e cabelo</h3>
                    <p>A Manteiga de Karité Special Fórmulas garante hidratação da pele e do cabelo, prevenindo ressecamento</p>
                    <h3>Auxilia na proteção contra radiação UV</h3>
                    <p>Atua como um filtro solar natural, auxiliando na proteção da pele e do cabelo contra a radiação UV</p>
                    <h3>Possui ação antioxidante</h3>
                    <p>A Manteiga de Karité Special Fórmulas possui propriedades antioxidantes, ou seja, combate os radicais livres, melhorando o aspecto da pele.</p>
                    <h3>Toque aveludado</h3>
                    <p>Excelente emoliente, previne o ressecamento e proporciona toque aveludado para a pele</p>
                    <h3>COMPOSIÇÃO:</h3>
                    <p>Manteiga de Karité 30g</p>
                    <h3>MODO DE USAR:</h3>
                    <p>Aplicar na pele, cabelos e lábios como desejar.</p>
                </ul>
                <br>
                <h2 class="advertencias">Advertências</h2>
                <ul>
                    <li>Pessoas com hipersensibilidade à substância não devem ifazer uso do produto.</li>
                    <li>Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico.</li>
                    <li>Não use o produto com o prazo de validade vencido.</li>
                    <li>Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem.</li>
                    <li>Manter fora do alcance das crianças.</li>
                    <li>Nunca compre medicamento sem orientação de um profissional habilitado</li>
                    <li>Este produto não deve ser utilizado por mulheres grávidas sem orientação médica.</li>
                    <li>Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica.</li>
                    <li>O uso do produto durante o período de amamentação também não é recomendado.</li>
                    <li>Este produto não deve ser utilizado por menores de 18 anos sem orientação médica.</li>
                    <li>Imagens meramente ilustrativas.</li>
                    <li>"SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO"</li>
                    <li>"Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.” </li>
                </ul>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>