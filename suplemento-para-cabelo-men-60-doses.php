<?php

    $title       = "Suplemento para Cabelo Men 60 Doses";
    $description = "Complexo formado por vitaminas, minerais e outros ativos, que juntos inibem a queda capilar."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Suplemento para Cabelo Men 60 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/suplemento-para-cabelo-men-60-doses.png" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>Silício Orgânico </h2>
                        <p class="text-justify"><strong>Composição:</strong> Cada 1 Dose (2 Caps) Contém:Complexo B Hair 174mg Selênio (como quelato) 50mcg Zinco (como quelato). 15mg Saw palmeto 250mg Dutasterida 0,5mg Silício (como quelato) 2,5mgFo-ti 50mg.</p>
                        <h3>SUPLEMENTO PARA CABELO MEN</h3>
                        <p class="text-justify">Complexo formado por vitaminas, minerais e outros ativos, que juntos inibem a queda capilar, o Suplemento para Cabelo Men Special Fórmulas é um poderoso estimulante para o crescimento dos fios.</p>
                        <h3>Trata a Calvície e Queda Capilar</h3>
                        <p class="text-justify">O Suplemento para Cabelo Men Special Fórmulas foi desenvolvido com ativos comprovadamente eficazes no tratamento da calvície e da queda capilar masculina.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">COMPOSIÇÃO SUPLEMENTO PARA CABELO MEN</h2>
                <h3 class="text-center">Complexo B Hair</h3>
                <p class="advertencias text-center">Essencial para manter a saúde das células no couro cabeludo, evitando a queda capilar.</p>
                <h3 class="text-center">Selênio (como quelato)</h3>
                <p class="advertencias text-center">Contém enzimas antioxidantes que previnem os danos aos folículos capilares.</p>
                <h3 class="text-center">Zinco (como quelato)</h3>
                <p class="advertencias text-center">Fortalece os fios! O Zinco é um nutriente essencial para manter a saúde dos cabelos.</p>
                <h3 class="text-center">Saw palmeto</h3>
                <p class="advertencias text-center">Trata-se de uma planta medicinal muito utilizada para tratar a calvície e a queda de cabelos.</p>
                <h3 class="text-center">Dutasterida</h3>
                <p class="advertencias text-center">Segundo diversos estudos, é considerada uma das substâncias mais eficazes contra a alopecia.</p>
                <h3 class="text-center">Silício (como quelato)</h3>
                <p class="advertencias text-center">Mineral essencial para a regeneração da pele e fortalecimento dos cabelos e unhas.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>