<?php
    $title       = "Controlar vontade de doces";
    $description = "Os manipulados para controlar vontade de doces normalmente são utilizados 30 minutos antes do almoço e do jantar, conforme a orientação do seu médico ou profissional que prescreve a fórmula.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Existem diversas e várias formas de <strong>controlar vontade de doces</strong>. Com a Special Fórmulas você encontra algumas delas. Entre dicas e fórmulas que auxiliam no processo de saciedade, controle de compulsão por alimentos doces, gordurosos e com alto teor calórico. Tenha controle sobre sua alimentação com auxílio das informações e fórmulas que separamos com atenção para você.</p>
<p>Para saciar melhor e <strong>controlar vontade de doces,</strong> você pode apostar em algumas dicas que separamos para você, se ainda assim precisa de um auxílio extra, a Special Fórmulas conta com fórmulas feitas com substâncias naturais e com toda cautela para uso comum, ou fórmulas personalizadas prescritas.</p>
<p>Caso você precise saciar urgentemente sua vontade de comer doces, você pode optar por doces caseiros, que você saiba os ingredientes, assim podendo dosar melhor as calorias ingeridas. Aposte em sabores aromáticos e treine seu paladar. Evite comer fora de horário e nunca coma tudo de uma vez só. Sinta o aroma e deguste de forma tranquila, sem pressa. Assim, você consegue <strong>controlar vontade de doces</strong> melhor, se saciando adequadamente e enviando para o cérebro a resposta no tempo certo.</p>
<p><strong>Controlar vontade de doces</strong> com auxílio de fórmulas manipuladas tem sido muito comum. Entre as substâncias normalmente encontradas e permitidas para manipulação estão o Picolinato de Cromo, 5HTP, Biotina, Koubo, entre outras. Consulte nosso atendimento e encontre fórmulas prontas ou personalizadas de acordo com sua prescrição. Tenha pontualidade na entrega de seus manipulados e todo compromisso da Special Fórmulas.</p>
<h2><strong>Fórmulas para controlar vontade de doces, como é feito o uso</strong></h2>
<p>Os manipulados para <strong>controlar vontade de doces</strong> normalmente são utilizados 30 minutos antes do almoço e do jantar, conforme a orientação do seu médico ou profissional que prescreve a fórmula. Dessa forma a sensação de saciedade é maior e a tentação de ceder ao doce fica totalmente controlável. Encontre todo suporte para comprar suas fórmulas, manipular e personalizar com a Special Fórmulas, farmácia de manipulação.</p>
<h2><strong>Precauções ao usar manipulados para controlar vontade de doces</strong></h2>
<p>Se você possuir qualquer hipersensibilidade ou alergia a qualquer componente prescrito nas fórmulas para <strong>controlar vontade de doces</strong>, é contra indicado sua utilização, da mesma forma que não deve ser utilizado por mulheres grávidas, lactantes, pacientes com históricos que caracterizam qualquer tipo de restrição a qualquer tipo de substância que possa estar presente nas fórmulas manipuladas. Consulte nosso atendimento para mais informações de compras e manipulação.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>