<header itemscope itemtype="http://schema.org/Organization">
    <div class="topo">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="tel">
                        <p>ENVIE SUA RECEITA PELO<a href="<?php echo $unidades[1]["whatsapp-link"]; ?>">WHATSAPP</a></p>
                    </div>
                </div>
                <div class="sobre-nos-topo col-md-6">
                    <a href="empresa">SOBRE NÓS</a> TELEFONE<a href="tel:114332-9032">(11) 4332-9032</a><a href="mailto:contato@specialformulas.com.br">contato@specialformulas.com.br</a>
                </div>
                <div class="col-md-2">
                    <a class="redes-topo" href="https://www.facebook.com/specialformulasfarmacia"><i class="fab fa-facebook-f"></i></a> <a class="redes-topo" href="https://www.instagram.com/specialformulas_oficial/"><i class="fab fa-instagram"></i></a><a class="redes-topo" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i></a>
                </div>
            </div>
        </div>
    </div>  

    <div class="container header-container-main">
        <div class="logo-info-contato">
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    <div class="logo">
                        <a href="<?php echo $url; ?>" title="<?php echo $h1 . " - " . $nome_empresa; ?>">
                            <span itemprop="image">
                                <img src="<?php echo $url; ?>imagens/logo.png" alt="<?php echo $nome_empresa; ?>" title="<?php echo $nome_empresa; ?>" class="img-responsive">
                            </span>
                        </a>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    <div class="contato-header">
                        <div class="icone">
                            <img src="<?php echo $url; ?>imagens/icons/telefone.png" alt="Telefone" title="Telefone" class="img-responsive">
                            <p>
                                <a title="Clique e ligue" href="tel:<?php echo $unidades[1]["ddd"].$unidades[1]["telefone"]; ?>"><span itemprop="telephone"> (<?php echo $unidades[1]["ddd"]; ?>) <?php echo $unidades[1]["telefone"]; ?></span></a> - <a title="Clique e ligue" href="tel:<?php echo $unidades[1]["ddd"].$unidades[1]["telefone2"]; ?>"><span itemprop="telephone"> (<?php echo $unidades[1]["ddd"]; ?>) <?php echo $unidades[1]["telefone2"]; ?></span></a>
                                <br>
                                <a title="whatsApp" href="<?php echo $unidades[1]["whatsapp-link"]; ?>">
                                <span itemprop="telephone"> (<?php echo $unidades[1]["ddd"]; ?>) <?php echo $unidades[1]["whatsapp"]; ?></span></a>
                                <br>
                                <a title="E-mail" href="mailto:<?php echo $emailContato; ?>">
                                    <span itemprop="telephone"> <?php echo $emailContato; ?></span>
                                </a> 
                            </p> 
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    <div class="contato-header">
                        <div class="icone">
                            <img src="<?php echo $url; ?>imagens/icons/mapa.png" alt="Telefone" title="Telefone" class="img-responsive">
                            <p class="endereco"><?php echo $unidades[1]["rua"] . " - " . $unidades[1]["bairro"] . " - " . $unidades[1]["cidade"] . " - " . $unidades[1]["uf"] . " - " . $unidades[1]["cep"]; ?>
                            </p>
                        </div>
                    </div>
                </div>    
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <nav class="menu">
                    <ul class="menu-list">
                        <?php if("http://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"] == $url){ ?>
                            <li><a href="<?php echo $url; ?>" title="Página inicial">Home</a></li>
                            <li><a href="<?php echo $url; ?>empresa.php" title="Sobre a Empresa">Sobre a Empresa</a></li>
                            <li><a title="Produtos">Produtos</a>
                                <ul class="sub-menu">
                                    <li><a title="Beleza">Beleza</a>
                                        <ul class="sub-menu-menu">
                                            <li><a href="<?php echo $url; ?>produtos-olheiras-rugas.php" title="Olheiras e Rugas">Olheiras e Rugas</a></li>
                                            <li><a href="<?php echo $url; ?>produtos-acne-oliosidade.php" title="Acne e Oliosidade">Acne e Oliosidade</a></li>
                                            <li><a href="produtos-anti-envelhecimento.php" title="Anti Envelhecimento">Anti Envelhecimento</a></li>
                                            <li><a href="<?php echo $url; ?>produtos-barba.php" title="Barba">Barba</a></li>
                                            <li><a href="<?php echo $url; ?>produtos-cabelo.php" title="Cabelo">Cabelo</a></li>
                                        </ul>
                                    </li>
                                    <li><a title="Emagrecimento">Emagrecimento</a>
                                        <ul class="sub-menu-menu">
                                            <li><a href="<?php echo $url; ?>produtos-remedios-para-emagrecer.php" title="Remédios para Emagrecimento">Remédios para Emagrecimento</a></li>
                                            <li><a href="<?php echo $url; ?>produtos-vontade-de-doces.php" title="Vontate de Doces">Vontate de Doces</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="produtos-massa-muscular.php" title="Massa Muscular">Massa Muscular</a></li>
                                    <li><a href="produtos-queda-capilar.php" title="Queda Capilar">Queda Capilar</a></li>
                                    <li><a href="produtos-vitaminas.php" title="Vitaminas">Vitaminas</a></li>
                                    <li><a href="produtos-dormir-bem.php" title="Dormir Bem">Dormir Bem</a></li>
                                    <li><a href="produtos-libido.php" title="Libido">Libido</a></li>
                                    <li><a href="produtos-saude.php" title="Saúde">Saúde</a></li>
                                    <li><a title="Homem">Homem</a>
                                        <ul class="sub-menu-menu">
                                            <li><a href="produtos-barba.php" title="produtos-barba">Barba</a></li>
                                            <li><a href="produtos-cabelo-homem-mulher.php" title="produtos-cabelo-homem-mulher">Cabelo</a></li>
                                            <li><a href="produtos-ginecomastia.php" title="produtos-ginecomastia">Ginecomastia</a></li>
                                            <li><a href="produtos-gordura-localizada.php" title="produtos-gordura-localizada">Gordura Localizada</a></li>
                                            <li><a href="produtos-libido.php" title="produtos-libido">Libido e Estimulante Sexuais</a></li>
                                            <li><a href="produtos-testosterona.php" title="produtos-testosterona">Testosterona</a></li>
                                        </ul>
                                    </li>
                                    <li><a title="Mulher">Mulher</a>
                                        <ul class="sub-menu-menu">
                                            <div class="sub-menu-mulher">
                                                <li><a href="produtos-cabelo-homem-mulher.php" title="produtos-cabelo-homem-mulher">Cabelo</a></li>
                                                <li><a href="produtos-celulite-estrias.php" title="produtos-celulite-estrias">Celulite e Estrias</a></li>
                                                <li><a href="produtos-cilios-sombrancelhas.php" title="produtos-cilios-sombrancelhas">Cilios e Sombrancelhas</a></li>
                                                <li><a href="produtos-gordura-localizada.php" title="produtos-gordura-localizada">Gordura Localizada</a></li>
                                                <li><a href="produtos-libido.php" title="produtos-libido">Libido e Estimulante Sexuais</a></li>
                                                <li><a href="produtos-libido-feminino.php" title="produtos-libido-feminino">Libido Feminino</a></li>
                                                <li><a href="produtos-menopausa.php" title="produtos-menopausa">Menopausa</a></li>
                                            </div>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="<?php echo $url; ?>contato.php" title="Contato">Contato</a></li>
                            <li><a href="<?php echo $url; ?>informacoes.php" title="Informações">Informações</a></li>                                        

                        <?php }else{ ?>
                            <li><a href="<?php echo $url; ?>#home" title="home">Home</a></li>
                            <li><a href="<?php echo $url; ?>empresa.php" title="Sobre a Empresa">Sobre a Empresa</a></li>
                            <li><a title="Produtos">Produtos</a>
                                <ul class="sub-menu">
                                    <li><a title="Beleza">Beleza</a>
                                        <ul class="sub-menu-menu">
                                            <li><a href="<?php echo $url; ?>produtos-olheiras-rugas.php" title="Olheiras e Rugas">Olheiras e Rugas</a></li>
                                            <li><a href="<?php echo $url; ?>produtos-acne-oliosidade.php" title="Acne e Oliosidade">Acne e Oliosidade</a></li>
                                            <li><a href="produtos-anti-envelhecimento.php" title="Anti Envelhecimento">Anti Envelhecimento</a></li>
                                            <li><a href="<?php echo $url; ?>produtos-barba.php" title="Barba">Barba</a></li>
                                            <li><a href="<?php echo $url; ?>produtos-cabelo.php" title="Cabelo">Cabelo</a></li>
                                        </ul>
                                    </li>
                                    <li><a title="Emagrecimento">Emagrecimento</a>
                                        <ul class="sub-menu-menu">
                                            <li><a href="<?php echo $url; ?>produtos-remedios-para-emagrecer.php" title="Remédios para Emagrecimento">Remédios para Emagrecimento</a></li>
                                            <li><a href="<?php echo $url; ?>produtos-vontade-de-doces.php" title="Vontate de Doces">Vontate de Doces</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="produtos-massa-muscular.php" title="Massa Muscular">Massa Muscular</a></li>
                                    <li><a href="produtos-queda-capilar.php" title="Queda Capilar">Queda Capilar</a></li>
                                    <li><a href="produtos-vitaminas.php" title="Vitaminas">Vitaminas</a></li>
                                    <li><a href="produtos-dormir-bem.php" title="Dormir Bem">Dormir Bem</a></li>
                                    <li><a href="produtos-libido.php" title="Libido">Libido</a></li>
                                    <li><a href="produtos-saude.php" title="Saúde">Saúde</a></li>
                                    <li><a title="Homem">Homem</a>
                                        <ul class="sub-menu-menu">
                                            <li><a href="produtos-barba.php" title="produtos-barba">Barba</a></li>
                                            <li><a href="produtos-cabelo-homem-mulher.php" title="produtos-cabelo-homem-mulher">Cabelo</a></li>
                                            <li><a href="produtos-ginecomastia.php" title="produtos-ginecomastia">Ginecomastia</a></li>
                                            <li><a href="produtos-gordura-localizada.php" title="produtos-gordura-localizada">Gordura Localizada</a></li>
                                            <li><a href="produtos-libido.php" title="produtos-libido">Libido e Estimulante Sexuais</a></li>
                                            <li><a href="produtos-testosterona.php" title="produtos-testosterona">Testosterona</a></li>
                                        </ul>
                                    </li>
                                    <li><a title="Mulher">Mulher</a>
                                        <ul class="sub-menu-menu">
                                            <div class="sub-menu-mulher">
                                                <li><a href="produtos-cabelo-homem-mulher.php" title="produtos-cabelo-homem-mulher">Cabelo</a></li>
                                                <li><a href="produtos-celulite-estrias.php" title="produtos-celulite-estrias">Celulite e Estrias</a></li>
                                                <li><a href="produtos-cilios-sombrancelhas.php" title="produtos-cilios-sombrancelhas">Cilios e Sombrancelhas</a></li>
                                                <li><a href="produtos-gordura-localizada.php" title="produtos-gordura-localizada">Gordura Localizada</a></li>
                                                <li><a href="produtos-libido.php" title="produtos-libido">Libido e Estimulante Sexuais</a></li>
                                                <li><a href="produtos-libido-feminino.php" title="produtos-libido-feminino">Libido Feminino</a></li>
                                                <li><a href="produtos-menopausa.php" title="produtos-menopausa">Menopausa</a></li>
                                            </div>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="<?php echo $url; ?>contato" title="Contato">Contato</a></li>
                            <li><a href="<?php echo $url; ?>informacoes" title="Informações">Informações</a>
                            </li>
                        <?php } ?>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>