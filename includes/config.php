<?php

    // Principais Dados do Cliente
    $nome_empresa = "Special Fórmulas";
    $emailContato = "contato@specialformulas.com.br";

    // Parâmetros de Unidade
    $unidades = array(
    1 => array(
        "nome" => "Special Fórmulas",
        "rua" => "Av. Kennedy, 352",
        "bairro" => "Jardim do Mar",
        "cidade" => "São Bernardo do Campo",
        "estado" => "São Paulo",
        "uf" => "SP",
        "cep" => "09726-251",
        "latitude_longitude" => "", // Consultar no maps.google.com
        "ddd" => "11",
        "telefone" => "4332-9032",
        "telefone2" => "95048-6357",
        "whatsapp" => "95048-6357",
        "whatsapp-link" => "https://api.whatsapp.com/send?phone=5511950486357&text=Ol%C3%A1%2C%20achei%20sua%20empresa%20no%20Google.%20Desejo%20mais%20informa%C3%A7%C3%B5es",
        "link_maps" => "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3653.7187001730026!2d-46.561213349536956!3d-23.686015672016808!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce43b22e674f8d%3A0x54e28b7c2a507d94!2zU3BlY2lhbCBGw7NybXVsYXMg4oCTIEZhcm3DoWNpYSBkZSBNYW5pcHVsYcOnw6Nv!5e0!3m2!1spt-BR!2sbr!4v1641567679381!5m2!1spt-BR!2sbr"
    ),

    2 => array(
        "nome" => "",
        "rua" => "",
        "bairro" => "",
        "cidade" => "",
        "estado" => "",
        "uf" => "",
        "cep" => "",
        "ddd" => "",
        "telefone" => ""
    )
);

    // Parâmetros para URL
$padrao = new classPadrao(array(
        // URL local
    "http://localhost/specialformulasonline.com.br/",
        // URL online
    "https://www.specialformulasonline.com.br/"
));

    // Variáveis da head.php
$url = $padrao->url;
$canonical = $padrao->canonical;

    // Parâmetros para Formulário de Contato
    $smtp_contato            = ""; // Solicitar ao líder do dpto técnico, 177.85.98.119
    $email_remetente         = ""; // Criar no painel de hospedagem, admin@...
    $senha_remetente         = "c0B1S3vH5eCvAO";

    // Contato Genérico (para sites que não se hospedam os e-mails)
    // $smtp_contato            = "111.111.111.111";
    // $email_remetente         = "formulario@temporario-clientes.com.br";
    // $senha_remetente         = "4567FGHJK";

    // Recaptcha Google
    $captcha                 = false; // https://www.google.com/recaptcha/
    $captcha_key_client_side = "";
    $captcha_key_server_side = "";

    // CSS default
    $padrao->css_files_default = array(
        "default/reset",
        "default/grid-system",
        "default/main",
        "default/slicknav-menu",
        "_main-style"
    );
    
    // JS Default
    $padrao->js_files_default = array(
        "default/jquery-1.9.1.min",
        "default/modernizr",
        "default/jquery.slicknav.min",
        "jquery.padrao.main"
    );

    // Listas de Palavras Chave
   $palavras_chave = array(
      "Azulzinho feminino",
      "Azulzinho natural manipulado",
      "Ação lipolitica manipulado",
      "Controlar vontade de doces",
      "Esmalte para crescimento de unhas",
      "Esmalte para tratamento de micose",
      "Estimulante sexual feminino",
      "Farmacia de manipuladas",
      "Gel para ereção turbinado",
      "Melhores farmacias de manipulacao",
      "Remedio manipulado inibidor de apetite",
      "Remedio manipulado para ansiedade",
      "Remedio manipulado para aumento de apetite",
      "Remedio manipulado para aumento de imunidade",
      "Remedio manipulado para bloqueio de gordura",
      "Remedio manipulado para depressão",
      "Remedio manipulado para manchas faciais",
      "Remedio manipulado para micose",
      "Remedio manipulado para pele",
      "Remedio manipulado para pressão alta",
      "Remedio manipulado para tratamento de rugas",
      "Remedio para ganho de massa muscular",
      "Remedios manipulados",
      "Remedios manipulados dermatológicos",
      "Remedios manipulados fitness",
      "Remedios manipulados para academia",
      "Remedios manipulados para cabelo",
      "Remedios manipulados para cachorro",
      "Remedios manipulados para celulite",
      "Remedios manipulados para colica",
      "Remedios manipulados para crescimento de barba",
      "Remedios manipulados para emagrecimento",
      "Remedios manipulados para engordar",
      "Remedios manipulados para espinha",
      "Remedios manipulados para gastrite",
      "Remedios manipulados para pets",
      "Remedios manipulados para presas alta",
      "Remedios manipulados para TPM",
      "Remedios para acelerar o metabolismo",
      "Tratamento capilar com minoxidil",
      "Tratamento de queda de cabelo"
);

    $palavras_chave_com_descricao = array(
        "Item 1" => "Lorem ipsum dolor sit amet.",
        "Item 2" => "Laudem dissentiunt ut per.",
        "Item 3" => "Solum repudiare dissentiunt at qui.",
        "Item 4" => "His at nobis placerat.",
        "Item 5" => "Ei justo lucilius nominati vim."
    );
    
     /**
     * Submenu
     * 
     * $opcoes = array(
     * "id" => "",
     * "class" => "",
     * "limit" => 9999,
     * "random" => false
     * );
     * 
     * $padrao->subMenu($palavras_chave, $opcoes);
     * 
     */

    /**
     * Breadcrumb
     * 
     * -> Propriedades
     * 
     * Altera a url da Home no breadcrumb
     * $padrao->breadcrumb_url_home = "";
     * 
     * Altera o texto que antecede a Home
     * $padrao->breadcrumb_text_before_home = "";
     * 
     * Altera o texto da Home no breadcrumb
     * $padrao->breadcrumb_text_home = "Home";
     * 
     * Altera o divisor de níveis do breadcrumb
     * $padrao->breadcrumb_spacer = " » ";
     * 
     * -> Função
     * 
     * Cria o breadcrumb
     * $padrao->breadcrumb(array("Informações", $h1));
     * 
     */

    /**
     * Lista Thumbs
     * 
     * $opcoes = array(
     * "id" => "",
     * "class_div" => "col-md-3",
     * "class_section" => "",
     * "class_img" => "img-responsive",
     * "title_tag" => "h2",
     * "folder_img" => "imagens/thumbs/",
     * "extension" => "jpg",
     * "limit" => 9999,
     * "type" => 1,
     * "random" => false,
     * "text" => "",
     * "headline_text" => "Veja Mais"
     * );
     * 
     * $padrao->listaThumbs($palavras_chave, $opcoes);
     * 
     */
    
    /**
     * Funções Extras
     * 
     * $padrao->formatStringToURL();
     * Reescreve um texto em uma URL válida
     * 
     */