<div class="btn-fixo">
	<a class="btn-telephone" title="Clique e ligue" href="tel:114332-9032">
        <i class="fas fa-phone-alt"></i>
    </a>
</div>
<div class="btn-fixo">
	<a class="btn-whatsapp" title="whatsApp" href="<?php echo $unidades[1]["whatsapp-link"]; ?>">
       	<i class="fab fa-whatsapp"></i>
    </a>
</div>
<div class="btn-fixo">
	<a class="btn-mail" title="E-mail" href="mailto:<?php echo $emailContato; ?>">
        <i class="fas fa-envelope-open-text"></i>
   	</a>
</div>