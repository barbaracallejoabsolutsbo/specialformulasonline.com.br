<ul>
    <li><a href="<?php echo $url; ?>" title="Página inicial">Home</a></li>
    <li><a href="<?php echo $url; ?>empresa" title="Sobre a Empresa">Sobre a Empresa</a></li>
    <li><a title="Produtos">Produtos</a>
                                <ul class="sub-menu">
                                    <li><a title="Beleza">Beleza</a>
                                        <ul class="sub-menu-menu">
                                            <li><a href="<?php echo $url; ?>produtos-olheiras-rugas.php" title="Olheiras e Rugas">Olheiras e Rugas</a></li>
                                            <li><a href="<?php echo $url; ?>produtos-acne-oliosidade.php" title="Acne e Oliosidade">Acne e Oliosidade</a></li>
                                            <li><a href="produtos-anti-envelhecimento.php" title="Anti Envelhecimento">Anti Envelhecimento</a></li>
                                            <li><a href="<?php echo $url; ?>produtos-barba.php" title="Barba">Barba</a></li>
                                            <li><a href="<?php echo $url; ?>produtos-cabelo.php" title="Cabelo">Cabelo</a></li>
                                        </ul>
                                    </li>
                                    <li><a title="Emagrecimento">Emagrecimento</a>
                                        <ul class="sub-menu-menu">
                                            <li><a href="<?php echo $url; ?>produtos-remedios-para-emagrecer.php" title="Remédios para Emagrecimento">Remédios para Emagrecimento</a></li>
                                            <li><a href="<?php echo $url; ?>produtos-vontade-de-doces.php" title="Vontate de Doces">Vontate de Doces</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="produtos-massa-muscular.php" title="Massa Muscular">Massa Muscular</a></li>
                                    <li><a href="produtos-queda-capilar.php" title="Queda Capilar">Queda Capilar</a></li>
                                    <li><a href="produtos-vitaminas.php" title="Vitaminas">Vitaminas</a></li>
                                    <li><a href="produtos-dormir-bem.php" title="Dormir Bem">Dormir Bem</a></li>
                                    <li><a href="produtos-libido.php" title="Libido">Libido</a></li>
                                    <li><a href="produtos-saude.php" title="Saúde">Saúde</a></li>
                                    <li><a title="Homem">Homem</a>
                                        <ul class="sub-menu-menu">
                                            <li><a href="produtos-barba.php" title="produtos-barba">Barba</a></li>
                                            <li><a href="produtos-cabelo-homem-mulher.php" title="produtos-cabelo-homem-mulher">Cabelo</a></li>
                                            <li><a href="produtos-ginecomastia.php" title="produtos-ginecomastia">Ginecomastia</a></li>
                                            <li><a href="produtos-gordura-localizada.php" title="produtos-gordura-localizada">Gordura Localizada</a></li>
                                            <li><a href="produtos-libido.php" title="produtos-libido">Libido e Estimulante Sexuais</a></li>
                                            <li><a href="produtos-testosterona.php" title="produtos-testosterona">Testosterona</a></li>
                                        </ul>
                                    </li>
                                    <li><a title="Mulher">Mulher</a>
                                        <ul class="sub-menu-menu">
                                            <div class="sub-menu-mulher">
                                                <li><a href="produtos-cabelo-homem-mulher.php" title="produtos-cabelo-homem-mulher">Cabelo</a></li>
                                                <li><a href="produtos-celulite-estrias.php" title="produtos-celulite-estrias">Celulite e Estrias</a></li>
                                                <li><a href="produtos-cilios-sombrancelhas.php" title="produtos-cilios-sombrancelhas">Cilios e Sombrancelhas</a></li>
                                                <li><a href="produtos-gordura-localizada.php" title="produtos-gordura-localizada">Gordura Localizada</a></li>
                                                <li><a href="produtos-libido.php" title="produtos-libido">Libido e Estimulante Sexuais</a></li>
                                                <li><a href="produtos-libido-feminino.php" title="produtos-libido-feminino">Libido Feminino</a></li>
                                                <li><a href="produtos-menopausa.php" title="produtos-menopausa">Menopausa</a></li>
                                            </div>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
    <li><a href="<?php echo $url; ?>contato" title="Contato">Contato</a></li>
    <li><a href="<?php echo $url; ?>informacoes" title="Informações">Informações</a>
        <ul>
            <?php echo $padrao->subMenu($palavras_chave); ?>
        </ul>
    </li>
</ul>