<?php

    $title       = "Complexo B 30 Cápsulas (Sem Acne)";
    $description = "A carência das vitaminas do complexo b podem acarretar problemas leves até consequências mais graves. Podemos citar a anemia, letargia..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Complexo B 30 Cápsulas (Sem Acne)</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/complexo-b-30-capsulas-sem-acne.png" alt="complexo-b-30-capsulas-(sem-acne)" title="complexo-b-30-capsulas-(sem-acne)">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>COMPLEXO B 30 CÁPSULAS (SEM ACNE)</h2>
                        <ul>
                            <li>TIAMINA: Conhecida também como vitamina B1</li>
                            <li>RIBOFLAVINA:Conhecida também como vitamina B2</li>
                            <li>NIACINA:Conhecida também como vitamina B3</li>
                            <li>ÁCIDO PANTOTÊNICO: Conhecida também como vitamina B5</li>
                            <li>BIOTINA: Assim como outras vitaminas do complexo B, a biotina está relacionado ao metabolismo das gorduras, carboidratos e proteínas.</li>
                            <li>PIRIDOXINA: A vitamina B6</li>
                            <li>FOLATO: A vitamina B9</li>
                            <li>COBALAMINA: Em conjunto com a B9, a cobalamina atua na produção de hemácias e hemoglobina, a proteína que transporta oxigênio.</li>
                        </ul>
                    </div>
                </div>
                <h2>A CARÊNCIA DO COMPLEXO B</h2>
                <p class="text-justify">A carência das vitaminas do complexo b podem acarretar problemas leves até consequências mais graves. Podemos citar a anemia, letargia, falta de apetite, depressão, dor abdominal, queda de cabelo, câimbras musculares, dores de cabeça, diarreia, gases, perda de peso, sonolência, cansaço, alergias de pele e eczema.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado.</p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>