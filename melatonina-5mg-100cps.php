<?php

    $title       = "Melatonina 5MG 100cps";
    $description = "A Melatonina traz inúmeros benefícios à saúde, tendo como principal induzir ao sono e proporcionar um descanso tranquilo e revigorante. Também possui..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Melatonina 5MG 100cps</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/melatonina-5mg-100cps.png" alt="melatonina-5mg-100cps" title="melatonina-5mg-100cps">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>DURMA BEM E ACORDE MELHOR AINDA!</h2>
                        <p class="text-justify">A Melatonina traz inúmeros benefícios à saúde, tendo como principal induzir ao sono e proporcionar um descanso tranquilo e revigorante. Também possui atividade antioxidante, ou seja, combate os radicais livres e o envelhecimento precoce, além de ajudar na recuperação dos neurônios afetados pela doença de Alzheimer, episódios de isquemia cerebral e epilepsia. A melatonina é um hormônio produzido pela glândula pineal ou hipófise, durante a noite, para ajudar a regular o sono e a vigília. Como com o passar dos anos, sua produção cai, logo, a suplementação pode ser uma forma de suprir o corpo. Devido a essa queda, a insônia costuma ser mais frequente em idosos.</p>
                        <p class="text-justify">Entre as várias ações da melatonina já comprovadas, se destacam também a atividade imunomoduladora, capaz de aumentar a imunidade e consequentemente, melhorar as defesas do organismo. Também ajuda a aumentar a produção natural de hormônio do crescimento (GH) e tem sido usada para tratar enxaquecas, distúrbios depressivos, doenças metabólicas, entre outros. A Melatonina é um excelente suplemento para quem sofre com noites mal dormidas. Apesar de ser produzida de forma natural pelo organismo, sua produção cai com o passar dos anos, o que faz com que idosos sofram mais de insônia do que os jovens. Também ajuda a aumentar a produção de GH no organismo, possui atividade antioxidante, entre muitos outros benefícios.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">A melatonina deve ser evitada por pessoas com diabetes mellitus, depressão, distúrbios de coagulação do sangue, pressão arterial alta ou baixa, epilepsia ou que estiverem usando qualquer medicamento para prevenir a rejeição de órgãos transplantados. Foram relatadas reações como fadiga, cefaleia, tontura e irritabilidade, além de sonolência diurna. O efeito em longo prazo ainda é desconhecido. Nunca compre medicamento sem orientação de um profissional habilitado. Imagens meramente ilustrativas. Pessoas com hipersensibilidade à substância não devem ingerir o produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o medicamento com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o medicamento se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Todo medicamento deve ser mantido fora do alcance das crianças.</p>
                <p class="advertencias text-justify">Este medicamento não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do medicamento durante o período de amamentação também não é recomendado. Este medicamento não deve ser utilizado por menores de 18 anos sem orientação médica. "Se persistirem os sintomas, o médico deverá ser consultado". "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como o uso correto do produto conforme descrito na posologia.”.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>