<?php

    $title       = "Composto Massa Muscular - 60 Doses";
    $description = "O Composto Massa Muscular Oficialfarma oferece o estímulo nutricional que seu corpo precisa para otimizar o aumento da massa magra de forma saudável..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Composto Massa Muscular - 60 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/composto-massa-muscular-60-doses.png" alt="composto-massa-muscular-60-doses" title="composto-massa-muscular-60-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">O Composto Massa Muscular Oficialfarma oferece o estímulo nutricional que seu corpo precisa para otimizar o aumento da massa magra de forma saudável. Desenvolvido com ativos selecionados que proporcionam o aumento da testosterona, o composto funciona como um detox, possui ação antioxidante, reduz os níveis de colesterol - auxiliando na atividade cardiovascular diminuindo as incidências de infarto -, auxilia no metabolismo de proteínas, carboidratos, lipídios e energia e principalmente, potencializa o ganho muscular.</p>
                        <br>
                        <h2>ATIVOS</h2>
                        <p class="text-justify">Crisina apresenta atividade fitoestrogênica, ou seja, aumenta a produção de testosterona impedindo a conversão em estrogênio, ajudando no tratamento da celulite e da gordura localizada. O Piper nigrum contribui para aumentar a absorção, diminuindo o esvaziamento gástrico e o trânsito intestinal. Zinco é essencial para um sistema imunológico saudável e para a resistência contra infecções. A Quercetina possui ação antiinflamatória, hepatoprotetora, gastroprotetora e antioxidante. Indol - 3 - Carbinol atua como um modulador de estrogênio. O resveratrol combate o colesterol (LDL) e melhora a flexibilidade dos vasos sanguíneos, atuando na melhora da pressão arterial. Na dieta calórica regula os níveis de insulina e glicose no sangue.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Deve ser evitado em mulheres com histórico de câncer induzido por estrógenos, pois este apresenta estrutura química semelhante ao estrógeno sintético Dietil-betaestradiol sugerindo que o Resveratrol possa agir como um agonista estrogênico. O resveratrol pode aumentar o risco de hemorragia quando administrado com varfarina, dipiridamol, AINES e aspirina. Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. O uso do produto durante o período de amamentação sem orientação médica, também não é recomendado.</p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. Nunca compre medicamentos sem orientação de um profissional habilitado. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>