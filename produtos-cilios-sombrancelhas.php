<?php

    $title       = "Cilios & Sombrancelhas";
    $description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elementum ultrices commodo. Donec quis dictum tortor."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "home",
        "produtos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="container">
            <section class="nossos-produtos catego-prod container">
                <h1 class="text-center">Cilios & Sombrancelhas</h1>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/serum-crescimento-de-cilios-e-sobrancelha-5ml.png" alt="serum-crescimento-de-cilios-e-sobrancelha-5ml" title="serum-crescimento-de-cilios-e-sobrancelha-5ml" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Sérum Crescimento De Cílios E Sobrancelha 5Ml</h3>
                            <p>O VEGF, conhecido como Fator de Crescimento...</p>
                            </div>
                            <a class="btn-entrectt" href="serum-crescimento-de-cilios-e-sobrancelha-5ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/serum-minoxidil-com-fator-de-crescimento-para-sobrancelhas-5ml.png" alt="serum-minoxidil-com-fator-de-crescimento-para-sobrancelhas-5ml" title="serum-minoxidil-com-fator-de-crescimento-para-sobrancelhas-5ml" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Serum Minoxidil C/ Fator De Crescimento P/ Sobrancelhas 5Ml</h3>
                            <p>Desenvolvido pela equipe Special Fórmulas...</p>
                            </div>
                            <a class="btn-entrectt" href="serum-minoxidil-com-fator-de-crescimento-para-sobrancelhas-5ml.php">Saiba +</a>
                        </div>
                    </div>
            </section>
        </div>    
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>