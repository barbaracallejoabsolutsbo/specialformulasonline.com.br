<?php
    $title       = "Remedios manipulados";
    $description = "Os remédios manipulados podem ser solicitados para vários fins, podendo ser estéticos, dermatológicos, suplementações alimentares, tratamento de doenças, entre outros.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Encontre as melhores opções para <strong>remédios manipulados</strong> em nossa farmácia magistral Special Fórmulas. Nosso estabelecimento manipula medicamentos em geral, fabricando fórmulas prescritas pelos seus médicos e doutores a fim de viabilizar a aquisição de dosagens personalizadas de compostos farmacêuticos. Com todo controle de qualidade e equipamentos modernos para manipulação, encontre serviços especiais de manipulação com nossa farmácia.</p>
<p>Os <strong>remédios manipulados</strong> podem ser solicitados para vários fins, podendo ser estéticos, dermatológicos, suplementações alimentares, tratamento de doenças, entre outros. Após consultar seu médico e receitar algum medicamento manipulado, a Special Fórmulas oferece todo suporte de uma farmácia magistral para manipulação dos seus remédios. Consulte preços e condições com nosso atendimento.</p>
<p>Ajudando também de uma forma mais completa combater patologias e problemas ou tratamentos mais específicos, os <strong>remédios manipulados</strong> auxiliam significativamente, isso porque dessa forma é possível isolar alguns compostos com ativos desejados para o uso mais específico, dessa forma criando fórmulas únicas que não é possível encontrar na indústria farmacêutica convencional.</p>
<p>Os <strong>remédios manipulados</strong> feitos com a Special Fórmulas contam com tecnologias e técnicas de fabricação que entregam uma alta performance e rápida absorção, aproveitando de uma forma eficiente e direta todos os compostos contidos no manipulado. Encontre opções exclusivas de manipulação como sachês, cremes, loções, xaropes, shampoos, além de cápsulas, comprimidos revestidos, entre outros.</p>
<h2><strong>Usos mais comuns dos remédios manipulados</strong></h2>
<p>Os <strong>remédios manipulados</strong>, não só são utilizados para tratamento de doenças e problemas médicos. Podem funcionar como suplementação alimentar, terapias, homeopatia, fitoterápicos, entre outras vertentes mais naturais, nutricionais e estéticos, a fim de personalizar fórmulas para cada tipo de demanda encontrada. O mercado farmacêutico hoje tem grande influência em diversas ramificações do cuidado humano, por isso a manipulação de medicamentos, fórmulas e cremes tem se tornado tão populares. Encontre todo o suporte profissional e experiência de uma farmácia magistral e tenha seus manipulados de alta qualidade com preço acessível.</p>
<h2><strong>Maiores vantagens em investir em remédios manipulados</strong></h2>
<p>Além de maior economia, pois produtos farmacêuticos convencionais carregam a marca do laboratório e por esse motivo encarecem, podem ser personalizados em dosagem e forma de consumo para cada tipo de tratamento e paciente com fórmulas mais seguras e eficientes. A Special Fórmulas trabalha com todo controle de qualidade e oferece os melhores serviços para <strong>remédios manipulados</strong> do mercado regional. Fale com nosso atendimento e conheça nossas condições.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>