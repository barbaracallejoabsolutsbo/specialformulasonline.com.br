<?php

    $title       = "Sabonete Auxiliar No Tratamento De Oleosidade 100Ml";
    $description = "O Extrato Glicólico de Hamamélis tem ação adstringente, vasoprotetora, vasoconstritora, descongestionante, antioleosidade, anti-acnêica."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Sabonete Auxiliar No Tratamento De Oleosidade 100Ml</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/sabonete-auxiliar-no-tratamento-de-oleosidade.png" alt="sabonete-auxiliar-no-tratamento-de-oleosidade" title="sabonete-auxiliar-no-tratamento-de-oleosidade">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>ATIVOS</h2>
                        <p class="text-justify">O Extrato Glicólico de Hamamélis tem ação adstringente, vasoprotetora, vasoconstritora, descongestionante, antioleosidade, anti-acnêica. Indicado nas afecções venosas como varizes, flebites, hemorróidas; em preparações para oleosidade excessiva da pele e do couro cabeludo. A Cânfora apresenta propriedades anti-sépticas e anti-inflamatórias. Os produtos à base dessa substância são ideais para quem tem pele e cabelo oleosos e seborréicos. Os produtos à base de cânfora, como sabonetes e shampoo, ajudam a combater inflamações, acne e os fios oleosos. Além disso, existem diversos tipos de cremes de massagem e anticelulite com cânfora que são bastante eficazes para o corpo.</p>
                    </div>
                </div>    
                <p class="text-justify">O Chá Verde contém: bases xantínicas (cafeína e teofilina); proantocianidinas (procianidinas, teasinensinas e asamicaínas); flavonóides (flavanonas, epicatecol, epigalocatecol e seus ésteres gálicos); taninos (ácido galotânico); vitaminas do complexo B; sais minerais (F, Ca, K, Mg); ácidos fenólicos, clorogênico, cafêico e gálico. O Ácido Salicílico além de regular a oleosidade, ter poder antiinflamatório e esfoliativo, também tem ação hidratante, tendo uma ação irritante bem menor do que a de outros ingredientes. Ele também tem sido bastante utilizado para o melhoramento da aparência da pele envelhecida, pois melhora sua aparência com baixa irritação. A medida que desempenha seu papel esfoliante, faz com que a pele se torne mais fina, o que facilita a penetração de outros ativos, maximizando a atuação do produto sobre a pele</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se trata de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="https://web.whatsapp.com/send?phone=5511944930219&text=Ol%C3%A1%21%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20da%20Exaustyair%20-%20Sistemas%20de%20Exaust%C3%A3o%20e%20Ventila%C3%A7%C3%A3o%20Industrial"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>