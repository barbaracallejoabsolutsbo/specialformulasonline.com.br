<?php

    $title       = "Trichoxidill Tônico Capilar Femme 50Ml";
    $description = "Tônico Capilar Femme Special Fórmulas é uma fórmula 100% natural, livre de álcool, parabenos e propilenoglicol. Além de auxiliar no..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Trichoxidill Tônico Capilar Femme 50Ml</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/trichoxidill-tonico-capilar-femme-50ml.png" alt="trichoxidill-tonico-capilar-femme-50ml" title="trichoxidill-tonico-capilar-femme-50ml">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>TRICHOXIDILL TÔNICO CAPILAR FEMME</h2>
                        <p class="text-justify">Tônico Capilar Femme Special Fórmulas é indicado no combate da queda capilar feminina e estimula o crescimento dos fios.</p>
                        <br>
                        <h2>POR QUE TOMAR?</h2>
                        <p class="text-justify">Tônico Capilar Femme Special Fórmulas é uma fórmula 100% natural, livre de álcool, parabenos e propilenoglicol. Além de auxiliar no tratamento da alopecia feminina, também estimula o crescimento dos fios e fortalece o cabelo sem causar ressecamento.</p>
                    </div>
                </div>
                <h2>COMPOSIÇÃO TRICHOXIDILL TÔNICO CAPILAR FEMME</h2>
                <h3>TrichoXidil</h3>
                <p>Nutriente para crescimento capilar. Trata a alopecia e cuida da beleza dos fios.</p>
                <h3>TrichoSol q.s.p.</h3>
                <p>Solução natural livre de álcool que mantem os fios macios enquanto estão sendo tratados</p>
                <h3>Auxina Tricógena</h3>
                <p>Fito complexo com ação tônica no couro cabeludo, que fortalece a raiz dos cabelos, exercendo efeito revitalizante.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>