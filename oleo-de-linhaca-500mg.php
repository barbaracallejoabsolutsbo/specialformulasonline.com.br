<?php

    $title       = "Óleo De Linhaça 500Mg 60 Cápsulas";
    $description = "O organismo humano tem necessidades próprias que precisam ser mantidas em constante equilíbrio.A alimentação é uma necessidade vital ao organismo humano."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Óleo De Linhaça 500Mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/oleo-de-linhaca-500mg.png" alt="oleo-de-linhaca-500mg" title="oleo-de-linhaca-500mg">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>ÓLEO DE LINHAÇA</h2>
                        <p class="text-justify">O organismo humano tem necessidades próprias que precisam ser mantidas em constante equilíbrio.A alimentação é uma necessidade vital ao organismo humano.Os hábitos alimentares da vida moderna não garantem que todos os nutrientes necessários para nosso corpo sejam ingeridos, desta forma os complementos alimentares são importantes para proporcionar saúde e bem-estar. Óleo de Linhaça é uma poderosa fonte de Ômega-3, 6 e 9 e na sua composição ainda contém uma elevada quantidade de fibras solúveis, vitaminas e minerais, que lhe conferem a propriedade de alimento funcional, sendo eficiente na prevenção de diversas doenças e no tratamento de alguns quadros de deficiência hormonal.</p>
                        <br>
                        <p class="text-justify">Auxilia na proteção contra doenças cardíacas, controle das triglicérides e do colesterol. Auxilia no controle dos radicais livres (controle do envelhecimento), na diminuição da retenção de líquidos, inclusive durante o período pré-menstrual minimizando a TPM nas mulheres. Por conter 2 ácidos graxos que ajudam a secar gorduras e que não são produzidos pelo nosso organismo, o Óleo de Linhaça auxilia no processp de emagrecimento. Enquanto o ômega 3 atua no combate de processos inflamatórios, diminuindo o tamanho de células adiposas, o ômega 6 permite que a gordura se torne mais disponível para o organismo que o carboidrato. O óleo de Linhaça é extraído das sementes da Linum usitatissimum, nome científico da Linhaça Marrom. A extração do óleo é feita através de prensagem a frio.</p>
                    </div>
                </div>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="https://web.whatsapp.com/send?phone=5511944930219&text=Ol%C3%A1%21%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20da%20Exaustyair%20-%20Sistemas%20de%20Exaust%C3%A3o%20e%20Ventila%C3%A7%C3%A3o%20Industrial"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>