<?php

    $title       = "KSM-66 300mg 60 Cápsulas";
    $description = "O ksm-66 através de sua capacidade de modular a função dos receptores gaba, gera uma sinalização hipotalâmica para a glândula adrenal..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">KSM-66 300mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/ksm-66-300mg-60-capsulas.png" alt="ksm-66-300mg-60-capsulas" title="ksm-66-300mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>KSM-66 300MG</h2>
                        <p class="text-justify">O ksm-66 através de sua capacidade de modular a função dos receptores gaba, gera uma sinalização hipotalâmica para a glândula adrenal, normalizando os níveis de produção de todos os hormônios, inclusive a testosterona. Desta forma, o paciente atinge novamente os índices normais dos hormônios sexuais, obtendo novamente todos os seus benefícios.</p>
                        <p class="text-justify">Em paralelo a isso, através de um mecanismo distinto, o ksm-66 é capaz de melhorar a capacidade cardiorrespiratória uma vez que é capaz de aumentar os níveis de energia mitocondrial e reduzir a atividade da enzima mg2 atpase, responsável pela quebra de atp. Ksm-66 tem a maior concentração de withanolides do mercado, além de ser o único que traz o doseamento de whiteferin a. Esses compostos são potentes adaptógenos, ou seja, são capazes de aumentar a capacidade de um organismo a se adaptar a fatores ambientais normalizando os processos fisiológicos. Dessa forma, a correria do dia-a-dia e o cansaço não interferem na produção dos hormônios mantendo uma saúde equilibrada.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado.</p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>