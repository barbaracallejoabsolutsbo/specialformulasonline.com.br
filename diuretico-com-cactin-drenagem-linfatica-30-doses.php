<?php

    $title       = "Diurético Com Cactin Drenagem Linfática 30 Doses";
    $description = "O Diurético com Cactin Drenagem Linfática , desenvolvido para ajudar no processo de emagrecimento, eliminando o excesso de líquido do corpo sem a..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Diurético Com Cactin Drenagem Linfática 30 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/diuretico-com-cactin-drenagem-linfatica-30-doses.png" alt="diuretico-com-cactin-drenagem-linfatica-30-doses" title="diuretico-com-cactin-drenagem-linfatica-30-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>“ELIMINA O INCHAÇO!”</h2>
                        <p class="text-justify">O Diurético com Cactin Drenagem Linfática , desenvolvido para ajudar no processo de emagrecimento, eliminando o excesso de líquido do corpo sem a perda de minerais. Sua função diurética e potente ação antioxidante são determinantes para o sucesso dos resultados. Composto totalmente natural e revolucionário, age de maneira direta nas células adiposas. É capaz de promover uma degradação progressiva da gordura corporal, ou seja, auxilia e acelera na perda de peso e elimina a sensação de inchaço.</p>
                        <br>
                        <h2>REDUTOR DE MEDIDAS</h2>
                        <p class="text-justify">O Cactin Drenagem Linfática nasceu há várias décadas no deserto do México, com os índios zapotecas. Estes índios utilizavam a Drenagem Linfática no dia da caça, pois observaram que tinham mais energia e controle da fome. Mais tarde se descobriu com pesquisas que o consumo da Drenagem Linfática faz com que o corpo utilize o depósito de gordura corporal. É o segredo milenar dos índios zapotecas em benefício do corpo e saúde. A junção de Alcachofra, Hibiscus e Cavalinha à Drenagem Linfática, proporciona ainda mais ação diurética e depurativa, melhorando o metabolismo e eliminando as toxinas presentes no organismo.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Contraindicado para alérgicos à alcachofra, quando há obstrução do canal biliar e em pacientes propensos à fermentação intestinal. Contraindicado na disfunção cardíaca e/ou renal. Pessoas com hipersensibilidade às substâncias não devem ingerir o produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o medicamento se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica.</p>
                <p class="advertencias text-justify">Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>