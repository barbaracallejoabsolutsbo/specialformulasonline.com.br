<?php

    $title       = "Oli Ola, Nutricolin E Vitamina C 30 Cápsulas";
    $description = "Beta-caroteno melhor conhecido como um precursor da vitamina A, é um importante antioxidante que ajuda a prevenir danos de radicais livres prejudiciais no corpo"; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Oli Ola, Nutricolin E Vitamina C 30 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/oli-ola-nutricolin-e-vitamina-c-30-capsulas.png" alt="oli-ola-nutricolin-e-vitamina-c-30-capsulas" title="oli-ola-nutricolin-e-vitamina-c-30-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>OLI OLA, NUTRICOLIN E VITAMINA C</h2>
                        <p class="text-justify">Possui propriedades antioxidantes, ou seja, é capaz de eliminar os radicais livres reduzindo os sinais de envelhecimento. É uma substância essencial sob todos os aspectos: no funcionamento da pele (incluindo o crescimento, manutenção e reparo, protegendo-a contra os estragos da oxidação causada pelos radicais livres), feridas e síntese de colágeno..</p>
                        <br>
                        <h3> NUTRICOLIN</h3>
                        <p class="text-justify">Silício inteligente estabilizado em colina auxilia no estímulo das Proteínas da Beleza.</p>
                    </div>
                </div>
                <ul>
                    <li><strong>Pele</strong> - Ajuda a promover a síntese de colágeno e elastina, retenção de água e colabora para promover uma rede estrutural na derme trazendo inúmeros benefícios para a pele.</li>
                    <li><strong>Cabelo</strong> - Colabora para nutrição dos fios e a compactação da cutícula do cabelo.</li>
                    <li><strong>Unha</strong> - Contribui para obter resultados permanentes também para as unhas por auxiliar no estímulo da síntese da queratina.</li>
                </ul>
                <br>
                <h3>Oli-Ola</h3>
                <p class="text-justify">Ativo natural e orgânico que contribui para uniformização da pele, renovação celular e produção de colágeno e elastina. Trata-se de um extrato do fruto da oliveira (oliva) que possui padronização em hidroxitirosol (3% de hidroxitirosol), um polifenol com potente ação antioxidante que tem efeito peeling na pele, como os peelings químico e físico.</p>
                <p class="text-justify">A expectativa e o que e o que o produto promete é que ele estimule a produção de colágeno, aumentando o tempo de vida dos fibroblastos. Ele está sendo considerado como a pílula da beleza, a revolução e solução para diversos problemas que as pessoas com manchas na pele, com pequenas rugas ou envelhecimento precoce enfrentam.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="https://web.whatsapp.com/send?phone=5511944930219&text=Ol%C3%A1%21%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20da%20Exaustyair%20-%20Sistemas%20de%20Exaust%C3%A3o%20e%20Ventila%C3%A7%C3%A3o%20Industrial"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>