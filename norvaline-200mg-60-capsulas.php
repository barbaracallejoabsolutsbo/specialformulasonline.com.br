<?php

    $title       = "Norvaline 200mg 60 Cápsulas";
    $description = "Norvaline é um análogo do aminoácido de cadeia ramificada Valina. Promove inibição da Arginase, enzima que promove a conversão de arginina em ornitina..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Norvaline 200mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/norvaline-200mg-60-capsulas.png" alt="norvaline-200mg-60-capsulas" title="norvaline-200mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>NORVALINE</h2>
                        <p class="text-justify">Norvaline é um análogo do aminoácido de cadeia ramificada Valina. Promove inibição da Arginase, enzima que promove a conversão de arginina em ornitina, deixando menos substrato para a enzima óxido nítrico sintetase. A arginina sofre ação da óxido nítrico sintetase (NOS), gerando óxido nítrico, que promove o relaxamento da musculatura lisa da parede dos vasos sanguíneos, diminuindo, assim, a pressão sanguínea e o risco de desenvolvimento de problemas cardiovasculares, e melhora da vascularização, aumentando o fluxo sanguíneo e a capacidade de contração muscular. O sistema imunológico também utiliza óxido nítrico para desativar células cancerígenas, e os tecidos musculares utilizam óxido nítrico para estimular o seu crescimento.</p>
                        <p class="text-justify">A arginase catalisa o quinto e último passo no ciclo da ureia. Especificamente, é responsável pela conversão de L-arginina em L-ornitina e ureia. Há duas isoformas distintas de arginase distribuídas e armazenadas diferenciadamente nos tecidos e células. Estas são designadas como arginase I (AI) e arginase II (AII). Enquanto a AI é encontrada predominantemente no citosol de células hepáticas, regulando o ciclo da ureia, a AII é amplamente distribuída em tecidos extra-hepáticos, localizada principalmente nas mitocôndrias de células renais, tendo como uma das suas funções a regulação do metabolismo da L-arginina, provendo L-ornitina como precursor para biossíntese de glutamato, poliaminas, creatina e prolina.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado.</p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>