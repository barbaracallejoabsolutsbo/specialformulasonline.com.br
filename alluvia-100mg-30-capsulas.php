<?php

    $title       = "Alluvia 100mg 30 Cápsulas";
    $description = "Alluvia é um extrato derivado de uma nova variedade de Camellia sinesis para perda de peso, colesterol, controle da glicose no sangue e melhoramento..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Alluvia 100mg 30 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/alluvia-100mg-30-capsulas.png" alt="alluvia-100mg-30-capsulas" title="alluvia-100mg-30-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">Alluvia é um extrato derivado de uma nova variedade de Camellia sinesis para perda de peso, colesterol, controle da glicose no sangue e melhoramento da pele de Camellia Sinesis. É recomendado como novo ingrediente é base de chá com excelentes atividades funcionais de beleza e saúde. O extrato de chá roxo (que compõe o Alluvia™), tem inúmeras propriedades benéficas, como ação antioxidante, anti-hipertensivo, redução do elevando nível de glicose no sangue, além de ser antibacteriano, melhora também o metabolismo dos lipídeos.</p>
                        <p class="text-justify">O extrato de chá roxo é naturalmente abundante em polifenóis antioxidantes. O chá roxo é cuidadosamente selecionado e apenas as folhas jovens e brotos são coletados a partir da plantação livre de pesticidas. Foi descoberto um componente polifenólico específico no chá roxo, o GHG (1,2-di-Galloyl-4,6-Hexahydroxydiphenoyl-ß-D-Glucose), com excelentes efeitos antiobesidade e antienvelhecimento, além de melhorar o metabolismo da gordura nos hepatócitos. O extrato de chá roxo e o GHG inibem a lipase pancreática com o aumento da concentração da mesma no organismo, diminuindo a absorção de gordura, e também a atividade da tirosinase de uma maneira dependente. Portanto, é sugestivo que o extrato de chá roxo pode ter um efeito clareador na pele.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>