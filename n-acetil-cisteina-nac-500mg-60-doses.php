<?php

    $title       = "N Acetil Cisteina (NAC) 500Mg 60 Doses";
    $description = "A N-Acetilcisteína (NAC) é uma forma acetilada do aminoácido cisteína. No organismo humano ela se converte na enzima antioxidante glutationa (GSH)..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">N Acetil Cisteina (NAC) 500Mg 60 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/n-acetil-cisteina-nac-500mg-60-doses.png" alt="n-acetil-cisteina-(nac)-500mg-60-doses" title="n-acetil-cisteina-(nac)-500mg-60-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>N ACETIL CISTEÍNA</h2>
                        <p class="text-justify">A N-Acetilcisteína (NAC) é uma forma acetilada do aminoácido cisteína. No organismo humano ela se converte na enzima antioxidante glutationa (GSH) - um dos mais essenciais para o bom funcionamento do organismo.</p>
                        <br>
                        <h2>METABOLISMO E A CONSTRUÇÃO MUSCULAR</h2>
                        <p class="text-justify">Ao contribuir com o fornecimento de glutationa, a N-Acetilcisteína também é responsável por evitar sobrecargas de toxinas, beneficiando o metabolismo, a construção dos músculos, da força e do sistema imunológico. Além disso, também pode auxiliar na melhor absorção de outros nutrientes. Devido ao seu poder antioxidante e a sua capacidade de criar um clima celular favorável para a contração muscular, a NAC é capaz de reduzir a fadiga. Também reduz a inflamação e acelera a recuperação após exercícios intensos.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>