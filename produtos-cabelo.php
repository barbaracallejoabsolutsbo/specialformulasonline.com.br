<?php

    $title       = "Cabelo";
    $description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elementum ultrices commodo. Donec quis dictum tortor."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "home",
        "produtos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="container">
            <section class="nossos-produtos catego-prod container">
                <h1 class="text-center">Cabelo</h1>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/minoxidil-turbinado-120ml.png" alt="minoxidil-turbinado-120ml" title="minoxidil-turbinado-120ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Minoxidil Turbinado 120Ml</h3>
                                <p>O Minoxidil Turbinado é uma fórmula desenvolvida pela Special Fórmulas...</p>
                            </div>
                            <a class="btn-entrectt" href="minoxidil-turbinado-120ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/pill-food-turbinado-120-capsulas.png" alt="pill-food-turbinado-120-capsulas" title="pill-food-turbinado-120-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Pill Food Turbinado 120 Cápsulas</h3>
                                <p>Pill Food Turbinado é um complexo alimentar formado...</p>
                            </div>
                            <a class="btn-entrectt" href="pill-food-turbinado-120-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/suplemento-para-cabelo-men-60-doses.png" alt="suplemento-para-cabelo-men-60-doses" title="suplemento-para-cabelo-men-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Suplemento para Cabelo Men 60 Doses</h3>
                                <p>Complexo formado por vitaminas, minerais e outros ativos...</p>
                            </div>
                            <a class="btn-entrectt" href="suplemento-para-cabelo-men-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/haired-x-50mg.png" alt="haired-x-50-ml-eclaire" title="haired-x-50-ml-eclaire" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Haired-X 50 ml – Éclaire</h3>
                                <p>Haired-X Special Fórmulas é o primeiro tônico de crescimento capilar...</p>
                            </div>
                            <a class="btn-entrectt" href="haired-x-50-ml-eclaire.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/cabelo-pele-e-unha-brave-60-doses-by-gabi-dezan.png" alt="cabelo-pele-e-unha-brave-60-doses-by-gabi-dezan" title="cabelo-pele-e-unha-brave-60-doses-by-gabi-dezan" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Cabelo, Pele e Unha Brave 60 Doses By Gabi Dezan+</h3>
                                <p>O composto constitui em um mix de vitaminas...</p>
                            </div>
                            <a class="btn-entrectt" href="cabelo-pele-e-unha-brave-60-doses-by-gabi-dezan.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/pantoficial-60-doses.png" alt="pantoficial-60-doses" title="pantoficial-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Betacaroteno 50Mg 30 Cápsulas</h3>
                                <p>O Pantoficial é um suplemento alimentar que serve...</p>
                            </div>
                            <a class="btn-entrectt" href="pantoficial-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/minoxidil-tonico-capilar-men-50ml.png" alt="minoxidil-tonico-capilar-men-50ml" title="minoxidil-tonico-capilar-men-50ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Minoxidil Tônico Capilar Men 50Ml</h3>
                                <p>Beta-caroteno melhor conhecido como um precursor...</p>
                            </div>
                            <a class="btn-entrectt" href="minoxidil-tonico-capilar-men-50ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/shampoo-para-dermatite-seborreica-110ml.png" alt="shampoo-para-dermatite-seborreica-110ml" title="shampoo-para-dermatite-seborreica-110ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Shampoo Para Dermatite Seborreica 110ML</h3>
                                <p>A caspa é uma doença crônica, que afeta muitas pessoas...</p>
                            </div>
                            <a class="btn-entrectt" href="shampoo-para-dermatite-seborreica-110ml.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/pantoficial-men-60-capsulas.png" alt="pantoficial-men-60-capsulas" title="pantoficial-men-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Pantoficial Men 60 Cápsulas</h3>
                                <p>Pantoficial Men é um suplemento de vitaminas e minerais...</p>
                            </div>
                            <a class="btn-entrectt" href="pantoficial-men-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/pueraria-mirifica-500mg-60-capsulas.png" alt="pueraria-mirifica-500mg-60-capsulas" title="pueraria-mirifica-500mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Pueraria Mirífica 500mg 60 Cápsulas</h3>
                                <p>A Pueraria Mirífica 500mg 60 cápsulas Special Fórmulas...</p>
                            </div>
                            <a class="btn-entrectt" href="pueraria-mirifica-500mg-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/oleo-de-coco-1000mg-120-capsulas.png" alt="oleo-de-coco-1000mg-120-capsulas" title="oleo-de-coco-1000mg-120-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Óleo De Coco 1000mg 120 Cápsulas</h3>
                                <p>O Óleo de Coco é rico em Ácidos Graxos de Cadeia Média...</p>
                            </div>
                            <a class="btn-entrectt" href="oleo-de-coco-1000mg-120-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/shampoo-com-carvao-ativado-200ml.png" alt="shampoo-com-carvao-ativado-200ml" title="shampoo-com-carvao-ativado-200ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Shampoo com Carvão Ativado 200ml</h3>
                                <p>O Shampoo com Carvão Ativado 200ml Special Fórmulas...</p>
                            </div>
                            <a class="btn-entrectt" href="shampoo-com-carvao-ativado-200ml.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/trichoxidill-tonico-capilar-femme-50ml.png" alt="trichoxidill-tonico-capilar-femme-50mls" title="trichoxidill-tonico-capilar-femme-50ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Trichoxidill Tônico Capilar Femme 50Ml</h3>
                                <p>Tônico Capilar Femme Special Fórmulas é uma fórmula 100% natural...</p>
                            </div>
                            <a class="btn-entrectt" href="trichoxidill-tonico-capilar-femme-50ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/suplemento-para-cabelo-femme-60-doses.png" alt="suplemento-para-cabelo-femme-60-doses" title="suplemento-para-cabelo-femme-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Suplemento para Cabelo Femme 60 Doses SENKEDA</h3>
                                <p>Complexo formado por vitaminas, minerais e outros ativos...</p>
                            </div>
                            <a class="btn-entrectt" href="suplemento-para-cabelo-femme-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/shampoo-anticaspa-com-cetoconazol-2.png" alt="shampoo-anticaspa-com-cetoconazol-2" title="shampoo-anticaspa-com-cetoconazol-2" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Shampoo Anticaspa Com Cetoconazol 2% 110ML</h3>
                                <p>O Shampoo Anticaspa Com Cetoconazol é indicado...</p>
                            </div>
                            <a class="btn-entrectt" href="shampoo-anticaspa-com-cetoconazol-2.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/shampoo-anticaspa-com-octopirox-e-oleo-de-melaleuca-200ml.png" alt="shampoo-anticaspa-com-octopirox-e-oleo-de-melaleuca-200ml" title="shampoo-anticaspa-com-octopirox-e-oleo-de-melaleuca-200ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Shampoo Anticaspa Com Octopirox E Óleo De Melaleuca 200ml</h3>
                                <p>A caspa acomete cerca de 40% da população...</p>
                            </div>
                            <a class="btn-entrectt" href="shampoo-anticaspa-com-octopirox-e-oleo-de-melaleuca-200ml.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/locao-de-fatores-de-crescimento-capilar-barba-60ml.png" alt="locao-de-fatores-de-crescimento-capilar-barba-60ml" title="locao-de-fatores-de-crescimento-capilar-barba-60ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Loção De Fatores De Crescimento Capilar / Barba 60Ml</h3>
                                <p>A Loção de fatores de crescimento capilar/barba...</p>
                            </div>
                            <a class="btn-entrectt" href="locao-de-fatores-de-crescimento-capilar-barba-60ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/anti-queda-capilar-novos-fios-120ml.png" alt="anti-queda-capilar-novos-fios-120ml" title="anti-queda-capilar-novos-fios-120ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Anti-Queda Capilar + Novos Fios 120Ml</h3>
                                <p>O Sérum Anti-queda + Novos fios, da Special Fórmulas...</p>
                            </div>
                            <a class="btn-entrectt" href="anti-queda-capilar-novos-fios-120ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/latanoprosta-com-minoxidil-100ml.png" alt="latanoprosta-com-minoxidil-100ml" title="latanoprosta-com-minoxidil-100ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>latanoprosta-com-minoxidil-100ml</h3>
                                <p>Latanoprosta Fagron é um análogo direto da prostaglandina F2-alfa...</p>
                            </div>
                            <a class="btn-entrectt" href="latanoprosta-com-minoxidil-100ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/condicionador-carvao-ativado.png" alt="condicionador-carvao-ativadol" title="condicionador-carvao-ativado" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Condicionador carvão Ativado</h3>
                                <p>Beta-caroteno melhor conhecido como um precursor da vitamina A...</p>
                            </div>
                            <a class="btn-entrectt" href="condicionador-carvao-ativado.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/tonico-capilar-alfaestradiol-alfa17-100ml.png" alt="tonico-capilar-alfaestradiol-(alfa17)-100ml" title="tonico-capilar-alfaestradiol-(alfa17)-100ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Tônico Capilar Alfaestradiol (Alfa17) 100ml</h3>
                                <p>Indicado para o tratamento e prevenção da alopecia....</p>
                            </div>
                            <a class="btn-entrectt" href="tonico-capilar-alfaestradiol-alfa17-100ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/shampoo-greasy-block-femina-200ml.png" alt="shampoo-greasy-block-femina-200ml" title="shampoo-greasy-block-femina-200ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>SHAMPOO GREASY BLOCK FEMINA 200ML</h3>
                                <p>Beta-caroteno melhor conhecido como um precursor da vitamina A...</p>
                            </div>
                            <a class="btn-entrectt" href="shampoo-greasy-block-femina-200ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/shampoo-p-barba-e-cabelo-120ml.png" alt="shampoo-p-barba-e-cabelo-120ml" title="shampoo-p-barba-e-cabelo-120ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Shampoo P/ Barba E Cabelo 120ml</h3>
                                <p>O shampoo para barba e cabelo da Special Fórmulas...</p>
                            </div>
                            <a class="btn-entrectt" href="shampoo-p-barba-e-cabelo-120ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/latanoprosta-espuma-capilar-100ml.png" alt="latanoprosta-espuma-capilar-100ml" title="latanoprosta-espuma-capilar-100ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Latanoprosta Espuma Capilar 100mL</h3>
                                <p>Visando garantir a qualidade dos produtos que necessitam de refrigeração, faremos envios exclusivamente de segunda-feira a quarta-feira....</p>
                            </div>
                            <a class="btn-entrectt" href="latanoprosta-espuma-capilar-100ml.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/oleo-de-abacate-1000mg-60-capsulas.png" alt="oleo-de-abacate-1000mg-60-capsulas" title="oleo-de-abacate-1000mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Óleo de Abacate 1000mg 60 Cápsulas</h3>
                                <p>O Abacate (Persea americana) é uma fruta de origem americana...</p>
                            </div>
                            <a class="btn-entrectt" href="oleo-de-abacate-1000mg-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/gelatina-500mg-60-capsulas.png" alt="gelatina-500mg-60-capsulas" title="gelatina-500mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Gelatina 500Mg 60 Cápsulas</h3>
                                <p>A gelatina nada mais é do que uma substância naturalmente...</p>
                            </div>
                            <a class="btn-entrectt" href="gelatina-500mg-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/composto-turbinado-para-cabelos-pele-e-unhas-30-capsulas.png" alt="composto-turbinado-para-cabelos-pele-e-unhas-30-capsulas" title="composto-turbinado-para-cabelos-pele-e-unhas-30-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Composto Turbinado Para Cabelos, Pele E Unhas 30 Cápsulas</h3>
                                <p>Nutricolin é o silício inteligente fundamental...</p>
                            </div>
                            <a class="btn-entrectt" href="composto-turbinado-para-cabelos-pele-e-unhas-30-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/rutina-150mg-60-capsulas-nupelli.png" alt="rutina-150mg-60-capsulas-nupelli" title="rutina-150mg-60-capsulas-nupelli" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Rutina 150mg 60 Cápsulas</h3>
                                <p>Rutina 150mg da Special Fórmulas, foi desenvolvida...</p>
                            </div>
                            <a class="btn-entrectt" href="rutina-150mg-60-capsulas-nupelli.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/leave-in-femme-60ml-fortalecedor-de-fios.png" alt="leave-in-femme-60ml-fortalecedor-de-fios" title="leave-in-femme-60ml-fortalecedor-de-fios" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Leave in Femme 60Ml - Fortalecedor de Fios SENKEDA</h3>
                                <p>O Senkeda Leave-in Femme Oficialfarma é um sérum...</p>
                            </div>
                            <a class="btn-entrectt" href="leave-in-femme-60ml-fortalecedor-de-fios.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/trichoxidil-25-locao-capilar-60ml.png" alt="trichoxidil-25-locao-capilar-60ml" title="trichoxidil-25-locao-capilar-60ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>TrichoXidil 2,5% Loção Capilar 60ml</h3>
                                <p>Indicado para combater a queda de cabelo, a Loção Capilar com TrichoXidil...</p>
                            </div>
                            <a class="btn-entrectt" href="trichoxidil-25-locao-capilar-60ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/biosil-300mg-60-capsulas.png" alt="biosil-300mg-60 -capsulas" title="biosil-300mg-60 -capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Biosil 300Mg 60 Cápsulas</h3>
                                <p>BioSil™ é a marca comercial e registrada da patente da molécula ch-OSA...</p>
                            </div>
                            <a class="btn-entrectt" href="biosil-300mg-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/serum-intensificador-de-brilho.png" alt="serum-intensificador-de-brilho" title="serum-intensificador-de-brilho" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Serum Intensificador De Brilho</h3>
                                <p>O D-pantenol é uma espécie de álcool hidrossolúvel e rico...</p>
                            </div>
                            <a class="btn-entrectt" href="serum-intensificador-de-brilho.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/keranat-150mg-por-25ml-150ml.png" alt="keranat-150mg-por-2.5ml-150ml" title="keranat-150mg-por-2.5ml-150ml" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Keranat 150mg Por 2,5ml 150ML</h3>
                                <p>O segredo contra a queda de cabelo e beleza dos fios...</p>
                            </div>
                            <a class="btn-entrectt" href="keranat-150mg-por-25ml-150ml.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/biosil-300mg-30-capsulas.png" alt="biosil-300mg-30-capsulas" title="biosil-300mg-30-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Biosil 300Mg 30 Cápsulas</h3>
                                <p>BioSil™ é a marca comercial e registrada da patente...</p>
                            </div>
                            <a class="btn-entrectt" href="biosil-300mg-30-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/exsynutriment-com-vit-c-30-capsulas.png" alt="exsynutriment-com-vit-c-30-capsulas" title="exsynutriment-com-vit-c-30-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Exsynutriment Com Vit C 30 Cápsulas</h3>
                                <p>É uma associação do silício orgânico de Exsynutriment...</p>
                            </div>
                            <a class="btn-entrectt" href="exsynutriment-com-vit-c-30-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/manteiga-de-karite-30g.png" alt="manteiga-de-karite-30g" title="manteiga-de-karite-30g" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Manteiga de Karité 30g</h3>
                                <p>Indicado pra hidratar pele e cabelos, a Manteiga...</p>
                            </div>
                            <a class="btn-entrectt" href="manteiga-de-karite-30g.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/padinactive-nutri-100mg-60-capsulas.png" alt="padinactive-nutri-100mg-60-capsulas" title="padinactive-nutri-100mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Padinactive Nutri 100mg 60 Cápsulas</h3>
                                <p>PADINACTIVE® NUTRI traz um novo conceito de tratamento anti-rugas...</p>
                            </div>
                            <a class="btn-entrectt" href="padinactive-nutri-100mg-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/shampoo-hidratante-com-babosa.png" alt="shampoo-hidratante-com-babosa" title="shampoo-hidratante-com-babosa" class="img-responsive">
                            <div class="descricao-prod">
                                <h3>Shampoo Hidratante com babosa</h3>
                                <p>O shampoo hidratante com Babosa 200ml Special Fórmulas...</p>
                            </div>
                            <a class="btn-entrectt" href="shampoo-hidratante-com-babosa.php">Saiba +</a>
                        </div>
                    </div>
            </section>
        </div>    
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>