<?php

    $title       = "Complexo B Com Vit C - 120 Doses";
    $description = "O complexo B é composto por vitaminas hidrossolúveis, ou seja, vitaminas que se dissolvem facilmente em água e são transportadas para as células..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Complexo B Com Vit C - 120 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/complexo-b-com-vit-c-120-doses.png" alt="complexo-b-com-vit-c-120-doses" title="complexo-b-com-vit-c-120-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>VITAMINA B</h2>
                        <p class="text-justify">O complexo B é composto por vitaminas hidrossolúveis, ou seja, vitaminas que se dissolvem facilmente em água e são transportadas para as células do corpo pela corrente sanguínea. O Complexo B facilita na absorção dos nutrientes, carboidratos, proteínas e gorduras, as vitaminas B ajudam na absorção e digestão dos alimentos, facilitando a suplementação da dieta.</p>
                        <br>
                        <h2>VITAMINA C</h2>
                        <p class="text-justify">A Vitamina C é uma vitamina hidrossolúvel encontrada naturalmente em frutas cítricas e em vegetais como pimenta, pimentão, couve-flor, brócolis e couve de Bruxelas. Usada principalmente para a produção de vários suplementos alimentares. A Vitamina C é essencial para a síntese de colágeno, assim como para aumentar a imunidade e resistência a infecções e como antioxidante, na captação de radicais livres.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem ingerir o produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado.</p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>