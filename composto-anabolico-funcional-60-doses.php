<?php

    $title       = "Composto Anabólico Funcional - 60 doses";
    $description = "Composto anabólico funcional, auxilia no ganho de massa muscular, reduz a fadiga muscular e estimula a liberação de hormônios como a testosterona..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Composto Anabólico Funcional - 60 doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/composto-anabolico-funcional-60-doses.png" alt="composto-anabolico-funcional-60-doses" title="composto-anabolico-funcional-60-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>VÁ ALÉM</h2>
                        <p class="text-justify">Composto anabólico funcional, auxilia no ganho de massa muscular, reduz a fadiga muscular e estimula a liberação de hormônios como a testosterona, além de serem fonte de vitaminas e minerais antioxidantes.</p>
                        <br>
                        <h2>ATIVOS</h2>
                        <h3>Gamma Oryzanol:</h3><p class="text-justify">O metabólito ativo é o ácido ferúlico que circula pela corrente sanguínea e pode ser absorvido por tecidos no corpo. O ácido ferúlico tem um efeito na liberação de hormônios, especialmente em aumentar os níveis de testosterona. Também conhecido por estimular o hipotálamo a liberar o hormônio do crescimento (GH), que estimula o organismo a produzir o (hGH).</p>
                    </div>
                </div>
                <h3>Fenulife (Feno grego):</h3><p class="text-justify">O Feno Grego é muito conhecido por sua atividade fito anabolizante, e os efeitos conduzidos por suas sementes são derivados sobretudo pela ação dos fenosídeos, o que resulta no aumento da produção da testosterona testicular e na síntese de testosterona. Maca Peruana: Além de oferecer diversos benefícios para a saúde, a maca peruana também é vantajosa para quem pratica atividades físicas e deseja potencializar os resultados dos treinos. Realizando a suplementação do fitoterápico, você garante muito mais energia e disposição, o que pode aumentar a resistência física e retardar a fadiga.</p>
                <h3>Beterraba:</h3><p class="text-justify">É um alimento remineralizante e vitamínico, excelente fonte de vitaminas A, C e do complexo B. Podem ser preparadas como suco, sendo altamente laxante. Fortalece o coração, fígado, vesícula biliar e glândulas endócrinas. Além disso, é depurativa do sangue e combate a anemia, obesidade e tumores em geral.</p>
                <h3>Marapuama:</h3><p class="text-justify">Um dos usos medicinais da marapuama é para o estímulo sexual, altamente indicada para o tratamento de impotência sexual, disfunção erétil, ejaculação precoce, infertilidade masculina, falta de libido, frigidez, fadiga, disfunções intestinais, depressão, queda de cabelo, estresse, perda de memória, inchaço e celulite. Melhora, também, os sintomas da TPM e cólica menstrual.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>