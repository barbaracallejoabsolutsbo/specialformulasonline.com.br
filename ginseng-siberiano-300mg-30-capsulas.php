<?php

    $title       = "Ginseng Siberiano 300mg 30 Cápsulas";
    $description = "O Ginseng Siberiano é recomendado e utilizado como tônico no tratamento da fadiga, queda de eficiência e concentração, bem como da convalescença."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Ginseng Siberiano 300mg 30 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/ginseng-siberiano-300mg-30-capsulas.png" alt="ginseng-siberiano-300mg-30-capsulas" title="ginseng-siberiano-300mg-30-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>PODEROSO ALIADO</h2>
                        <p class="text-justify">O Ginseng Siberiano é recomendado e utilizado como tônico no tratamento da fadiga, queda de eficiência e concentração, bem como da convalescença. É utilizado também como estimulante dos sistemas circulatório e imunológico, regulador da pressão arterial reduzir a inflamação e tratar a insônia provocada por ansiedade prolongada. Assim, tem numerosas indicações, entre as quais excesso de trabalho, cansaço devido a diferenças horárias, trabalho físico intenso, calor ou frio excessivos, exposição a radiações e qualquer situação em que haja esforço prolongado (mas não em caso de hipertensão).</p>
                        <p class="text-justify">Ginseng Siberiano pode fornecer proteção contra radiação, servindo de auxílio no tratamento de pacientes de câncer com radiação. Isso é confirmado por estudos nos quais o Ginseng Siberiano demonstrou inibição do câncer. É utilizado para enfraquecer a vitalidade das células cancerígenas e reduzir a velocidade com que as metástases se espalham em outros tecidos. Além disso, Ginseng Siberiano demonstra um aumento de resistência à infecção, redução da biossíntese de colesterol no fígado, aumento da capacidade reprodutiva e contagem de espermatozoides, significante atividade antioxidante e estimulação às enzimas de reparo de células, bem como à síntese de proteínas, resultando em efeito anabólicos.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>