<?php

    $title       = "Cavalinha 100Mg 60 Cápsulas";
    $description = "A cavalinha é uma planta medicinal que pode trazer vários benefícios para a saúde. Ela auxilia na redução de peso,mantém a pele mais saudável,"; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Cavalinha 100Mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/cavalinha100mg.png" alt="cavalinha100mg" title="cavalinha100mg">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">A cavalinha é uma planta medicinal que pode trazer vários benefícios para a saúde. Ela auxilia na redução de peso,mantém a pele mais saudável, tem ação antioxidante, acelera o metabolismo e proteje as articulações. A maneira mais comum de se consumir a cavalinhaé através de seu chá. Porém já pode ser encontrada em formato de cápsulas aqui na Special Formulas.</p>
                        <br>
                        <h2>BENEFÍCIOS</h2>
                        <p class="text-justify">Combate a retenção de líquido; Aceleração do metabolismo; Auxilia na redução da oleosidade da pele e combate e previne o aparecimento de espinhas; Ação antioxidante; Propriedade anti-inflamatória; Atividade antimicrobiana; Memória e cognição; Bronquite, tosse, resfriado e gripe; Amigdalite, gengivite e feridas bucais.</p>
                    </div>
                </div>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="text-justify">Se persistirem os sintomas, o medico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>