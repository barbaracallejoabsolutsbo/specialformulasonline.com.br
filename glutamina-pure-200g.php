<?php

    $title       = "Glutamina Pure 200G";
    $description = "A glutamina é um aminoácido condicionalmente essencial e estima-se que aproximadamente 60% de todos os aminoácidos livres no corpo..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Glutamina Pure 200G</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/glutamina-pure-200g.png" alt="glutamina-pure-200g" title="glutamina-pure-200g">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>GLUTAMINA PURE 200G</h2>
                        <p class="text-justify">A glutamina é um aminoácido condicionalmente essencial e estima-se que aproximadamente 60% de todos os aminoácidos livres no corpo estão na forma de glutamina, sendo o músculo o órgão de maior armazenamento. Denominam a glutamina como “condicionalmente essencial”, pois ele precisa estar presentes em determinadas e muito importantes condições, como quando há sua depleção (diminuição).</p>
                        <br>
                        <h2>O QUE A GLUTAMINA-PURE FAZ?</h2>
                        <p class="text-justify">A glutamina é a proteína mais abundante no nosso corpo e é sintetizada a partir das necessidades do organismo. Esse aminoácido corresponde a 20% do total de aminoácidos e essa quantidade é 2 vezes maior que a alanina. Ela é desenvolvida a partir de 3 substâncias: ácido glutâmico (o grupo amida), valina e isoleucina. Para ser metabolizada, duas enzimas são necessárias sendo uma delas a glutamina sintetase, fazendo a interação do glutamato com a amônia e a outra, é a glutaminase.</p>
                    </div>
                </div>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>