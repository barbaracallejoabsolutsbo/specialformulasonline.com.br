<?php

    $title       = "Ácido Hialurônico 50Mg 60 Doses";
    $description = "O ÁCIDO HIALURÔNICO é um polissacarídeo da família das Glicosaminoglicanas (GAG’s), naturalmente presente na derme. Descoberto em 1934..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Ácido Hialurônico 50Mg 60 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/acido-hialuronico-50mg.png" alt="acido-hialuronico-50mg" title="acido-hialuronico-50mg">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>ÁCIDO HIALURÔNICO</h2>
                        <p class="text-justify">O ÁCIDO HIALURÔNICO é um polissacarídeo da família das Glicosaminoglicanas (GAG’s), naturalmente presente na derme. Descoberto em 1934 no humor vítreo dos olhos de vaca, despertou um grande interesse devido à sua excepcional capacidade hidratante. É um dos mais importantes componentes da derme envolvidos nesta função, devido à sua elevada capacidade de retenção de água. Aplicado topicamente, forma um filme hidratante sobre a epiderme, que ajuda a compensar a perda de água, melhorando as condições da pele e proporcionando desta forma elasticidade, suavidade e uma superfície mais homogênea.</p>
                        <p class="text-justify">Substância naturalmente presente no organismo humano, uma molécula de açúcar que atrai a água e pode atuar como um lubrificante e absorver impactos em partes móveis do corpo como as articulações. Do Ácido Hialurônico no nosso corpo, 56% dele está na pele, onde ele atua preenchendo o espaço entre as células, o que a mantém lisa, elástica e bem hidratada. Porém, com o tempo, sua concentração na pele diminui, o que causa o aparecimento de rugas e também seu ressecamento. O Ácido Hialurônico é indicado principalmente para melhorar o viço da pele, suavizando rugas e outras marcas da idade.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>