<?php

    $title       = "L-Arginina Aspartato 500Mg 100 Cápsulas";
    $description = "Trata-se de um aminoácido semi-essencial, mais precursor do óxido nítrico. É mais um agente para a perda do excesso de gordura corporal e aumento dos músculos."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">L-Arginina Aspartato 500Mg 100 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/l-arginina-aspartato-500mg-100-capsulas.png" alt="l-arginina-aspartato-500mg-100-capsulas" title="l-arginina-aspartato-500mg-100-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">Trata-se de um aminoácido semi-essencial, mais precursor do óxido nítrico. É mais um agente para a perda do excesso de gordura corporal e aumento dos músculos, assim como a melhoria da resistência física e da força. A arginina é a única fonte de nitrogênio atuante na NO sintase (síntase de óxido nítrico). O estímulo do NO contribuirá no aumento do abastecimento contínuo de sangue nos músculos. A partir daí podemos compreender, já de início, que a sua função principal está na saúde muscular. Em resumo, podemos ver que a arginina tem atuação importantíssima para a síntese proteica no organismo.</p>
                        <p class="text-justify">Portanto é um grande agente no ganho de massa magra e perda de gordura, pois esta acaba tendo seu armazenamento limitado. Tudo isso devido à melhora da retenção de nitrogênio. Então, esse suplemento trabalha no desenvolvimento da massa magra em detrimento da gorda, através da chamada NO sintase. Mas é claro que depois disso, a arginina traz outras funções também relevantes, principalmente aos atletas. O estímulo à produção do hormônio do crescimento, mais insulina e glucagon, elementos essenciais para o progresso da performance nos treinos, é um bom exemplo. Aliás, a explosão de força recebe muita melhora, já que a arginina é um dos intermediários nas sínteses do fosfato e da creatina. Com isso, o atleta pode contar com um aliado suplementar para ganhar progressão nos treinos de curta duração e alta intensidade.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>