<?php

    $title       = "L-Teanina 200Mg 60 Cápulas";
    $description = "L-Teanina é um aminoácido não comum na nossa dieta (não é um dos aminoácidos essenciais ou mesmo um dos aminoácidos não essenciais comuns)..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">L-Teanina 200Mg 60 Cápulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/l-teanina-200mg-60-capulas.png" alt="l-teanina-200mg-60-capulas" title="l-teanina-200mg-60-capulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">L-Teanina é um aminoácido não comum na nossa dieta (não é um dos aminoácidos essenciais ou mesmo um dos aminoácidos não essenciais comuns). Ela foi descoberta como um dos componentes do chá verde em 1949. É substancialmente presente nos chás derivados da Camellia sinensis, também conhecidos como chá branco, verde e preto, e também em alguns cogumelos. A L-Teanina tem semelhança estrutural com a Glutamina e outros neurotransmissores que são produzidos a partir dele (GABA e glutamato) e é conhecido por ter ação no sistema nervoso central (cérebro) após a ingestão por via oral.</p>
                        <br>
                        <h2>MAIOR CONCENTRAÇÃO</h2>
                        <p class="text-justify">A principal propriedade da L-Teanina é atuar como um agente relaxante sem sedação – diferente da erva-cidreira, que relaxa, mas também pode sedar. Também está associada a uma redução da percepção de estresse e melhora de atenção. Desta forma, enquanto a L-Teanina não parece induzir o sono, ela pode ajudar com o sono, devido ao seu efeito relaxante. Curiosamente, as propriedades relaxantes e que proporcionam mais atenção, em conjunto com a falta de sedação da L-Teanina pode ter um papel suplementar mais significativo como um atenuante de efeitos excessivos de muitos estimulantes.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>