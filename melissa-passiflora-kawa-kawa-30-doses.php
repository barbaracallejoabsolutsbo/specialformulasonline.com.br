<?php

    $title       = "Melissa + Passiflora + Kawa Kawa 30 Doses";
    $description = "A passiflora é mais conhecida por seu fruto, o maracujá. Ela possui boas quantidades de flavonoides. Eles apresentam vários efeitos biológicos e farmacológicos."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Melissa + Passiflora + Kawa Kawa 30 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/melissa-passiflora-kawa-kawa-30-doses.png" alt="melissa-passiflora-kawa-kawa-30-doses" title="melissa-passiflora-kawa-kawa-30-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>Passiflora</h2>
                        <p class="text-justify">A passiflora é mais conhecida por seu fruto, o maracujá. Ela possui boas quantidades de flavonoides. Eles apresentam vários efeitos biológicos e farmacológicos, incluindo atividade antibacteriana, antiviral, anti-inflamatória, antialérgica e vasodilatadora. Além disso, estas substâncias inibem a peroxidação lípidica e reduzem o risco de doenças cardiovasculares, efeitos estes relacionados à sua atividade antioxidante, caracterizada pela capacidade de sequestrar radicais livres em organismos vivos.</p>
                        <p class="text-justify">Os benefícios da passiflora são:</p>
                        <ul>
                            <li>Ação calmante: A passiflora estimula a liberação de hormônios que levam ao relaxamento e sensação de bem-estar. Por isso, a planta é benéfica em casos de insônia.</li>
                            <li>Previne a depressão: A passiflora ajuda a evitar a depressão devido à sensação de bem-estar que ela proporciona. Seus componentes químicos primários, calcalóides e flavonoides agem de forma relaxante e antidepressiva no organismo.</li>
                            <li>Melhora a concentração: Ao proporcionar maior sensação de bem-estar e relaxamento, a pessoa consegue alcançar maior estado de concentração.</li>
                        </ul>
                    </div>
                </div>
                <ul>
                    <li>Boa contra infecções: A passiflora possui ação antibactericida.</li>
                    <li>Boa contra a doença de Parkinson: Acredita-se que a presença dos alcaloides harmina e harmalina, possam ser eficazes contra o Mal de Parkinson, porém ainda não há detalhamento dos mecanismos.</li>
                    <li>Controla a pressão arterial: As harmalas, substâncias presentes na passiflora inibem o consumo excessivo e desnecessário de oxigênio pelo cérebro. Acredita-se que estes compostos também são responsáveis por diminuir os níveis de circulação e respiração, reduzindo a pressão arterial.</li>
                </ul>
                <h2>Melissa</h2>
                <p class="text-justify">Melissa também conhecida como Erva Cidreira tem ação sedativa, eupéptica e espasmolítica. Auxiliar no tratamento dos casos de insônia, depressões e tensões nervosas. Tem ação digestiva e é coadjuvante no tratamento de afecções do estômago, gases e cólicas intestinais.</p>
                <h2>Kawa kawa</h2>
                <p class="text-justify">A kawa kawa, de nome científico Piper methysticum, é uma planta originária das ilhas da Oceania (Polinésia, Indonésia e Melanésia), também conhecida por outras denominações como cava cava, awa-irai, kawa e outras. Trata-se de uma planta bastante utilizada e consumida na forma de um “leite” extraído de seu rizoma, e contribui muito com a economia e com a vida social das ilhas do Pacífico. A kawa kawa possui várias propriedades medicinais bastante eficientes e, por ser de fácil cultivo, pode ser plantada no jardim de casa, assim como outras ervas medicinais.</p>
                <p class="text-justify">A propriedade medicinal mais comum da kawa kawa é a de calmante e/ou sedativo, devido à presença do Waka, um relaxante ósseo e muscular que acalma todo o sistema nervoso central. Dentre as propriedades medicinais desta planta, podemos destacar as seguintes:</p>
                <ul>
                    <li>Ansiolítica;</li>
                    <li>Espasmolítica;</li>
                    <li>Calmante;</li>
                    <li>Anticonvulsionante;</li>
                    <li>Analgésica;</li>
                    <li>Antisséptica;</li>
                    <li>Sedativa;</li>
                    <li>Tranquilizante.</li>
                </ul>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>