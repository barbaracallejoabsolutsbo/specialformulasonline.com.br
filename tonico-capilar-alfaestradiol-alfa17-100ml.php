<?php

    $title       = "Tônico Capilar Alfaestradiol (Alfa17) 100ml";
    $description = "Indicado para o tratamento e prevenção da alopecia androgenética, o Tônico Capilar Com Alfaestradiol, da Special Fórmulas, combate a queda de cabelo."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Tônico Capilar Alfaestradiol (Alfa17) 100ml</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/tonico-capilar-alfaestradiol-alfa17-100ml.png" alt="tonico-capilar-alfaestradiol-(alfa17)-100ml" title="tonico-capilar-alfaestradiol-(alfa17)-100ml">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>Descrição</h2>Indicado para o tratamento e prevenção da alopecia androgenética, o Tônico Capilar Com Alfaestradiol, da Special Fórmulas, combate a queda de cabelo causada por fatores hormonais em homens e mulheres. Deve ser aplicado exclusivamente no couro cabeludo e não ser aplicado em outras áreas do corpo. O mecanismo de ação do Alfaestradiol se dá através da inibição das duas isoformas atualmente conhecidas da 5-alfaredutase na pele (enzima mediadora da conversão da testosterona andrógena em DHT), metabólito biologicamente ativo que acelera o ciclo dos cabelos e conduz, por fim, a um aumento do percentual de fios na fase telógena (fase de queda dos fios).</p>
                        <br>
                        <h2>COMBATE A QUEDA CAPILAR</h2>
                        <p class="text-justify">O tratamento tópico com Alfaestradiol consiste em atuar de um modo específico, sobre os processos bioquímicos na raiz dos fios, uma vez que a administração mantém concentrações eficazes do alfa-estradiol na pele. Ao contrário do 17- beta-estradiol, o alfa-estradiol apresenta baixa afinidade com os receptores de estrógeno, ou seja, o fármaco não possui ação hormonal em doses terapêuticas.</p>
                    </div>
                </div>
                <h2>NÃO TEM ALTERAÇÃO HORMONAL</h2>
                <p class="text-justify">O Tônico Capilar Com Alfaestradiol, da Special Fórmulas, é indicado para o tratamento e prevenção da alopecia androgenética, que é responsável pela queda de cabelo por fatores hormonais, em homens e mulheres. Estimula também o crescimento de novos fios e não possui ação hormonal em doses terapêuticas.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pode causar coceira e vermelhidão no local de aplicação devido a presença de álcool na formulação. Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. O uso do produto durante o período de amamentação também não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>