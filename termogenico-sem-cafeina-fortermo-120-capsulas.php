<?php

    $title       = "Termogênico Sem Cafeína Fortermo 120 Cápsulas";
    $description = "Beta-caroteno melhor conhecido como um precursor da vitamina A, é um importante antioxidante que ajuda a prevenir danos de radicais livres prejudiciais no corpo"; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Termogênico Sem Cafeína Fortermo 120 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/termogenico-sem-cafeina-fortermo-120-capsulas.png" alt="termogenico-sem-cafeina-fortermo-120-capsulas" title="termogenico-sem-cafeina-fortermo-120-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>TERMOGÊNICO SEM CAFEÍNA "FORTERMO" 120 CÁPSULAS</h2>
                        <p class="text-justify">O fortermo é um dos principais termogênicos do mercado, e por ser bastante acessível e apresentar ótimos resultados, acabou se popularizando entre as pessoas que buscam aumentar o seu gasto calórico e intensificar a queima de gordura. Porém, algumas pessoas não podem fazer uso dessa substância, seja por intolerância ou por interação com outros suplementos. Pensando nisso, hoje vamos falar dos produtos termogênicos sem cafeína, suas particularidades e indicações.</p>
                        <br>
                        <h2>POR QUE OPTAR POR UM TERMOGÊNICO SEM CAFEÍNA?</h2>
                        <p class="text-justify">Os suplementos a base de cafeína podem causar alguns efeitos colaterais, sendo que os principais são:</p>
                        <ul>
                            <li>Dores de cabeça</li>
                            <li>Problemas cardiovasculares e aceleração dos batimentos cardíacos</li>
                            <li>Alterações de humor</li>
                            <li>Aumento da ansiedade, sobretudo em pessoas com predisposição</li>
                            <li>Insônia</li>
                            <li>Náuseas e problemas gastrointestinais</li>
                        </ul>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar.</p>
                <p class="advertencias text-justify">Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>