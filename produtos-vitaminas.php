<?php

    $title       = "Vitaminas";
    $description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elementum ultrices commodo. Donec quis dictum tortor."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "home",
        "produtos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="container">
            <section class="nossos-produtos catego-prod container">
                <h1 class="text-center">Vitaminas</h1>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/vitamina-d3-10000ui-60-doses.png" alt="vitamina-d3-10000ui-60-doses" title="vitamina-d3-10000ui-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Vitamina D3 10.000Ui 60 Doses</h3>
                            <p>Conhecida como a “vitamina do sol" por ser formada no corpo pela...</p>
                            </div>
                            <a class="btn-entrectt" href="vitamina-d3-10000ui-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/vitamina-d3-50000ui-12-doses.png" alt="vitamina-d3-50000ui-12-doses" title="vitamina-d3-50000ui-12-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Vitamina D3 50.000Ui 12 Doses</h3>
                            <p>Conhecida como a “vitamina do sol" por ser formada no corpo...</p>
                            </div>
                            <a class="btn-entrectt" href="vitamina-d3-50000ui-12-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/vitamina-d3-vitamina-k2-mk-7-30-doses.png" alt="vitamina-d3-vitamina-k2-(mk-7)-30-doses" title="vitamina-d3-vitamina-k2-(mk-7)-30-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Vitamina D3 + Vitamina K2 (Mk-7) 30 Doses</h3>
                            <p>A suplementação de Vitamina D3+ Vitamina K2...</p>
                            </div>
                            <a class="btn-entrectt" href="vitamina-d3-vitamina-k2-mk-7-30-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/zinco-50mg-60-doses.png" alt="zinco-50mg-60-doses" title="zinco-50mg-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Zinco 50Mg 60 Doses</h3>
                            <p>Zinco é um oligoelemento essencial para nosso organismo...</p>
                            </div>
                            <a class="btn-entrectt" href="zinco-50mg-60-doses.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/magnesio-dimalato-300mg-90-doses.png" alt="magnesio-dimalato-300mg-90-doses" title="magnesio-dimalato-300mg-90-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Magnésio Dimalato 300Mg 90 Doses</h3>
                            <p>O Magnésio é um mineral essencial para os todos os processos...</p>
                            </div>
                            <a class="btn-entrectt" href="magnesio-dimalato-300mg-90-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/picolinato-de-cromo-350mcg-90-capsulas.png" alt="picolinato-de-cromo-350mcg-90-capsulas" title="picolinato-de-cromo-350mcg-90-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Picolinato De Cromo 350Mcg 90 Cápsulas</h3>
                            <p>O picolinato de cromo é especialmente indicado no caso...</p>
                            </div>
                            <a class="btn-entrectt" href="picolinato-de-cromo-350mcg-90-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/zma-60-doses.png" alt="zma-60-doses" title="zma-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Zma 60 Doses</h3>
                            <p>O ZMA é um suplemento que tem em sua fórmula a combinação...</p>
                            </div>
                            <a class="btn-entrectt" href="zma-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/vitamina-k2-mk-7-100mcg-60-capsulas.png" alt="vitamina-k2-mk-7-100mcg-60-capsulas" title="vitamina-k2-mk-7-100mcg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Vitamina K2 Mk-7 100Mcg 60 Cápsulas</h3>
                            <p>A vitamina K-1, também conhecida como filoquinona...</p>
                            </div>
                            <a class="btn-entrectt" href="vitamina-k2-mk-7-100mcg-60-capsulas.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/vitamina-d3-vitamina-k2-mk-7-60-capsulas.png" alt="vitamina-d3-vitamina-k2-(mk-7)-60-capsulas" title="vitamina-d3-vitamina-k2-(mk-7)-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Vitamina D3 + Vitamina K2 (Mk-7) 60 Cápsulas</h3>
                            <p>Sem dúvidas o benefício mais conhecido da vitamina D3...</p>
                            </div>
                            <a class="btn-entrectt" href="vitamina-d3-vitamina-k2-mk-7-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/omega-1g-dha-50-ultra-60-capsulas.png" alt="omega-1g-dha-50-(ultra)-60-capsulas" title="omega-1g-dha-50-(ultra)-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Ômega 1g DHA 50% (ultra) - 60 Cápsulas</h3>
                            <p>O Ômega 3 é essencial para a saúde do cérebro e memória...</p>
                            </div>
                            <a class="btn-entrectt" href="omega-1g-dha-50-ultra-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/vitamina-a-10000ui-60-doses.png" alt="vitamina-a-10000ui-60-doses" title="vitamina-a-10000ui-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Vitamina A 10.000Ui 60 Doses</h3>
                            <p>Imunidade: A Vitamina A não apenas fortalece os "pontos de entrada"...</p>
                            </div>
                            <a class="btn-entrectt" href="vitamina-a-10000ui-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/vitamina-d3-5000ui-60-doses.png" alt="vitamina-d3-5000ui-60-doses" title="vitamina-d3-5000ui-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Vitamina D3 5.000Ui 60 Doses</h3>
                            <p>Conhecida como a “vitamina do sol" por ser formada no corpo...</p>
                            </div>
                            <a class="btn-entrectt" href="vitamina-d3-5000ui-60-doses.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/vitamina-b12-metilcobalamina-500mcg-60-doses.png" alt="vitamina-b12-metilcobalamina-500mcg-60-doses" title="vitamina-b12-metilcobalamina-500mcg-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Vitamina B12 Metilcobalamina 500Mcg 60 Doses</h3>
                            <p>A Vitamina B12 está envolvida em vários processos metabólicos...</p>
                            </div>
                            <a class="btn-entrectt" href="vitamina-b12-metilcobalamina-500mcg-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/vitamina-d3-2000ui-60-doses.png" alt="vitamina-d3-2000ui-60-doses" title="vitamina-d3-2000ui-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Vitamina D3 2.000Ui 60 Doses</h3>
                            <p>Conhecida como a “vitamina do sol" por ser formada no corpo pela...</p>
                            </div>
                            <a class="btn-entrectt" href="vitamina-d3-2000ui-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/imune-of-multivitaminico-60-caps.png" alt="imune-of-multivitaminico-60-caps" title="imune-of-multivitaminico-60-caps" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Imune Of Multivitaminico 60 Cáps</h3>
                            <p>O Multivitamínico Imune OF da Oficial Farma tem uma fórmula...</p>
                            </div>
                            <a class="btn-entrectt" href="imune-of-multivitaminico-60-caps.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/picolinato-de-cromo-100-mcg-60-doses.png" alt="picolinato-de-cromo-100-mcg-60-doses" title="picolinato-de-cromo-100-mcg-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Picolinato De Cromo 100 Mcg 60 Doses</h3>
                            <p>O Picolinato de cromo é considerado como a melhor fonte de cromo...</p>
                            </div>
                            <a class="btn-entrectt" href="picolinato-de-cromo-100-mcg-60-doses.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/complexo-b-30-doses.png" alt="complexo-b-30-doses" title="complexo-b-30-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Complexo B 30 DOSES</h3>
                            <p>Composto por 8 vitaminas hidrossolúveis, ou seja...</p>
                            </div>
                            <a class="btn-entrectt" href="complexo-b-30-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/vitamina-e-400ui-30-doses.png" alt="vitamina-e-400ui-30-doses" title="vitamina-e-400ui-30-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Vitamina E 400Ui 30 Doses</h3>
                            <p>A Vitamina E é uma vitamina lipossolúvel essencial na nutrição...</p>
                            </div>
                            <a class="btn-entrectt" href="vitamina-e-400ui-30-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/selenio-400mcg-30-doses.png" alt="selenio-400mcg-30-doses" title="selenio-400mcg-30-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Selenio 400Mcg 30 Doses</h3>
                            <p>O Selênio é um mineral essencial que o organismo necessita...</p>
                            </div>
                            <a class="btn-entrectt" href="selenio-400mcg-30-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/metilcobalamina-1mg-60-capsulas.png" alt="metilcobalamina-1mg-60-capsulas" title="metilcobalamina-1mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Metilcobalamina 1mg 60 Cápsulas</h3>
                            <p>Metilcobalamina é uma forma ativa de vitamina B12...</p>
                            </div>
                            <a class="btn-entrectt" href="metilcobalamina-1mg-60-capsulas.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/vitamina-a-5000ui-60-capsulas.png" alt="vitamina-a-5000ui-60-capsulas" title="vitamina-a-5000ui-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Vitamina A 5.000Ui 60 Cápsulas</h3>
                            <p>Imunidade: A Vitamina A não apenas fortalece os "pontos de entrada"...</p>
                            </div>
                            <a class="btn-entrectt" href="vitamina-a-5000ui-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/vitamina-k2-mk-7-100mcg-30-doses.png" alt="vitamina-k2-mk-7-100mcg-30-doses" title="vitamina-k2-mk-7-100mcg-30-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Vitamina K2 Mk-7 100Mcg 30 Doses</h3>
                            <p>A Vitamina K2 é uma subclasse da vitamina K. Dentro de vitamina K2...</p>
                            </div>
                            <a class="btn-entrectt" href="vitamina-k2-mk-7-100mcg-30-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/vitamina-e-400ui-60-doses.png" alt="vitamina-e-400ui-60-doses" title="vitamina-e-400ui-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Vitamina E 400Ui 60 Doses</h3>
                            <p>A Vitamina E é uma vitamina lipossolúvel essencial na nutrição...</p>
                            </div>
                            <a class="btn-entrectt" href="vitamina-e-400ui-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/vitamina-b6-piridoxina-100mg-30-capsulas.png" alt="vitamina-b6-(piridoxina)-100mg-30-capsulas" title="vitamina-b6-(piridoxina)-100mg-30-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Vitamina B6 (Piridoxina) 100Mg 30 Cápsulas</h3>
                            <p>A Vitamina B6 também conhecida como cloridrato de piridoxina...</p>
                            </div>
                            <a class="btn-entrectt" href="vitamina-b6-piridoxina-100mg-30-capsulas.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/vitamina-b5-acido-pantotenico-500mg-60-doses.png" alt="vitamina-b5-(acido-pantotenico)-500mg-60-doses" title="vitamina-b5-(acido-pantotenico)-500mg-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Vitamina B5 (Ácido Pantotênico) 500Mg 60 Doses</h3>
                            <p>A vitamina b5, também chamada de ácido pantotênico...</p>
                            </div>
                            <a class="btn-entrectt" href="vitamina-b5-acido-pantotenico-500mg-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/ferro-18mg-90-capsulas.png" alt="ferro-18mg-90-capsulas" title="ferro-18mg-90-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Ferro 18Mg 90 Cápsulas</h3>
                            <p>O Ferro é um mineral encontrado fundamentalmente no sangue...</p>
                            </div>
                            <a class="btn-entrectt" href="ferro-18mg-90-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/magnesio-200mg-60-doses.png" alt="magnesio-200mg-60-doses" title="magnesio-200mg-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Magnésio 200Mg 60 Doses</h3>
                            <p>O Magnésio é um mineral essencial para os processos enzimáticos...</p>
                            </div>
                            <a class="btn-entrectt" href="magnesio-200mg-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/complexo-b-30-capsulas-sem-acne.png" alt="complexo-b-30-capsulas-(sem-acne)" title="complexo-b-30-capsulas-(sem-acne)" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Complexo B 30 Cápsulas (Sem Acne)</h3>
                            <p>A carência das vitaminas do complexo b podem acarretar problemas leves...</p>
                            </div>
                            <a class="btn-entrectt" href="complexo-b-30-capsulas-sem-acne.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/carbonato-de-calcio-1200mg-vit-d3-1000ui-60-capsulas.png" alt="carbonato-de-calcio-1200mg-vit-d3-1000ui-60-capsulas" title="carbonato-de-calcio-1200mg-vit-d3-1000ui-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Carbonato De Cálcio 1200Mg + Vit D3 1000Ui 60 Cápsulas</h3>
                            <p>O seu corpo não pode criar seu próprio cálcio...</p>
                            </div>
                            <a class="btn-entrectt" href="carbonato-de-calcio-1200mg-vit-d3-1000ui-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/same-200mg-30-capsulas.png" alt="same-200mg-30-capsulas" title="same-200mg-30-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Same 200Mg 30 Cápsulas</h3>
                            <p>Visando garantir a qualidade dos produtos que necessitam...</p>
                            </div>
                            <a class="btn-entrectt" href="same-200mg-30-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/potassio-99mg-100-capsulas.png" alt="potassio-99mg-100-capsulas" title="potassio-99mg-100-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Potássio 99Mg 100 Cápsulas</h3>
                            <p>Potássio é um mineral que ajuda os rins a funcionar normalmente...</p>
                            </div>
                            <a class="btn-entrectt" href="potassio-99mg-100-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/thiomucase-400-utr-90-doses.png" alt="thiomucase-400-utr-90-doses" title="thiomucase-400-utr-90-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Thiomucase 400 UTR 90 Doses</h3>
                            <p>A Thiomucase se tornou uma grande aliada das pessoas que procuram...</p>
                            </div>
                            <a class="btn-entrectt" href="thiomucase-400-utr-90-doses.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/same-200mg-60-capsulas.png" alt="same-200mg-60-capsulas" title="same-200mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Same 200Mg 60 Cápsulas</h3>
                            <p>SAME (S-Adenosil-L-Metionina) é uma substância que atua como...</p>
                            </div>
                            <a class="btn-entrectt" href="same-200mg-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/vitamina-b2-riboflavina-250mg-60-capsulas.png" alt="vitamina-b2-(riboflavina)-250mg-60-capsulas" title="vitamina-b2-(riboflavina)-250mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Vitamina B2 (Riboflavina) 250mg 60 Cápsulas</h3>
                            <p>A Vitamina B2 (riboflavina) pertence ao Complexo B...</p>
                            </div>
                            <a class="btn-entrectt" href="vitamina-b2-riboflavina-250mg-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/magnesio-400mg-30-doses.png" alt="magnesio-400mg-30-doses" title="magnesio-400mg-30-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Magnésio 400Mg 30 Doses</h3>
                            <p>As principais fontes alimentares de Magnésio são os cereais integrais...</p>
                            </div>
                            <a class="btn-entrectt" href="magnesio-400mg-30-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/complexo-b-com-vit-c-120-doses.png" alt="complexo-b-com-vit-c-120-doses" title="complexo-b-com-vit-c-120-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Complexo B Com Vit C - 120 Doses</h3>
                            <p>O complexo B é composto por vitaminas hidrossolúveis...</p>
                            </div>
                            <a class="btn-entrectt" href="complexo-b-com-vit-c-120-doses.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/vanadio-quelato-250mcg-90-capsulas.png" alt="vanadio-quelato-250mcg-90-capsulas" title="vanadio-quelato-250mcg-90-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Vanádio Quelato 250Mcg 90 Cápsulas</h3>
                            <p>A insulina é um hormônio anabólico potente que atua no armazenamento...</p>
                            </div>
                            <a class="btn-entrectt" href="vanadio-quelato-250mcg-90-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/vitamina-b1-250mg-tiamina-100-capsulas.png" alt="vitamina-b1-250mg-(tiamina)-100-capsulas" title="vitamina-b1-250mg-(tiamina)-100-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Vitamina B1 250Mg (Tiamina) 100 Cápsulas</h3>
                            <p>A Vitamina B1, também chamada Tiamina ou aneurina...</p>
                            </div>
                            <a class="btn-entrectt" href="vitamina-b1-250mg-tiamina-100-capsulas.php">Saiba +</a>
                        </div>
                    </div>
            </section>
        </div>    
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>