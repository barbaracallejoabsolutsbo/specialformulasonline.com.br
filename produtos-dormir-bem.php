<?php

    $title       = "Dormir Bem";
    $description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elementum ultrices commodo. Donec quis dictum tortor."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "home",
        "produtos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <div class="container">
            <section class="nossos-produtos catego-prod container">
                <h1 class="text-center">Dormir Bem</h1>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/melatonina-5mg-100-doses.png" alt="melatonina-5mg-100-doses" title="melatonina-5mg-100-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>melatonina-5mg-100-doses</h3>
                            <p>A Melatonina traz inúmeros benefícios à saúde, tendo como principal...</p>
                            </div>
                            <a class="btn-entrectt" href="melatonina-5mg-100-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/melatonina-3mg-100-doses.png" alt="melatonina-3mg-100-doses" title="melatonina-3mg-100-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Melatonina 3Mg 100 Doses</h3>
                            <p>A Melatonina Spefical Fórmulas traz inúmeros benefícios à saúde...</p>
                            </div>
                            <a class="btn-entrectt" href="melatonina-3mg-100-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/melatonina-5mg-100cps.png" alt="melatonina-5mg-100cps" title="melatonina-5mg-100cps" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Melatonina 5MG 100cps</h3>
                            <p>A Melatonina traz inúmeros benefícios à saúde, tendo como principal...</p>
                            </div>
                            <a class="btn-entrectt" href="melatonina-5mg-100cps.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/gaba-400mg-60-doses.png" alt="gaba-400mg-60-doses" title="gaba-400mg-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Gaba 400Mg 60 Doses</h3>
                            <p>O ácido gama aminobutírico, popularmente conhecido como GABA...</p>
                            </div>
                            <a class="btn-entrectt" href="gaba-400mg-60-doses.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/melatonina-5mg-30-doses.png" alt="melatonina-5mg-30-doses" title="melatonina-5mg-30-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Melatonina 5mg 30 Doses</h3>
                            <p>A Melatonina traz inúmeros benefícios à saúde...</p>
                            </div>
                            <a class="btn-entrectt" href="melatonina-5mg-30-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/gaba-200mg-60-doses.png" alt="gaba-200mg-60-doses" title="gaba-200mg-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Gaba 200Mg 60 Doses</h3>
                            <p>O ácido gama aminobutírico, popularmente conhecido como GABA...</p>
                            </div>
                            <a class="btn-entrectt" href="gaba-200mg-60-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/composto-auxiliar-no-tratamento-contra-insonia-dormir-bem-30-capsulas.png" alt="composto-auxiliar-no-tratamento-contra-insonia-dormir-bem-30-capsulas" title="composto-auxiliar-no-tratamento-contra-insonia-dormir-bem-30-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Composto Auxiliar no Tratamento Contra Insônia "DORMIR BEM" 30 Cápsulas</h3>
                            <p>O composto  auxiliar para...</p>
                            </div>
                            <a class="btn-entrectt" href="composto-auxiliar-no-tratamento-contra-insonia-dormir-bem-30-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/mulungu-200mg-60-doses.png" alt="mulungu-200mg-60-doses" title="mulungu-200mg-60-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Mulungu 200Mg 60 Doses</h3>
                            <p>O mulungu tem longo uso na medicina popular brasileira...</p>
                            </div>
                            <a class="btn-entrectt" href="mulungu-200mg-60-doses.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/passiflora-maracuja-200mg-60-capsulas.png" alt="passiflora-(maracuja)-200mg-60-capsulas" title="passiflora-(maracuja)-200mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Passiflora (Maracujá) 200Mg 60 Cápsulas</h3>
                            <p>A passiflora é mais conhecida por seu fruto, o maracujá...</p>
                            </div>
                            <a class="btn-entrectt" href="passiflora-maracuja-200mg-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/valeriana-100mg-60-capsulas.png" alt="valeriana-100mg-60-capsulas" title="valeriana-100mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Valeriana 100Mg 60 Cápsulas</h3>
                            <p>A valeriana é uma alternativa natural e poderosa para acalmar os ânimos...</p>
                            </div>
                            <a class="btn-entrectt" href="valeriana-100mg-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/melissa-passiflora-kawa-kawa-30-doses.png" alt="melissa-passiflora-kawa-kawa-30-doses" title="melissa-passiflora-kawa-kawa-30-doses" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Melissa + Passiflora + Kawa Kawa 30 Doses</h3>
                            <p>A passiflora é mais conhecida por seu fruto, o maracujá...</p>
                            </div>
                            <a class="btn-entrectt" href="melissa-passiflora-kawa-kawa-30-doses.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/ginseng-siberiano-300mg-30-capsulas.png" alt="ginseng-siberiano-300mg-30-capsulas" title="ginseng-siberiano-300mg-30-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Ginseng Siberiano 300mg 30 Cápsulas</h3>
                            <p>O Ginseng Siberiano é recomendado e utilizado como tônico...</p>
                            </div>
                            <a class="btn-entrectt" href="ginseng-siberiano-300mg-30-capsulas.php">Saiba +</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                           <img src="<?php echo $url; ?>imagens/kawa-kawa-150mg-60-capsulas.png" alt="kawa-kawa-150mg-60-capsulas" title="kawa-kawa-150mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Kawa Kawa 150mg 60 Cápsulas</h3>
                            <p>A kawa kawa possui propriedades analgésica...</p>
                            </div>
                            <a class="btn-entrectt" href="kawa-kawa-150mg-60-capsulas.php">Saiba +</a>
                        </div>
                        <div class="caixa-prod col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="<?php echo $url; ?>imagens/relora-250mg-60-capsulas.png" alt="relora-250mg-60-capsulas" title="relora-250mg-60-capsulas" class="img-responsive">
                            <div class="descricao-prod">
                            <h3>Relora 250Mg 60 Cápsulas</h3>
                            <p>Relora é a combinação patenteada de constituintes ativos de plantas...</p>
                            </div>
                            <a class="btn-entrectt" href="relora-250mg-60-capsulas.php">Saiba +</a>
                        </div>
                    </div>
            </section>
        </div>    
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>