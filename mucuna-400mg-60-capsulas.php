<?php

    $title       = "Mucuna 400Mg 60 Cápsulas";
    $description = "Mucuna Pruriens é uma planta da família das leguminosas, também chamada no Brasil de café berão e pó de mico. A planta é considerada um fixador..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Mucuna 400Mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/mucuna-400mg-60-capsulas.png" alt="mucuna-400mg-60-capsulas" title="mucuna-400mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">Mucuna Pruriens é uma planta da família das leguminosas, também chamada no Brasil de café berão e pó de mico. A planta é considerada um fixador de nitrogênio e é comumente usada como cobertura fertilizante do solo em terras degradadas pela agricultura. A ingestão do extrato de Mucuna contén L- Dopa, um aminoácido que contém substâncias quer podem estimular a liberação natural do hormônio do crescimento. Este hormônio, também chamado de GH, ajuda a regular os fluidos corporais, o crescimento dos músculos e ossos, o nível de açúcar e o metabolismo da gordura no organismo.</p>
                        <br>
                        <h2>BENEFÍCIOS</h2>
                        <p class="text-justify">Entre os benefícios já associados ao consumo do extrato das sementes de Mucuna Pruriens ainda podemos listar os seguintes: Uma fonte natural de L-Dopa (levodopa), aumenta a dopamina no cérebro. Ajuda a relaxar o corpo diminuindo o estresse e a ansiedade. Aumenta a libido e revitaliza o sistema reprodutivo. Pode oferecer resultados semelhantes aos antidepressivos. Promove um sono melhor. Coadjuvante na recuperação de dependência de drogas. Melhora as funções cognitivas ede memória. Estimula a produção do GH e outros hormônios que ajudam a manter a jovialidade. Normaliza as habilidades motoras e age como relaxante dos movimentos musculares. Pode ajudar a regular os níveis de açúcar no sangue. Promove a digestão. Ajuda a energizar o corpo naturalmente</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>