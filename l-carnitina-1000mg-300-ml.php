<?php

    $title       = "L-Carnitina 1000Mg 300 Ml";
    $description = "A L-Carnitina é um nutriente de extrema eficiência utilizado por pessoas que procuram não perder tempo na hora de melhorar o condicionamento físico..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">L-Carnitina 1000Mg 300 Ml</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/l-carnitina-1000mg-300-ml.png" alt="l-carnitina-1000mg-300-ml" title="l-carnitina-1000mg-300-ml">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>VERSATILIDADE</h2>
                        <p class="text-justify">A L-Carnitina é um nutriente de extrema eficiência utilizado por pessoas que procuram não perder tempo na hora de melhorar o condicionamento físico. Por proporcionar uma variedade de benefícios para o corpo, ela acaba funcionando como um mix que ajuda o corpo a produzir mais energia, colabora para a perda de peso, aumenta as defesas imunológicas, atua no desenvolvimento das faculdades mentais, e ainda abaixa os níveis de colesterol e triglicerídeos.</p>
                        <br>
                        <h2>MELHORA A SAÚDE CARDIOVASCULAR</h2>
                        <p class="text-justify">A carnitina consegue gerar maior quantidade de energia para os músculos e assim melhorar o desempenho nos treinos. Essa energia é retirada das células de gordura. Com a ajuda desse nutriente as células adiposas de cadeia longa são oxidadas e só assim conseguem atravessar a membrana e chegar na mitocôndria (organela vital para a produção de energia celular), onde finalmente, serão metabolizadas e transformadas em energia para ser consumida pelos músculos e pelo coração.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>