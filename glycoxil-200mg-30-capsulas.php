<?php

    $title       = "Glycoxil 200mg 30 Cápsulas";
    $description = "Glycoxil é o resultado de uma modificação molecular derivado da carcinina, ele atua na prevenção e tratamento coadjuvante de diversas desordens metabólicas..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Glycoxil 200mg 30 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/glycoxil-200mg-30-capsulas.png" alt="glycoxil-200mg-30-capsulas" title="glycoxil-200mg-30-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">Glycoxil é o resultado de uma modificação molecular derivado da carcinina, ele atua na prevenção e tratamento coadjuvante de diversas desordens metabólicas e doenças associadas ao envelhecimento sistêmico. O excesso de açúcar no organismo favorece a glicação, o qual é responsável pela formação de “Advanced Glycation End Products (A.G.Es)”. A glicação se trata da incorporação da glicose as proteínas, alterando e modificando as funções dos tecidos. A hemoglobina glicada, por exemplo, tem menor capacidade de transporte de oxigênio do que a hemoglobina comum.</p>
                        <br>
                        <p class="text-justify">A glicação ocorre quando temos um excesso de açúcar no nosso organismo oriundo de açúcares e outros carboidratos que geram alto índice glicêmico, causando danos às proteínas (elastina e colágeno) por meio de ligações cruzadas (crosslinking), os quais produzem novas moléculas denominadas A.G.Es. O Glycoxil® apresenta atividade antiglicante, assim como outros derivados peptídicos da carcinina, previne a peroxidação lipídica, protegendo as membranas do estresse oxidativo. A inibição da peroxidação lipídica se dá pela combinação da propriedade varredora de radicais livres e da propriedade quelante dos metais.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Imagens meramente ilustrativas. Pessoas com hipersensibilidade à substância não devem ingerir o produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Este produto não deve ser utilizado por gestantes e lactantes sem orientação médica. Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. Todo medicamento deve ser mantido fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="https://web.whatsapp.com/send?phone=5511944930219&text=Ol%C3%A1%21%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20da%20Exaustyair%20-%20Sistemas%20de%20Exaust%C3%A3o%20e%20Ventila%C3%A7%C3%A3o%20Industrial"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>