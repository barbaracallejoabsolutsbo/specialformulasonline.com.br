<?php

    $title       = "Pill Food 120 Cápsulas";
    $description = "Pill Food é um complexo alimentar formado por vitaminas, proteínas e aminoácidos que participam do desenvolvimento do cabelo, da estrutura da pele e das unhas."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Pill Food 120 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/pill-food-120-capsulas.png" alt="pill-food-120-capsulas" title="pill-food-120-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>PILL FOOD</h2>
                        <p class="text-justify">Pill Food é um complexo alimentar formado por vitaminas, proteínas e aminoácidos que participam do desenvolvimento do cabelo, da estrutura da pele e das unhas. Este complexo favorece o crescimento capilar, evita a queda de cabelo, fortalece as unhas e melhora o aspecto geral da pele.</p>
                        <br>
                        <h2>O PILL FOOD CONTEM:</h2>
                        <p class="text-justify">O Pill Food possui dois grandes diferenciais da fórmula tradicional do Pill Food: o Silício, um mineral que potencializa seus efeitos aumentando a espessura dos fios de cabelo e dando mais resistência contra quebra e o MSM ou Enxofre Orgânico que é indispensável para a saúde e manutenção da pele, unhas e do cabelo.</p>
                    </div>
                </div>
                <h2>Composição</h2>
                <ul>
                   <li>PORÇÃO 1 CÁPSULA</li>
                   <li>DL METIONINA - 200MGL CISTEINA MONOCLORIDRATO - 80MG</li>
                   <li>L CISTINA - 25MG</li>
                   <li>QUERATINA - 25MG</li>
                   <li>AMIDO DE MILHO - 20MG</li>
                   <li>CALCIO PANTOTENATO - 25MG</li>
                   <li>VIT B2 - 1MG</li>
                   <li>VIT B6 - 10MG</li>
                   <li>BIOTINA - 0,2MG</li>
                   <li>VIT E ACETATO - 3MG</li>
                </ul>
                <hr>
                <br>
                <a class="compre-aqui" href="https://web.whatsapp.com/send?phone=5511944930219&text=Ol%C3%A1%21%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20da%20Exaustyair%20-%20Sistemas%20de%20Exaust%C3%A3o%20e%20Ventila%C3%A7%C3%A3o%20Industrial"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>