<?php

    $title       = "Vitamina D3 + K2 + E 60 Doses";
    $description = "À Vitamina D3 é primariamente atribuído o papel de importante regulador da fisiologia osteomineral, em especial do metabolismo do cálcio."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Vitamina D3 + K2 + E 60 Doses</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/vitamina-d3-k2-e-60-doses.png" alt="vitamina-d3-k2-e-60-doses" title="vitamina-d3-k2-e-60-doses">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>VITAMINA D3 + K2 + E</h2>
                        <p class="text-justify">À Vitamina D3 é primariamente atribuído o papel de importante regulador da fisiologia osteomineral, em especial do metabolismo do cálcio. Entretanto, ela também está envolvida na homeostase de vários outros processos celulares, entre eles a síntese de antibióticos naturais pelas células de defesa dos mamíferos, modulação da autoimunidade e síntese de interleucinas inflamatórias, e como participa da regulação dos processos de multiplicação e diferenciação celular, é atribuído também a ela papel antioncogênico.</p>
                        <p class="text-justify">Através do envelhecimento, desnutrição ou doenças, há uma deficiência na produção de Vitamina K2 no organismo. Como um dos principais resultados dessa deficiência, o cálcio não é incorporado corretamente aos ossos, tornando-os fracos e quebradiços, enquanto há um acumulo nas artérias. Esse cálcio acumulado endurece e bloqueia as artérias. Atualmente a deficiência desta vitamina está se tornando um problema importante, pois está associada a diversos problemas de saúde como osteoporose, fraturas ósseas frequentes, e riscos cardiovasculares.</p>
                    </div>
                </div>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="text-justify">Pessoas com hipersensibilidade à substância não devem fazer uso do produto. Em caso de hipersensibilidade ao produto, recomenda-se descontinuar o uso e consultar o médico. Não use o produto com o prazo de validade vencido. Manter em temperatura ambiente (15 a 30ºC). Proteger da luz, do calor e da umidade. Nestas condições, o produto se manterá próprio para o consumo, respeitando o prazo de validade indicado na embalagem. Manter fora do alcance das crianças. Nunca compre medicamento sem orientação de um profissional habilitado. Este produto não deve ser utilizado por mulheres grávidas sem orientação médica. Siga corretamente o modo de usar. Não desaparecendo os sintomas, procure orientação médica. O uso do produto durante o período de amamentação também não é recomendado.</p>
                <p class="advertencias text-justify">Este produto não deve ser utilizado por menores de 18 anos sem orientação médica. Imagens meramente ilustrativas. "SE PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO" "Os resultados e indicações referentes ao uso desse produto foram avaliados e comprovados pelo fabricante deste insumo farmacêutico. Não garantimos os resultados descritos, estes variam de pessoa para pessoa dependendo de diversos fatores como alimentação, prática de exercícios físicos, presença de outras patologias, bem como, o uso correto do produto conforme descrito na posologia.”</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>