<?php

    $title       = "Pholia Magra 300Mg 60 Cápsulas";
    $description = "É um tipo de suplemento produzido a partir da erva chamada Cordia Eucalyculata Vell, encontrada somente em farmácias e/ou em casas de produtos naturais..."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "servicos"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <h1 class="text-center title-produtos">Pholia Magra 300Mg 60 Cápsulas</h1>
            <div class="descricao">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-produtos" src="imagens/pholia-magra-300mg-60-capsulas.png" alt="pholia-magra-300mg-60-capsulas" title="pholia-magra-300mg-60-capsulas">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h2>O QUE É</h2>
                        <p class="text-justify">É um tipo de suplemento produzido a partir da erva chamada Cordia Eucalyculata Vell, encontrada somente em farmácias e/ou em casas de produtos naturais. Segundo pesquisas, foi comprovado que a Pholia Magra® é diferente das outras ervas usadas para a perda de peso, pois não causa efeitos colaterais e auxilia em um emagrecimento de forma natural. Além é claro, de realizar o grande sonho da barriga “chapada”. Também pode ser usada no tratamento de retenção de líquidos, pois possui uma forte ação diurética!</p>
                        <p class="text-justify">A Pholia Magra® é super recomendada para auxiliar no emagrecimento porque é rica em princípios ativos que agem de diferentes formas no corpo, influenciando de forma direta e indireta na perda de peso. Podemos citar a cafeína como ator principal, já que possui uma grande ação termogênica, ou seja, é responsável por acelerar o metabolismo mesmo quando estamos dormindo, de forma a aumentar o gasto calórico, e consequentemente a perda de peso.</p>
                    </div>
                </div>
                <p class="text-justify">Além de todos os benefícios para um emagrecimento saudável, a Pholia Magra também é super recomendada para pessoas com mais de 30 anos. Isso porque a partir dessa idade, de forma natural, o corpo passa a produzir menos colágeno, o grande responsável pela firmeza, elasticidade e tonicidade da pele. Por isso que os sinais da idade começam a ficar mais evidentes a partir dos 30 anos! A Pholia Magra é rica em estimulantes e tonificantes que ajudam no rejuvenescimento da pele, assim evitando o envelhecimento precoce.</p>
                <br>
                <h2 class="advertencias text-center">Advertências</h2>
                <p class="advertencias text-justify">Se persistirem os sintomas, o médico ou farmacêutico deverá ser consultado. Evite a automedicação. O medicamento mesmo livre de obrigação de prescrição médica merece cuidado. As indicações postas não se tratam de propaganda, e sim de descrição do produto. Consulte sempre um especialista. As imagens postas são meramente ilustrativas. As indicações dos produtos são baseadas no conhecimento científico do profissional farmacêutico e laudos de aquisição dos produtos junto aos fornecedores autorizados pela Anvisa.</p>
                <hr>
                <br>
                <a class="compre-aqui" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i>COMPRE AQUI</a>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>